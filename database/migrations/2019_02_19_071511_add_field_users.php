<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users', function (Blueprint $table) {
          $table->text('verification_code');
          $table->char('flag_verify',1); //0=>waiting verification, 1=>active, 2=> reject, 3=>reset password
          $table->char('status_user',1);
          $table->string('user_level');
          $table->string('password_string');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
