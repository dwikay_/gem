<?php

use Illuminate\Database\Seeder;
use App\Model\Module as Module;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Module::create([
          'menu_parent' =>  '0',
          'module_name' =>  'Monitoring',
          'menu_mask' =>  'Monitoring',
          'menu_path' =>  '',
          'menu_icon' =>  'icon-screen-desktop',
          'menu_order'  =>  '1',
          'divider' =>  0,
      ]);
      Module::create([
          'menu_parent' =>  '0',
          'module_name' =>  'Data Statis',
          'menu_mask' =>  'Data Statis',
          'menu_path' =>  '',
          'menu_icon' =>  'icon-layers',
          'menu_order'  =>  '2',
          'divider' =>  0,
      ]);
      Module::create([
          'menu_parent' =>  '0',
          'module_name' =>  'Transaksi',
          'menu_mask' =>  'Transaksi',
          'menu_path' =>  '',
          'menu_icon' =>  'icon-star',
          'menu_order'  =>  '3',
          'divider' =>  0,
      ]);
      Module::create([
          'menu_parent' =>  '0',
          'module_name' =>  'Pengaturan User',
          'menu_mask' =>  'Pengaturan User',
          'menu_path' =>  '',
          'menu_icon' =>  'icon-people',
          'menu_order'  =>  '4',
          'divider' =>  0,
      ]);





    //Monitoring Child
    Module::create([
          'menu_parent' =>  '1',
          'module_name' =>  'Monitoring Transaksi',
          'menu_mask' =>  'Monitoring Transaksi',
          'menu_path' =>  'monitoring/transaksi',
          'menu_icon' =>  '',
          'menu_order'  =>  '1',
          'divider' =>  0,
      ]);
    Module::create([
          'menu_parent' =>  '1',
          'module_name' =>  'Harga Penutupan',
          'menu_mask' =>  'Harga Penutupan',
          'menu_path' =>  'monitoring/closingPrice',
          'menu_icon' =>  '',
          'menu_order'  =>  '2',
          'divider' =>  0,
      ]);
    Module::create([
          'menu_parent' =>  '1',
          'module_name' =>  'Haircut',
          'menu_mask' =>  'Haircut',
          'menu_path' =>  'monitoring/haircut',
          'menu_icon' =>  '',
          'menu_order'  =>  '3',
          'divider' =>  0,
      ]);


      //Data Statis Child
      Module::create([
            'menu_parent' =>  '2',
            'module_name' =>  'Global Parameter',
            'menu_mask' =>  'Global Parameter',
            'menu_path' =>  'dataStatis/globalParam',
            'menu_icon' =>  '',
            'menu_order'  =>  '1',
            'divider' =>  0,
        ]);
      Module::create([
            'menu_parent' =>  '2',
            'module_name' =>  'Nasabah',
            'menu_mask' =>  'Nasabah',
            'menu_path' =>  'dataStatis/nasabah',
            'menu_icon' =>  '',
            'menu_order'  =>  '2',
            'divider' =>  0,
        ]);

        Module::create([
              'menu_parent' =>  '2',
              'module_name' =>  'Sekuritas',
              'menu_mask' =>  'Sekuritas',
              'menu_path' =>  'dataStatis/pengaturan',
              'menu_icon' =>  '',
              'menu_order'  =>  '3',
              'divider' =>  0,
          ]);
        Module::create([
              'menu_parent' =>  '2',
              'module_name' =>  'Bank Kustodi',
              'menu_mask' =>  'Bank Kustodi',
              'menu_path' =>  'dataStatis/custodi',
              'menu_icon' =>  '',
              'menu_order'  =>  '4',
              'divider' =>  0,
          ]);
        Module::create([
                'menu_parent' =>  '2',
                'module_name' =>  'Efek',
                'menu_mask' =>  'Efek',
                'menu_path' =>  'dataStatis/efek-resources',
                'menu_icon' =>  '',
                'menu_order'  =>  '5',
                'divider' =>  0,
            ]);
        Module::create([
                'menu_parent' =>  '2',
                'module_name' =>  'Hari Libur',
                'menu_mask' =>  'Hari Libur',
                'menu_path' =>  'dataStatis/holiday',
                'menu_icon' =>  '',
                'menu_order'  =>  '6',
                'divider' =>  0,
            ]);
        Module::create([
                'menu_parent' =>  '2',
                'module_name' =>  'Negara',
                'menu_mask' =>  'Negara',
                'menu_path' =>  'dataStatis/country',
                'menu_icon' =>  '',
                'menu_order'  =>  '7',
                'divider' =>  0,
            ]);
        Module::create([
                'menu_parent' =>  '2',
                'module_name' =>  'Provinsi',
                'menu_mask' =>  'Provinsi',
                'menu_path' =>  'dataStatis/provinsi',
                'menu_icon' =>  '',
                'menu_order'  =>  '8',
                'divider' =>  0,
            ]);
          Module::create([
                'menu_parent' =>  '2',
                'module_name' =>  'Kota',
                'menu_mask' =>  'Kota',
                'menu_path' =>  'dataStatis/city',
                'menu_icon' =>  '',
                'menu_order'  =>  '9',
                'divider' =>  0,
            ]);

          Module::create([
                'menu_parent' =>  '2',
                'module_name' =>  'Kode Pajak',
                'menu_mask' =>  'Kode Pajak',
                'menu_path' =>  'dataStatis/tax',
                'menu_icon' =>  '',
                'menu_order'  =>  '10',
                'divider' =>  0,
            ]);


        //Transaksi Child

        Module::create([
              'menu_parent' =>  '3',
              'module_name' =>  'Pengajuan Pinjaman',
              'menu_mask' =>  'Pengajuan Pinjaman',
              'menu_path' =>  'transaksi/loan-request',
              'menu_icon' =>  '',
              'menu_order'  =>  '1',
              'divider' =>  0,
          ]);


          //Pengaturan User Child
          Module::create([
                'menu_parent' =>  '4',
                'module_name' =>  'Manajemen User',
                'menu_mask' =>  'Manajemen User',
                'menu_path' =>  'usersGadai/userManage',
                'menu_icon' =>  '',
                'menu_order'  =>  '1',
                'divider' =>  0,
            ]);
          Module::create([
                'menu_parent' =>  '4',
                'module_name' =>  'Hak Akses',
                'menu_mask' =>  'Hak Akses',
                'menu_path' =>  'usersGadai/userAccess',
                'menu_icon' =>  '',
                'menu_order'  =>  '2',
                'divider' =>  0,
            ]);
          Module::create([
                'menu_parent' =>  '4',
                'module_name' =>  'Grup User',
                'menu_mask' =>  'Grup User',
                'menu_path' =>  'usersGadai/userGroup',
                'menu_icon' =>  '',
                'menu_order'  =>  '3',
                'divider' =>  0,
            ]);
    }
}
