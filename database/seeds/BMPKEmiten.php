<?php

use Illuminate\Database\Seeder;
use App\Model\Module as Module;
class BMPKEmiten extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Module::create([
            'menu_parent' =>  '2',
            'module_name' =>  'BMPK Emiten',
            'menu_mask' =>  'BMPK Emiten',
            'menu_path' =>  'dataStatis/bmpkEmiten',
            'menu_icon' =>  '',
            'menu_order'  =>  '3',
            'divider' =>  0,
            'pathParent' =>  "dataStatis",
            'kdModule' =>  "99999999925"
        ]);
    }
}
