<?php

use Illuminate\Database\Seeder;
use App\Model\Module as Module;

class SimulasiTopupSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Module::create([
            'menu_parent' =>  '3',
            'module_name' =>  'Simulasi Topup',
            'menu_mask' =>  'Simulasi Topup',
            'menu_path' =>  'transaksi/stock-deposit/simulation',
            'menu_icon' =>  '',
            'menu_order'  =>  '5',
            'divider' =>  0,
            'pathParent' =>  "transaksi",
            'kdModule' =>  "99999999926"
        ]);
    }
}
