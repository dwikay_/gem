<?php

use Illuminate\Database\Seeder;
use App\Model\Module as Module;

class SpesifikParameterSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Module::create([
            'menu_parent' =>  '2',
            'module_name' =>  'Batas Pinjaman',
            'menu_mask' =>  'Batas Pinjaman',
            'menu_path' =>  'dataStatis/batasPinjaman',
            'menu_icon' =>  '',
            'menu_order'  =>  '2',
            'divider' =>  0,
            'pathParent' =>  "dataStatis",
            'kdModule' =>  "99999999924"
        ]);
    }
}
