<?php

use Illuminate\Database\Seeder;
use App\Model\Roleacl as Roleacls;

class Roleacl extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      //Monitoring
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 5, 'module_parent' => 1,
      //   'create_acl' => 5, 'read_acl' => 5, 'update_acl' => 5, 'delete_acl' => 5
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 6, 'module_parent' => 1,
      //   'create_acl' => 6 , 'read_acl' => 6, 'update_acl' => 6, 'delete_acl' => 6
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 7, 'module_parent' => 1,
      //   'create_acl' => 7, 'read_acl' => 7, 'update_acl' => 7, 'delete_acl' => 7
      // ]);
      //
      // //Data Statis
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 8, 'module_parent' => 2,
      //   'create_acl' => 8, 'read_acl' => 8, 'update_acl' => 8, 'delete_acl' => 8
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 9, 'module_parent' => 2,
      //   'create_acl' => 9, 'read_acl' => 9, 'update_acl' => 9, 'delete_acl' => 9
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 10, 'module_parent' => 2,
      //   'create_acl' => 10, 'read_acl' => 10, 'update_acl' => 10, 'delete_acl' => 10
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 11, 'module_parent' => 2,
      //   'create_acl' => 11, 'read_acl' => 11, 'update_acl' => 11, 'delete_acl' => 11
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 12, 'module_parent' => 2,
      //   'create_acl' => 12, 'read_acl' => 12, 'update_acl' => 12, 'delete_acl' => 12
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 13, 'module_parent' => 2,
      //   'create_acl' => 13, 'read_acl' => 13, 'update_acl' => 13, 'delete_acl' => 13
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 14, 'module_parent' => 2,
      //   'create_acl' => 14, 'read_acl' => 14, 'update_acl' => 14, 'delete_acl' => 14
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 14, 'module_parent' => 2,
      //   'create_acl' => 15, 'read_acl' => 15, 'update_acl' => 15, 'delete_acl' => 15
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 16, 'module_parent' => 2,
      //   'create_acl' => 16, 'read_acl' => 16, 'update_acl' => 16, 'delete_acl' => 16
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 17, 'module_parent' => 2,
      //   'create_acl' => 17, 'read_acl' => 17, 'update_acl' => 17, 'delete_acl' => 17
      // ]);
      //
      // //Transaksi
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 18, 'module_parent' => 3,
      //   'create_acl' => 18, 'read_acl' => 18, 'update_acl' => 18, 'delete_acl' => 18
      // ]);
      //
      // //User
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 19, 'module_parent' => 4,
      //   'create_acl' => 19, 'read_acl' => 19, 'update_acl' => 19, 'delete_acl' => 19
      // ]);
      // Roleacls::create([
      //   'role_id' => 1, 'module_id' => 21, 'module_parent' => 4,
      //   'create_acl' => 21, 'read_acl' => 21, 'update_acl' => 21, 'delete_acl' => 21
      // ]);
      Roleacls::create([
        'role_id' => 1, 'module_id' => 20, 'module_parent' => 4,
        'create_acl' => 20, 'read_acl' => 20, 'update_acl' => 20, 'delete_acl' => 20
      ]);

    }
}
