<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        // $this->call(pushToken::class);
        // $this->call(RoleTable::class);
        // $this->call(ModuleSeeder::class);
        // $this->call(Roleacl::class);
        // $this->call(seedPelunasanModule::class);
        // $this->call(reportSeeder::class);
        // $this->call(SpesifikParameterSeed::class);
        // $this->call(BMPKEmiten::class);
        $this->call(SimulasiTopupSeed::class);
    }
}
