<?php

use Illuminate\Database\Seeder;
use App\Model\Module as Module;

class seedPelunasanModule extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Module::create([
            'menu_parent' =>  '1',
            'module_name' =>  'Monitoring Pelunasan',
            'menu_mask' =>  'Monitoring Pelunasan',
            'menu_path' =>  'monitoring/pelunasan',
            'menu_icon' =>  '',
            'menu_order'  =>  '4',
            'divider' =>  0,
            'pathParent' =>  "monitoring",
            'kdModule' =>  "99999999920"
        ]);
    }
}
