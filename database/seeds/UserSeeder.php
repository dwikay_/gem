<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = new User();
       $user->name = "Administrator";
       $user->email = "admin";
       $user->password = bcrypt('admin');
       $user->verification_code = "admin";
       $user->flag_verify = "1";
       $user->status_user = "1";
       $user->user_level = "0";
       $user->save();
    }
}
