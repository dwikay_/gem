<?php

use Illuminate\Database\Seeder;
use App\Model\Akses;

class UserGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $akses = new Akses();
      $akses->akses = "administrator";
      $akses->save();
    }
}
