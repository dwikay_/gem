<?php

use Illuminate\Database\Seeder;
use App\Model\Module as Module;

class reportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Module::create([
          'menu_parent' =>  '0',
          'module_name' =>  'Laporan',
          'menu_mask' =>  'Laporan',
          'menu_path' =>  '',
          'menu_icon' =>  'icon-people',
          'menu_order'  =>  '5',
          'divider' =>  0,
      ]);

      Module::create([
            'menu_parent' =>  '5',
            'module_name' =>  'Risk Parameter',
            'menu_mask' =>  'Risk Parameter',
            'menu_path' =>  'report/riskParameter',
            'menu_icon' =>  '',
            'menu_order'  =>  '1',
            'divider' =>  0,
        ]);
        Module::create([
              'menu_parent' =>  '5',
              'module_name' =>  'Stock Position',
              'menu_mask' =>  'Stock Position',
              'menu_path' =>  'report/stockPosition',
              'menu_icon' =>  '',
              'menu_order'  =>  '2',
              'divider' =>  0,
          ]);
          Module::create([
                'menu_parent' =>  '5',
                'module_name' =>  'Stock Limit',
                'menu_mask' =>  'Stock Limit',
                'menu_path' =>  'report/stockLimit',
                'menu_icon' =>  '',
                'menu_order'  =>  '3',
                'divider' =>  0,
            ]);
    }
}
