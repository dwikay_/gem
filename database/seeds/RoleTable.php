<?php

use Illuminate\Database\Seeder;
use App\Model\Role as Role;

class RoleTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Role::create([
          'role_name' =>  'Administrator',
          'description'  =>  'Admin',
          'akses' => 'koperasi'
      ]);

      Role::create([
          'role_name' =>  'Operator',
          'description'  =>  'Operator',
          'akses' => 'koperasi'
      ]);
    }
}
