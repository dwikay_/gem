let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .scripts('node_modules/autonumeric/dist/autoNumeric.js','public/js/autoNumeric.js')
   .scripts('node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js','public/js/datepicker.js')
   .scripts('node_modules/chosen-npm/public/chosen.jquery.min.js','public/js/chosen.js')
   .scripts('node_modules/jszip/dist/jszip.min.js','public/js/jszip.js')
   .scripts([
   		'node_modules/datatables.net/js/jquery.dataTables.min.js',
   		'node_modules/datatables.net-responsive/js/dataTables.responsive.min.js',
   		'node_modules/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
         'node_modules/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js',
   		'node_modules/datatables.net-colreorder/js/dataTables.colReorder.min.js',
   		'node_modules/datatables.net-autofill/js/dataTables.autoFill.min.js',
         'node_modules/datatables.net-buttons/js/dataTables.buttons.min.js',
         'node_modules/datatables.net-buttons/js/buttons.colVis.min.js',
         'node_modules/datatables.net-buttons/js/buttons.html5.min.js',
         'node_modules/datatables.net-buttons/js/buttons.flash.min.js',
         'node_modules/datatables.net-buttons/js/buttons.print.min.js',

         'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
         'node_modules/datatables.net-responsive/js/responsive.bootstrap4.min.js',
         'node_modules/datatables.net-fixedheader/js/fixedHeader.bootstrap4.min.js',
         'node_modules/datatables.net-colreorder/js/colReorder.bootstrap4.min.js',
         'node_modules/datatables.net-autofill/js/autoFill.bootstrap4.min.js',
         'node_modules/datatables.net-buttons/js/buttons.bootstrap4.min.js',

         'public/js/dataTables.pageResize.min.js'
   	],'public/js/datatables.js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('node_modules/bootstrap4c-chosen/src/scss/build.scss', 'public/css')
   .styles([
   		'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
   		'node_modules/datatables.net-autofill-bs4/css/autoFill.bootstrap4.min.css',
   		'node_modules/datatables.net-colreorder-bs4/css/colReorder.bootstrap4.min.css',
   		'node_modules/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css',
   		'node_modules/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css',
         'node_modules/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css'
   	], 'public/css/datatables.css')
   .styles([
   		'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
   	], 'public/css/datepicker.css')
   .styles([
         'node_modules/jquery-confirm/css/jquery-confirm.css',
      ], 'public/css/jquery-confirm.css')
   .scripts([
         'node_modules/jquery-confirm/js/jquery-confirm.js',
      ],'public/js/jquery-confirm.js')
   .version();
