<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Token;
use App\User;
use App\Library\CurlGen;
class getToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CurlGen $curlGen)
    {
      
    date_default_timezone_set("Asia/Jakarta");
    $url = "/api/authenticate";
    $user = "admin";
    $old_token = Token::orderby('id','desc')->first();
    $passwordString = "Cakra987$";
    $data = array(
        'password' => $passwordString,
        'rememberMe' => true,
        'username' => $user
      );

    $param = $curlGen->getToken($url, $data, $old_token);

    // print_r($param[2]);

    $token = Token::orderby('id','desc')->first();
    $token->token = $param[1]->id_token;
    $token->save();

    }
}
