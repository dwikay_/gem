<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
  protected $table = 'modules';
  protected $primaryKey = 'id';

  protected $fillable = [
    'menu_parent',
    'module_name',
    'menu_mask',
    'menu_path',
    'menu_icon',
    'menu_order',
    'divider'
  ];
}
