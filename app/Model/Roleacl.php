<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Roleacl extends Model
{
  protected $table = 'role_acl';
  protected $primaryKey = 'id';

  protected $fillable = [
    'role_id',
    'module_id',
    'create_acl',
    'read_acl',
    'update_acl',
    'delete_acl',
    'module_parent'
  ];

  public function module()
  {
    return $this->hasOne('App\Model\Module', 'id','module_id');
  }

  public function role()
  {
    return $this->hasOne('App\Model\Role', 'id','role_id');
  }
}
