<?php
namespace App\Library;

use Illuminate\Support\Facades\Storage;
use App\Model\Token;

class ReportGenerator{
	public function Generate($reportPath,$fileType,$paramArray){
		$path = storage_path('app\public\report\\');

		$file = fopen($path.'temp', 'w');

		$reportname = $reportPath;
		$format = $fileType;

		$reportServer = config('custom.report_server_address');

		$parameter = '';
		foreach($paramArray as $key => $value){
			$parameter = $parameter.$key."=".$value."&";
		}
		$parameter = substr_replace($parameter ,"",-1);

		$txt = $this->get_remote_data($reportServer, 'result?report='.$reportname.'&'.$parameter.'&format='.$format);
		fwrite($file, $txt);

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename='"."test.".$format);
		readfile($path.'temp'); // or echo file_get_contents($temp_file);

		//See Updates and explanation at: https://github.com/tazotodua/useful-php-scripts/
	}
	protected function get_remote_data($url, $post_paramtrs=false){

    date_default_timezone_set("Asia/Jakarta");
    $url = "http://103.78.215.82:9442/api/brokers";
    $content_type = "application/json";
    $user = Token::orderby('id','desc')->first();
    $authorization = $user->token

		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		if($post_paramtrs)
		{
			curl_setopt($c, CURLOPT_POST,TRUE);
			curl_setopt($c, CURLOPT_POSTFIELDS, "var1=bla&".$post_paramtrs );
		}
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST,false);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0");
		curl_setopt($c, CURLOPT_COOKIE, 'CookieName1=Value;');
		curl_setopt($c, CURLOPT_MAXREDIRS, 10);
		$follow_allowed= ( ini_get('open_basedir') || ini_get('safe_mode')) ? false:true;
		if ($follow_allowed)
		{
			curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
		}
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
		curl_setopt($c, CURLOPT_REFERER, $url);
		curl_setopt($c, CURLOPT_TIMEOUT, 60);
		curl_setopt($c, CURLOPT_AUTOREFERER, true);
		curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
		$data=curl_exec($c);
		$status=curl_getinfo($c);
		curl_close($c);
		preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si',  $status['url'],$link);
		$data=preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/|\/)).*?)(\'|\")/si','$1=$2'.$link[0].'$3$4$5', $data);
		$data=preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/)).*?)(\'|\")/si','$1=$2'.$link[1].'://'.$link[3].'$3$4$5', $data);
		if($status['http_code']==200)
		{
			return $data;
		}
		elseif($status['http_code']==301 || $status['http_code']==302)
		{
			if (!$follow_allowed)
			{
				if (!empty($status['redirect_url']))
				{
					$redirURL=$status['redirect_url'];
				}
				else
				{
					preg_match('/href\=\"(.*?)\"/si',$data,$m);
					if (!empty($m[1]))
					{
						$redirURL=$m[1];
					}
				}
				if(!empty($redirURL))
				{
					return  call_user_func( __FUNCTION__, $redirURL, $post_paramtrs);
				}
			}
		}
		return "ERRORCODE22 with $url!!<br/>Last status codes<b/>:".json_encode($status)."<br/><br/>Last data got<br/>:$data";
	}
}
?>
