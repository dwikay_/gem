<?php

namespace App\Http\Controllers\Bank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Library\CurlGen;
use App\Model\User;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CustodyController extends Controller
{
  public function index(){
    return view('static_data.custodi.index');
  }
  public function create(){
    return view('static_data.custodi.create');
  }
  public function getIndex(CurlGen $curlGen, Request $request){

    $url = "/api/bank-custodis";

    $param = $curlGen->getIndex($url);

    return Datatables::of($param)->escapeColumns([])->make(true);

  }

  public function store(CurlGen $curlGen, Request $request){

    $url = "/api/bank-custodis";
    $data = array(
        'kodeCustodi' => $request->kodeCustodi,
        'namaCustodi' => $request->namaCustodi
      );

      $httpCode = $curlGen->store($url,$data);

      return redirect(url('dataStatis/custodi'));

  }
  public function edit(CurlGen $curlGen, $id){

      $url = "/api/bank-custodis/".$id;
      $param = $curlGen->edit($url);

      return view('static_data.custodi.edit')
      ->with('custodi', $param[1]);
      }

  public function update(CurlGen $curlGen, Request $request, $id){

    $url = "/api/bank-custodis";
    $data = array(
        'id' => $id,
        'kodeCustodi' => $request->kodeCustodi,
        'namaCustodi' => $request->namaCustodi
      );

    $param = $curlGen->update($url,$data);

      return redirect(url('dataStatis/custodi'));
  }

  public function delete(CurlGen $curlGen, $id){

     $url = "/api/bank-custodis/".$id;
     $param = $curlGen->delete($url);

      return redirect(url('dataStatis/custodi'));
  }
}
