<?php

namespace App\Http\Controllers\Topup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use App\Library\CurlGen;
use yajra\Datatables\Datatables;
use Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TopupController extends Controller
{

  function getEfek(CurlGen $curlGen,$kodeEfek){
    $url = "/api/efekByKodeEfek/".$kodeEfek;
    $param = $curlGen->getIndex($url);

    return $param;
  }
  public function indexSimulation(CurlGen $curlGen){

    $url = "/api/gemefeks/S";
    $param = $curlGen->getIndex($url);

    $urlGlobal = "/api/parameter-globals/8";
    $paramGlobal = $curlGen->getIndex($urlGlobal);

    return view('transaction.topup.simulasi')
    ->with('efek', $param)
    ->with('paramGlobal',$paramGlobal);
  }
  public function kseiDoc(CurlGen $curlGen, Request $request){
    $url = "/api/top-up/kseidoc?docType=".$request->type."&noStockDepositList=".$request->checks."&tipeEfek=S";

    if($request->type == 'xml'){
      $param = $curlGen->kseiDoc($url, $request->checks);
      echo $param->original;
    }else{
      $param = $curlGen->kseiDocCSv($url, $request->checks);
      return $param;
    }


  }

  public function index(){
	    return view('transaction.topup.index');
	}

	public function create(CurlGen $curlGen){
    $kustodiUrl = "/api/bank-custodis";
    $kustodi = $curlGen->getIndex($kustodiUrl);

	    return view('transaction.topup.create')
      ->with('brokers', $kustodi);
	}
  public function sendInstruksi(Request $request, CurlGen $curlGen){

    $url = "/api/top-up-generate-and-send-email-instruksi-to-custody";

    $data = array(
      "sdCodeList" => array_keys($request->checks),
      "tipeEfek" => "S"
    );

    $param = $curlGen->store($url,$data);
    // return $param;
    if($param!="200"){
        $info = "Error";
        $colors = "red";
        $icons = "fas fa-times";
        if($param['1']['message']==null){$msg="Internal Server Error [500]";}
        else{$msg = $param['1']['message'];}
        $alert = $msg;
    }else{
        $info = "Success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = 'Tersimpan';
    }
    // return $alert;

    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return redirect(url('transaksi/stock-deposit'));

  }
  public function pengajuan(CurlGen $curlGen, $noDeps, $tipeEfek){

    $url = "/api/top-up-generate-and-send-email-instruksi-to-custody";
    $data = array(
        "sdCodeList" => [$noDeps],
      "tipeEfek" => $tipeEfek
    );

    $param = $curlGen->store($url,$data);

    // return $param;
    if($param!="200"){
        $info = "Error";
        $colors = "red";
        $icons = "fas fa-times";
        if($param['1']['message']==null){
          $msg="Internal Server Error [500]";
        }
        else{
          $msg = $param['1']['message'];
        }
        $alert = $msg;
    }else{
        $info = "Success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = 'Tersimpan';
    }
    // return $alert;

    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return redirect(url('transaksi/stock-deposit'));

  }
  public function settle(CurlGen $curlGen, $act, $noDeps, $qty){

    $url = "/api/top-up-dari-custody";
    $data = array(
      "action" => $act,
      "noDeposit" => $noDeps,
      "qtySettle" => $qty
    );

    // return $data;
    $param = $curlGen->update($url,$data);
    // return $param;
    if($param[0]!="200"){
        $info = "Error";
        $colors = "red";
        $icons = "fas fa-times";
        $alert = $param['1']['message'];
    }else{
        $info = "Success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = 'Tersimpan';
    }
    // return $param[0];
    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return redirect(url('transaksi/stock-deposit'));


  }
  public function getIndex(CurlGen $curlGen, Request $request){

    date_default_timezone_set("Asia/Jakarta");
    if($request->type_efek=="A"){
      $url = "/api/stock-deposit-list";
    }else{
      $url = "/api/stock-deposit-list/".$request->type_efek;
    }

    $param = $curlGen->getIndex($url);

    return Datatables::of($param)->escapeColumns([])->make(true);
  }

  public function getPengajuan(CurlGen $curlGen, $noPengajuan){

    $url = "/api/transaksi-gadai-efek-monitoring-detail/".$noPengajuan;
    $param = $curlGen->getIndex($url);
    return $param;
  }

  public function topupCollateral(CurlGen $curlGen, Request $request){

    // return $request->all();
    $url = "/api/top-up-collateral";
    $data = array(
    "collateralList" => [],
    "counterPart" => $request->kustodi,
    "noPengajuan" => $request->noKontrak,
    "settlementDate" => $request->tglPengajuan
    );

    foreach ($request->kodeEfek as $keys => $value) {
              foreach ($request->qtyEfek as $key => $values) {
                $data["collateralList"][$key] = array(
                    "kodeEfek"=> $request->kodeEfek[$key],
                    "qtyEfek"=> $request->qtyEfek[$key]
                  );
              }
            }
    // return $data;
    $param = $curlGen->update($url,$data);
    // return $param;
    if($param[0]!="200"){
        $info = "Error";
        $colors = "red";
        $icons = "fas fa-times";
        $alert = $param['1']['message'];
    }else{
        $info = "Success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = 'Tersimpan';
    }

    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return redirect(url('transaksi/stock-deposit'));
  }
}
