<?php

namespace App\Http\Controllers\Holiday;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\User;

class HolidayController extends Controller
{
  public function index(){
    return view('static_data.holiday.index');
  }
  public function create(){
    return view('static_data.holiday.create');
  }
    public function getIndex(CurlGen $curlGen, $tahun){

      if($tahun=="A"){
        $url = "/api/idx-holidays";
      }else{
        $url = "/api/idx-holidays-tahunan/".$tahun;
      }

      $param = $curlGen->getIndex($url);

      return Datatables::of($param)->escapeColumns([])->make(true);

    }

    public function store(CurlGen $curlGen, Request $request){

      $url = "/api/idx-holidays";
      $data = array(
          'tanggal' => $request->tanggal,
          'keterangan' => $request->keterangan
        );

        $param = $curlGen->store($url, $data);

        return redirect(url('dataStatis/holiday'));

    }
    public function edit(CurlGen $curlGen , $id){

      $url = "/api/idx-holidays/".$id;

      $param = $curlGen->edit($url);

      return view('static_data.holiday.edit')
      ->with('holiday', $param[1]);

    }

    public function update(CurlGen $curlGen, Request $request, $id){

      $url = "/api/idx-holidays";
      $data = array(
          'id' => $id,
          'tanggal' => $request->tanggal,
          'keterangan' => $request->keterangan
        );

      $param =  $curlGen->update($url,$data);

      return redirect(url('dataStatis/holiday'));
    }

    public function delete(CurlGen $curlGen, $id){

         $url = "/api/idx-holidays/".$id;
         $param = $curlGen->delete($url);

        return redirect(url('dataStatis/holiday'));

    }
}
