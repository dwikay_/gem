<?php

namespace App\Http\Controllers\Eksekusi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EksekusiController extends Controller
{

  public function kseiDoc(CurlGen $curlGen, Request $request){

    // $data = array(
    //   'noStockEksekusiList' => array_keys($request->checks)
    // );
    // foreach ($data['noStockEksekusiList'] as $box){
      $url = "/api/eksekusi/kseidoc?docType=".$request->type."&noEksekusi=".$request->checks;
      $param = $curlGen->kseiDoc($url, $request->checks);
    //   print_r("<pre>".print_r($param, true)."<pre>");
    //   // return $param;
    // }

    echo $param->original;
      // return $box;
  }

  public function kseiDocCsv(CurlGen $curlGen, Request $request){

      $box = "ET000000D";

      $url = "/api/eksekusi/kseidoc?docType=".$request->type."&noEksekusi=".$request->checks;
      $param = $curlGen->kseiDocCsv($url, $request->checks);

      return $param;
      // echo $param->original;
  }

  public function settleEksekusi(CurlGen $curlGen, $action, $noEksekusi){

    $url = "/api/eksekusi-action-gadai-efek";
    $data = array(
      "action" => $action,
      "noEksekusi" => $noEksekusi
    );
    $param = $curlGen->store($url, $data);
    // return $param;

    if($param!="200"){
        $info = "Error";
        $colors = "red";
        $icons = "fas fa-times";
        // $alert = $param['1']['message'];
        $alert = "Please check your request";
    }else{
        $info = "Success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = 'Tersimpan';
    }

    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return redirect(url('transaksi/eksekusi'));

  }

  public function create(CurlGen $curlGen){
    $kustodiUrl = "/api/bank-custodis";
    $kustodi = $curlGen->getIndex($kustodiUrl);

      return view('transaction.execution.create')
      ->with('brokers', $kustodi);
  }

  public function postAlokasi(Request $request, CurlGen $curlGen){

    // print_r("<pre>".print_r($request->all(), true)."<pre>");
    // return;

    $data = array(
      "collaterals" => [],
      "noEksekusi" => $request->noEksekusi
    );

    foreach ($request->record as $keyRecord => $valueRecord) {
      if(empty($valueRecord['detail'])){
        continue;
      }
    foreach ($valueRecord['detail'] as $keyDetail => $valueDetail) {
        $data["collaterals"][$keyRecord] = array(
            "matchedPrice"=> $valueDetail['matchedPrice'],
            "matchedQty"=> $valueDetail['matchedQty'],
            "charges"=> $valueDetail['charges'],
            "additionalCharges" => $valueDetail['additionalCharges'],
            "grossAmount" => $valueDetail['grossAmount'],
            "kodeEfek" => $valueRecord['kodeEfek'],
            "netAmount" => $valueDetail['netAmount'],
            "noKontrak" => $valueDetail['noKontrak'],
            "tradeRefference" => $valueDetail['tradeRefference']
          );
      }
    }
    // return $data;
    $url = "/api/eksekusi-konfirmasi-gadai-efek";
    $param = $curlGen->store($url, $data);
    // return $param;
    if($param!="200"){
        $info = "Error";
        $colors = "red";
        $icons = "fas fa-times";
        $alert = $param['1']['message'];
    }else{
        $info = "Success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = 'Tersimpan';
    }

    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return redirect(url('transaksi/eksekusi'));

  }
  public function monitoringEksekusi(CurlGen $curlGen, $sekuritas, $tipeEfek, $noKontrak){

    $url = "/api/eksekusi-from-monitoring";
    $data = array(
      "brokerPartner" => $sekuritas,
      "tipeEfek" => $tipeEfek
    );
    $urlBroker = "/api/brokers/";
    $getData = $curlGen->storeResView($url, $data);
    $broker = $curlGen->getIndex($urlBroker);
    // return $getData;

    return view('transaction.execution.create')
    ->with('eksekusi', $getData[1])
    ->with('brokers', $broker)
    ->with('sekuritas',$sekuritas)
    ->with('noKontrak', $noKontrak)
    ->with('tipeEfek', $tipeEfek);
  }

  public function monitoringEksekusiGet(CurlGen $curlGen, Request $request, $noKontrak){

    // dd($noKontrak);
    $url = "/api/transaksi-gadai-efek-monitoring-detail/".$noKontrak;
    $data = array(
      "brokerPartner" => $request->broker,
      "tipeEfek" => $request->type_efek
    );
    $urlBroker = "/api/brokers/";
    $getData = $curlGen->getIndex($url);
    $broker = $curlGen->getIndex($urlBroker);

    return $getData;
  }

  public function updateEksekusi(CurlGen $curlGen, Request $request, $broker, $noKontrak, $tipeEfek){

    // return $request->all();
    $url = "/api/eksekusi-gadai-efek";
    $data = array(
      "brokerPartner" => $request->broker,
      "collaterals" => [],
      "dueDate" => $request->jatuhTempo1,
      "tipeEfek" => $request->tipeEfek1,
      "tradeDate" => $request->tradeDate1
    );

    foreach ($request->kodeEfek as $keys => $values) {
      if($request->settle[$keys]>0){
          $data["collaterals"][$keys] = array(
                "kodeEfek"=> $request->kodeEfek[$keys],
                "noKontrak"=> $request->noKontrak,
                "qtyEfek"=> $request->settle[$keys]
              );
      }
    }

    // print_r("<pre>".print_r($data, true)."<pre>");
    // return;

    $data['collaterals'] = array_values($data['collaterals']);
    // $json = json_encode($data);

    // return $data;
    $result = $curlGen->storeResView($url, $data);
    // return $result;
    if($result[0]!=201){

      $info = "Error";
      $colors = "red";
      $icons = "fas fa-times";
      $alert = $result['1']['title'];

      Session::flash('info', $info);
      Session::flash('colors', $colors);
      Session::flash('icons', $icons);
      Session::flash('alert', $alert);

      return redirect(url('monitoring/transaksi/eksekusi/'.$broker.'/'.$tipeEfek.'/'.$noKontrak));

    }else{

      $info = "Success";
      $colors = "green";
      $icons = "fas fa-check-circle";
      $alert = 'Tersimpan';

      return redirect(url('transaksi/eksekusi'));
    }



  }
  public function index(){
    return view('transaction.execution.index');
  }

  public function getIndex(CurlGen $curlGen){

    $url = "/api/eksekusi-list";
    $result = $curlGen->getIndex($url);

    return Datatables::of($result)->make(true);
  }

  public function subDetail(CurlGen $curlGen, $noEksekusi){

    $url = "/api/eksekusi-view/".$noEksekusi;
    $result = $curlGen->getIndex($url);

    return $result;
  }

  public function alokasi(CurlGen $curlGen, $noEksekusi){

    $url = "/api/eksekusi-view/".$noEksekusi;
    $result = $curlGen->getIndex($url);

    $urlBroker = "/api/brokers/";
    $broker = $curlGen->getIndex($urlBroker);

    // return $broker[0];
    return view('transaction.execution.alokasi')
    ->with('result', $result)
    ->with('brokers', $broker);
  }

  public function alokasiView(CurlGen $curlGen, $noEksekusi){

    $url = "/api/eksekusi-view/".$noEksekusi;
    $result = $curlGen->getIndex($url);

    $urlBroker = "/api/brokers/";
    $broker = $curlGen->getIndex($urlBroker);

    // return $broker[0];
    return view('transaction.execution.alokasiView')
    ->with('result', $result)
    ->with('brokers', $broker);
  }
  public function getAlokasi(CurlGen $curlGen, $noEksekusi){

    $url = "/api/eksekusi-view/".$noEksekusi;
    $result = $curlGen->getIndex($url);

    return $result;
  }

}
