<?php

namespace App\Http\Controllers\Withdraw;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use App\Library\CurlGen;
use yajra\Datatables\Datatables;
use Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class WithdrawController extends Controller
{

  public function kseiDoc(CurlGen $curlGen, Request $request){
    $url = "/api/withdraw/kseidoc?docType=".$request->type."&noStockWithdrawList=".$request->checks."&tipeEfek=S";

    if($request->type == 'xml'){
      $param = $curlGen->kseiDoc($url, $request->checks);
      echo $param->original;
    }else{
      $param = $curlGen->kseiDocCSv($url, $request->checks);
      return $param;
    }


  }

  public function getDetail2(CurlGen $curlGen, $noKontrak){

    $url = "/api/transaksi-gadai-efek-monitoring-detail/".$noKontrak;
    $param = $curlGen->getIndex($url);

    return $param;
  }
  public function index(){
	    return view('transaction.withdraw.index');
	}

	public function create(CurlGen $curlGen){
    $kustodiUrl = "/api/bank-custodis";
    $kustodi = $curlGen->getIndex($kustodiUrl);

	    return view('transaction.withdraw.create')
      ->with('brokers', $kustodi);
	}

  public function sendInstruksi(Request $request, CurlGen $curlGen){

    $url = "/api/withdraw-generate-and-send-email-instruksi-to-custody";
    $data = array(
      "swCodeList" => array_keys($request->checks),
      "tipeEfek" => "S"
    );

    $param = $curlGen->store($url,$data);
    // return $param;
    if($param[0]!="200"){
        $info = "Error";
        $colors = "red";
        $icons = "fas fa-times";
        if($param['1']['message']==null){
          $msg="Internal Server Error [500]";
        }
        else{
          $msg = $param['1']['message'];
        }
        $alert = $msg;
    }else{
        $info = "Success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = 'Tersimpan';
    }
    // return $alert;

    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return redirect(url('transaksi/stock-withdraw'));

  }
  public function pengajuan(CurlGen $curlGen, $noDeps, $tipeEfek){

    $url = "/api/withdraw-generate-and-send-email-instruksi-to-custody";
    $data = array(
      "swCodeList" => [$noDeps],
      "tipeEfek" => $tipeEfek
    );

    $param = $curlGen->store($url,$data);
    // return $param;
    if($param!="200"){
        $info = "Error";
        $colors = "red";
        $icons = "fas fa-times";
        if($param['1']['message']==null){$msg="Internal Server Error [500]";}
        else{$msg = $param['1']['message'];}
        $alert = $msg;
    }else{
        $info = "Success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = 'Tersimpan';
    }
    // return $alert;

    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return redirect(url('transaksi/stock-withdraw'));

  }
  public function settle(CurlGen $curlGen, $act, $noDeps, $qty){

    $url = "/api/withdraw-dari-custody";
    $data = array(
      "action" => $act,
      "noWithdraw" => $noDeps,
      "qtySettle" => $qty
    );

    //return $data;
    $param = $curlGen->update($url,$data);
    // return $param;
    if($param[0]!="200"){
        $info = "Error";
        $colors = "red";
        $icons = "fas fa-times";
        $alert = $param['1']['message'];
    }else{
        $info = "Success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = 'Tersimpan';
    }
    // return $param[0];
    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return redirect(url('transaksi/stock-withdraw'));


  }
  public function getIndex(CurlGen $curlGen, Request $request){

    date_default_timezone_set("Asia/Jakarta");
    if($request->type_efek=="A"){
      $url = "/api/stock-withdraw-list";
    }else{
      $url = "/api/stock-withdraw-list/".$request->type_efek;
    }

    $param = $curlGen->getIndex($url);

    return Datatables::of($param)->escapeColumns([])->make(true);
  }

  public function getPengajuan(CurlGen $curlGen, $noPengajuan){

    $url = "/api/transaksi-gadai-efek-monitoring-detail/".$noPengajuan;
    $param = $curlGen->checkPengajuan($url);

    return $param;
  }

  public function withdrawCollateral(CurlGen $curlGen, Request $request){

    // return $request->all();

    $url = "/api/withdraw-collateral";
    $data = array(
    "collateralList" => [],
    "counterPart" => $request->kustodi,
    "noPengajuan" => $request->noKontrak,
    "settlementDate" => $request->tglPengajuan
    );

    foreach ($request->qtyRequest as $key => $values) {
        if($request->qtyRequest==0){
          continue;
        }
      foreach ($request->kodeEfek as $keys => $value) {

      $qtyReq[$key] = str_replace('.','', $request->qtyRequest[$key]);

                if($request->qtyRequest!="0"){
                  $data["collateralList"][$key] = array(
                      "kodeEfek"=> $request->kodeEfek[$key],
                      "qtyEfek"=> $qtyReq[$key]
                    );
                }
              }
            }

    // return $data;
    $param = $curlGen->update($url,$data);
    if($param[0]=="200" || $param[0]==200){

    $info = "Success";
    $colors = "green";
    $icons = "fas fa-check-circle";
    $alert = 'Tersimpan';

    Session::flash('info', $info);
    Session::flash('colors', $colors);
    Session::flash('icons', $icons);
    Session::flash('alert', $alert);

    return $param;
    // return redirect(url('transaksi/stock-withdraw'));

    }else{
      return $param;
    }


  }
}
