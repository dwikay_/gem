<?php

namespace App\Http\Controllers\ClosePrice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use App\Library\CurlGen;
use yajra\Datatables\Datatables;
use File;
use Carbon\Carbon;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClosePriceController extends Controller
{
  public function index(){
    return view('monitoring.closingPrice.index');
  }
  public function create(){
    return view('monitoring.closingPrice.upload');
  }
  public function storeUpload(CurlGen $curlGen, Request $request){

    $this->validate($request, array(
        'doc'      => 'required'
    ));

    if($request->hasFile('doc')){

        $extension = File::extension($request->doc->getClientOriginalName());

        if ($extension == "xls" || $extension == "csv") {

            $path = $request->doc->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();

            return $data;

            if(!empty($data) && $data->count()){

                foreach ($data as $key => $value) {
                    $insert[] = [
                    'tanggal' => $value->tanggal->toDateString(),
                    'kodeEfek' => $value->kode_efek,
                    'harga' => $value->harga,                    
                    ];
                }
                if(!empty($insert)){

                    if ($insert) {
                      foreach ($insert as $inserts){
                        $url = "/api/harga-penutupans";
                        $data = array(
                            'tanggal'      => $inserts['tanggal'],
                            'kodeEfek'     => $inserts['kodeEfek'],
                            'harga'        => $inserts['harga']
                          );

                          $httpCode = $curlGen->store($url,$data);

                      }

                        Session::flash('alert', 'Your Data has successfully imported');
                        return redirect(url('monitoring/closingPrice'));
                    }else {
                        Session::flash('info', 'Error!');
                        Session::flash('alert', 'Failed inserting the data..');
                        return back();
                    }
                }
            }
            return back();
        }else {
            Session::flash('info', 'Error');
            Session::flash('alert', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
            return back();
          }
      }
  }

  public function import(CurlGen $curlGen, Request $request){
    

    if($request->hasFile('saham')){

        $extension = File::extension($request->saham->getClientOriginalName());

        if ($extension == "txt" || $extension == "csv") {

        $destinationPath = "file/closePrice/upload/".date('Ymdh');
      // dd($destinationPath);
      if (!is_dir($destinationPath)) {
         File::makeDirectory(public_path().'/'.$destinationPath, 0777, true);
      }

      $file = $request->file('saham');
      $fileName = $file->getClientOriginalName();
      $fileExt = $file->getClientOriginalExtension();
      $file->move($destinationPath, $file->getClientOriginalName());

      $pub = public_path($destinationPath);
      $af_pub = explode("\\",$pub);
      $afs = implode("/", $af_pub);
      $cfile = new \CURLFile(realpath($destinationPath.'/'.$fileName));


           $data = array(
              'file' => $cfile
              //'file' => 'http://localhost:8000/file/haircut/upload/2019050705/sampel.txt'
           );

          // return $afs;           
           $url = "/api/latest-price-saham/upload";
           // return $cfile;
           $param = $curlGen->uploads($url, $data, $afs);

            // return $param;

           if($param[1]!=200){
             Session::flash('info', 'Error');
             Session::flash('colors', 'red');
             Session::flash('icons', 'fas fa-times');
             Session::flash('alert', 'Gagal mengupload, harap cek tipe file terlebih dahulu');
             return back();
           }else{
             Session::flash('info', 'Success');
             Session::flash('colors', 'green');
             Session::flash('icons', 'fas fa-check-circle');
             Session::flash('alert', 'Berhasil di upload');
             return back();
           }
        }else {
            Session::flash('info', 'Error');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Gagal mengupload, harap cek tipe file terlebih dahulu');
            return back();
          }
      }     
       
    if($request->hasFile('obligasi')){

        $extension = File::extension($request->obligasi->getClientOriginalName());
                
        if ($extension == "txt" || $extension == "csv") {

        $destinationPath = "file/closePrice/upload/".date('Ymdh');
      // dd($destinationPath);
      if (!is_dir($destinationPath)) {
         File::makeDirectory(public_path().'/'.$destinationPath, 0777, true);
      }

      $file = $request->file('obligasi');
      $fileName = $file->getClientOriginalName();
      $fileExt = $file->getClientOriginalExtension();
      $file->move($destinationPath, $file->getClientOriginalName());

      $pub = public_path($destinationPath);
      $af_pub = explode("\\",$pub);
      $afs = implode("/", $af_pub);
      $cfile = new \CURLFile(realpath($destinationPath.'/'.$fileName));


           $data = array(
              'file' => $cfile
              //'file' => 'http://localhost:8000/file/haircut/upload/2019050705/sampel.txt'
           );

          // return $afs;           
           $url = "/api/latest-price-obligasi/upload";
           // return $cfile;  
           $param = $curlGen->uploads($url, $data, $afs);

            // return $param;

           if($param[1]!=200){
             Session::flash('info', 'Error');
             Session::flash('colors', 'red');
             Session::flash('icons', 'fas fa-times');
             Session::flash('alert', 'Gagal mengupload, harap cek tipe file terlebih dahulu');
             return back();
           }else{
             Session::flash('info', 'Success');
             Session::flash('colors', 'green');
             Session::flash('icons', 'fas fa-check-circle');
             Session::flash('alert', 'Berhasil di upload');
             return back();
           }
        }else {
            Session::flash('info', 'Error');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Gagal mengupload, harap cek tipe file terlebih dahulu');
            return back();
          }
      }
  }


  public function getIndex(CurlGen $curlGen, Request $request){

    $url = "/api/harga-penutupans-harian/".$request->tipeEfek."/".$request->tanggal;

    $param = $curlGen->getIndex($url);

    return Datatables::of($param)->escapeColumns([])->make(true);
  }
  public function getSaham(CurlGen $curlGen, Request $request){
    ini_set('max_execution_time','3600');

    // return $request->tglGetDatas;
    $url = "/api/loadDataSaham/".$request->tglGetDatas;

    $param = $curlGen->getIndex($url);
    // return $param;

    return redirect(url('monitoring/closingPrice'));
  }

  
  public function downloadO(){

    $responses = response()->download(storage_path("app/public/sampelObligasi.csv"));
    ob_end_clean();
    return $responses;

  }

  public function downloadS(){

    $responses = response()->download(storage_path("app/public/sampelSaham.txt"));
    ob_end_clean();
    return $responses;

  }

  public function indexUpload()
  {
    return view('monitoring.closingPrice.upload');
  }
}
