<?php

namespace App\Http\Controllers\Monitoring;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Library\CurlGen;
use App\User;
use yajra\Datatables\Datatables;
use App\Model\Role;
use App\Model\Module;
use App\Model\Roleacl;
use DB;
use Auth;
use Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MonitoringController extends Controller
{
    public function stockMutationReport(CurlGen $curlGen, $kodeEfek, $noKontrak){

      $url = "/api/stock-mutation-report-by-no-kontrak-and-kode-efek?kodeEfek=".$kodeEfek."&noKontrak=".$noKontrak;
      $param = $curlGen->getIndex($url);
      $list = $param[0]['trHeaderStockMutationList'][0]['trDetailStockMutations'];
      
      return view('monitoring.valuasi.detail')
      ->with('param',$param[0])
      ->with('list', $list);
    }
    public function getIndex(CurlGen $curlGen){

      $url = "/api/transaksi-gadai-efek-monitoring";
      $param = $curlGen->getIndex($url);

      return Datatables::of($param)->escapeColumns([])->make(true);
    }

    public function index(CurlGen $curlGen){

      $url = "/api/brokers/";
      $broker = $curlGen->getIndex($url);
      // dd($broker);
      return view('monitoring.valuasi.index')
      ->with('brokers', $broker);
    }

    public function getIndexPelunasan(CurlGen $curlGen){

      $url = "/api/transaksi-gadai-efek-monitoring-pelunasan";
      $param = $curlGen->getIndex($url);

      return Datatables::of($param)->escapeColumns([])->make(true);
    }

    public function createFromMonitoring(CurlGen $curlGen, $noKontrak){

      $kustodiUrl = "/api/bank-custodis";
      $kustodi = $curlGen->getIndex($kustodiUrl);

      $url = "/api/pengajuan-gadai-efek-monitoring-detail/".$noKontrak;
      $param = $curlGen->getIndex($url);

      // return $param;
  	    return view('transaction.withdraw.createFromMonitoring')
        ->with('brokers', $kustodi)
        ->with('param', $param);

    }

    public function indexPelunasan(){

      return view('monitoring.valuasi.pelunasan');
    }

    public function getDetail(CurlGen $curlGen, $noKontrak){

      $url = "/api/transaksi-gadai-efek-monitoring-detail/".$noKontrak;
      $param = $curlGen->getIndex($url);

      return $param;
    }

    public function actionPelunasan(CurlGen $curlGen, $noPengajuan, $action){

      $url = "/api/transaksi-gadai-efek-action";

      $data = array(
        "action" => $action,
        "noPengajuan" => $noPengajuan
      );

      $param = $curlGen->storeSec($url,$data);

      // return $param;

      if($param[0]!=201){
          $info = "Error";
          $colors = "red";
          $icons = "fas fa-times";
          if($param[1]['title']==null){ $msg="Internal Server Error [500]"; }
          else{$msg = $param[1]['title'];}
          $alert = $msg;
      }else{
          $info = "Success";
          $colors = "green";
          $icons = "fas fa-check-circle";
          $alert = 'Tersimpan';
      }
      // return $alert;

      Session::flash('info', $info);
      Session::flash('colors', $colors);
      Session::flash('icons', $icons);
      Session::flash('alert', $alert);

      return redirect(url('monitoring/pelunasan'));



    }
}
