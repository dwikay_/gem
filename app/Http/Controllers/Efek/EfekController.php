<?php

namespace App\Http\Controllers\Efek;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use App\Library\CurlGen;
use yajra\Datatables\Datatables;
use Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EfekController extends Controller
{
  public function index(){
    return view('static_data.efek.index');
  }
  public function create(){
    return view('static_data.efek.create');
  }
  public function store(CurlGen $curlGen, Request $request){

    // $parsingCP1 = str_replace(".","",$request->closingPrice);
    // $parsingCP2 = str_replace(",",".",$parsingCP1);
    //
    // $parsingHC1 = str_replace(".","",$request->hairCut);
    // $parsingHC2 = str_replace(",",".",$parsingHC1);


    if($request->status_gadai=="1"){
      $statusGadai = true;
    }else{
      $statusGadai = false;
    }
    $url = "/api/efeks";
    $data = array(
      'id' => $id,
      'kodeEfek' => $request->kodeEfek,
      'namaEfek' => $request->namaEfek,
      'closingPrice' => 0,
      'closingDate' => date('Y-m-d'),
      'hairCut' => 0,
      'hairCutDate' => date('Y-m-d'),
      'statusGadai' => true,
      'jlhEfekBeredar' => $request->efekBeredar,
      'bmpk' => $request->bnpk,
      'bondRating' => null,
      'bondMaturityDate' => date('Y-m-d'),
      'satuan' => $request->satuan,
      'status' => $request->status,
      'tipeEfek' => [
          'id' => $request->id_efek,
          ]
      );

      // $httpCode = $curlGen->store($url,$data);

      return redirect(url('dataStatis/efek-resources'));
  }
          public function updateStatusGadai(CurlGen $curlGen, $id){



        if($request->statusGadai=="on"){
          $statusGadai = true;
        }else{
          $statusGadai = false;
        }
        $url = "/api/efeks/saham";
        $data = array(
          'id' => $id,
          'bmpk' => $request->bmpk,
          'bondMaturityDate' => date('Y-m-d'),
          'bondRating' => null,
          'closingPrice' => $request->closePrice,
          'closingDate' => $request->closingDate,
          'hairCut' => $request->hairCut,
          'hairCutDate' => $request->hairCutDate,
          'kodeEfek' => $request->kodeEfek,
          "maxLoanQty" => $request->maxLoanQty,
          "usedLoanQty" => $request->usedLoanQty,
          'namaEfek' => $request->namaEfek,
          'statusGadai' => $statusGadai,
          'jlhEfekBeredar' => $request->beredar,
          'satuan' => $request->satuan,
          'status' => $request->status,
          'tipeEfek' => [
              'id' => $request->statusEfek,
              ]
          );

          // return $data;
          $param = $curlGen->patch($url,$data);
          // return $param;
          return redirect(url('dataStatis/efek-resources'));
        }
      public function update(CurlGen $curlGen, Request $request, $id){

    if($request->statusGadai=="on"){
      $statusGadai = true;
    }else{
      $statusGadai = false;
    }
    $url = "/api/efeks/saham";
    $data = array(
      'id' => $id,
      'bmpk' => $request->bmpk,
      'bondMaturityDate' => date('Y-m-d'),
      'bondRating' => null,
      'closingPrice' => $request->closePrice,
      'closingDate' => $request->closingDate,
      'hairCut' => $request->hairCut,
      'hairCutDate' => $request->hairCutDate,
      'kodeEfek' => $request->kodeEfek,
      "maxLoanQty" => $request->maxLoanQty,
      "usedLoanQty" => $request->usedLoanQty,
      'namaEfek' => $request->namaEfek,
      'statusGadai' => $statusGadai,
      'jlhEfekBeredar' => $request->beredar,
      'satuan' => $request->satuan,
      'status' => $request->status,
      'tipeEfek' => [
          'id' => $request->statusEfek,
          ]
      );

      //return $data;
      $param = $curlGen->patch($url,$data);
      //return $param;
      return redirect(url('dataStatis/efek-resources'));
  }

  public function updateObligasi(CurlGen $curlGen, Request $request, $id){


        // $parsingCP1 = str_replace(".","",$request->closingPrice);
        // $parsingCP2 = str_replace(",",".",$parsingCP1);
        //
        // $parsingHC1 = str_replace(".","",$request->hairCut);
        // $parsingHC2 = str_replace(",",".",$parsingHC1);

        if($request->statusGadai=="on"){
          $statusGadai = true;
        }else{
          $statusGadai = false;
        }
        $url = "/api/efeks/obligasi";
        $data = array(
          'id' => $id,
          'bmpk' => $request->bmpk,
          'bondCouponRate' => $request->bondCouponRate,
          'bondIsin' => $request->bondIsin,
          'bondIssuerCode' => $request->bondIssuerCode,
          'bondListingDate' => $request->bondListingDate,
          'bondMaturityDate' => $request->bondMaturityDate,
          'bondRating' => $request->bondRating,
          'bondTrustee' => $request->bondTrustee,
          'bondTtm' => $request->bondTtm,
          'closingPrice' => $request->closePrice,
          'closingDate' => $request->closingDate,
          'hairCut' => $request->hairCut,
          'hairCutDate' => $request->hairCutDate,
          'kodeEfek' => $request->kodeEfek,
          "maxLoanQty" => $request->maxLoanQty,
          "usedLoanQty" => $request->usedLoanQty,
          'namaEfek' => $request->namaEfek,
          'statusGadai' => $statusGadai,
          'jlhEfekBeredar' => $request->beredar,
          'satuan' => $request->satuan,
          'status' => $request->status,
          'tipeEfek' => [
              'id' => $request->statusEfek,
              ]
          );

          // return $data;
          $param = $curlGen->patch($url,$data);
          // return $param;
          return redirect(url('dataStatis/efek-resources'));

  }
  public function getIndex(CurlGen $curlGen, Request $request){

    date_default_timezone_set("Asia/Jakarta");
    if($request->type_efek=="A"){
      $url = "/api/efeks/?size=100000000";
    }else{
      $url = "/api/gemefeks/".$request->type_efek;
    }

    $param = $curlGen->getIndex($url);

    return Datatables::of($param)->escapeColumns([])->make(true);
  }

  public function edit(CurlGen $curlGen, $kodeEfek){

    $url = "/api/efekByKodeEfek/".$kodeEfek;

    $param = $curlGen->edit($url);
    if($param[1]['tipeEfek']['tipeEfek']=="S"){
      $views = "edit";
    }else{
      $views = "obligasi";
    }
    return view('static_data.efek.'.$views)
      ->with('efek', $param[1]);

  }
  public function delete(CurlGen $curlGen, $id){

       $url = "/api/efeks/".$id;
       $param = $curlGen->delete($url);

      return redirect(url('dataStatis/efek-resources'));
  }
}
