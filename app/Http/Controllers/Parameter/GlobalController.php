<?php

namespace App\Http\Controllers\Parameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use App\Library\CurlGen;
use Auth;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GlobalController extends Controller
{
  public function index(){
    return view('static_data.globalParam.index');
  }
  public function create(){
    return view('static_data.globalParam.create');
  }
  public function getIndex(CurlGen $curlGen, Request $request){

    $url = "/api/parameter-globals";
    $param = $curlGen->getIndex($url);

      return Datatables::of($param)->escapeColumns([])->make(true);

  }

  public function store(CurlGen $curlGen, Request $request){

    // return $request->all();

    if($request->prmType=="S"){ //STRING
      $strings = $request->hasVal;
      $integers = 0;
      $dates = null;
      $floats = 0;
    }else if($request->prmType=="L"){ //LONG
      $strings = null;
      $integers = $request->hasVal;
      $dates = null;
      $floats = 0;
    }else if($request->prmType=="D"){ //DOUBLE
      $strings = null;
      $integers = 0;
      $dates = null;
      $floats = $request->hasVal;
    }else if($request->prmType=="T"){ //DATE
      $strings = null;
      $integers = 0;
      $dates = $request->hasVal;
      $floats = 0;
    }
    if($request->editable=="on"){$edits = true;}else{$edits = false;}
    if($request->showable=="on"){$shows = true;}else{$shows = false;}

    $url = "/api/parameter-globals";
    $data   = array(
        'appType' => "",
        'dateVal' => $dates,
        'doubleVal' => $floats,
        'edit' => $edits,
        'longVal' => $integers,
        'prmId' => $request->prmId,
        'prmTy' => $request->prmType,
        'prmnName' => $request->prmName,
        'show' => $shows,
        'strVal' => $strings
      );

      $httpCode = $curlGen->store($url,$data);

      return redirect(url('dataStatis/globalParam'));
  }
  public function edit(CurlGen $curlGen, $id){

    $url = "/api/parameter-globals/".$id;
    $param = $curlGen->edit($url);

      if($param[1]['prmTy']=="S"){$vals = $param[1]['strVal'];}
      else if($param[1]['prmTy']=="L"){$vals = $param[1]['longVal'];}
      else if($param[1]['prmTy']=="D"){$vals = $param[1]['doubleVal'];}
      else if($param[1]['prmTy']=="T"){$vals = $param[1]['dateVal'];}
      if($param[1]['edit']==true){$editable = "checked";}else{$editable = "";}
      if($param[1]['show']==true){$showable = "checked";}else{$showable = "";}

      return view('static_data.globalParam.edit')
      ->with('globalParam', $param[1])
      ->with('vals', $vals)
      ->with('editable', $editable)
      ->with('showable', $showable);
  }

  public function update(CurlGen $curlGen, Request $request, $id){

    // return $request->all();

    if($request->prmType=="S"){ //STRING
      $strings = $request->hasVal;
      $integers = 0;
      $dates = null;
      $floats = 0;
    }else if($request->prmType=="L"){ //LONG
      $strings = null;
      $integers = $request->hasVal;
      $dates = null;
      $floats = 0;
    }else if($request->prmType=="D"){ //DOUBLE
      $strings = null;
      $integers = 0;
      $dates = null;
      $floats = $request->hasVal;
    }else if($request->prmType=="T"){ //DATE
      $strings = null;
      $integers = 0;
      $dates = $request->hasVal;
      $floats = 0;
    }
    if($request->editable=="on"){$edits = true;}else{$edits = false;}
    if($request->showable=="on"){$shows = true;}else{$shows = false;}

    $url = "/api/parameter-globals";
    $data   = array(
        'appType' => "",
        'dateVal' => $dates,
        'doubleVal' => $floats,
        'edit' => $edits,
        'id' => $id,
        'longVal' => $integers,
        'prmId' => $request->prmId,
        'prmTy' => $request->prmType,
        'prmnName' => $request->prmName,
        'show' => $shows,
        'strVal' => $strings
      );

      $param = $curlGen->update($url, $data);

      return redirect(url('dataStatis/globalParam'));
  }

  public function delete(CurlGen $curlGen, $id){

    $url = "/api/parameter-globals/".$id;
    $param = $curlGen->delete($url);

      return redirect(url('dataStatis/globalParam'));
  }
}
