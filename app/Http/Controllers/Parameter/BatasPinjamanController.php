<?php

namespace App\Http\Controllers\Parameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use App\Library\CurlGen;
use yajra\Datatables\Datatables;
use File;
use Carbon\Carbon;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BatasPinjamanController extends Controller
{
  public function index(){
    return view('static_data.batasPinjaman.index');
  }
  public function create(){
    return view('static_data.batasPinjaman.upload');
  }

  public function import(CurlGen $curlGen, Request $request){

    $this->validate($request, array(
        'doc'      => 'required'
    ));

    if($request->hasFile('doc')){

        $extension = File::extension($request->doc->getClientOriginalName());

        if ($extension == "txt" || $extension == "csv") {

        $destinationPath = "file/batasPinjaman/upload/".date('Ymdh');
      // dd($destinationPath);
      if (!is_dir($destinationPath)) {
         File::makeDirectory(public_path().'/'.$destinationPath, 0777, true);
      }

      $file = $request->file('doc');
      $fileName = $file->getClientOriginalName();
      $fileExt = $file->getClientOriginalExtension();
      $file->move($destinationPath, $file->getClientOriginalName());

      $pub = public_path($destinationPath);
      $af_pub = explode("\\",$pub);
      $afs = implode("/", $af_pub);
      $cfile = new \CURLFile(realpath($destinationPath.'/'.$fileName));

           $data = array(
              'file' => $cfile,
              'tipeEfek' => "S"
              //'file' => 'http://localhost:8000/file/haircut/upload/2019050705/sampel.txt'
           );

           $url = "/api/batas-pinjaman/efek/upload";
           $param = $curlGen->uploads($url, $data, $afs);
           //return $param;
           if($param[1]!=200){
             Session::flash('info', 'Error');
             Session::flash('colors', 'red');
             Session::flash('icons', 'fas fa-times');
             Session::flash('alert', 'Gagal mengupload, harap cek tipe file terlebih dahulu');
             return back();
           }else{
             Session::flash('info', 'Success');
             Session::flash('colors', 'green');
             Session::flash('icons', 'fas fa-check-circle');
             Session::flash('alert', 'Berhasil di upload');
             return back();
           }
        }else {
            Session::flash('info', 'Error');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Gagal mengupload, harap cek tipe file terlebih dahulu');
            return back();
          }
      }else{
        Session::flash('info', 'Error');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-times');
        Session::flash('alert', 'Gagal mengupload, harap cek file terlebih dahulu');
        return back();
      }
  }
  public function getIndex(CurlGen $curlGen, Request $request){

    $url = "/api/batas-pinjaman/S/".$request->tanggal;
    $param  = $curlGen->getIndex($url);
    return Datatables::of($param)->escapeColumns([])->make(true);
  }

  public function download(){
    $responses = response()->download(storage_path("app/public/BatasPinjamanEfek.txt"));
    ob_end_clean();
    return $responses;

  }
}
