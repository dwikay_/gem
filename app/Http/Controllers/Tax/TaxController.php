<?php

namespace App\Http\Controllers\Tax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\User;

class TaxController extends Controller
{
  public function index(){
    return view('static_data.tax.index');
  }
  public function create(){
    return view('static_data.tax.create');
  }
    public function getIndex(CurlGen $curlGen, Request $request){

      $url = "/api/tax-ids";

      $param = $curlGen->getIndex($url);

      return Datatables::of($param)->make(true);

    }

    public function store(CurlGen $curlGen, Request $request){

      $url = "/api/tax-ids";
      $data = array(
          'taxCode' => $request->taxCode,
          'shortDesc' => $request->shortDesc,
          'longDesc' => $request->longDesc
        );

        $param = $curlGen->store($url, $data);

        return redirect(url('dataStatis/tax'));

    }
    public function edit(CurlGen $curlGen , $id){

      $url = "/api/tax-ids/".$id;

      $param = $curlGen->edit($url);

      return view('static_data.tax.edit')
      ->with('tax', $param[1]);

    }

    public function update(CurlGen $curlGen, Request $request, $id){

      $url = "/api/tax-ids";
      $data = array(
          'id' => $id,
          'taxCode' => $request->taxCode,
          'shortDesc' => $request->shortDesc,
          'longDesc' => $request->longDesc
        );

      $param =  $curlGen->update($url,$data);

      return redirect(url('dataStatis/tax'));
    }

    public function delete(CurlGen $curlGen, $id){

         $url = "/api/tax-ids/".$id;
         $param = $curlGen->delete($url);

        return redirect(url('dataStatis/tax'));

    }
}
