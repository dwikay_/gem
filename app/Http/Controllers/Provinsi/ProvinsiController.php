<?php

namespace App\Http\Controllers\Provinsi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\User;

class ProvinsiController extends Controller
{
  public function index(CurlGen $curlGen){

    $url = "/api/countries";

    $param = $curlGen->getIndex($url);

    return view('static_data.provinsi.index')
    ->with('param', $param);
  }
  public function create(CurlGen $curlGen){

    $url = "/api/countries";

    $param = $curlGen->getIndex($url);

    return view('static_data.provinsi.create')
    ->with('negara', $param);
  }
    public function getIndex(CurlGen $curlGen, Request $request){

      $url = "/api/provinces";

      $param = $curlGen->getIndex($url);

      return Datatables::of($param)->escapeColumns([])->make(true);

    }

    public function store(CurlGen $curlGen, Request $request){

      $url = "/api/provinces";
      $data = array(
	        'provinceCode' => $request->kodeProvinsi,
	        'provinceName' => $request->namaProvinsi,
	        'pegadaianCode' => $request->pegadaianCode,
	        'country'=>array(
	        	'id'=>$request->country
	        )
	    );

        $param = $curlGen->store($url, $data);

        return redirect(url('dataStatis/provinsi'));

    }
    public function edit(CurlGen $curlGen , $id){

      $url = "/api/provinces/".$id;
      $param = $curlGen->edit($url);

      $urls = "/api/countries/";
      $negara = $curlGen->getIndex($urls);

      return view('static_data.provinsi.edit')
      ->with('provinsi', $param[1])
      ->with('negara',$negara);

    }

    public function update(CurlGen $curlGen, Request $request, $id){

      $url = "/api/provinces";
      $data = array(
          'id' => $id,
	        'provinceCode' => $request->kodeProvinsi,
	        'provinceName' => $request->namaProvinsi,
	        'pegadaianCode' => $request->pegadaianCode,
	        'country'=>array(
	        	'id'=>$request->country
	        )
        );

      $param =  $curlGen->update($url,$data);

      return redirect(url('dataStatis/provinsi'));
    }

    public function delete(CurlGen $curlGen, $id){

         $url = "/api/provinces/".$id;
         $param = $curlGen->delete($url);

        return redirect(url('dataStatis/provinsi'));

    }
}
