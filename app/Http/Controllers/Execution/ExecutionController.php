<?php

namespace App\Http\Controllers\Execution;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ExecutionController extends Controller
{	
    public function index(){
	    return view('transaction.execution.index');
	}

  	public function getIndex(CurlGen $curlGen){
  		$eksekusiDetail = array(
		      array(
		      	'id' => 1,
		      	'tanggalTransaksi' => '2019-02-25',
						'kodeTransaksi' => "FS-0001",
						'kodeNasabah' => "R0001",
						'namaNasabah' => "Malye",
						'tanggalPerdagangan' => '2019-02-25',
						'tanggalJatuhTempo' => '2019-02-25',
						'brokerPartner' => "MS",
						'status' => "1",
						'detailEksekusi'=> array(
							 array(
								'id'=> 1,
								'stockCode' => 'TLKM',
								'quantity' => 100000,
								'requestPrice' => 1500,
								'matchedQty' => 100000,
								'matchedPrice' => 1450,
								'grossAmount' => 145000000,
								'charges' => 435000,
								'netAmount' => 144565000,
								'allocationQty' => 100000,
								'allocationQtyAmount' => 144565000,
                'alokasiEksekusi' =>
                array(
                  'id' => 1,
                  'noKontrak' => "CR-0001",
                  'qty' => 40000,
                  'amount' => 57826000
                )
							 ),
             ),
           ),

		      	);

        // return $eksekusiDetail;
      	return Datatables::of($eksekusiDetail)->make(true);
  	}
}
