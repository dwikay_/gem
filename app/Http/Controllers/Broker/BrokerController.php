<?php

namespace App\Http\Controllers\Broker;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BrokerController extends Controller
{
  public function index(){
    return view('static_data.broker.index');
  }
  public function create(CurlGen $curlGen){

    $url = "/api/bank-custodis";

    $param = $curlGen->getIndex($url);

    return view('static_data.broker.create')
    ->with('kustodi', $param);
  }
  public function getIndex(CurlGen $curlGen, Request $request){

    $url = "/api/brokers";

    $param = $curlGen->getIndex($url);

    return Datatables::of($param)->escapeColumns([])->make(true);

  }

  public function store(CurlGen $curlGen, Request $request){

    $url = "/api/brokers";

    $data = array(
      'kodeBroker'  => $request->kodeBroker,
      'namaBroker'  => $request->namaBroker,
      'bankCustodi' => array(
          'id' => $request->kustodi
        )
    );

    $httpCode = $curlGen->store($url,$data);

      return redirect(url('dataStatis/broker'));

  }
  public function edit(CurlGen $curlGen, $id){

      $url = "/api/brokers/".$id;

      $param = $curlGen->edit($url);

      $urls = "/api/bank-custodis";

      $kustodi = $curlGen->getIndex($urls);

      return view('static_data.broker.edit')
      ->with('broker', $param[1])
      ->with('kustodi', $kustodi);
      }


  public function update(CurlGen $curlGen, Request $request, $id){

    $url = "/api/brokers";

    $data = array(
        'id' => $id,
        'kodeBroker' => $request->kodeBroker,
        'namaBroker' => $request->namaBroker,
	      'bankCustodi'   => array(
	        	'id'=> $request->kustodi
	        )
	    );

      $param = $curlGen->update($url,$data);

      return redirect(url('dataStatis/broker'));
  }

  public function delete(CurlGen $curlGen, $id){

    $url = "/api/brokers/".$id;
    $param = $curlGen->delete($url);
      return redirect(url('dataStatis/broker'));
  }
}
