<?php

namespace App\Http\Controllers\Country;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CountryController extends Controller
{
    public function index(){
	    return view('static_data.country.index');
	}
	public function create(){
	    return view('static_data.country.create');
	}

	public function getIndex(CurlGen $curlGen, Request $request){

    $url = "/api/countries";

    $param = $curlGen->getIndex($url);

    return Datatables::of($param)->escapeColumns([])->make(true);

  	}

  	public function store(CurlGen $curlGen, Request $request){
	    $url = "/api/countries";
	    $data = array(
	        'countryCode' => $request->countryCode,
	        'countryName' => $request->countryName,
	        'nationality' => $request->nationality,
	        'pegadaianCode' => $request->pegadaianCode,
	    );

	    $param = $curlGen->store($url, $data);

	    return redirect(url('dataStatis/country'));
  	}

  	public function edit(CurlGen $curlGen , $id){

	    $url = "/api/countries/".$id;

	    $param = $curlGen->edit($url);

	    return view('static_data.country.edit')
	    ->with('country', $param[1]);
  	}

  	public function update(CurlGen $curlGen, Request $request, $id){
	    $url = "/api/countries";
	    $data = array(
	        'id' => $id,
	        'countryCode' => $request->countryCode,
	        'countryName' => $request->countryName,
	        'nationality' => $request->nationality,
	        'pegadaianCode' => $request->pegadaianCode,
	      );

	    $param =  $curlGen->update($url,$data);

	    return redirect(url('dataStatis/country'));
  	}

  	public function delete(CurlGen $curlGen, $id){

       $url = "/api/countries/".$id;
       $param = $curlGen->delete($url);

      return redirect(url('dataStatis/country'));

  	}
}
