<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LookupController extends Controller
{
    public function customer(CurlGen $curlGen){
    	$url = "/api/nasabahs";

    	$param = $curlGen->getIndex($url);

    	return Datatables::of($param)->escapeColumns([])->make(true);
    }

    public function contract(CurlGen $curlGen){
    	$url = "/api/transaksi-gadai-efek-headers";

    	$param = $curlGen->getIndex($url);

    	return Datatables::of($param)->escapeColumns([])->make(true);
    }

    public function stock(CurlGen $curlGen){
      $url = "/api/gemefeks/S";

      $param = $curlGen->getIndex($url);

      return Datatables::of($param)->escapeColumns([])->make(true);
    }
}
