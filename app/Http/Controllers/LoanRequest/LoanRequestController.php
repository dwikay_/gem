<?php

namespace App\Http\Controllers\LoanRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LoanRequestController extends Controller
{

  public function kseiDoc(CurlGen $curlGen, Request $request){

    $url = "/api/pengajuan/kseidoc?docType=".$request->type."&noPengajuan=".$request->checks;

    if($request->type == 'xml'){
      $param = $curlGen->kseiDoc($url, $request->checks);
      echo $param->original;
    }else{
      $param = $curlGen->kseiDocCSv($url, $request->checks);
      return $param;
    }
  }

  public function kseiDocSingle(CurlGen $curlGen, $noPengajuan){

    $url = "/api/pengajuan/kseidoc?docType=".$request->type."&noPengajuan=".$noPengajuan;

    if($request->type == 'xml'){
      $param = $curlGen->kseiDoc($url, $request->checks);
      echo $param->original;
    }else{
      $param = $curlGen->kseiDocCSv($url, $request->checks);
      return $param;
    }
  }

  public function index(){
	    return view('transaction.loanrequest.index');
	}
	public function create(){
	    return view('transaction.loanrequest.create');
	}
	public function getIndex(CurlGen $curlGen, Request $request){

      	$url = '/api/pengajuan-gadai-efek-monitoring';

      	$LoanRequest = $curlGen->getIndex($url);


      	return Datatables::of($LoanRequest)->make(true);
  	}

  	public function edit(CurlGen $curlGen, $id){

      // return $id;
  		$kustodiUrl = "/api/bank-custodis";
    	$kustodi = $curlGen->getIndex($kustodiUrl);

      $pengajuanGadaiUrl = "/api/pengajuan-gadai-efek-monitoring-detail/".$id;
      $pengajuanGadai = $curlGen->getIndex($pengajuanGadaiUrl);

      // return $pengajuanGadai;

	    return view('transaction.loanrequest.updateCounter')
      ->with('pengajuanGadai',$pengajuanGadai)
      ->with('brokers',$kustodi);
	}
  public function terimaEfek(CurlGen $curlGen, $pengajuan){

    // return $id;
    $kustodiUrl = "/api/bank-custodis";
    $kustodi = $curlGen->getIndex($kustodiUrl);

    $pengajuanGadaiUrl = "/api/pengajuan-gadai-efek-monitoring-detail/".$pengajuan;
    $pengajuanGadai = $curlGen->getIndex($pengajuanGadaiUrl);

    // return $pengajuanGadai;

    return view('transaction.loanrequest.terimaEfek')
    ->with('pengajuanGadai',$pengajuanGadai)
    ->with('brokers',$kustodi);
}

  public function store(Request $request, CurlGen $curlGen, $noPengajuan){

      $url = "/api/pengajuan-gadai-efek-konfirmasi-pengajuan";
      $data = array(
        "counterPart" => $request->kustodi,
        "noPengajuan" => $noPengajuan,
        "settlementDate" => $request->tglEfekTerima
      );

      $param = $curlGen->update($url,$data);
      // return $param;
      return redirect(url('transaksi/loan-request'));
  }
  public function updatePengajuan(Request $request, CurlGen $curlGen, $noPengajuan){

      $url = "/api/pengajuan-gadai-efek-dari-custody";


      $data = array(
      "collaterals" => [],
      "noPengajuan" => $noPengajuan,
      );

      foreach ($request->kodeEfek as $keys => $value) {
                foreach ($request->settle as $key => $values) {
                  $data["collaterals"][$key] = array(
                      "kodeEfek"=> $request->kodeEfek[$key],
                      "qtyEfek"=> $request->settle[$key]
                    );
                }
              }
      $param = $curlGen->update($url,$data);
      // return $param;
      return redirect(url('transaksi/loan-request'));
  }
	public function getDetail(CurlGen $curlGen, $noPengajuan){

      	$LoanRequestDetailUrl = "/api/pengajuan-gadai-efek-monitoring-detail/".$noPengajuan;

      	$LoanRequestDetail = $curlGen->getIndex($LoanRequestDetailUrl);

        return $LoanRequestDetail;

  	}
}
