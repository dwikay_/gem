<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Akses;
use App\Model\Role;
use App\Model\Roleacl;
use App\Model\Module;
use Validator;
use Session;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserGroupController extends Controller
{
  public function index(){

    return view('userManagement.UserGroup.index');
  }
  public function getUsers(){

    $user = Role::all();
    return Datatables::of($user)->escapeColumns([])->make(true);
  }
  public function create(){

    $modules = Module::where('menu_parent', '0')->get();
    return view('userManagement.UserGroup.create')
    ->with('module', $modules);
  }
  public function edit($id){

      $role = Role::find($id);
      $module = Module::where('menu_parent', '0')->get();

      return view('userManagement.UserGroup.edit')
      ->with('role', $role)->with('module', $module);
  }
  public function store(Request $request){

      // return $request->all();

      $role_name = $request->input('role_name');
      $description = $request->input('description');

      $validator = Validator::make($request->all(), [
        'role_name' => 'required',
        'description' => 'required'
      ]);

      if($validator->fails()) {
        return redirect(url('usersGadai/userGroup/create'));
      }

      $role = Role::create([
        'role_name' => $role_name,
        'description' => $description,
        'akses' => "Pegadaian"
      ]);

      $roles = Module::where('menu_parent', '!=', 0)->get();



      foreach ($roles as $module) {
        Roleacl::create([
          'module_id' => $module->kdModule,
          'role_id' => $role->id,
          'create_acl' => $request->input($module->kdModule.'_create'),
          'read_acl' => $request->input($module->kdModule.'_read'),
          'update_acl' => $request->input($module->kdModule.'_update'),
          'delete_acl' => $request->input($module->kdModule.'_delete'),
          'module_parent' =>  $module->menu_parent,
        ]);
      }

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Berhasil disimpan');
      return redirect(url('usersGadai/userGroup'));
  }
  public function update($id, Request $r)
  {
    $role_name = $r->input('role_name');
    $description = $r->input('description');

    $role = Role::find($id)->update([
      'role_name' => $role_name,
      'description' => $description,
      'akses' => "Pegadaian"
    ]);

    $roles = Module::where('menu_parent', '!=', 0)->get();

    foreach ($roles as $modules) {
        $data = [
            'module_id' => $modules->kdModule,
            'role_id' => $id,
            'create_acl' => $r->input($modules->kdModule.'_create'),
            'read_acl' => $r->input($modules->kdModule.'_read'),
            'update_acl' => $r->input($modules->kdModule.'_update'),
            'delete_acl' => $r->input($modules->kdModule.'_delete'),
            'module_parent' => $modules->menu_parent,
        ];
        $cek = RoleAcl::where('module_id', $modules->kdModule)->where('role_id', $id)->first();
        if ($cek == null) {
            $rolenya = RoleAcl::create($data);
        } else {
            $rolenya = RoleAcl::find($cek->id);
            $rolenya->update($data);
        }
    }


    Session::flash('info', 'Success');
    Session::flash('colors', 'green');
    Session::flash('icons', 'fas fa-check-circle');
    Session::flash('alert', 'Berhasil disimpan');
    return redirect(url('usersGadai/userGroup'));

  }
  public function delete($id)
    {
      Role::where('id', $id)->delete();
      Roleacl::where('role_id', $id)->delete();

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Berhasil dihapus');
      return redirect(url('usersGadai/userGroup'));
    }
}
