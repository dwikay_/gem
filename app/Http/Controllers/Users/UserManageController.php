<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Akses;
use App\Model\Role;
use Session;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserManageController extends Controller
{
    public function viewPassword($user, $id){

        $user = User::findOrFail($id);

        return view('userManagement.change-password')
        ->with('user', $user);
    }
    public function updatePassword(Request $request, $id){

      // return $request->all();
      $user = User::find($id);
      $user->password = bcrypt($request->password);
      $user->save();

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Berhasil disimpan');
      return redirect()->back();
    }
    public function index(){
      $akses = Akses::all();
      return view('userManagement.UserManage.index')
      ->with('akses', $akses);
    }
    public function getUsers(){

      $user = User::with('role')->get();

      return Datatables::of($user)->escapeColumns([])->make(true);
    }
    public function create(){
      $role = Role::all();
      return view('userManagement.UserManage.create')
      ->with('role', $role);
    }
    public function store(Request $request){

      $user = new User();
      $user->name = $request->nama;
      $user->email = $request->email;
      $user->password = bcrypt($request->password);
      $user->status_user = $request->status_user;
      $user->role_id = $request->role;
      $user->save();

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Berhasil disimpan');

      return redirect(url('usersGadai/userManage'));
    }

    public function update(Request $request, $id){

      $user = User::find($id);

      $user->name = $request->nama;
      $user->email = $request->email;
      if($request->password!=""){
        $user->password = bcrypt($request->password);
      }
      $user->status_user = $request->status_user;
      $user->role_id = $request->role;
      $user->save();

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Berhasil disimpan');
      return redirect(url('usersGadai/userManage'));

    }

    public function edit($id){

      try {

      $user = User::with('role')->where('id',$id)->firstOrFail();
      $role = Role::all();
      return view('userManagement.UserManage.edit')
      ->with('user', $user)->with('role', $role);

      } catch (ModelNotFoundException $e) {
        Session::flash('info', 'Error');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-times');
        Session::flash('alert', 'User tidak ditemukan');
        return redirect(url('usersGadai/userManage'));
      }

    }

    public function checkingDelete($id){
      User::destroy($id);
      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Berhasil dihapus');
      return redirect(url('usersGadai/userManage'));
    }

    public function delete($id){
      User::destroy($id);
      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Berhasil dihapus');
      return redirect(url('usersGadai/userManage'));
    }

    public function disabled($ids){

      $cek_user = User::find($ids);
      $cek_user->limit_password = 3;
      $cek_user->status_user = 0;
      $cek_user->save();

      $info = "success";
      $colors = "green";
      $icons = "fas fa-check-circle";
      $alert = "User Enabled";

      return redirect(url('usersGadai/userManage'))
      ->with('info', $info)
      ->with('colors', $colors)
      ->with('icons', $icons)
      ->with('alert', $alert);
    }

    public function enabled($ids){

      $cek_user = User::find($ids);
      $cek_user->limit_password = 0;
      $cek_user->status_user = 1;
      $cek_user->save();

      $info = "success";
      $colors = "green";
      $icons = "fas fa-check-circle";
      $alert = "User Enabled";

      return redirect(url('usersGadai/userManage'))
      ->with('info', $info)
      ->with('colors', $colors)
      ->with('icons', $icons)
      ->with('alert', $alert);

    }

}
