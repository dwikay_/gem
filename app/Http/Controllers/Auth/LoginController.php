<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Auth;
use App\Model\Role;
use App\Model\Roleacl;
use Illuminate\Http\Request;
use Session;
use Artisan;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/monitoring/transaksi';
    protected $loginPath = 'login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function getLogin(){
		
	Artisan::call('get:token');
      return view('auth.login');
    }
    public function logout(){
      Auth::logout();
      return redirect(url('login'));
    }
    public function postLoginOld(Request $r){
      $this->validate($r, [
            'captcha' => 'required|captcha'
        ]);
        $username = $r->input('email');
        $password = $r->input('password');
        $remember = ($r->input('remember')) ? true : false;

        if (Auth::attempt(['email' => $username, 'password' => $password],$remember)) {
            if (Auth::viaRemember()) {
              return redirect()->intended('/monitoring/transaksi');
            }
            else{
              return redirect(url('/monitoring/transaksi'));
            }
        }
          // dd("ini");
          Session::flash('info', 'Error');
          Session::flash('colors', 'red');
          Session::flash('icons', 'fas fa-times');
          Session::flash('alert', 'Username/Password salah');
          return redirect()->back();

      }

      public function postLogin(Request $r){
        $username = $r->input('email');
        $password = $r->input('password');
        $remember = ($r->input('remember')) ? true : false;

        $cek_user = User::where('email', $username)->first();
        // return $cek_user;
        if($cek_user!=null){
           if($cek_user->limit_password==3 || $cek_user->limit_password=="3"){

            $alert = "Akun anda tidak bisa terpakai, silahkan hubungi administrator!";

          }

          else{

          if (Auth::attempt(['email' => $username, 'password' => $password])) {
            if($cek_user->limit_password==3 || $cek_user->limit_password=="3"){

              $alert = "Akun anda tidak bisa terpakai, silahkan hubungi administrator!";
              Auth::logout();

            }else{

              $cek_role = Roleacl::where('role_id', $cek_user->role_id)
              ->where('create_acl', null)
              ->where('read_acl', null)
              ->where('update_acl', null)
              ->where('delete_acl', null)
              ->get();

              if(count($cek_role)==0){
                $act = "Login";
                $desc = "Login Aplikasi";

                return redirect(url('/monitoring/transaksi'));

              } else if(count($cek_role)>0){
                $act = "Login";
                $desc = "Login Aplikasi";

                return redirect(url('/monitoring/transaksi'));
              }else{

                $alert = "You dont have any access, please contact your superadmin!";
                Auth::logout();

              }

            }

          }else{
            if($cek_user!=null){
              $cek_users = User::where('email', $username)->first();
              $cek_users->limit_password = $cek_user->limit_password+1;
              if($cek_user->limit_password==2){
                $cek_users->status_user = 0;
              }
              $cek_users->save();
              $alert = "Please Check Username / Password";
            }
          }
        }
      }else{
        $alert = "Please Check Username / Password";
      }
          // dd("ini");
          Session::flash('info', 'Error');
          Session::flash('colors', 'red');
          Session::flash('icons', 'fas fa-times');
          Session::flash('alert', $alert);
          return redirect()->back();

      }
}
