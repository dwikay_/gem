<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\User;
use Session;
use PDF;

class ReportController extends Controller
{
    public function indexRisk(){

      return view('report.index_risk');
    }

    public function riskParameterDownload(Request $request, CurlGen $curlGen){
      ini_set('memory_limit', '-1');
      ini_set('max_execution_time', 3600); //3 minutes

      if($request->tipeEfek=="all"){
        $te = "%25";
      }else if($request->tipeEfek=="saham"){
        $te = "S";
      }else{
        $te = "O";
      }

      $url = "/api/risk-parameter-report?tipeEfek=".$te;
      $param = $curlGen->getIndex($url);

      // return $param;

      if($request->tipeEfek=="all"){
        $pdf = PDF::loadview('report.riskParameterAll', ['tipeEfeks' => $param[0]['keterangan'], 'tipeEfeko' => $param[1]['keterangan'], 'efek'=> $param[0], 'obligasi' => $param[1]])->setPaper('a4', 'landscape');

        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();

        $canvas = $dom_pdf ->get_canvas();
        $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

        return $pdf->download('riskParameterAll.pdf');
      }else{
        // return $param[0];
        $pdf = PDF::loadview('report.riskParameter', ['tipeEfek' => $param[0]['keterangan'],'efek'=> $param[0]])->setPaper('a4', 'landscape');;

        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();

        $canvas = $dom_pdf ->get_canvas();
        $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));
        return $pdf->download('riskParameter'.$param[0]['keterangan'].'.pdf');
      }




    }
    public function riskParameterPreview(Request $request, CurlGen $curlGen){
      ini_set('memory_limit', '-1');
      ini_set('max_execution_time', 3600); //3 minutes

      if($request->tipeEfek=="all"){
        $te = "%25";
      }else if($request->tipeEfek=="saham"){
        $te = "S";
      }else{
        $te = "O";
      }

      $url = "/api/risk-parameter-report?tipeEfek=".$te;
      $param = $curlGen->getIndex($url);

      // return $param;

      if($request->tipeEfek=="all"){
        $pdf = PDF::loadview('report.riskParameterAll', ['tipeEfeks' => $param[0]['keterangan'], 'tipeEfeko' => $param[1]['keterangan'], 'efek'=> $param[0], 'obligasi' => $param[1]])->setPaper('a4', 'landscape');
        return $pdf->stream('riskParameter.pdf');
      }else{
        // return $param[0];
        if($param == false){
          $pdf = PDF::loadview('report.noDataRisk')->setPaper('a4', 'landscape');;

          $pdf->output();
          $dom_pdf = $pdf->getDomPDF();

          $canvas = $dom_pdf ->get_canvas();
          $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

          return $pdf->stream('stockLimit.pdf');
        }else{
          $pdf = PDF::loadview('report.riskParameter', ['tipeEfek' => $param[0]['keterangan'],'efek'=> $param[0]])->setPaper('a4', 'landscape');;
          return $pdf->stream('riskParameter.pdf');
        }
      }



    }
    public function indexStockLimit(CurlGen $curlGen){

      $url = "/api/gemefeks/S";
      $param = $curlGen->getIndex($url);
      // return $param;
      return view('report.index_limit')
      ->with('efek', $param);
    }

    public function stockLimitPreview(Request $request, CurlGen $curlGen){
      // return $request->all();
      ini_set('memory_limit', '-1');
      ini_set('max_execution_time', 3600); //3 minutes

      if($request->tipeEfek=="all"){
        $te = "%25";
      }else if($request->tipeEfek=="kodeEfek"){
        $te = $request->kodeEfek;
      }
        // return $te;
      $url = "/api/stock-concentration-limit-report?kodeEfek=".$te;
      $param = $curlGen->getIndex($url);

      // return $param;

      if($request->tipeEfek=="all"){
        $pdf = PDF::loadview('report.stockLimitAll', ['efek' => $param])->setPaper('a4', 'landscape');

        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();

        $canvas = $dom_pdf ->get_canvas();
        $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));


        return $pdf->stream('stockLimitAll.pdf');
      }else{
        // return $param;
        if($param == false){
          $pdf = PDF::loadview('report.noDataLimit')->setPaper('a4', 'landscape');;

          $pdf->output();
          $dom_pdf = $pdf->getDomPDF();

          $canvas = $dom_pdf ->get_canvas();
          $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

          return $pdf->stream('stockLimit.pdf');
        }else{
          $pdf = PDF::loadview('report.stockLimit', ['efek' => $param[0]])->setPaper('a4', 'landscape');;

          $pdf->output();
          $dom_pdf = $pdf->getDomPDF();

          $canvas = $dom_pdf ->get_canvas();
          $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

          return $pdf->stream('stockLimit-'.$te.'.pdf');


        }
      }

    }
    public function stockLimitDownload(Request $request, CurlGen $curlGen){
      // return $request->all();
      ini_set('memory_limit', '-1');
      ini_set('max_execution_time', 3600); //3 minutes

      if($request->tipeEfek=="all"){
        $te = "%25";
      }else if($request->tipeEfek=="kodeEfek"){
        $te = $request->kodeEfek;
      }

      $url = "/api/stock-concentration-limit-report?kodeEfek=".$te;
      $param = $curlGen->getIndex($url);

      // return $param;

      if($request->tipeEfek=="all"){
        $pdf = PDF::loadview('report.stockLimitAll', ['efek' => $param])->setPaper('a4', 'landscape');

        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();

        $canvas = $dom_pdf ->get_canvas();
        $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

        return $pdf->download('stockLimitAll.pdf');
      }else{
        // return $param[0];
        if($param == false){

          $info = "Error";
          $colors = "red";
          $icons = "fas fa-times";
          $alert = 'Data Kosong !';

          Session::flash('info', $info);
          Session::flash('colors', $colors);
          Session::flash('icons', $icons);
          Session::flash('alert', $alert);

          return back();
        }else{
          $pdf = PDF::loadview('report.stockLimit', ['efek' => $param[0]])->setPaper('a4', 'landscape');;

          $pdf->output();
          $dom_pdf = $pdf->getDomPDF();

          $canvas = $dom_pdf ->get_canvas();
          $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

          return $pdf->download('stockLimit-'.$te.'.pdf');
        }
      }

    }

    public function indexStockPosition(){
      return view('report.index_position');
    }
    public function stockPositionPreview(Request $request, CurlGen $curlGen){

      ini_set('memory_limit', '-1');
      ini_set('max_execution_time', 3600); //3 minutes

      $url = "/api/stock-mutation-report-all";
      $param = $curlGen->getIndex($url);

      // return $param;
      if($param == false){
          $pdf = PDF::loadview('report.noDataPosition')->setPaper('a4', 'landscape');;

          $pdf->output();
          $dom_pdf = $pdf->getDomPDF();

          $canvas = $dom_pdf ->get_canvas();
          $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

          return $pdf->stream('stockLimit.pdf');
        }else{

          $pdf = PDF::loadview('report.stockPosition', [
          'data'    => $param])->setPaper('a4', 'landscape');

          $pdf->output();
          $dom_pdf = $pdf->getDomPDF();

          $canvas = $dom_pdf ->get_canvas();
          $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

          return $pdf->stream('stockPosition.pdf');

        }


    }
    public function stockPositionDownload(Request $request, CurlGen $curlGen){

      ini_set('memory_limit', '-1');
      ini_set('max_execution_time', 3600); //3 minutes

      $url = "/api/stock-position-report";
      $param = $curlGen->getIndex($url);

      // return $param[0]['subRek'];

      $pdf = PDF::loadview('report.stockPosition', [
      'data'    => $param])->setPaper('a4', 'landscape');

      $pdf->output();
      $dom_pdf = $pdf->getDomPDF();

      $canvas = $dom_pdf ->get_canvas();
      $canvas->page_text(760, 550, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

      return $pdf->download('stockPosition.pdf');

    }
}
