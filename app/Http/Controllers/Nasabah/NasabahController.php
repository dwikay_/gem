<?php

namespace App\Http\Controllers\Nasabah;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use Session;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class NasabahController extends Controller
{
  public function index(){
    return view('static_data.nasabah.index');
  }

  public function kseiDoc(CurlGen $curlGen,Request $request){
    
    $urlNasabah = "/api/nasabahs/individu/".$request->checks;
    $getNasabah = $curlGen->getIndex($urlNasabah);

    $action = "";

    if($getNasabah['nasabah']['bankSubRek'] == "")
    {
      $action = "A";
    }else{
      $action = "M";
    }

    $url = "/api/nasabahs/kseidoc?action=".$action."&docType=".$request->type."&kodeNasabah=".$request->checks;
    // $url = "/api/nasabahs/kseidoc?action=A&docType=xml&kodeNasabah=ID0000001";

    if($request->type == "xml")
    {      
      $param = $curlGen->kseiDoc($url, $request->checks);
      echo $param->original;
    }else if($request->type == "csv"){
      $param = $curlGen->kseiDocCsv($url, $request->checks);
      echo $param;
    }

  }
  public function getIndex(CurlGen $curlGen, Request $request){

    date_default_timezone_set("Asia/Jakarta");
    $url = "/api/nasabahs";

    $param = $curlGen->getIndex($url);

    return Datatables::of($param)->escapeColumns([])->make(true);
  }
  //
  // public function editSubrRek(CurlGen $curlGen, $id){
  //
  //     $url = "/api/nasabahs/".$id;
  //
  //     $param = $curlGen->edit($url);
  //
  //     return $param;
  //     return view('static_data.nasabah.subRek')
  //     ->with('subrek', $param[1]);
  //     }
  public function reqSubrek(CurlGen $curlGen, $kodeNasabah, $tipeNasabah){

      if($tipeNasabah=="R"){
        $cek_nasabah = "/api/nasabahs/individu/".$kodeNasabah;
      }else{
        $cek_nasabah = "/api/nasabahs/insti/".$kodeNasabah;
      }

      $nasabah = $curlGen->getIndex($cek_nasabah);

      if($nasabah['nasabah']['status']=="D"){
        Session::flash('info', 'Peringatan');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-exclamation-triangle');
        Session::flash('alert', 'Mohon lengkapi data nasabah tersebut!');
        return redirect(url('dataStatis/nasabah/edit/'.$nasabah['nasabah']['tipeNasabah'].'/'
        .$kodeNasabah.'/'.$nasabah['nasabah']['id'])); //{tipeNasabah}/{kodeNasabah}/{id}
      }else{
        return view('static_data.nasabah.subRek2')
        ->with('kodeNasabah', $kodeNasabah);
      }


  }
  public function reqSubrek2(CurlGen $curlGen, $kodeNasabah, $tipeNasabah){

      if($tipeNasabah=="R"){
        $cek_nasabah = "/api/nasabahs/individu/".$kodeNasabah;
      }else{
        $cek_nasabah = "/api/nasabahs/insti/".$kodeNasabah;
      }

      $nasabah = $curlGen->getIndex($cek_nasabah);

      if($nasabah['nasabah']['status']=="D"){
        Session::flash('info', 'Peringatan');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-exclamation-triangle');
        Session::flash('alert', 'Mohon lengkapi data nasabah tersebut!');
        return redirect(url('dataStatis/nasabah/edit/'.$nasabah['nasabah']['tipeNasabah'].'/'
        .$kodeNasabah.'/'.$nasabah['nasabah']['id'])); //{tipeNasabah}/{kodeNasabah}/{id}
      }else{
        return view('static_data.nasabah.subRek')
        ->with('subrek', $nasabah['nasabah']);
      }


  }
  public function reqSubrekView(CurlGen $curlGen, $kodeNasabah, $tipeNasabah, $kdNasabah){

    if($tipeNasabah=="R"){
      $url = "/api/nasabahs/individu/".$kodeNasabah;
    }else{
      $url = "/api/nasabahs/insti/".$kodeNasabah;
    }

    $url_city = "/api/cities";
    $url_country = "/api/countries";
    $url_tax = "/api/tax-ids";
    $url_provinces = "/api/provinces";

    $Cities = $curlGen->getIndex($url_city);
    $Countries = $curlGen->getIndex($url_country);
    $Taxid = $curlGen->getIndex($url_tax);
    $Provinces = $curlGen->getIndex($url_provinces);

    $param = $curlGen->edit($url);

    $fund = explode(",",$param[1]['investorFundSource']);
    $obj = explode(",",$param[1]['investorInvestmentObjective']);
    $id = $param[1]['nasabah']['id'];
    // return $obj;
    if($param[0]!=200){
      return redirect(url('dataStatis/nasabah'))
      ->with('alert','Kode Nasabah tidak ditemukan')->with('info');
    }else{
      if($tipeNasabah=="R"){
        // return $param[1];
        return view('static_data.nasabah.formIndividuR')
        ->with('nasabah', $param[1])
        ->with('tipeNasabah', $tipeNasabah)
        ->with('idNasabah', $id)
        ->with('Cities', $Cities)
        ->with('Countries',$Countries)
        ->with('taxid', $Taxid)
        ->with('Provinces',$Provinces)
        ->with('fund', $fund)
        ->with('obj', $obj);
      }else{
        return view('static_data.nasabah.formInstitusiR ')
        ->with('nasabah', $param[1])
        ->with('tipeNasabah', $tipeNasabah)
        ->with('idNasabah', $id)
        ->with('Cities', $Cities)
        ->with('Countries',$Countries)
        ->with('taxid', $Taxid)
        ->with('Provinces',$Provinces)
        ->with('fund', $fund)
        ->with('obj', $obj);
      }
    }
  }

  public function updateSubRek(CurlGen $curlGen, Request $request, $kodeNasabah){
        $url = "/api/sendsubrek/";
        $data = array(
            // 'id' => $id,
            'kodeNasabah' => $kodeNasabah,
            'subRek' => $request->subRek,
            // 'sid' => $sid
          );

        $param =  $curlGen->update($url,$data);
        // return $param;
        return redirect(url('dataStatis/nasabah'));
  }
  public function edit(CurlGen $curlGen, $tipeNasabah, $kodeNasabah, $id){

    if($tipeNasabah=="R"){
      $url = "/api/nasabahs/individu/".$kodeNasabah;
    }else{
      $url = "/api/nasabahs/insti/".$kodeNasabah;
    }

    $url_city = "/api/cities";
    $url_country = "/api/countries";
    $url_tax = "/api/tax-ids";
    $url_provinces = "/api/provinces";

    $Cities = $curlGen->getIndex($url_city);
    $Countries = $curlGen->getIndex($url_country);
    $Taxid = $curlGen->getIndex($url_tax);
    $Provinces = $curlGen->getIndex($url_provinces);
//return $Countries;
    $param = $curlGen->edit($url);

    $fund = explode(",",$param[1]['investorFundSource']);
    $obj = explode(",",$param[1]['investorInvestmentObjective']);

    // return $obj;
    if($param[0]!=200){
      return redirect(url('dataStatis/nasabah'))
      ->with('alert','Kode Nasabah tidak ditemukan')->with('info');
    }else{
      if($tipeNasabah=="R"){
        // return $param[1];
        return view('static_data.nasabah.formIndividu')
        ->with('nasabah', $param[1])
        ->with('tipeNasabah', $tipeNasabah)
        ->with('idNasabah', $id)
        ->with('Cities', $Cities)
        ->with('Countries',$Countries)
        ->with('taxid', $Taxid)
        ->with('Provinces',$Provinces)
        ->with('fund', $fund)
        ->with('obj', $obj);
      }else{
       // return $param[1];
        return view('static_data.nasabah.formInstitusi')
        ->with('nasabah', $param[1])
        ->with('tipeNasabah', $tipeNasabah)
        ->with('idNasabah', $id)
        ->with('Cities', $Cities)
        ->with('Countries',$Countries)
        ->with('taxid', $Taxid)
        ->with('Provinces',$Provinces)
        ->with('fund', $fund)
        ->with('obj', $obj);
      }
    }
  }

  public function individu(){
    return view('static_data.nasabah.formIndividu');
  }
  public function institusi(){
    return view('static_data.nasabah.formInstitusi');
  }
  public function update(CurlGen $curlGen, Request $request, $id, $tipe, $idNasabah){

    //C corporate ; R individu
    // return $request->INV_CLIENT_TYPE;
    $urlNasabah = "/api/nasabahs";

    if($tipe=="R"){

      $url = "/api/nasabah-individus";

      if($request->INV_CLIENT_TYPE=="DIRECT"){
        $fund1 = implode($request->SOURCEFUND,',');
        $obj = implode($request->INV_OBJECTIVE,',');
      }else{
        if($request->SOURCEFUND==""){
          $fund1 = "";
        }else{
          $fund1 =  implode($request->SOURCEFUND,',');
        }
        if($request->INV_OBJECTIVE==""){
          $obj = "";
        }else{
          $obj = implode($request->INV_OBJECTIVE,',');
        }
      }
       //return $obj;
      $data = array(
          'actSave' => "C",
          'id' => $id,
          'investorType' => $request->INV_TYPE,
          'investorCategory' => $request->INV_CLIENT_TYPE,
          'memberCode' => '00000',
          'clientCode' => $request->ACCCODE,
          'investorFirstName' => $request->FIRSTNAME,
          'investorMiddleName' => $request->MIDDLENAME,
          'investorLastName' => $request->LASTNAME,
          'investorNationality' => $request->NATIONALITY,
          'investorKTPNumber' => $request->KTP,
          'investorKTPExpiredDate' => $request->KTP_EXPDATE,
          'investorNPWPNumber' => $request->NPWP,
          'investorNPWPRegistrationDate' => $request->NPWP_REGDATE,
          'investorPassportNumber' => $request->PASSPORT,
          'investorPassportExpiredDate' => $request->PASSPORT_EXPDATE,
          'investorKitasSKDNumber' => null,
          'investorKitasSKDExpiredDate' => null,
          'investorBirthPlace' => $request->BIRTH_PLACE,
          'investorBirthDate' => $request->BIRTH_DATE,
          'investorAddress1' => $request->ADDRESS1,
          'investorAddress2' => $request->ADDRESS2,
          'investorAddress3' => $request->ADDRESS3,
          'investorCity' => $request->investorCity,
          'investorProvince' => $request->investorProvince,
          'investorPostalCode' => $request->ZIPCODE,
          'investorCountry' => $request->COUNTCODE,
          'investorHomePhone' => $request->PHONE,
          'investorMobilePhone' => $request->MOBILEPHONE,
          'investorEmail' => $request->EMAIL,
          'investorFax' => $request->FAX,
          'secAddress1Line1' => $request->secAddress1Line1,
          'secAddress2Line2' => $request->secAddress2Line2,
          'secAddress3Line3' => $request->secAddress3Line3,
          'secCity' => $request->secCity,
          'secProvince' => $request->secProvince,
          'secPostalCode' => $request->secPostalCode,
          'secCountry' => $request->secCountry,
          'secPhone' => $request->secPhone,
          'secMobilePhone' => $request->secMobilePhone,
          'secEmailAddress' => $request->secEmailAddress,
          'secFaxNumber' => $request->secFaxNumber,
          'investorSex' => $request->SEX,
          'investorMaritalStatus' => $request->MARITAL_STATUS,
          'investorSpouseName' => $request->SPOUSE_NAME,
          'investorHeirName' => $request->HEIR_NAME,
          'investorHeirRelation' => $request->HEIR_RELATION,
          'investorEducationalBackground' => $request->EDUCATION_BACKGROUND,
          'investorOccupation' => $request->OCCUPATION,
          'investorNatureofBusiness' => $request->NATURE_OF_BUSINESS,
          'investorIncomePerAnnum' => $request->ANNUAL_INCOME,
          'investorFundSource' => $fund1,
          'accountDescription' => $request->DESCRIPTION,
          'investorBankAccountName1' => $request->investorBankAccountName1,
          'investorBankAccountNumber1' => $request->investorBankAccountNumber1,
          'investorBankAccountBICCode1' => $request->investorBankAccountBICCode1,
          'investorBankAccountHolderName1' => $request->investorBankAccountHolderName1,
          'investorBankAccountCurrency1' => $request->investorBankAccountCurrency1,
          'investorBankAccountName2' => $request->investorBankAccountName2,
          'investorBankAccountNumber2' => $request->investorBankAccountNumber2,
          'investorBankAccountBICCode2' => $request->investorBankAccountBICCode2,
          'investorBankAccountHolderName2' => $request->investorBankAccountHolderName2,
          'investorBankAccountCurrency2' => $request->investorBankAccountCurrency2,
          'investorBankAccountName3' => $request->investorBankAccountName3,
          'investorBankAccountNumber3' => $request->investorBankAccountNumber3,
          'investorBankAccountBICCode3' => $request->investorBankAccountBICCode3,
          'investorBankAccountHolderName3' => $request->investorBankAccountHolderName3,
          'investorBankAccountCurrency3' => $request->investorBankAccountCurrency3,
          'investorInvestmentObjective' => $obj,
          'investorMothersMaidenName' => $request->MOTHER_NAME,
          'directSid' => $request->DIRECT_SID,
          'assetOwner' => $request->ASSET_OWNER,
          'accountTaxCode' => $request->TAXID,
          'investorOtherAddress1' => $request->investorOtherAddress1,
          'investorOtherAddress2' => $request->investorOtherAddress2,
          'investorOtherAddress3' => $request->investorOtherAddress3,
          'investorOtherCity' => $request->investorOtherCity,
          'investorOtherProvince' => $request->investorOtherProvince,
          'investorOtherPostalCode' => $request->investorOtherPostalCode,
          'investorOtherCountry' => $request->investorOtherCountry,
          'investorOtherHomePhone' => $request->investorOtherHomePhone,
          'investorOtherMobilePhone' => $request->investorOtherMobilePhone,
          'investorOtherEmail' => $request->investorOtherEmail,
          'investorOtherFax' => $request->investorOtherFax,
          'investorAuthorizedPersonKTPRegistrationDate2' => $request->KTP_REG,
          'nasabah' => array(
              'id' => $idNasabah,
          )
        );
        $urlNasabah1 = "/api/nasabahs/".$idNasabah;
        $getNasabah = $curlGen->getIndex($urlNasabah1);
        // return $getNasabah;
        $nasabah = array(
          'id' => $idNasabah,
          'kodeNasabah' => $request->ACCCODE,
          'namaNasabah' => $request->FIRSTNAME." ".$request->MIDDLENAME." ".$request->LASTNAME,
          'tipeNasabah' => "R",
          'sid' => $request->SID,
          'cif' => $getNasabah['cif'],
          'bankSubRek' => $getNasabah['bankSubRek'],
          'statusSubRek' => $getNasabah['statusSubRek'],
          'status' => "A"
        );
        // return $nasabah;
    }else{

      $url = "/api/nasabah-instis";

	$url = "/api/nasabah-instis";

      if($request->INV_CLIENT_TYPE=="DIRECT"){
        $fund1 = implode($request->investorFundSource,',');
        $obj = implode($request->INV_OBJECTIVE,',');
      }else{
        if($request->investorFundSource==""){
          $fund1 = "";
        }else{
          $fund1 =  implode($request->investorFundSource,',');
        }
        if($request->INV_OBJECTIVE==""){
          $obj = "";
        }else{
          $obj = implode($request->INV_OBJECTIVE,',');
        }
      }

      $data = array(
          'actSave' => "C", //done
          'id' => $id, //done
          'investorType' => $request->INV_TYPE, //done
          'investorCategory' => $request->INV_CLIENT_TYPE, //done
          'investorMemberCode' => '00000', //done
          'accountClientCode' => $request->ACCCODE, //done
          'investorCompanyName' => $request->investorCompanyName, //done
          'investorCompanyBICCode' => $request->BICCODE, //done
          'investorTaxID' => $request->TAXID, //done
          'investorLegalDomicile' => $request->LEGALDOMICILE, //done
          'investorNPWPNumber' => $request->NPWP, //done
          'investorNPWPRegistrationDate' => $request->NPWP_REGDATE, //done
          'investorSKDNumber' => $request->investorSKDNumber, //done
          'investorSKDExpiredDate' => $request->investorSKDExpiredDate, //done
          'investorCompanyEstablishmentPlace' => $request->investorCompanyEstablishmentPlace, //done
          'investorCompanyEstablishmentDate' => $request->investorCompanyEstablishmentDate, //done
          'investorAddress1' => $request->ADDRESS1, //done
          'investorAddress2' => $request->ADDRESS2, //done
          'investorAddress3' => $request->ADDRESS3, //done
          'investorCity' => $request->CITY, //done
          'investorProvince' => $request->PROVCODE, //done
          'investorPostalCode' => $request->ZIPCODE, //done
          'investorCountry' => $request->COUNTCODE, //done
          'investorHomePhone' => $request->PHONE, //done
          'investorMobilePhone' => $request->MOBILEPHONE, //done
          'investorEmail' => $request->EMAIL, //done
          'investorFax' => $request->FAX, //done
          'investorBusinessType' => $request->BUSINESS_TYPE, //done
          'investorCompanyCharacteristic' => $request->COMPANY_CHARACTERISTIC, //done
          'investorFundSource' => $fund1, //done
          'investorArticleOfAssociation' => $request->investorArticleOfAssociation,  //done
          'investorBusinessRegistrationCertificateNumber' => $request->investorBusinessRegistrationCertificateNumber,  //done
          'investorAsset1' => $request->investorAsset1,  //done
          'investorAsset2' => $request->investorAsset2,   //done
          'investorAsset3' => $request->INVESTOR_ASSET_3, //done
          'investorOperatingProfit1' => $request->investorOperatingProfit1,  //done
          'investorOperatingProfit2' => $request->INVESTOR_OPERATING_PROFIT_2,  //done
          'investorOperatingProfit3' => $request->INVESTOR_OPERATING_PROFIT_3,  //done
          'accountDescription' => $request->accountDescription,  //done
          'investorBankAccountName1' => $request->investorBankAccountName1,
          'investorBankAccountNumber1' => $request->investorBankAccountNumber1,
          'investorBankAccountBICCode1' => $request->investorBankAccountBICCode1,
          'investorBankAccountHolderName1' => $request->investorBankAccountHolderName1,
          'investorBankAccountCurrency1' => $request->investorBankAccountCurrency1,
          'investorBankAccountName2' => $request->investorBankAccountName2,
          'investorBankAccountNumber2' => $request->investorBankAccountNumber2,
          'investorBankAccountBICCode2' => $request->investorBankAccountBICCode2,
          'investorBankAccountHolderName2' => $request->investorBankAccountHolderName2,
          'investorBankAccountCurrency2' => $request->investorBankAccountCurrency2,
          'investorBankAccountName3' => $request->investorBankAccountName3,
          'investorBankAccountNumber3' => $request->investorBankAccountNumber3,
          'investorBankAccountBICCode3' => $request->investorBankAccountBICCode3,
          'investorBankAccountHolderName3' => $request->investorBankAccountHolderName3,
          'investorBankAccountCurrency3' => $request->investorBankAccountCurrency3,
          'investorInvestmentObjective' => $obj,
          'directSid' => $request->SID, //done
          'assetOwner' => $request->assetOwner, //done
          'investorTypeOfSupplementaryDocs' => $request->investorTypeOfSupplementaryDocs, //done
          'investorExpiredDateOfSupplementaryDocs' => $request->investorExpiredDateOfSupplementaryDocs, //done
          'accountTaxCode' => $request->TAXID, //done
          'investorOtherAddress1' => $request->investorOtherAddress1, //done
          'investorOtherAddress2' => $request->investorOtherAddress2, //done
          'investorOtherAddress3' => $request->investorOtherAddress3, //done
          'investorOtherCity' => $request->OTHERCITY, //done
          'investorOtherProvince' => $request->investorOtherProvince, //done
          'investorOtherPostalCode' => $request->investorOtherPostalCode, //done
          'investorOtherCountry' => $request->investorOtherCountry, //done
          'investorOtherHomePhone' => $request->investorOtherHomePhone, //done
          'investorOtherMobilePhone' => $request->investorOtherMobilePhone, //done
          'investorOtherEmail' => $request->investorOtherEmail, //done
          'investorOtherFax' => $request->investorOtherFax, //done
          'investorAuthorizedPersonKTPRegistrationDate2' => $request->investorAuthorizedPersonKTPRegistrationDate2,
          'nasabah' => array(
            'id' => $idNasabah //done
          )
      );

      $urlNasabah1 = "/api/nasabahs/".$idNasabah;
        $getNasabah = $curlGen->getIndex($urlNasabah1);
        // return $getNasabah;
        $nasabah = array(
          'id' => $idNasabah,
          'kodeNasabah' => $request->ACCCODE,
          'namaNasabah' => $request->investorCompanyName,
          'tipeNasabah' => "C",
          'sid' => $request->SID,
          'cif' => $getNasabah['cif'],
          'bankSubRek' => $getNasabah['bankSubRek'],
          'statusSubRek' => $getNasabah['statusSubRek'],
          'status' => "A"
        );

    }
      //return $data;
      $param = $curlGen->update($url,$data);
//return $param;
      $updateNasabah = $curlGen->update($urlNasabah,$nasabah);
      // return $updateNasabah;
      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Berhasil disimpan');
      return redirect(url('dataStatis/nasabah'));

  }
}
