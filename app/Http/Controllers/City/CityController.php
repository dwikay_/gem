<?php

namespace App\Http\Controllers\City;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Token;
use App\Model\User;
use yajra\Datatables\Datatables;
use App\Library\CurlGen;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CityController extends Controller
{
    public function index(){
	    return view('static_data.city.index');
	}
	public function create(CurlGen $curlGen){
	    $url = "/api/provinces";

	    $param = $curlGen->getIndex($url);

	    return view('static_data.city.create')->with('provinces',$param);
	}

	public function getIndex(CurlGen $curlGen, Request $request){

    $url = "/api/cities";

    $param = $curlGen->getIndex($url);

    return Datatables::of($param)->escapeColumns([])->make(true);

  	}

  	public function store(CurlGen $curlGen, Request $request){
	    $url = "/api/cities";
	    $data = array(
	        'cityCode' => $request->cityCode,
	        'cityName' => $request->cityName,
	        'pegadaianCode' => $request->pegadaianCode,
	        'province'=>array(
	        	'id'=>$request->province
	        )
	    );

	    $param = $curlGen->store($url, $data);

	    return redirect(url('dataStatis/city'));
  	}

  	public function edit(CurlGen $curlGen , $id){
  		$url = "/api/provinces";

	    $provinces = $curlGen->getIndex($url);


	    $url = "/api/cities/".$id;

	    $param = $curlGen->edit($url);

	    return view('static_data.city.edit')
	    ->with('city', $param[1])->with('provinces',$provinces);
  	}

  	public function update(CurlGen $curlGen, Request $request, $id){
	    $url = "/api/cities";
	    $data = array(
	        'id' => $id,
	        'cityCode' => $request->cityCode,
	        'cityName' => $request->cityName,
	        'pegadaianCode' => $request->pegadaianCode,
	        'province'=>array(
	        	'id'=>$request->province
	        )
	      );

	    $param =  $curlGen->update($url,$data);

	    return redirect(url('dataStatis/city'));
  	}

  	public function delete(CurlGen $curlGen, $id){

       $url = "/api/cities/".$id;
       $param = $curlGen->delete($url);

      return redirect(url('dataStatis/city'));

  	}
}
