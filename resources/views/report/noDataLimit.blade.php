<!DOCTYPE html>
<html>
<head>

    <style type="text/css">
        table, td {
            border: 0px solid black;
            border-collapse: collapse;
            font-size:9pt;
        }
        th {
            background: #d8d8d8;
            border-top: 1px solid black;
            border-collapse: collapse;
            font-size:10pt;
        }
        th {
            border-bottom: 1px solid;
            vertical-align: middle;
        }
        p {
            margin-top: auto;
            margin-left: 3%;
            font-size:10pt;
            position: absolute;
        }
    </style>

    <title>Stock Concentration Limit</title>
</head>

<body class="metro">
<table width="100%" style="margin-top:3%;">
    <thead>
    <tr>
      <th align="left" width="10%">No</th>
      <th align="left" width="30%">Contract Code</th>
      <th align="left" width="30%">SID</th>
      <th align="left" width="45%">Nama Nasabah</th>
      <th align="left" width="30%">Subrek</th>
      <th align="center" width="25%">Tipe Nasabah</th>
      <th align="center" width="25%">Qty On Hand</th>
      <th align="center" width="25%">Qty Pengajuan</th>
      <th align="center" width="25%">Qty Top Up</th>
      <th align="center" width="25%">Qty Total</th>
    </tr>
    </thead>
    <tbody>      
        <tr>
          <td colspan="10" style="text-align: center">No Data</td>
        </tr>      
    </tbody>
</table>
<br><br>


</body>
</html>
