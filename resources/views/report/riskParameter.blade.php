<!DOCTYPE html>
<html>
<head>

    <style type="text/css">
        table, td {
            border: 0px solid black;
            border-collapse: collapse;
            font-size:9pt;
        }
        th {
            background: #d8d8d8;
            border-top: 1px solid black;
            border-collapse: collapse;
            font-size:10pt;
        }
        th {
            border-bottom: 1px solid;
            vertical-align: middle;
        }
        p {
            margin-top: auto;
            margin-left: 3%;
            font-size:10pt;
            position: absolute;
        }
    </style>

    <title>Risk Parameter</title>
</head>

<body class="metro">
<header>
    <h2 class=""><center>Risk Parameter</center></h2>
    <table style="">
        <tr style="font-size: 5pt">
            <td>Tipe Efek</td>
            <td>:</td>
            <td>{{$tipeEfek}}</td>
        </tr>
    </table>
</header><br>
<table width="100%" style="">
    <thead>
    <tr>
        <th align="left" width="10%">No</th>
        <th align="left" width="15%">Kode Efek</th>
        <th align="left" width="45%">Nama Efek</th>
        <th align="right" width="30%">Jumah Efek Beredar</th>
        <th align="center" width="20%">BMPK</th>
        <th align="right" width="25%">Maximum Agunan</th>
        <th align="right" width="25%">Used Quantity</th>
        <th align="right" width="25%">Available Quantity</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 1;
    ?>
    @foreach($efek['stockRiskParameterList'] as $efeks)
        <tr>
            <td>{{$i++}}</td>
            <td>{{$efeks['kodeEfek']}}</td>
            <td>{{$efeks['namaEfek']}}</td>
            <td align="right">{!! number_format($efeks['jlhEfekBeredar'] ,0,",",".") !!}</td>
            <td align="center">{{$efeks['bmpk']}}</td>
            <td align="right">{!! number_format($efeks['maxLoanQty'] ,0,",",".") !!}</td>
            <td align="right">{!! number_format($efeks['usedLoanQty'] ,0,",",".") !!}</td>
            <td align="right">{!! number_format($efeks['availableQty'] ,0,",",".") !!}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>
<br>

</body>
</html>
