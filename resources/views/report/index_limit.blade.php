@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
      <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Laporan</h4>
              <p class="card-description">
                Stock Limit
              </p>
              <form id="fpro" class="forms-sample">
                <div class="row">
                  <div class="col-3">
                    <div class="form-group">
                      <label for="exampleInputEmail1"></label>
                      <select class="form-control" id="tipeEfek" name="tipeEfek">
                        <option value="all">Semua Tipe</option>
                        <option value="kodeEfek">Kode Efek</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1"></label>
                      <select style="display:none" class="form-control" placeholder="Kode Efek" id="kodeEfek" name="kodeEfek">
                        <option value="" disabled selected></option>
                        @foreach($efek as $efeks)
                        <option value="{{$efeks['kodeEfek']}}">{{$efeks['kodeEfek']}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3 mt-4">
                    <a class="btn btn-sm btn-success btn-preview" style="color:white"><span class="fas fa-search"></span>&nbsp;&nbsp;Preview</a>
                    <a class="btn btn-sm btn-primary btn-download" style="color:white"><span class="fas fa-download"></span>&nbsp;&nbsp;Download</a>
                  </div>
                </div>
              </form><br>
              <div id="POLimit"></div>
              <div class="preloader" id="preloader">
                <div class="loading">
                  <img src="{{asset('img/loading.gif')}}" width="80">
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
  var base_url = window.location.origin;
    $('#preloader').hide();

  function viewLoading(){
      $('.preloader').fadeIn();
    }

  function hideLoading(){
      $(".preloader").fadeOut();
    }


  $('#tipeEfek').on('change', function(){

    if($('#tipeEfek').val()=="kodeEfek"){
      $('#kodeEfek').css('display','block');
    }else{
      $('#kodeEfek').css('display','none');
    }
  });

  $('.btn-preview').on('click', function(){

    var tipeEfek = $('#tipeEfek').val();
    var kodeEfek = $('#kodeEfek').val();
    $('#POLimit').attr('style', 'height:70vh');

    if($('#tipeEfek').val()=="kodeEfek"){
      var urls = base_url+ "/index.php" +'/laporan/stockLimit-generate-preview?tipeEfek='+ tipeEfek +'&kodeEfek='+ kodeEfek;
    }else{
      var urls = base_url+ "/index.php" +'/laporan/stockLimit-generate-preview?tipeEfek='+ tipeEfek +'&kodeEfek=%25';
    }

                console.log(urls);
                // console.log('url');

                var url = urls;
                var options = {
                  pdfOpenParams: {
                      zoom: 75
                  }
                };

                viewLoading();
                PDFObject.embed(url, "#POLimit",options);
                hideLoading();

    // $.ajax({
    //         type:'get',
    //         url: urls,
    //         data: {},
    //         dataType: "json",
    //         success:function(data){

    //           console.log(data);
    //             // if(data[0]['hasil']=="gagal"){

    //             //   console.log(urls);
    //             // } else {
    //             console.log(urls);
    //             var url = urls;
    //             console.log('url');
    //             var options = {
    //               pdfOpenParams: {
    //                   zoom: 75
    //               }
    //             };

    //             viewLoading();
    //             PDFObject.embed(url, "#POLimit",options);
    //             hideLoading();
    //         // }


    //         }
    //     });
  });

  $('.btn-download').on('click', function(){
    $('#fpro').attr('action','{{url('laporan/stockLimit-generate-download')}}');
    $('#fpro').attr('method','get');
    $('#fpro').submit();
  });

</script>
@endsection
