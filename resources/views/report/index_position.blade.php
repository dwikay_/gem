@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
      <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Laporan</h4>
              <div class="row">
                <div class="col-2 mt-2">
                    <p class="card-description">
                      Stock Position
                    </p>
                </div>
                <div class="col-4">
                  <form id="fpro" class="forms-sample">
                    <div class="col-md-12 mt-10 text-right">
                      <a class="btn btn-sm btn-success btn-preview" style="color:white"><span class="fas fa-search"></span>&nbsp;&nbsp;Preview</a>
                      <a class="btn btn-sm btn-primary btn-download" style="color:white"><span class="fas fa-download"></span>&nbsp;&nbsp;Download</a>
                    </div>
                  </form>
                </div>
              </div>
              <div class="preloader">
                <div class="loading">
                  <img src="{{asset('img/loading.gif')}}" width="80">
                </div>
              </div>
              <div id="POPosition"></div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>

  var base_url = window.location.origin;
  $('.preloader').hide();

  function viewLoading(){
    $('.preloader').fadeIn();
  }

  function hideLoading(){
    $(".preloader").fadeOut();
  }

$('.btn-preview').on('click', function(){
  // $('#fpro').attr('action','{{url('report/stockPosition-generate-preview')}}');
  // $('#fpro').attr('method','get');
  // $('#fpro').submit();

  $('#POPosition').attr('style', 'height:500px');
  var url = base_url+ "/index.php" +'/laporan/stockPosition-generate-preview?';
  var options = {
    pdfOpenParams: {
        zoom: 75
    }
  };

  viewLoading();
  PDFObject.embed(url, "#POPosition",options);
  hideLoading();
});

$('.btn-download').on('click', function(){
  $('#fpro').attr('action','{{url('laporan/stockPosition-generate-download')}}');
  $('#fpro').attr('method','get');
  $('#fpro').submit();
});

</script>
@endsection
