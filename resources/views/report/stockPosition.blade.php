<!DOCTYPE html>
<html>
<head>

    <style type="text/css">
        table, td {
            border: 0px solid black;
            border-collapse: collapse;
            font-size:9pt;
        }
        th {
            background: #d8d8d8;
            border-top: 1px solid black;
            border-collapse: collapse;
            font-size:10pt;
        }
        th {
            border-bottom: 1px solid;
            vertical-align: middle;
        }
        p {
            margin-top: auto;
            margin-left: 3%;
            font-size:10pt;
            position: absolute;
        }
    </style>

    <title>Stock Position</title>
</head>

<body class="metro">
  <header>
      <h2 class=""><center>Stock Position</center></h2>
      @foreach($data as $datas)
      <table class="col-md-6">
        <tr style="font-size: 5pt">
            <td>Name</td>
            <td>:</td>
            <td>{{$datas['namaNasabah']}}</td>
        </tr>
          <tr style="font-size: 5pt">
              <td>CIF</td>
              <td>:</td>
              <td>{{$datas['cif']}}</td>
          </tr>
          <tr style="font-size: 5pt">
              <td>SID</td>
              <td>:</td>
              <td>{{$datas['sid']}}</td>
          </tr>
          <tr style="font-size: 5pt">
              <td>No Kontrak</td>
              <td>:</td>
              <td>{{$datas['noKontrak']}}</td>
          </tr>
          <tr style="font-size: 5pt">
              <td>SRE</td>
              <td>:</td>
              <td>{{$datas['subRek']}}</td>
          </tr>
      </table>

  </header>
  <br><br>
@foreach($datas['trHeaderStockMutationList'] as $efeks)
<table class="col-md-6">
  <tr style="font-size: 5pt">
      <td>Efek</td>
      <td>:</td>
      <td>{{$efeks['kodeEfek']}} - {{$efeks['namaEfek']}}</td>
  </tr>
</table>
<table width="100%" style="margin-top:3%;">
    <thead>
    <tr>
      <th align="left" width="5%">No</th>
      <th align="center" width="15%">Settlement Date</th>
      <th align="center" width="15%">Transaction Date</th>
      <th align="left" width="15%">Description</th>
      <th align="center" width="10%">Credit</th>
      <th align="center" width="10%">Debit</th>
      <th align="center" width="10%">Balance</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($efeks['trDetailStockMutations'] as $hasil)

      <tr>
          <td>{!!$i++!!}</td>
          <td align="center">{!!$hasil['settleDate']!!}</td>
          <td align="center">{!!$hasil['transDate']!!}</td>
          @if($hasil['transType'] == "PG")
          <td>Pencairan Gadai</td>
          @elseif($hasil['transType'] == "TU")
          <td>Top Up Saham</td>
          @else
          <td>Cicil Gadai</td>
          @endif
          <td align="right">{!! number_format($hasil['credit'] ,0,",",".") !!}</td>
          <td align="right">{!! number_format($hasil['debit'] ,0,",",".") !!}</td>
          <td align="right">{!! number_format($hasil['balance'] ,0,",",".") !!}</td>
      </tr>
      @endforeach
    </tbody>
</table>
<br>
@endforeach
@endforeach
</body>
</html>
