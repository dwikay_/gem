<!DOCTYPE html>
<html>
<head>

    <style type="text/css">
        table, td {
            border: 0px solid black;
            border-collapse: collapse;
            font-size:9pt;
        }
        th {
            background: #d8d8d8;
            border-top: 1px solid black;
            border-collapse: collapse;
            font-size:10pt;
        }
        th {
            border-bottom: 1px solid;
            vertical-align: middle;
        }
        p {
            margin-top: auto;
            margin-left: 3%;
            font-size:10pt;
            position: absolute;
        }
    </style>

    <title>Stock Concentration Limit</title>
</head>

<body class="metro">
  <header>
      <h2 class=""><center>Stock Concentration Limit</center></h2>
      <table style="">
          <tr style="font-size: 5pt">
              <td>Stock Code</td>
              <td>:</td>
              <td>{{$efek['kodeEfek']}}</td>
          </tr>
          <tr style="font-size: 5pt">
              <td>Stock Name</td>
              <td>:</td>
              <td>{{$efek['namaEfek']}}</td>
          </tr>
          <tr style="font-size: 5pt">
              <td>BMPK</td>
              <td>:</td>
              <td>{{$efek['bmpk']}}%</td>
          </tr>
          <tr style="font-size: 5pt">
              <td>Maksimum Agunan</td>
              <td>:</td>
              <td>{!! number_format($efek['maxLoanQty'] ,0,",",".") !!}</td>
          </tr>
          <tr style="font-size: 5pt">
              <td>Used Quantity</td>
              <td>:</td>
              <td>{!! number_format($efek['usedLoanQty'] ,0,",",".") !!}</td>
          </tr>
          <tr style="font-size: 5pt">
              <td>Available Quantity</td>
              <td>:</td>
              <td>{!! number_format($efek['availableQty'] ,0,",",".") !!}</td>
          </tr>
      </table>
  </header>
<table width="100%" style="margin-top:3%;">
    <thead>
    <tr>
      <th align="left" width="10%">No</th>
      <th align="left" width="30%">Contract Code</th>
      <th align="left" width="30%">SID</th>
      <th align="left" width="30%">Nama Nasabah</th>
      <th align="left" width="30%">Subrek</th>
      <th align="left" width="25%">Tipe Nasabah</th>
      <th align="center" width="25%">Qty On Hand</th>
      <th align="center" width="25%">Qty Pengajuan</th>
      <th align="center" width="25%">Qty Top Up</th>
      <th align="center" width="25%">Qty Total</th>
    </tr>
    </thead>
    <tbody>
      <?php $i=1; ?>
      @foreach($efek['trStockConcentrationList'] as $list)
      <tr>
          <td align="left">{{$i++}}</td>
          <td align="left">{{$list['noKontrak']}}</td>
          <td align="left">{{$list['sid']}}</td>
          <td align="left">{{$list['namaNasabah']}}</td>
          <td align="left">{{$list['bankSubRek']}}</td>
          @if($list['tipeNasabah']=="R")
          <td align="left">Individu</td>
          @else
          <td align="left">Badan Usaha</td>
          @endif
          <td align="right">{!! number_format($list['qtyOnhand'] ,0,",",".") !!}</td>
          <td align="right">{!! number_format($list['qtyRequest'] ,0,",",".") !!}</td>
          <td align="right">{!! number_format($list['qtyDeposit'] ,0,",",".") !!}</td>
          <td align="right">{!! number_format($list['qtyTotal'] ,0,",",".") !!}</td>
      </tr>
      @endforeach

    </tbody>
</table>
<br>

</body>
</html>
