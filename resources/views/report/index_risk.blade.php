@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
      <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Laporan</h4>
              <p class="card-description">
                Risk Parameter
              </p>
              <form id="fpro" class="forms-sample">
                <div class="form-group">
                  <div class="row">
                    <div class="col-3">
                      <label for="exampleInputEmail1"></label>
                      <select class="form-control" id="tipeEfek" name="tipeEfek">
                        <option value="all">Semua Tipe</option>
                        <option value="saham">Saham</option>
                        <option value="obligasi">Obligasi</option>
                      </select>
                    </div>
                    <div class="col-3">
                      <div class="mt-4">
                      <a class="btn btn-sm btn-success btn-preview" style="color:white"><span class="fas fa-search"></span>&nbsp;&nbsp;Preview</a>
                      <a class="btn btn-sm btn-primary btn-download" style="color:white"><span class="fas fa-download"></span>&nbsp;&nbsp;Download</a>
                      </div>
                    </div>
                  </div>
                </div><br>
              </form>
              <div class="preloader">
                <div class="loading">
                  <img src="{{asset('img/loading.gif')}}" width="80">
                </div>
              </div>
              <div id="PORisk"></div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){

  var base_url = window.location.origin;

  $('.preloader').hide();

  function viewLoading(){
    $('.preloader').fadeIn();
  }

  function hideLoading(){
    $(".preloader").fadeOut();
  }

  $('.btn-preview').on('click', function(){
      // $('#fpro').attr('action','{{url('report/riskParameter-generate-preview')}}');
      // $('#fpro').attr('method','get');
      // $('#fpro').submit();
      var data = $('#tipeEfek').val();
      $('#PORisk').attr('style', 'height:500px');

      var url = base_url+"/index.php" +'/laporan/riskParameter-generate-preview?tipeEfek='+ data;
      var options = {
        pdfOpenParams: {
            zoom: 75
        }
      };

      viewLoading();
      PDFObject.embed(url, "#PORisk",options);
      hideLoading();
  });

  $('.btn-download').on('click', function(){
    $('#fpro').attr('action','{{url('laporan/riskParameter-generate-download')}}');
    $('#fpro').attr('method','get');
    $('#fpro').submit();
  });

})
</script>
@endsection
