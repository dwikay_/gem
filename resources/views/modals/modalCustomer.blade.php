@extends('inc.modal',['modalId'=>'modal-customer','modalTitle'=>'List Nasabah','modalClass'=>'','isForm'=>0])

@section('modalContent')
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-sm" id="customer-lookup" width="100%">
				<thead>
					<tr>
						<th>
							Kode Nasabah
						</th>
						<th>
							Nama Nasabah
						</th>
						<th>
							Tipe
						</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
@overwrite

@section('script')
	@parent
	<script>
        var tableCustomerLookup = $('#customer-lookup').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url('/lookup/customer') }}/',
                columns: [
                    {data: 'kodeNasabah'},
                    {data: 'namaNasabah'},
                    {data: 'tipeNasabah'},
                ],
                createdRow: function (row, data, index) {
                    $(row).addClass("result-row");
                },
                columnDefs: [
                        {
                            "targets": [0],
                            "createdCell": function (td, cellData, rowData, row, col) {
                                $(td).append($('<input>')
                                    .attr('type','hidden')
                                    .addClass('id')
                                    .val(rowData.id)
                                );
                            }
                        },
												{
							              "targets": [2],
							              "data": null,
							              "createdCell": function (td, cellData, rowData, row, col) {
							                $(td).empty();

							                var tipeEfek;
							                switch(rowData.tipeNasabah){
							                  case 'C' : tipeEfek = 'Coorporate'; break;
							                  case 'R' : tipeEfek = 'Retail'; break;
							                }
							                $(td).text(tipeEfek);
							              },
							          },
                ],
                oLanguage: {
                   sSearch: "Filter :"
                },
                searching : true,
                fnInitComplete: function(){
                     $('#customer-lookup').parent().css('height','calc(320px)').css('max-height','calc(320px)');
                },
            });
        $('body').on('click','.btn-search-customer',function(){
            $('#modal-customer').modal();
        })

        $('#customer-lookup').on('click', '.result-row', function() {
            $('#customer-lookup').find('.result-row').each(function(){
                $(this).removeClass('table-success');
            })

            $(this).addClass('table-success');
        });

        function modalcustomerClear(){
            tableCustomerLookup.clear();
            tableCustomerLookup.draw();
        }
    </script>
@endsection
