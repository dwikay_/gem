@extends('inc.modal',['modalId'=>'modal-monitoring','modalTitle'=>'Eksekusi','modalClass'=>'modal-xl','isForm'=>0])

@section('modalContent')
<div class="row">
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-2 col-form-label">No. Eksekusi</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="noEksekusi" name="noEksekusi" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">Tanggal Input</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="tanggalInput" name="tanggalInput" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">Nilai Jual</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="nilaiJual" name="nilaiJual" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-2 col-form-label">Tanggal Trading</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="tanggalTrading" name="tanggalTrading" placeholder="" disabled="disabled">
      </div>
      <label class=" col-sm-2 col-form-label">Jatuh Tempo</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="jatuhTempo" name="jatuhTempo" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">Partner Sekuritas</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="sekuritas" name="sekuritas" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-2 col-form-label">Status</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm " id="status" name="status" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12 mt-30">
    <table class="table table-sm table-striped table-bordered" id="monitoring-lookup" width="100%">
      <thead class="text-center" style="border:1px solid #000">
        <tr>
          <th class="borders"></th>
          <th class="borders">Kode Efek</th>
          <th class="borders">Nama Efek</th>
          <th class="borders">Matched QTY</th>
          <th class="borders">Matched Price</th>
          <th class="borders">Gross Amount</th>
          <th class="borders">Charges</th>
          <th class="borders">Net Amount</th>
        </tr>
      </thead>
      <tbody>

      </tbody>
      <tfoot>
        <tr>
              <th colspan="6"></th>
          </tr>
      </tfoot>
    </table>
  </div>
</div>
@overwrite
@section('script')
	@parent
	<script>

  $('.borders')
  .css('border-left','1px solid #DEE2E7')
  .css('border-right','1px solid #DEE2E7')
  .css('border-top','1px solid #DEE2E7');

  var tableEditAlokasi = $('#monitoring-lookup').DataTable({
      bPaginate: false,
      searching : false,
      bSort: false,
      bInfo: false,
      scrollX: true,
      scrollY: '100vh',
      scrollCollapse: true,
      columns: [
        { data: 'kodeEfek' },
        { data: 'kodeEfek' },
        { data: 'nameEfek' },
        { data: 'matchedQty' },
        { data: 'matchedPrice'},
        { data: 'grossAmount'},
        { data: 'charges'},
        { data: 'netAmount'}
      ]
  });

  $('#noEksekusi').val("");
  $('#tanggalInput').val("");
  $('#nilaiJual').val("");

  $('#tanggalTrading').val("");
  $('#jatuhTempo').val("");
  $('#sekuritas').val("");

  function loadData(data){
    var efek;
    $('#modal-monitoring').find('.modal-footer').remove();

    $('#noEksekusi').val(data.noEksekusi);
    $('#nilaiJual').val(data.netAmount);
    $('#sekuritas').val(data.brokerPartner);

    var entryDate = moment(data.entryDate).locale('en').format('LL');
    var tradeDate = moment(data.tradeDate).locale('en').format('LL');
    var dueDate = moment(data.dueDate).locale('en').format('LL');

    $('#tanggalInput').val(entryDate);
    $('#tanggalTrading').val(tradeDate);
    $('#jatuhTempo').val(dueDate);

    if(data.status=="N"){
      $('#status').val("Normal");
      $('#status').removeClass("bg-warning");
      $('#status').removeClass("bg-danger");
      $('#status').addClass("bg-success");
      $('#status').addClass("text-white");
    }
    else if(data.status=="MC"){
      $('#status').val("Topup");
      $('#status').removeClass("bg-success");
      $('#status').removeClass("bg-danger");
      $('#status').removeClass("text-white");
      $('#status').addClass("bg-warning");
      $('#status').addClass("text-black");
    }
    else{
      $('#status').removeClass("bg-success");
      $('#status').removeClass("bg-warning");
      $('#status').val("Eksekusi");
      $('#status').addClass("bg-danger");
      $('#status').addClass("text-white");
    }

    $('#monitoring-lookup').dataTable().fnDestroy();

    tableEditAlokasi = $('#monitoring-lookup').dataTable({
      data: data.eksekusiMonitoringDetailList,
      bPaginate: false,
      searching : false,
      bSort: false,
      bInfo: false,
      scrollX: true,
      scrollY: '100vh',
      scrollCollapse: true,
      autoWidth: false,
      columns: [
        { data: 'kodeEfek' },
        { data: 'kodeEfek' },
        { data: 'nameEfek' },
        { data: 'matchedQty' },
        { data: 'matchedPrice'},
        { data: 'grossAmount'},
        { data: 'charges'},
        { data: 'netAmount'}
      ],
      columnDefs: [
        {
          "targets": [0],
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();

            $(td)
            .append($('<button>')
                .attr('type','button')
                .addClass('details-control')
                .text('+')
            )
            .append($('<input>')
                .attr('type','hidden')
                .val(rowData.id)
              )
          },
          orderable: false
        },
        {
            "targets": [3,4,5,6,7,],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
      ],

      createdRow: function ( row, data, index ) {
          $(row).attr('id','table_'+index);
          // if(data.kodeEfek=="ADRO"){
          //   $(row).css('color','red');
          //   $(row).css('font-weight','700');
          // }
      },

      // footerCallback: function ( row, data, start, end, display ) {
      //       var api = this.api(), data;
      //
      //       // Remove the formatting to get integer data for summation
      //       var intVal = function (i) {
      //           return typeof i === 'string' ?
      //               i.replace(/[\$.]/g, '')*1 :
      //               typeof i === 'number' ?
      //                   i : 0;
      //       },
      //
      //       // Total over all pages
      //       total = api
      //           .column(5)
      //           .data()
      //           .reduce( function (a, b) {
      //               return intVal(a) + intVal(b);
      //           },0);
      //
      //           total2 = api
      //               .column(3)
      //               .data()
      //               .reduce( function (a, b) {
      //                   return intVal(a) + intVal(b);
      //               },0);
      //       // console.log(total);
      //       // // Total over this page
      //       // pageTotal = api
      //       //     .column( 4, { page: 'current'} )
      //       //     .data()
      //       //     .reduce( function (a, b) {
      //       //         return intVal(a) + intVal(b);
      //       //     }, 0 );
      //
      //       // Update footer
      //       $(api.column(5).footer()).empty();
      //       $(api.column(5).footer())
      //       .append($('<span>')
      //                   .addClass('currencyNoComma')
      //                   .text(total)
      //       );
      //       // $(api.column(3).footer()).html(total2).addClass("currencyNoComma");
      //   },
        drawCallback: function(settings) {
               initAutoNumeric();
            },

    });
  }

  function createDetail(index){
    return $('<table>')
      .addClass('table detail-table table-striped table-bordered')
      .attr('id','detail_'+index)
          .append($('<thead>')
            .addClass('thead-light')
              .append($('<tr>')
                  .append($('<th>')
                  .addClass("text-center")
                  .text('No. Kontrak')
                  )
                  .append($('<th>')
                  .addClass("text-center")
                  .text('Qty')
                  )
                  .append($('<th>')
                  .addClass("text-center")
                  .text('Total Harga')
                  )
              )
          )
  }

  $('#monitoring-lookup tbody').on('click', '.details-control', function () {
      var tr = $(this).closest('tr');
      var row = tableEditAlokasi.api().row(tr);
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = tableEditAlokasi.fnGetData();
      var data_detail = data[index].eksekusiMonitoringAllocations;
      if ( row.child.isShown() ) {
          // This row is already open - close it
          row.child.hide();
          $(this).find('.details-control').text('+');
      }else {
          // Open this row
          console.log(data_detail)
          row.child( createDetail(index)).show();

          $('#detail_'+index).dataTable({
            data: data_detail,
            bPaginate: false,
            searching : false,
            bSort: true,
            bInfo: false,
            columns: [
                {data:'noKontrak'},
                {data:'requestQty'},
                {data:'netAmount'}
            ],
            columnDefs: [
              {
                "targets": [0],
                "data": 0,
                "createdCell": function (td, cellData, rowData, row, col) {
                  $(td).empty();
                  $(td).addClass('text-center');
                  $(td).text(cellData);
                }
              },
              {
                "targets": [1,2],
                "data": 0,
                "createdCell": function (td, cellData, rowData, row, col) {
                  $(td).empty();
                  $(td).append($('<span>')
                              .addClass('currencyNoComma')
                              .text(cellData)
                          )
                },
              }
            ],
            drawCallback: function(settings) {
               initAutoNumeric();
            },
          });

          $(this).find('.details-control').text('-');
      }
  });

  </script>
@endsection
