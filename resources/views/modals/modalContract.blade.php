@extends('inc.modal',['modalId'=>'modal-contract','modalTitle'=>'List Pengajuan','modalClass'=>'modal-lg','isForm'=>0])

@section('modalContent')
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-sm" id="contract-lookup" width="100%">
				<thead>
					<tr>
						<th>
							Contract Code
						</th>
						<th>
							Total Liabilities
						</th>
						<th>
							Total Collateral
						</th>
            <th>
              Ratio
            </th>
            <th>
              Ratio Status
            </th>
            <th>
              Topup Collateral Sugestion
            </th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
@overwrite

@section('script')
	@parent
	<script>
        var tableContractLookup = $('#contract-lookup').DataTable({
            columns: [
                {data: 'noKontrak'},
                {data: 'nilaiKewajiban'},
                {data: 'nilaiAgunan'},
                {data: 'rasio'},
                {data: 'rasioStatus'},
                {data: 'topupAgunan'},
            ],
        });
        @if(!isset($customScript))
        $('body').on('click','.btn-search-contract',function(){

            if($('#noKontrak').val()==""){
                $.alert({
                    title: 'Warning',
                    content: 'Nomor Pengajuan tidak boleh kosong',
                });
            }else{
							var noPengajuan = $('#noKontrak').val();
							$.ajax({
									url: "{!! url('transaksi/check-pengajuan') !!}/" + noPengajuan ,
									data: {},
									dataType: "json",
									type: "get",
							success:function(data)
								{
									if(data["kodeNasabah"]==undefined){
										$.alert({
	                      title: 'Error',
	                      content: 'Nomor Kontrak tidak ada',
	                  });
										$('#noKontrak').val("");
									}else{
										$('#kodeNasabah').val(data["kodeNasabah"]);
										$('#namaNasabah').val(data["namaNasabah"]);
										$('#totalKewajiban').val(data["nilaiKewajiban"]);
										$('#totalAgunan').val(data["totalCollateral"]);
										$('#rasioPinjaman').val(data["ratioPinjaman"]*100);
										$('#topupCash').val(data["topUpCash"]);
										$('#topupSaham').val(data["topUpCollateral"]);

										// $('#kustodi')
										// .append($('<option>')
										// 		.attr('value','ABNA1')
										// 		.html("ABNA1 - THE ROYAL BANK OF SCOTLAND N.V.")
										// ).append($('<option>')
										// 	.attr('value','AD001')
										// 	.html("AD001 - PT OSO SECURITIES"));

										$('#kustodi')
										@foreach($brokers as $broker)
											.append($('<option>')
													.attr('value','{{ $broker['kodeCustodi'] }}')
													.html("{{ $broker['kodeCustodi']." - ".$broker['namaCustodi'] }}")
													@if($broker['kodeCustodi'])
														.attr('selected','selected')
													@endif
											)
										@endforeach

										if(data["status"]=="N"){
								      $('#status').val("Normal");
								      $('#status').removeClass("bg-warning");
								      $('#status').removeClass("bg-danger");
								      $('#status').addClass("bg-success");
								      $('#status').addClass("text-white");
								    }
								    else if(data["status"]=="MC"){
								      $('#status').val("Topup");
								      $('#status').removeClass("bg-success");
								      $('#status').removeClass("bg-danger");
								      $('#status').removeClass("text-white");
								      $('#status').addClass("bg-warning");
								      $('#status').addClass("text-black");
								    }
								    else{
								      $('#status').removeClass("bg-success");
								      $('#status').removeClass("bg-warning");
								      $('#status').val("Eksekusi");
								      $('#status').addClass("bg-danger");
								      $('#status').addClass("text-white");
								    }

										loadStock(noPengajuan);
									}
								}
							});
            }
        })
        @endif

        $('#contract-lookup').on('click', '.result-row', function() {
            $('#contract-lookup').find('.result-row').each(function(){
                $(this).removeClass('table-success');
            })

            $(this).addClass('table-success');
        });

        function modalcontractClear(){
            tableContractLookup.clear();
            tableContractLookup.draw();
        }
    </script>
@endsection
