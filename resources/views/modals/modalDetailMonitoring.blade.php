@extends('inc.modal',['modalId'=>'modal-monitoring','modalTitle'=>'Monitoring Detalil','modalClass'=>'modal-xl','isForm'=>0])

@section('modalContent')
<div class="row">
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-2 col-form-label">No. Kontrak</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="noKontrak" name="noKontrak" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-2 col-form-label">Kode Nasabah</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="kodeNasabah" name="kodeNasabah" placeholder="" disabled="disabled">
      </div>
      <label class=" col-sm-2 col-form-label">Nama Nasabah</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm" id="namaNasabah" name="namaNasabah" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-2 col-form-label">Total Kewajiban</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="totalKewajiban" name="totalKewajiban" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">Total Agunan</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="totalAgunan" name="totalAgunan" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">Rasio pinjaman</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm percent" id="rasioPinjaman" name="rasioPinjaman" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-2 col-form-label">Status</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm " id="status" name="status" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">Topup Cash</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="topupCash" name="topupCash" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">Topup Saham</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="topupSaham" name="topupSaham" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
</div>
	<div class="row">
		<div class="col-sm-12 mt-30">
			<table class="table table-sm table-striped table-bordered" id="monitoring-lookup" width="100%">
				<thead class="text-center" style="border:1px solid #000">
					<tr>
						<th class="borders">
							Kode Efek
						</th>
						<th class="borders">
							Jumlah
						</th>
						<th class="borders">
							Harga
						</th>
            <th class="borders">
							Nilai Pasar
						</th>
            <th class="borders">
							Haircut
						</th>
            <th class="borders">
							Nilai Agunan
						</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
        <tfoot>
          <tr>
                <th colspan="6"></th>
            </tr>
        </tfoot>
			</table>
		</div>
	</div>
@overwrite

@section('script')
	@parent
	<script>

  $('.borders')
  .css('border-left','1px solid #DEE2E7')
  .css('border-right','1px solid #DEE2E7')
  .css('border-top','1px solid #DEE2E7');

  var tableEditAlokasi = $('#monitoring-lookup').DataTable({
      bPaginate: false,
      searching : false,
      bSort: false,
      bInfo: false,
      scrollX: true,
      scrollY: '100vh',
      scrollCollapse: true,
      columns: [
          { data: 'kodeEfek' },
          { data: 'efekQty' },
          { data: 'efekPrice' },
          { data: 'marketValue' },
          { data: 'haircut' },
          { data: 'collateralValue' },
      ]
  });

  $('#noKontrak').val("");
  $('#tglPencairan').val("");
  $('#tglJatuhTempo').val("");

  $('#kodeNasabah').val("");
  $('#namaNasabah').val("");
  $('#totalKewajiban').val("");
  $('#totalAgunan').val("");
  $('#rasioPinjaman').val("");

  $('#topupCash').val("");
  $('#topupSaham').val("");

  function loadData(data){
    $('#modal-monitoring').find('.modal-footer').remove();
    $('#noKontrak').val(data.noKontrak);
    $('#tglPencairan').val(data.tanggalPencairan);
    $('#tglJatuhTempo').val(data.tanggalJatuhTempo);

    $('#kodeNasabah').val(data.kodeNasabah);
    $('#namaNasabah').val(data.namaNasabah);
    $('#totalKewajiban').val(data.nilaiKewajiban);
    $('#totalAgunan').val(data.totalCollateral);
    $('#rasioPinjaman').val(data.ratioPinjaman*100);

    $('#topupCash').val(data.topUpCash);
    $('#topupSaham').val(data.topUpCollateral);
    if(data.status=="N"){
      $('#status').val("Normal");
      $('#status').removeClass("bg-warning");
      $('#status').removeClass("bg-danger");
      $('#status').addClass("bg-success");
      $('#status').addClass("text-white");
    }
    else if(data.status=="MC"){
      $('#status').val("Topup");
      $('#status').removeClass("bg-success");
      $('#status').removeClass("bg-danger");
      $('#status').removeClass("text-white");
      $('#status').addClass("bg-warning");
      $('#status').addClass("text-black");
    }
    else{
      $('#status').removeClass("bg-success");
      $('#status').removeClass("bg-warning");
      $('#status').val("Eksekusi");
      $('#status').addClass("bg-danger");
      $('#status').addClass("text-white");
    }

    // console.log(data.transaksiGadaiEfekDetailList);

    $('#monitoring-lookup').dataTable().fnDestroy();

    tableEditAlokasi = $('#monitoring-lookup').dataTable({
      data: data.transaksiGadaiEfekDetailList,
      bPaginate: false,
      searching : false,
      bSort: false,
      bInfo: false,
      scrollX: true,
      scrollY: '100vh',
      scrollCollapse: true,
      autoWidth: false,
      columns: [
        { data: 'kodeEfek' },
        { data: 'efekQty' },
        { data: 'efekPrice' },
        { data: 'marketValue' },
        { data: 'haircut' },
        { data: 'collateralValue' }
      ],
      columnDefs: [
        {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).text(cellData)
            },
        },
        {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [4],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('percent')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [5],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },

        },
      ],

      createdRow: function ( row, data, index ) {
          $(row).attr('id','table_'+index);
          console.log(data);
          if(data.collateralValue==0&&data.efekQty>0){
            $(row).css('color','red');
            $(row).css('font-weight','700');
          }
          // if(data.kodeEfek=="ADRO"){
          //   $(row).css('color','red');
          //   $(row).css('font-weight','700');
          // }
      },

      footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$.]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            },

            // Total over all pages
            total = api
                .column(5)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                },0);

                total2 = api
                    .column(3)
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    },0);
            console.log(total);
            // // Total over this page
            // pageTotal = api
            //     .column( 4, { page: 'current'} )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );

            // Update footer
            $(api.column(5).footer()).empty();
            $(api.column(5).footer())
            .append($('<span>')
                        .addClass('currencyNoComma')
                        .text(total)
            );
            // $(api.column(3).footer()).html(total2).addClass("currencyNoComma");
        },
        drawCallback: function(settings) {
               initAutoNumeric();
            },

    });
  }
  </script>
@endsection
