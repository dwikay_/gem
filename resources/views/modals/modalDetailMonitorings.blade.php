@extends('inc.modal',['modalId'=>'modal-monitoring','modalTitle'=>'Monitoring Detail','modalClass'=>'modal-xl','isForm'=>0])

@section('modalContent')
<div class="row">
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-2 col-form-label">No. Kontrak</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="noKontrak" name="noKontrak" placeholder="" disabled="disabled">
      </div>
      <label class=" col-sm-2 col-form-label">Nama Nasabah</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm" id="namaNasabah" name="namaNasabah" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-2 col-form-label">Kode Nasabah</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="kodeNasabah" name="kodeNasabah" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">SID</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="sid" name="sid" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">Subrek</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="subRek" name="subRek" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
       <label class="col-sm-2 col-form-label">Tipe Efek</label>
       <div class="col-sm-2">
         <input type="text" class="form-control form-control-sm" id="tipeEfek" name="tipeEfek" placeholder="" disabled="disabled">
       </div>
       <label class="col-sm-2 col-form-label">Kode Bank Custody</label>
       <div class="col-sm-2">
         <input type="text" class="form-control form-control-sm" id="kodeBank" name="kodeBank" placeholder="" disabled="disabled">
       </div>
       <label class="col-sm-2 col-form-label">Status</label>
       <div class="col-sm-2">
         <input type="text" class="form-control form-control-sm " id="status" name="status" placeholder="" disabled="disabled">
       </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
       <label class="col-sm-2 col-form-label">Nilai On Hand</label>
       <div class="col-sm-2">
         <input type="text" class="form-control form-control-sm currencyNoComma" id="cashOnhand" name="cashOnhand" placeholder="" disabled="disabled">
       </div>
       <label class="col-sm-2 col-form-label">Nilai Eksekusi</label>
       <div class="col-sm-2">
         <input type="text" class="form-control form-control-sm currencyNoComma" id="nilaiEksekusi" name="nilaiEksekusi" placeholder="" disabled="disabled">
       </div>
       <label class="col-sm-2 col-form-label">Nilai Corporate Action</label>
       <div class="col-sm-2">
         <input type="text" class="form-control form-control-sm currencyNoComma" id="nilaiCorpact" name="nilaiCorpact" placeholder="" disabled="disabled">
       </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-2 col-form-label">Nilai Kewajiban</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="nilaiKewajiban" name="nilaiKewajiban" placeholder="" disabled="disabled">
      </div>
       <label class="col-sm-2 col-form-label">Total Kewajiban</label>
       <div class="col-sm-2">
         <input type="text" class="form-control form-control-sm currencyNoComma" id="totalKewajiban" name="totalKewajiban" placeholder="" disabled="disabled">
       </div>
       <label class="col-sm-2 col-form-label">Total Agunan</label>
       <div class="col-sm-2">
         <input type="text" class="form-control form-control-sm currencyNoComma" id="totalAgunan" name="totalAgunan" placeholder="" disabled="disabled">
       </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
       <label class="col-sm-2 col-form-label">Rasio pinjaman</label>
       <div class="col-sm-2">
         <input type="text" class="form-control form-control-sm percent" id="rasioPinjaman" name="rasioPinjaman" placeholder="" disabled="disabled">
       </div>
      <label class="col-sm-2 col-form-label">Topup Cash</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="topupCash" name="topupCash" placeholder="" disabled="disabled">
      </div>
      <label class="col-sm-2 col-form-label">Topup Saham</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="topupSaham" name="topupSaham" placeholder="" disabled="disabled">
      </div>
    </div>
  </div>
</div>

	<div class="row">

    <div class="col-md-12 grid-margin stretch-card mt-4">
      <div class="card">
        <div class="card-body">
              <div class="box-tab" id="mtab2">
                                <ul class="nav nav-tabs tab-basic">
                                    <li class="nav-item active" id="91"><a class="nav-link active" href="#pospengaturan" data-toggle="tab">List Agunan</a></li>
                                    <li class="nav-item" id="92"><a class="nav-link" href="#pospenjualan" data-toggle="tab">Nota Transaksi</a></li>
                                </ul><br>
                                <div class="tab-content text-center">
                                    <div class="tab-pane fade in active show" id="pospengaturan">
                                        <div class="table-responsive">
                                          <table class="table table-sm table-striped table-bordered" id="monitoring-lookup" width="100%">
                                            <thead class="text-center" style="border:1px solid #000">
                                              <tr class="">
                                                <th class="borders">Kode Efek</th>
                                                <th class="borders">Qty On Hand</th>
                                                <th class="borders">Qty Deliver</th>
                                                <th class="borders">Qty Balance</th>
                                                <th class="borders">Harga</th>
                                                <th class="borders">Nilai Pasar</th>
                                                <th class="borders">Haircut</th>
                                                <th class="borders">Nilai Agunan</th>
                                                <th class="borders text-center">Action</th>
                                              </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="pospenjualan">
                                        <div class="table-responsive">
                                          <table class="table table-sm table-striped table-bordered" id="monitoring-nota" width="100%">
                                            <thead class="text-center" style="border:1px solid #000">
                                              <tr class="">
                                                <th class="borders">No. Nota</th>
                                                <th class="borders">Tanggal Pencairan</th>
                                                <th class="borders">Jatuh Tempo</th>
                                                <th class="borders">Uang Pinjaman</th>
                                                <th class="borders">Nilai Kewajiban</th>
                                              </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
      </div>
    </div>
  </div>
	</div>
@overwrite
@section('script')
	@parent
	<script>

  $('.borders')
  .css('border-left','1px solid #DEE2E7')
  .css('border-right','1px solid #DEE2E7')
  .css('border-top','1px solid #DEE2E7');

  var tableEditAlokasi = $('#monitoring-lookup').DataTable({
      bPaginate: false,
      searching : false,
      bSort: false,
      bInfo: false,
      scrollX: true,
      scrollY: '100vh',
      scrollCollapse: true,
      columns: [
          { data: 'kodeEfek' },
          { data: 'qtyOnhand' },
          { data: 'qtyDlv' },
          { data: 'efekQty' },
          { data: 'efekPrice' },
          { data: 'marketValue' },
          { data: 'haircut' },
          { data: 'collateralValue' },
          { data: 'kodeEfek' },
      ]
  });

  var tableNota = $('#monitoring-nota').DataTable({
      bPaginate: false,
      searching : false,
      bSort: false,
      bInfo: false,
      scrollX: true,
      scrollY: '100vh',
      scrollCollapse: true,
      columns: [
          { data: 'noNota' },
          { data: 'tanggalPencairan' },
          { data: 'tanggalJatauhTempo' },
          { data: 'uangPinjaman' },
          { data: 'nilaiKewajiban' }
      ]
  });

  $('#noKontrak').val("");
  $('#tglPencairan').val("");
  $('#tglJatuhTempo').val("");

  $('#kodeNasabah').val("");
  $('#namaNasabah').val("");
  $('#totalKewajiban').val("");
  $('#totalAgunan').val("");
  $('#rasioPinjaman').val("");

  $('#topupCash').val("");
  $('#topupSaham').val("");

  function loadData(data){
    var efek;
    $('#modal-monitoring').find('.modal-footer').remove();
    $('#noKontrak').val(data.noKontrak);
    $('#tglPencairan').val(data.tanggalPencairan);
    $('#tglJatuhTempo').val(data.tanggalJatuhTempo);

    $('#kodeNasabah').val(data.kodeNasabah);
    $('#namaNasabah').val(data.namaNasabah);
    $('#totalKewajiban').val(data.totalKewajiban);
    $('#totalAgunan').val(data.totalCollateral);
    $('#kodeBank').val(data.kodeBankCustody);

    $('#nilaiEksekusi').val(data.nilaiEksekusi);
    $('#nilaiCorpact').val(data.nilaiCorpact);
    $('#cashOnhand').val(data.cashOnhand);
    ;
    $('#sid').val(data.sid);
    $('#subRek').val(data.subRek);
    $('#nilaiKewajiban').val(data.nilaiKewajiban);

    $('#rasioPinjaman').val(data.ratioPinjaman*100);

    $('#topupCash').val(data.topUpCash);
    $('#topupSaham').val(data.topUpCollateral);

    if(data.tipeEfek=="S"){
      efek = "Saham";
    }else if(data.tipeEfek=="O"){
      efek = "Obligasi";
    }
    $('#tipeEfek').val(efek);

    if(data.status=="N"){
      $('#status').val("Normal");
      $('#status').removeClass("bg-warning");
      $('#status').removeClass("bg-danger");
      $('#status').addClass("bg-success");
      $('#status').addClass("text-white");
    }
    else if(data.status=="MC"){
      $('#status').val("Topup "+ data.callDays);
      $('#status').removeClass("bg-success");
      $('#status').removeClass("bg-danger");
      $('#status').removeClass("text-white");
      $('#status').addClass("bg-warning");
      $('#status').addClass("text-black");
    }
    else{
      $('#status').removeClass("bg-success");
      $('#status').removeClass("bg-warning");
      $('#status').val("Eksekusi");
      $('#status').addClass("bg-danger");
      $('#status').addClass("text-white");
    }

    // console.log(data.transaksiGadaiEfekDetailList);

    $('#monitoring-lookup').dataTable().fnDestroy();
    $('#monitoring-nota').dataTable().fnDestroy();

    tableEditAlokasi = $('#monitoring-lookup').DataTable({
      data: data.transaksiGadaiEfekDetailList,
      bPaginate: false,
      searching : false,
      bSort: false,
      bInfo: false,
      scrollX: true,
      scrollY: '100vh',
      scrollCollapse: true,
      autoWidth: false,
      columns: [
        { data: 'kodeEfek' },
        { data: 'qtyOnhand' },
        { data: 'qtyDlv' },
        { data: 'efekQty' },
        { data: 'efekPrice' },
        { data: 'marketValue' },
        { data: 'haircut' },
        { data: 'collateralValue' },
        { data: 'kodeEfek', width:'50px' }
      ],
      columnDefs: [
        {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).text(cellData)
            },
        },
        {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [4],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [5],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [6],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('percent')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [7],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },

        },
        {
            "targets": [8],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<a>')
                .attr('href','#')
                .attr('onclick','detailTransaksi(\'' + rowData.kodeEfek + '\',\'' + $('#noKontrak').val() + '\')')
                .addClass('btn btn-md btn-info float-right btn-query')
                .css('color','white')
                .css('height','30px')
                .attr('title','Detail Transaksi')
                .append($('<span>')
                    .addClass('fa fa-pencil fa-xs')
                    .css('padding-top','40%')

                ).text("Detail")
              ).addClass("text-center")
            },

        },
      ],

      createdRow: function ( row, data, index ) {
          $(row).attr('id','tableMonitoring_'+index);
          if(data.collateralValue==0&&data.efekQty>0){
            $(row).css('color','red');
            $(row).css('font-weight','700');
          }
          // if(data.kodeEfek=="ADRO"){
          //   $(row).css('color','red');
          //   $(row).css('font-weight','700');
          // }
      },

      footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$.]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            },

            // Total over all pages
            total = api
                .column(5)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                },0);

                total2 = api
                    .column(3)
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    },0);

            // Update footer
            $(api.column(5).footer()).empty();
            $(api.column(5).footer())
            .append($('<span>')
                        .addClass('currencyNoComma')
                        .text(total)
            );
            // $(api.column(3).footer()).html(total2).addClass("currencyNoComma");
        },
        drawCallback: function(settings) {
               initAutoNumeric();
            },

    });

    //NOTA
    tableNota = $('#monitoring-nota').DataTable({
      data: data.notaTransaksiGadaiEfekList,
      bPaginate: false,
      searching : false,
      bSort: false,
      bInfo: false,
      scrollX: true,
      scrollY: '100vh',
      scrollCollapse: true,
      autoWidth: false,
      columns: [
        { data: 'noNota'},
        { data: 'tanggalPencairan' },
        { data: 'tanggalJatauhTempo' },
        { data: 'uangPinjaman' },
        { data: 'nilaiKewajiban' }
      ],
      columnDefs: [
        {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).text(cellData)
            },
        },
        {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              var dates = moment(cellData).locale('en').format('LL');
              $(td).text(dates);
            },
        },
        {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              var dates = moment(cellData).locale('en').format('LL');
              $(td).text(dates);
            },
        },
        {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
        {
            "targets": [4],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            },
        },
      ],

      createdRow: function ( row, data, index ) {
          $(row).attr('id','tables_'+index);
      },
        drawCallback: function(settings) {
               initAutoNumeric();
            },

    });

  }
  </script>
@endsection
