<div class="modal" id="modalDownload" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Format Instruksi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>            
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-center">                   
              <a href="javascript::void(0)" id="btn-instruksi-xml" class="btn btn-lg btn-primary">XML</a>
              <a href="javascript::void(0)" id="btn-instruksi-csv" class="btn btn-lg btn-primary">CSV</a>              
          </div>
        </div>              
      </div>            
    </div>
  </div>
</div>