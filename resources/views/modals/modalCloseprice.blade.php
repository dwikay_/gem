@extends('inc.modal',['modalId'=>'modal-closeprice','modalTitle'=>'Load Data Saham','modalClass'=>'modal-sm','isForm'=>0])

@section('modalContent')
<div class="row">
  <div class="col-md-12">
     <div class="form-group row">
      <form id="form-sample" method="get" class="forms-sample col-12" action="{{url('monitoring/closingPrice/getSaham')}}">
        <label class="col-sm-2 col-form-label">Tanggal</label>
      <div class="col-sm-12">
        <input type="text" class="form-control form-control-sm" id="tglGetData" placeholder="">
        <input class="form-control form-control-sm" type="hidden" id="tglGetDatas" name="tglGetDatas" placeholder="">
      </div>
    </form>
    </div>
  </div>
</div>
@overwrite
@section('script')
	@parent
	<script>

  $('#tglGetData').datepicker({
      locale: 'id',
      format: 'dd M yyyy',
      autoclose: true
  });
  $('#tglGetData').on('change', function(){
      convertMoments($('#tglGetData').val());
      $('#tglGetDatas').val(dates);
  });
  function convertMoments(date){
    var dates = moment(date).locale('id').format('YYYY-MM-DD');
    $('#tglGetDatas').val(dates);
    // return dates;
  }
  $(document).ready(function() {
    $('.btn-ok').on('click', function(){
        $('#form-sample').submit();
    });

  });
  </script>
@endsection
