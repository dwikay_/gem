@extends('inc.modal',['modalId'=>'modal-eksekusi','modalTitle'=>'Pilih Sekuritas','modalClass'=>'','isForm'=>0])

@section('modalContent')
<div class="row">
  <div class="col-md-12">
     <div class="form-group row">
      <form id="form-sample" method="post" class="forms-sample col-12" action="{{url('monitoring/transaksi/eksekusi')}}">
        @csrf
       <div class="col-sm-12">
         <div class="form-group row">
             <label class="col-md-4 col-form-label">Tipe Efek</label>
             <div class="col-md-8">
               <select class="form-control form-control-sm" name="tipeEfek" id="tipeEfek">
                 <option value="">-</option>
                 <option value="S">Saham</option>
                 <option value="O">Obligasi</option>
               </select>
           </div>
         </div>
         <div class="form-group row">
             <label class="col-md-4 col-form-label">Sekuritas</label>
             <div class="col-md-8">
               <select class="form-control form-control-sm"  name="sekuritas" id="sekuritas">
                 <option value="">-</option>
                 @foreach($brokers as $broker)
                   <option value="{{ $broker['kodeBroker'] }}">{{ $broker['kodeBroker']." - ".$broker['namaBroker'] }}</option>
                 @endforeach
               </select>
           </div>
         </div>
      </div>
    </form>
    </div>
  </div>
</div>
@overwrite
@section('script')
	@parent
	<script>

  $(document).ready(function() {
    $('.btn-ok').on('click', function(){
        $('#form-sample').submit();
    });

  });
  </script>
@endsection
