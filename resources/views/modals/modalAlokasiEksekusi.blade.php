@extends('inc.modal',['modalId'=>'modal-alokasi-eksekusi','modalTitle'=>'Modifikasi Alokasi Eksekusi','modalClass'=>'modal-xl','isForm'=>0])

@section('modalContent')
<div class="row">
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-2 col-form-label">Kode Transaksi Eksekusi</label>
      <div class="col-sm-3">
        <input type="text" class="form-control form-control-sm" id="noEksekusi" name="noEksekusi" placeholder="" disabled="disabled" value="{{$eksekusi['noEksekusi']}}">
      </div>
      <label class=" col-sm-2 col-form-label">Tanggal Eksekusi</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm datepicker" id="tanggalEksekusi" name="tanggalEksekusi" placeholder="" disabled="disabled" value="{{$eksekusi['tanggalEksekusi']}}">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-2 col-form-label">Broker Partner</label>
      <div class="col-sm-3">
        <input type="text" class="form-control form-control-sm" id="brokerCode" name="brokerCode" placeholder="" disabled="disabled" value="{{$eksekusi['kodeBroker'].' - '.$eksekusi['namaBroker']}}">
      </div>
      <label class=" col-sm-2 col-form-label">Tanggal Jatuh Tempo</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm datepicker" id="tanggalJatuhTempo" name="tanggalJatuhTempo" placeholder="" disabled="disabled" value="{{$eksekusi['tanggalJatuhTempo']}}">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <hr>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-2 col-form-label">Stock Code</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="detailStockCode" name="detailStockCode" placeholder="" disabled="disabled" value="">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-2 col-form-label">Request Quantity (lot)</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" id="detailRequestQuantity" name="detailRequestQuantity" placeholder="" disabled="disabled" value="">
      </div>
      <label class=" col-sm-2 col-form-label">Request Price</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" id="detailRequestPrice" name="detailRequestPrice" placeholder="" disabled="disabled" value="">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-2 col-form-label">Matched Quantity (lot)</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="detailMatchedQuantity" name="detailMatchedQuantity" placeholder="" value="">
      </div>
      <label class=" col-sm-2 col-form-label">Matched Price</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="detailMatchedPrice" name="detailMatchedPrice" placeholder="" value="">
      </div>
      <label class=" col-sm-2 col-form-label">Gross Amount </label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="detailGrossAmount" name="detailGrossAmount" placeholder="" value="">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class="offset-sm-4 col-sm-2 col-form-label">Charges</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoComma" id="detailCharges" name="detailCharges" placeholder="" value="">
      </div>
      <label class=" col-sm-2 col-form-label">Net Amount</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" id="detailNetAmount" name="detailNetAmount" placeholder="" value="">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-2 col-form-label">Allocation Quantity (lot)</label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" id="detailAllocationQuantity" name="detailAllocationQuantity" placeholder="" value="">
      </div>
      <label class=" col-sm-2 col-form-label">Allocation Amount </label>
      <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" id="detailAllocationAmount" name="detailAllocationAmount" placeholder="" value="">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <hr>
  </div>
  <div class="offset-md-2 col-md-8">
    <table class="table table-sm table-striped" id="editAlokasi">
      <thead>
        <tr>
          <th>
            Contract Code
          </th>
          <th>
            Quantity
          </th>
          <th>
            Amount
          </th>
          <th>
          </th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="col-md-2">
    <button type="button" class="btn btn-sm btn-success float-right" id="btn-add-record"><i class="fas fa-plus fa-sm"></i> Add Record</button>
  </div>
</div>
@overwrite

@section('script')
	@parent
	<script>
    var tableEditAlokasi = $('#editAlokasi').DataTable({
        bPaginate: false,
        searching : false,
        bSort: false,
        bInfo: false,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        columns: [
            { data: 'contractCode' },
            { data: 'quantity' },
            { data: 'amount' },
            { data: 'id' },
        ]
    });

    function loadDataEdit(data){
      $('#detailStockCode').val(data.stockCode);
      $('#detailRequestQuantity').val(data.requestQuantity);
      $('#detailRequestPrice').val(data.requestPrice);

      $('#detailMatchedQuantity').val(data.matchedQuantity);
      $('#detailMatchedPrice').val(data.matchedPrice);
      $('#detailGrossAmount').val(data.grossAmount);
      $('#detailCharges').val(data.charges);
      $('#detailNetAmount').val(data.grossAmount - data.charges);

      $('#detailAllocationQuantity').val(data.allocationQuantity);
      $('#detailAllocationAmount').val(data.allocationAmount);

      $('#modal-alokasi-eksekusi').find('.modal-footer').empty();

      $('#editAlokasi').dataTable().fnDestroy();
      tableEditAlokasi = $('#editAlokasi').DataTable({
        data: data.alokasi,
        bPaginate: false,
        searching : false,
        bSort: false,
        bInfo: false,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        autoWidth: false,
        columns: [
            { data: 'contractCode',width: "30%" },
            { data: 'quantity',width: "30%" },
            { data: 'amount',width: "30%" },
            { data: 'id',width: "10%" },
        ],
        columnDefs: [
          {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<div>')
                  .addClass('input-group')
                  .append($('<input>')
                    .addClass('form-control form-control-sm alokasi_contract')
                    .val(cellData)
                    .attr('id','alokasi_'+row+'_contract')
                    .attr('readonly','readonly')
                  )
                  .append($('<div>')
                    .addClass('input-group-append')
                    .append($('<button>')
                      .addClass('btn btn-outline-info btn-search-contract')
                      .attr('type','button')
                      .append($('<i>')
                        .addClass('fas fa-search')
                      )
                    )
                  )
              );
            },
          },
          {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                  .addClass('form-control form-control-sm currencyNoComma alokasi_quantity')
                  .val(cellData)
                  .attr('id','alokasi_'+row+'_quantity')
              );
            },
          },
          {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                  .addClass('form-control form-control-sm currencyNoComma alokasi_amount')
                  .val(cellData)
                  .attr('id','alokasi_'+row+'_amount')
              );
            },
          },
          {
            "targets": [3],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td)
                .append($('<a>')
                  .addClass('btn btn-danger btn-sm btn-delete')
                  .attr('title','Remove Record')
                  .append($('<span>')
                    .addClass('fas fa-times fa-xs')
                  )
                );
            },
            orderable: false
          },
        ],
        drawCallback: function(settings) {
           initAutoNumeric();
        },
      });
    }

    $('#editAlokasi').on('click','.btn-search-contract',function(){
      $('#contract-lookup').dataTable().fnDestroy();

      tableContractLookup = $('#contract-lookup').DataTable({
        // processing: true,
        // serverSide: true,
        // ajax: '{{ url('/lookup/contract') }}/'+idNasabah,
        columns: [
            {data: 'noKontrak'},
            {data: 'nilaiKewajiban'},
            {data: 'nilaiAgunan'},
            {data: 'rasio'},
            {data: 'rasioStatus'},
            {data: 'topupAgunan'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass("result-row");
        },
        columnDefs: [
                {
                    "targets": [0],
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).append($('<input>')
                            .attr('type','hidden')
                            .addClass('id')
                            .val(rowData.id)
                        );
                    }
                },
        ],
        oLanguage: {
           sSearch: "Filter :"
        },
        searching : true,
        fnInitComplete: function(){
             $('#contract-lookup').parent().css('height','calc(320px)').css('max-height','calc(320px)');
        },
      });

      $('#modal-contract').modal();
    })

    $('#btn-add-record').on('click',function(){
      var no = tableEditAlokasi.rows().count()

      var rowdetail = $('<tr>')
        .append($('<td>')
          .append($('<div>')
            .addClass('input-group')
            .append($('<input>')
              .addClass('form-control form-control-sm alokasi_contract')
              .attr('id','alokasi_'+no+'_contract')
              .attr('readonly','readonly')
            )
            .append($('<div>')
              .addClass('input-group-append')
              .append($('<button>')
                .addClass('btn btn-outline-info btn-search-contract')
                .attr('type','button')
                .append($('<i>')
                  .addClass('fas fa-search')
                )
              )
            )
          )
        )
        .append($('<td>')
          .append($('<input>')
            .addClass('form-control form-control-sm currencyNoComma alokasi_quantity')
            .attr('id','alokasi_'+no+'_quantity')
          )
        )
        .append($('<td>')
          .append($('<input>')
              .addClass('form-control form-control-sm currencyNoComma alokasi_amount')
              .attr('id','alokasi_'+no+'_amount')
          )
        )
        .append($('<td>')
          .append($('<a>')
            .addClass('btn btn-danger btn-sm btn-delete')
            .attr('title','Remove Record')
            .append($('<span>')
              .addClass('fas fa-times fa-xs')
            )
          )
        );

      tableEditAlokasi.row.add(rowdetail).draw( false );
      $('#alokasi_'+no+'_contract').val('');
      $('#alokasi_'+no+'_quantity').val(0);
      $('#alokasi_'+no+'_amount').val(0);
      initAutoNumeric();
    })

    $('#editAlokasi').on('click', '.btn-delete', function() {
        var elem = $(this).parents('tr');

        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure to delete this record?',
            buttons: {
                cancel: function () {
                },
                confirm: function () {
                    tableEditAlokasi.row(elem).remove().draw( true );
                },
            }
        });
    });

  </script>
@endsection
