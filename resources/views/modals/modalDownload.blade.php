@extends('inc.modal',['modalId'=>'modal-download','modalTitle'=>'Download Sampel','modalClass'=>'modal-sm','isForm'=>0])

@section('modalContent')
<div class="row">
  <div class="col-md-12">
     <div class="form-group row">
      <a href="" class="btn btn-lg btn-primary">Obligasi</a>
      <a href="" class="btn btn-lg btn-primary">Saham</a>
    </div>
  </div>
</div>
@overwrite
