<!DOCTYPE html>
<html lang="zxx">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="author" content="cakra-tech.co.id" />
  <title>Gadai Efek Mesin</title>
  <link rel="shortcut icon" type="image/icon" href="images/favicon-16x16.html"/>
  <link  rel="stylesheet" href="{{asset('logins/css/style.css')}}">
  </head>

  <body>
    <div id="preload-block">
      <div class="square-block"></div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4" style="padding-left:0px;margin-top:100px">
          <img src="{{asset('img/right.png')}}" width="250" alt="right" data-target="_blank">
        </div>
        <div class="col-lg-4 col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">

          <div class="authfy-login">
            <div class="authfy-panel panel-login text-center active">
              <div class="authfy-heading">
                <div class="brand-logo text-center">
                  <img src="{{asset('img/logo.png')}}" width="200" alt="brand-logo">
                </div><br><br>
                <p>Welcome ! Please login to your account</p>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12">
                  <form action="{{url('login')}}" method="post" class="loginForm">
                    {{csrf_field()}}
                    <div class="form-group wrap-input">
                      <input type="text" class="form-control email" name="email" placeholder="Email address" autocomplete="off">
                      <span class="focus-input"></span>
                    </div>
                    <div class="form-group wrap-input">
                      <div class="pwdMask">
                        <input type="password" class="form-control password" name="password" placeholder="Password" autocomplete="off">
                        <span class="focus-input"></span>
                        <span class="fa fa-eye-slash pwd-toggle"></span>
                      </div>
                    </div>
                    <div class="row remember-row">
                      <div class="col-xs-6 col-sm-6">
                        <label class="checkbox text-left">
                          <input type="checkbox" value="remember-me"><span class="label-text">Remember me</span>
                        </label>
                      </div>
                      <div class="col-xs-6 col-sm-6">
                        <p class="forgotPwd">
                          <!-- <a class="lnk-toggler" data-panel=".panel-forgot">Forgot password?</a> -->
                        </p>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-12">
                        <button class="btn btn-lg btn-warning btn-block" type="submit">Login</button>
                      </div>
                      <!-- <div class="col-xs-6 col-sm-6">
                        <a class="btn btn-lg btn-warning btn-block lnk-toggler" data-panel=".panel-signup">Signup</a>
                      </div> -->
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4" style="padding-right:0px;margin-top:100px;">
          <img src="{{asset('img/left.png')}}" width="250" style="float:right" alt="right" data-target="_blank">
        </div>
      </div>
      <div class="row">

      </div>
    </div>
    <script src="{{asset('logins/js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('logins/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('logins/js/custom.js')}}"></script>
  </body>
</html>
