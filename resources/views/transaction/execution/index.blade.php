@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
              <div class="form-group mt-10" role="group" aria-label="Basic example">
                @include('inc.button.instruksi')
              </div>
                <h4 class="card-title">Eksekusi</h4><br>
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <form id="fpro" action="{{url('transaksi/eksekusi/doc-send-intruksi-all')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No. Eksekusi</th>
                        <th class="text-center">Tanggal Input</th>
                        <th class="text-center">Tanggal Trading</th>
                        <th class="text-center">Jatuh Tempo</th>
                        <th>Kode Sekuritas</th>
                        <th class="text-center">Harga</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                        <th class="text-center"><input type="checkbox" clacc="form-control" id="select_all" style="cursor:pointer"></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('modal')
    @include('modals.modalEksekusi')

    @include('modals.modalGenerate')
@endsection
@section('script')
<script>

$(document).ready(function() {

  $('#select_all').on('click',function(){
      if(this.checked){
          $('.checkbox').each(function(){
              this.checked = true;
          });
      }else{
           $('.checkbox').each(function(){
              this.checked = false;
          });
      }
  });

  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    searching: true,
    ajax:{
      url: "{{ url('transaksi/eksekusi/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="11">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
      {data: 'noEksekusi'},
      {data: 'entryDate'},
      {data: 'tradeDate'},
      {data: 'dueDate'},
      {data: 'brokerPartner'},
      {data: 'netAmount'},
      {data: 'status'},
      {data: 'status'},
      {data: 'noEksekusi'},
    ],
    "scrollX": '100vw',
    'scrollY': '100vh',
    "scrollCollapse": true,
    'autoWidth': true,
    'bSort': true,
    'bPaginate': true,
    'searching' : true,
    columnDefs: [
      {
          "targets": [1,2,3],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            var dates = moment(cellData).locale('id').format('ll');
            $(td).text(dates);
            $(td).addClass("text-center");
          },
      },
      {
          "targets": [5],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [6],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).addClass('text-center');
            var status;
            var classnya;
            switch(cellData){
              case '1' : status = 'Request Eksekusi'; classnya = "primary"; break;
              case '2' : status = 'Konfirmasi Trade'; classnya = "primary"; break;
              case '3' : status = 'Mengirim Dokumen KSEI'; classnya = "info text-white"; break;
              case '4' : status = 'Eksekusi Berhasil'; classnya = "success"; break;
              case '5' : status = 'Eksekusi Batal'; classnya = "danger"; break;
            }
            $(td).append($('<span>')
                        .addClass('badge badge-'+classnya)
                        .text(status)
                    )
          },
      },
      {
        "targets": [7],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).addClass('text-center');
          if(cellData=="1"){
            $(td).append($('@include('inc.button.group_btn_konfirmasi')'));
          } else if(cellData=="2"){
            $(td).append($('@include('inc.button.btn_group_eksekusi')'));
          } else if(cellData=="4" || cellData=="5"){
            $(td).append($('@include('inc.button.btnBlockInstruksi')'));
          }

        },
        orderable: false
      },
      {
        "targets": [8],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).append(
            $('<input type="checkbox" class="checkbox checkbox-instruksi" style="cursor:pointer" id="checks'+rowData.noEksekusi+'" name="checks['+rowData.noEksekusi+']" value="'+rowData.noEksekusi+'")">')
          );
          // $(td).append(
          //   $('<input type="hidden" class="efek" style="cursor:pointer" id="efek'+rowData.tipeEfek+'" name="efek['+rowData.tipeEfek+']" value="'+rowData.tipeEfek+'" onclick="efek('+rowData.tipeEfek+')">')
          // );
        },
        orderable: false
      },
    ],
    drawCallback: function(settings) {
           initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }
  });

  $('.table').on('click','.btn-detail', function(){

      var tr = $(this).closest('tr');
      var row = table.api().row( tr );
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData();

      $.ajax({
        url: "{!! url('transaksi/eksekusi/getDetail') !!}/" + data[index].noEksekusi,
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
        {
          loadData(data);
        }
        });
        $('#modal-monitoring').modal();

      // loadData(data[index]);
  });



  $('.table').on('click','.btn-konfirmasi', function(){

      var tr = $(this).closest('tr');
      var row = table.api().row( tr );
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData();

      location.href="{{url('transaksi/eksekusi/alokasi/create')}}" + "/" + data[index].noEksekusi;
      // loadData(data[index]);
  });

  $('.table').on('click','.btn-view', function(){

      var tr = $(this).closest('tr');
      var row = table.api().row( tr );
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData();

      location.href="{{url('transaksi/eksekusi/alokasi/view')}}" + "/" + data[index].noEksekusi;
      // loadData(data[index]);
  });

  $('.table').on('click','.btn-settle-record', function(){
      var tr = $(this).closest('tr');

      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData()

      $.confirm({
          title: 'Confirmation',
          content: 'Apa anda yakin ingin menyetujui eksekusi ini ?',
          icon: 'fas fa-exclamation-triangle',
          buttons: {
              confirm: function () {
                  location.href="{{url('transaksi/eksekusi/settle')}}"
                  + "/" + 0
                  + "/" + data[index].noEksekusi;
              },
              cancel: function () {

              }
          }
      });
  });

  $('.table').on('click','.btn-unsettle-record', function(){
      var tr = $(this).closest('tr');

      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData()

      $.confirm({
          title: 'Confirmation',
          content: 'Apa anda yakin ingin membatalkan eksekusi ini ?',
          icon: 'fas fa-exclamation-triangle',
          buttons: {
              confirm: function () {
                  location.href="{{url('transaksi/eksekusi/settle')}}"
                  + "/" + 1
                  + "/" + data[index].noEksekusi;
              },
              cancel: function () {

              }
          }
      });
  });

  $('#btn-instruksi-xml').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              var type = 'xml';
              generateXML(type);
            },
            cancel: function () {

            }
        }
    });
  });

  $('#btn-instruksi-csv').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              generateCSV();
            },
            cancel: function () {

            }
        }
    });
  });

  $('.btn-instruksi').on('click', function(){
     if($('.checkbox-instruksi').is(':checked')){
      $('#modalDownload').modal();
    }else{
      $.alert({
          title: 'Data Kosong !',
          content: 'Harap cek data terlebih dahulu',
      });
    }
  });

  function generateXML(type){
    $('.checkbox-instruksi').each(function( index ) {
      if($(this).is(':checked')){
        var checks = $(this).val();

        $.ajax({
           type:'POST',
           url:"{{ url('transaksi/eksekusi/doc-send-intruksi-all') }}",
           data: {checks:checks},
           success:function(data){
            var blob = new Blob([data], { type: 'application/'+type+'charset=utf-8;' });

            var link = document.createElement("a");
              if (link.download !== undefined) { // feature detection
                  // Browsers that support HTML5 download attribute
                  var url = URL.createObjectURL(blob);
                  link.setAttribute("href", url);
                  link.setAttribute("download", checks+'.'+type);
                  link.style.visibility = 'hidden';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
              }
            // console.log(data);
           }
        });
      }
    });
  }

  function generateCSV(){
    $('.checkbox-instruksi').each(function( index ) {
      if($(this).is(':checked')){
        var checks = $(this).val();

        $.ajax({
            type:'POST',
            url:"{{ url('transaksi/eksekusi/doc-send-intruksi-all-csv') }}",
            data: {checks:checks,type:'csv'},
            success:function(data){
              // data = data.replace(/"/g,'');
              // var csvContent = "data:text/csv;charset=utf-8,"+data;

              var blob = new Blob([data], { type: 'data:text/csv;charset=utf-8;' });

              var link = document.createElement("a");
              if (link.download !== undefined) { // feature detection
                  // Browsers that support HTML5 download attribute
                  var url = URL.createObjectURL(blob);
                  link.setAttribute("href", url);
                  link.setAttribute("download", checks+'.csv');
                  link.style.visibility = 'hidden';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
              }
            }
        });
      }
    });
  }
});
</script>
@endsection
