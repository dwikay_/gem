@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Alokasi Eksekusi</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('transaksi/eksekusi/alokasi/postAlokasi')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-2 col-form-label">No. Eksekusi</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="noEksekusi" name="noEksekusi" placeholder="" value="{{$result['noEksekusi']}}" readonly>
                      </div>
                      <label class="col-sm-2 col-form-label">Tanggal Input</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm"  readonly id="tanggalInput" name="tanggalInput" value="{{$result['entryDate']}}" placeholder="">
                        <input type="hidden" class="form-control form-control-sm" name="tanggalInput1" id="tanggalInput1" placeholder=""  value="{{$result['tradeDate']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Tanggal Trading</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" readonly id="tradeDate" value="{{$result['tradeDate']}}" name="tradeDate" placeholder="">
                        <input type="hidden" class="form-control form-control-sm" name="tradeDate1" id="tradeDate1" placeholder=""  value="{{$result['tradeDate']}}">
                      </div>
                      <label class=" col-sm-2 col-form-label">Jatuh Tempo</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" readonly id="jatuhTempo" value="{{$result['dueDate']}}" name="jatuhTempo" placeholder="">
                        <input type="hidden" class="form-control form-control-sm" name="jatuhTempo1" id="jatuhTempo1" placeholder=""  value="{{$result['dueDate']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                       <label class="col-sm-2 col-form-label">Partner Sekuritas</label>
                       <div class="col-sm-6">
                         <select readonly class="form-control form-control-sm" name="kustodi" id="kustodi">
                           <option value="">-</option>
                           @foreach($brokers as $broker)
                             <option value="{{ $broker['kodeBroker'] }}" {{ $broker['kodeBroker'] == $result['brokerPartner'] ? 'selected' : ''}}>{{ $broker['kodeBroker']." - ".$broker['namaBroker'] }}</option>
                           @endforeach
                         </select>
                       </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12 mt-30">
                    <table class="table table-sm table-striped" id="monitoring-lookup" width="100%">
                      <thead class="">
                        <tr>
                          <th class="borders"></th>
                          <th class="borders">Kode Efek</th>
                          <th class="borders">Nama Efek</th>
                          <th class="borders text-center">Matched QTY</th>
                          <th class="borders text-center">Matched Price</th>
                          <th class="borders text-center">Gross Amount</th>
                          <th class="borders text-center">Charges</th>
                          <th class="borders text-center">Net Amount</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                      <tfoot>
                        <tr>
                              <th colspan="6"></th>
                          </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                  @include('inc.button.cancel')
                  @include('inc.button.submit')
              </form>
            </div>
          </div>
        </div>
@endsection

@section('script')
  @parent
  <script>

  $(document).ready(function() {

    var tanggal = $('#tradeDate1').val();
    var tanggal1 = $('#jatuhTempo1').val();
    var tanggal2 = $('#tanggalInput1').val();

    $.ajax({
      url: "{!! url('transaksi/eksekusi/alokasi/getCreate') !!}/" + $('#noEksekusi').val(),
      data: {},
      dataType: "json",
      type: "get",
      success:function(data)
        {
          loadData(data);
        }
      });

  });

    var table = $("#monitoring-lookup").dataTable({
        columns: [
          { data: 'kodeEfek' },
          { data: 'kodeEfek' },
          { data: 'nameEfek' },
          { data: 'matchedQty' },
          { data: 'matchedPrice' },
          { data: 'grossAmount' },
          { data: 'charges' },
          { data: 'netAmount' },
        ],
      });



    function loadData(data){

      $('#monitoring-lookup').dataTable().fnDestroy();

      tableEditAlokasi = $('#monitoring-lookup').dataTable({
        data: data.eksekusiMonitoringDetailList,
        bPaginate: false,
        searching : false,
        bSort: false,
        bInfo: false,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        autoWidth: false,
        columns: [
          { data: 'kodeEfek',width:"20px"},
          { data: 'kodeEfek',width: "7%"},
          { data: 'nameEfek',width: "20%"},
          { data: 'matchedQty',width: "15%"},
          { data: 'matchedPrice',width: "15%"},
          { data: 'grossAmount',width: "15%"},
          { data: 'charges',width: "15%"},
          { data: 'netAmount',width: "15%"},
        ],
        columnDefs: [
          {
            "targets": [0],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();

              $(td)
              .append($('<button>')
                  .attr('type','button')
                  .addClass('details-control')
                  .text('+')
              )
              .append($('<input>')
                  .attr('type','hidden')
                  .val(rowData.id)
                )
            },
            orderable: false
          },
          {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','master_'+row+'_kodeEfek')
                          .attr('name','record['+row+'][kodeEfek]')
                      )

            },
          },
          {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','master_'+row+'_namaEfek')
                          .attr('name','record['+row+'][namaEfek]')
                      )

            },
          },
          {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoComma')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','master_'+row+'_matchedqty')
                          .attr('name','record['+row+'][matchedQty]')
                      )

            },
          },
          {
            "targets": [4],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoComma')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','master_'+row+'_matchedprice')
                          .attr('name','record['+row+'][matchedPrice]')
                      )
            },
          },
          {
            "targets": [5],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoComma')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','master_'+row+'_grossamount')
                          .attr('name','record['+row+'][grossAmount]')
                      )
            },
          },
          {
            "targets": [6],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoComma')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','master_'+row+'_charges')
                          .attr('name','record['+row+'][charges]')
                      )
            },
          },
          {
            "targets": [7],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoComma')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','master_'+row+'_netamount')
                          .attr('name','record['+row+'][netAmount]')
                      )
            },
          }
        ],
        drawCallback: function(settings) {
           initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }
      });
    }

    function createDetail(index){
      return $('<table>')
        .addClass('table detail-table table-striped table-bordered')
        .attr('id','detail_'+index)
            .append($('<thead>')
              .addClass('thead-light')
                .append($('<tr>')
                    .append($('<th>')
                        .addClass("text-left")
                        .text('Nomor Kontrak')
                    )
                    .append($('<th>')
                    .addClass("text-left")
                    .text('Kode Nasabah')
                    )
                    .append($('<th>')
                    .addClass("text-left")
                    .text('Nama Nasabah')
                    )
                    .append($('<th>')
                    .addClass("text-center")
                    .text('Request Qty')
                    )
                    .append($('<th>')
                    .addClass("text-center")
                    .text('Matched Qty')
                    )
                    .append($('<th>')
                    .addClass("text-center")
                    .text('Matched Price')
                    )
                    .append($('<th>')
                    .addClass("text-center")
                    .text('Gross Amount')
                    )
                    .append($('<th>')
                    .addClass("text-center")
                    .text('Charges')
                    )
                    .append($('<th>')
                    .addClass("text-center")
                    .text('Additional Charges')
                    )
                    .append($('<th>')
                    .addClass("text-center")
                    .text('Net Amount')
                    )
                    .append($('<th>')
                    .addClass("text-center")
                    .text('Trade Refference')
                    )
                )
            )
    }

    $('#monitoring-lookup tbody').on('click', '.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.api().row( tr );
        var id = tr.attr('id').split('_');

        var index = id[1];

        var data = table.fnGetData();

        var data_detail = data[index];

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            $(this).find('.details-control').text('+');
        }else {
            // Open this row
            row.child(createDetail(index)).show();

            $('#detail_'+index).dataTable({
              data: data_detail.eksekusiMonitoringAllocations,
              bPaginate: false,
              searching : false,
              bSort: true,
              bInfo: false,
              columns: [
                  { data: 'noKontrak'},
                  { data: 'kodeNasabah'},
                  { data: 'namaNasabah'},
                  { data: 'requestQty'},
                  { data: 'matchedQty'},
                  { data: 'matchedPrice'},
                  { data: 'grossAmount'},
                  { data: 'charges'},
                  { data: 'additionalCharges'},
                  { data: 'netAmount'},
                  { data: 'tradeRefference'},
              ],
              columnDefs: [
                {
                  "targets": [0],
                  "data": 0,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).addClass('text-left');
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm')
                                .val(cellData)
                                .attr('readonly','readonly')
                                .attr('id','detail_'+index+'_'+row+'_nomorKontrak')
                                .attr('name','record['+index+'][detail]['+row+'][noKontrak]')
                            )
                  },
                },
                {
                  "targets": [1],
                  "data": 0,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm')
                                .val(cellData)
                                .attr('readonly','readonly')
                                .attr('id','detail_'+index+'_'+row+'_kodeNasabah')
                                .attr('name','record['+index+'][detail]['+row+'][kodeNasabah]')
                            )
                  },
                },
                {
                  "targets": [2],
                  "data": 0,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm')
                                .val(cellData)
                                .attr('readonly','readonly')
                                .attr('id','detail_'+index+'_'+row+'_namaNasabah')
                                .attr('name','record['+index+'][detail]['+row+'][namaNasabah]')
                            )
                  },
                },
                {
                  "targets": [3],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm currencyNoComma calculated')
                                .val(cellData)
                                .attr('readonly','readonly')
                                .attr('id','detail_'+index+'_'+row+'_requestqty')
                                .attr('name','record['+index+'][detail]['+row+'][requestqty]')
                            )
                  },
                },
                {
                  "targets": [4],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm currencyNoComma calculated')
                                .val(cellData)
                                .attr('id','detail_'+index+'_'+row+'_matchedqty')
                                .attr('name','record['+index+'][detail]['+row+'][matchedQty]')
                            )
                  },
                },
                {
                  "targets": [5],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm currencyNoComma calculated')
                                .val(cellData)
                                .attr('id','detail_'+index+'_'+row+'_matchedprice')
                                .attr('name','record['+index+'][detail]['+row+'][matchedPrice]')
                            )
                  },
                },
                {
                  "targets": [6],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm currencyNoComma calculated')
                                .val(cellData)
                                .attr('id','detail_'+index+'_'+row+'_grossamount')
                                .attr('name','record['+index+'][detail]['+row+'][grossAmount]')
                            )
                  },

                },
                {
                  "targets": [7],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm currencyNoComma calculated')
                                .val(cellData)
                                .attr('id','detail_'+index+'_'+row+'_charges')
                                .attr('name','record['+index+'][detail]['+row+'][charges]')
                            )
                  },
                },
                {
                  "targets": [8],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm currencyNoComma calculated')
                                .val(cellData)
                                .attr('id','detail_'+index+'_'+row+'_additionalcharges')
                                .attr('name','record['+index+'][detail]['+row+'][additionalCharges]')
                            )
                  },
                },
                {
                  "targets": [9],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm currencyNoComma calculated')
                                .val(cellData)
                                .attr('id','detail_'+index+'_'+row+'_netamount')
                                .attr('name','record['+index+'][detail]['+row+'][netAmount]')
                            )
                  },
                },
                {
                  "targets": [10],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                                .addClass('form-control form-control-sm')
                                .val(cellData)
                                .attr('id','detail_'+index+'_'+row+'_traderefferences')
                                .attr('name','record['+index+'][detail]['+row+'][tradeRefference]')
                            )
                  },
                },
              ],
              drawCallback: function(settings) {
                 initAutoNumeric();
              },
            });

            $(this).find('.details-control').text('-');
        }
    });

    $('#monitoring-lookup').on('change', '.calculated', function(){
        var id = $(this).attr('id');
        var idArray = id.split('_');

        var masterRow = idArray[1];
        var detailRow = idArray[2]
        var columnName = idArray[3];

        var data = table.fnGetData();

        var data_detail = data[masterRow].eksekusiMonitoringAllocations;
        var total = 0;
        data_detail.forEach(function(item, index){
          // console.log(item);
          var value = AutoNumeric.getNumber('#detail_'+masterRow+'_'+index+'_'+columnName);
          total = total+value;
        });

        $('#master_'+masterRow+'_'+columnName).val(total);
    });
  </script>
@endsection
