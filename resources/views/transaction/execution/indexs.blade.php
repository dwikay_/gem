
@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title float-left">Eksekusi</h4>
              </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table width="100%" class="table table-sm table-striped" id="lookup" width="100%">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Tanggal Transaksi</th>
                        <th>Kode Transaksi</th>
                        <th>Kode Nasabah</th>
                        <th>Nama Nasabah</th>
                        <th>Tanggal Perdagangan</th>
                        <th>Tanggal Jatuh Tempo</th>
                        <th>Sekuritas</th>
                        <th>Status</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

var table = $("#lookup").dataTable({
    columns: [
      {data: 'id'},
      {data: 'tanggalTransaksi'},
      {data: 'kodeTransaksi'},
      {data: 'kodeNasabah'},
      {data: 'namaNasabah'},
      {data: 'tanggalPerdagangan'},
      {data: 'tanggalJatuhTempo'},
      {data: 'brokerPartner'},
      {data: 'status'},
      {data: 'status'},
    ],
  });

$(document).ready(function() {
  var msg = '{{Session::get('info')}}';
  var msgclass = '{{Session::get('alert')}}';
  var exist = '{{Session::has('info')}}';
  if(exist){
    swal(msgclass,"",msg)
  }

  $('#lookup').dataTable().fnDestroy()

  table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    ajax:{
      url: "{{ url('transaksi/eksekusi/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="11">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    createdRow: function ( row, data, index ) {
        $(row).attr('id','row_'+index);
    },
    columns: [
      {data: 'id',width: "30px"},
      {data: 'tanggalTransaksi',width: "100px"},
      {data: 'kodeTransaksi',width: "100px"},
      {data: 'kodeNasabah',width: "100px"},
      {data: 'namaNasabah',width: "200px"},
      {data: 'tanggalPerdagangan',width: "100px"},
      {data: 'tanggalJatuhTempo',width: "100px"},
      {data: 'brokerPartner',width: "50px"},
      {data: 'status',width: "30px"},
      {data: 'status',width: "30px"},
    ],
    "scrollX": true,
    'scrollY': '100vh',
    "scrollCollapse": true,
    'bSort': true,
    'autoWidth': false,
    'bPaginate': true,
    'searching' : true,
    columnDefs: [
      {
        "targets": [0],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();

          $(td)
          .append($('<button>')
              .attr('type','button')
              .addClass('details-control')
              .text('+')
          )
          .append($('<input>')
              .attr('type','hidden')
              .val(rowData.id)
            )
        },
        orderable: false
      },
      {
          "targets": [1,5,6],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            var dates = moment(cellData).locale('en').format('LL');
            $(td).text(dates);
          },
      },
      {
        "targets": [8],
        "data": null,
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          var status;
          var classnya;
          switch(cellData){
            case '0' : status = 'Request SubRek'; classnya = "secondary"; break;
            case '1' : status = 'Konfirmasi Efek'; classnya = "warning"; break;
            case '2' : status = 'Terima Efek'; classnya = "info text-white"; break;
            case '3' : status = 'Cair'; classnya = "success"; break;
            case '4' : status = 'Batal'; classnya = "danger"; break;
          }
          $(td).append($('<span>')
                      .addClass('badge badge-'+classnya)
                      .text(status)
                  )
        },
      },
      {
        "targets": [9],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          var warna;
          var icon;
          var title;
          var cursor;
          switch(cellData){
            case '0' : icon = 'fa fa-id-card-alt fa-xs'; warna = "btn-secondary text-white"; title="Ubah SubRek"; cursor=""; break;
            case '1' : icon = 'far fa-edit fa-xs'; warna = "btn-primary"; title="Ubah CounterPart"; cursor=""; break;
            case '2' : icon = 'fas fa-box'; warna = "btn-primary"; title="Terima Efek"; cursor=""; break;
            case '3' : icon = 'fas fa-ban'; warna = "btn-secondary text-white"; title=""; cursor="no-drop"; break;
            case '4' : icon = 'fas fa-times'; warna = "btn-danger text-white"; title="Pembatalan Pengajuan"; cursor="no-drop"; break;
          }
          $(td)
            .append($('<a>')
              .addClass('btn btn-sm btn-modify '+warna)
              .attr('title', title)
              .css('cursor', cursor)
              .append($('<span>')
                .addClass(icon)
              )
            );
        },
        orderable: false
      },
    ],
    drawCallback: function(settings) {
      initAutoNumeric();
    },
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    }
  });
});

function createDetail(index){
  return $('<table>')
    .addClass('table detail-table table-striped table-bordered')
    .attr('id','detail_'+index)
        .append($('<thead>')
          .addClass('thead-light')
            .append($('<tr>')
                .append($('<th>')
                  .addClass("text-center")
                  .text('#')
                )
                .append($('<th>')
                    .addClass("text-center")

                    .text('Kode Efek')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Jumlah Pengajuan')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Harga Pengajuan')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Jumlah Tetap')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Harga Tetap')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Total Gross')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Potongan Biaya')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Total Net')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Jumlah Alokasi')
                )
            )
        )
}
function createChild(index){
  return $('<table>')
    .addClass('table detail-table table-striped table-bordered')
    .attr('id','detailChild_'+index)
        .append($('<thead>')
          .addClass('thead-light')
            .append($('<tr>')

                .append($('<th>')
                .addClass("text-center")
                .text('No Kontrak')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Jumlah')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Total Harga')
                )
            )
        )
}

$('#lookup tbody').on('click', '.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.api().row( tr );

    var id = tr.attr('id').split('_');

    var index = id[1];

    var data = table.fnGetData();

    var data_detail = data[index].detailEksekusi;

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        $(this).find('.details-control').text('+');
    }else {
        // Open this row

        row.child( createDetail(index)).show();

        $('#detail_'+index).dataTable({
          data: data_detail,
          bPaginate: false,
          searching : false,
          bSort: true,
          bInfo: false,
          columns: [
              { data: 'stockCode' },
              { data: 'quantity' },
              { data: 'requestPrice' },
              { data: 'matchedQty' },
              { data: 'matchedPrice' },
              { data: 'grossAmount' },
              { data: 'charges' },
              { data: 'netAmount' },
              { data: 'allocationQty' },
              { data: 'allocationQtyAmount' },
          ],
          columnDefs: [
            {
              "targets": [0],
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();

                $(td)
                .append($('<button>')
                    .attr('type','button')
                    .addClass('detailsChild-control')
                    .text('+')
                )
                .append($('<input>')
                    .attr('type','hidden')
                    .val(rowData.id)
                  )
              },
              orderable: false
            },
            {
              "targets": [1,2,3,4,5,6,7,8,9],
              "data": 0,
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();
                $(td).append($('<span>')
                            .addClass('currencyNoComma')
                            .text(cellData)
                        )
              },
            },
          ],
          createdRow: function ( row, data, index ) {
              $(row).attr('id','table_'+index);
          },
          drawCallback: function(settings) {
             initAutoNumeric();
          },
        });

        $(this).find('.details-control').text('-');
    }
});

//child

$('#lookup tbody').on('click', '.detailsChild-control', function () {
    var tr = $(this).closest('tr');
    var row = table.api().row( tr );

    var id = tr.attr('id').split('_');

    var index = id[1];

    var data = tablea.fnGetData();

    var data_detail = data[index].alokasiEksekusi;
    console.log(data_detail);

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        $(this).find('.detailsChild-control').text('+');
    }else {
        // Open this row

        row.child( createChild(index)).show();

        $('#detailsChild'+index).dataTable({
          data: data_detail,
          bPaginate: false,
          searching : false,
          bSort: true,
          bInfo: false,
          columns: [
              { data: 'noKontrak' },
              { data: 'qty' },
              { data: 'amount' },
          ],
          columnDefs: [
            {
              "targets": [1,2],
              "data": 0,
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();
                $(td).append($('<span>')
                            .addClass('currencyNoComma')
                            .text(cellData)
                        )
              },
            },
          ],
          drawCallback: function(settings) {
             initAutoNumeric();
          },
        });

        $(this).find('.detailsChild-control').text('-');
    }
});

$('.table').on('click','.btn-modify', function(){

    var tr = $(this).closest('tr');
    var row = table.api().row( tr );
    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData();

    var url;
    switch(data[index].status){
      case '0' : url = '{{url('dataStatis/nasabah/reqSubrek/')}}'; routing = data[index].kodeNasabah;break;
      case '1' : url = '{{url('transaksi/loan-request/edit')}}'; routing = data[index].noPengajuan; break;
      case '2' : url = '{{url('transaksi/loan-request/terimaEfek')}}'; routing = data[index].noPengajuan; break;
      case '3' : url = '#'; break;
      case '4' : url = '#'; break;
    }
    location.href= url + "/" + routing;
});

</script>
@endsection
