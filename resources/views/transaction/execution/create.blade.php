@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Eksekusi</h4>
              <p class="card-description">

              </p><br>
              <form class="forms-sample" action="{{url('transaksi/eksekusi/updateEksekusi/'.$sekuritas.'/'.$noKontrak.'/'.$tipeEfek)}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">No Kontrak</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" name="noKontrak" id="noKontrak" placeholder="" readonly value="{{$noKontrak}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Tanggal Perdagangan</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" name="tradeDate" id="tradeDate" placeholder=""  value="{{$eksekusi['tradeDate']}}">
                        <input type="hidden" class="form-control form-control-sm" name="tradeDate1" id="tradeDate1" placeholder=""  value="{{$eksekusi['tradeDate']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Jatuh Tempo</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm"  id="jatuhTempo" name="jatuhTempo" placeholder=""  value="{{$eksekusi['dueDate']}}">
                        <input type="hidden" class="form-control form-control-sm"  id="jatuhTempo1" name="jatuhTempo1" placeholder=""  value="{{$eksekusi['dueDate']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Tipe Efek</label>
                      <div class="col-sm-2">
                        @if($eksekusi['tipeEfek']=="S")
                        <input type="text" class="form-control form-control-sm" name="tipeEfek" id="tipeEfek" placeholder="" value="Saham">
                        <input type="hidden" class="form-control form-control-sm" name="tipeEfek1" id="tipeEfek1" placeholder="" value="S">
                        @else
                        <input type="text" class="form-control form-control-sm" name="tipeEfek" id="tipeEfek" placeholder="" value="Obligasi">
                        <input type="hidden" class="form-control form-control-sm" name="tipeEfek1" id="tipeEfek1" placeholder="" value="O">
                        @endif

                      </div>
                      <label class=" col-sm-3 col-form-label">Sekuritas</label>
                      <div class="col-sm-3">
                        <select class="form-control form-control-sm" name="broker" id="broker" >
                          <option value="">-</option>
                          @foreach($brokers as $broker)
                            <option value="{{ $broker['kodeBroker'] }}" {{$eksekusi['brokerPartner'] == $broker['kodeBroker'] ? 'selected' : '' }}>{{ $broker['kodeBroker']." - ".$broker['namaBroker'] }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <br><table id="lookup" class="table table-sm table-striped" style="width: 100%">
                      <thead>
                        <th>Kode Efek</th>
                        <th>Qty On Hand</th>
                        <th>Qty Available For Execution</th>
                        <th>Qty Request</th>
                      </thead>
                    </table>
                  </div>
                </div><br>
                  @include('inc.button.cancel')
                  @include('inc.button.submit')
                  <!-- @include('inc.button.instruksi') -->
              </form>
            </div>
          </div>
        </div>
@endsection

@section('script')
  @parent
  <script>

  function getDate(){
      var yest = new Date(new Date() - 24 * 60 * 60 * 1000);
      var month = yest.getMonth();
      var year = yest.getFullYear();
      var date = yest.getDate();
      var startDate = new Date(year, month, date);
      $('#tradeDate').datepicker({
          locale: 'id',
          format: 'MM dd yyyy',
          autoclose: true
      });
      $('#tradeDate1').datepicker({
          locale: 'id',
          format: 'yyyy-mm-dd',
          autoclose: true
      });
      $('#jatuhTempo').datepicker({
          locale: 'id',
          format: 'MM dd yyyy',
          autoclose: true
      });
      $('#jatuhTempo1').datepicker({
          locale: 'id',
          format: 'yyyy-mm-dd',
          autoclose: true
      });
      $('#tradeDate').datepicker('setDate', startDate);
      $('#tradeDate1').datepicker('setDate', startDate);
      $('#jatuhTempo').datepicker('setDate', startDate);
      $('#jatuhTempo1').datepicker('setDate', startDate);
  }


    function convertMoment(date){
      var dates = moment(date).locale('id').format('YYYY-MM-DD');
      $('#tradeDate1').val(dates);
      // return dates;
    }
    function convertMoment1(date){
      var dates = moment(date).locale('id').format('YYYY-MM-DD');
      $('#jatuhTempo1').val(dates);
      // return dates;
    }

    $('#tradeDate').on('change', function(){
        convertMoment($('#tradeDate').val());
    });
    $('#jatuhTempo').on('change', function(){
        convertMoment1($('#jatuhTempo').val());
    });

  $(document).ready(function() {
    lods();
    getDate();
    var tanggal = $('#tradeDate1').val();
    var tanggal1 = $('#jatuhTempo1').val();
  })

  function lods(){
    var type_efek = $('#tipeEfek1').val();
    var broker = $('#broker').val();
    $.ajax({
      url: "{!! url('monitoring/transaksi/eksekusiGet') !!}/" + $('#noKontrak').val(),
      dataType: "json",
      type: "GET",
      data: {},
      success:function(data)
        {
          // console.log(data.transaksiGadaiEfekDetailList);
          loadData(data.transaksiGadaiEfekDetailList);
        }
      });
  }


    var tableEditAlokasi = $('#lookup').DataTable({
        bPaginate: false,
        searching : false,
        bSort: false,
        bInfo: false,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        columns: [
          { data: 'kodeEfek' },
          { data: 'qtyOnhand' },
          { data: 'qtyOnhand' },
          { data: 'qtyRequest' }
        ]
    });

    function loadData(data){
      console.log(data);
      $('#lookup').dataTable().fnDestroy();

      tableEditAlokasi = $('#lookup').dataTable({
        data: data,
        bPaginate: false,
        searching : false,
        bSort: false,
        bInfo: false,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        autoWidth: false,
        columns: [
          { data: 'kodeEfek' },
          { data: 'qtyOnhand' },
          { data: 'qtyOnhand' },
          { data: 'qtyRequest' }
        ],
        createdRow: function( row, data, dataIndex ) {
          $(row).attr('id', 'tr_'+dataIndex);
        },
        columnDefs: [
          {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              console.log(cellData);
                      $(td).append($('<input>')
                                  .addClass('form-control form-control-sm')
                                  .val(cellData)
                                  .attr('readonly','readonly')
                                  .attr('name','kodeEfek['+row+']')
                                  .attr('id','detail_'+row+'_kodeEfek')
                              )
            },
          },
          {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
                      $(td).append($('<input>')
                                  .addClass('form-control form-control-sm')
                                  .val(cellData)
                                  .attr('readonly','readonly')
                                  .attr('name','qtyOnhand['+row+']')
                                  .attr('id','detail_'+row+'_qtyOnHand')
                              )
            },
          },
          {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              var hasil = rowData.qtyOnhand - rowData.qtyWithdraw - rowData.qtyEksekusi - rowData.qtyDlv;
                      $(td).append($('<input>')
                                  .addClass('form-control form-control-sm')
                                  .val(hasil)
                                  .attr('readonly','readonly')
                                  .attr('name','qtyAvail['+row+']')
                                  .attr('id','detail_'+row+'_qtyAvail')
                              )
            },
          },
          {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoComma qtyReq inputForm')
                          .val("0")
                          .attr('name','settle['+row+']')
                          .attr('id','detail_'+row+'_qtyRequest')
                      )
            },
          }
        ],
        drawCallback: function(settings) {
           initAutoNumeric();
        },
      });
    }

  function checkform() {
    // get all the inputs within the submitted form
    var inputs = $('input').val();

    for (var i = 0; i < inputs.length; i++) {
        // only validate the inputs that have the required attribute
        if(inputs[i].hasAttribute("required")){
            if(inputs[i].value == ""){
                // found an empty field that is required
                alert("Please fill all required fields");
                return false;
            }
        }
    }
    return true;
}

    $('#lookup').on('change','.qtyReq', function(){

        var tr = $(this).closest('tr');
        // var row = table.api().row( tr );
        var id = tr.attr('id').split('_');
        var index = id[1];
        // var data = table.fnGetData();

        var qtyAvail = $('#detail_'+index+'_qtyAvail').val();
        var qtyReq = $('#detail_'+index+'_qtyRequest').val();


        var hslQtyAvail = qtyAvail.replace('.','');
        var hslQtyReq = qtyReq.replace('.','');

        var hslQtyAvail2 = parseInt(hslQtyAvail);
        var hslQtyReq2 = parseInt(hslQtyReq);

        console.log(hslQtyReq2);
        console.log(" - ");
        console.log(hslQtyAvail2);

        if(index==0){
          if(hslQtyReq2>hslQtyAvail2){

            $.confirm({
            title: 'Information',
            content: 'Pengajuan Efek lebih besar dari Stok tersedia',
            buttons: {
                ok: function () {
                },
            }
        });

        }
      } else{
        if(hslQtyReq2>hslQtyAvail2){

        $.confirm({
        title: 'Information',
        content: 'Pengajuan Efek lebih besar dari Stok tersedia',
        buttons: {
            ok: function () {
            },
        }
      });

    } else{
      console.log('lolos')
      }
    }

    });



  </script>
@endsection
