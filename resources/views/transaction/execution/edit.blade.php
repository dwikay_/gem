@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Eksekusi</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('transaksi/eksekusi/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Kode Transaksi Eksekusi</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" placeholder="" disabled="disabled" value="{{$eksekusi['noEksekusi']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Tanggal Eksekusi</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datepicker" placeholder="" disabled="disabled" value="{{$eksekusi['tanggalEksekusi']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Tanggal Jatuh Tempo</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datepicker" placeholder="" disabled="disabled" value="{{$eksekusi['tanggalJatuhTempo']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Broker Partner</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" placeholder="" disabled="disabled" value="{{$eksekusi['kodeBroker'].' - '.$eksekusi['namaBroker']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <table id="table-edit" class="table table-sm table-striped" style="width: 100%">
                      <thead>
                        <th>

                        </th>
                        <th>
                          Stock Code
                        </th>
                        <th>
                          Request Quantity (lot)
                        </th>
                        <th>
                          Request Price
                        </th>
                        <th>
                          Matched Quantity (lot)
                        </th>
                        <th>
                          Matched Price
                        </th>
                        <th>
                          Gross Amount
                        </th>
                        <th>
                          Charges
                        </th>
                        <th>
                          Net Amount
                        </th>
                        <th>
                          Allocation Quantity (lot)
                        </th>
                        <th>
                          Allocation Amount
                        </th>
                        <th>
                        </th>
                      </thead>
                    </table>
                  </div>
                </div>
                  @include('inc.button.cancel')
                  @include('inc.button.submit')
              </form>
            </div>
          </div>
        </div>
@endsection

@section('modal')
  @include('modals.modalAlokasiEksekusi')
  @include('modals.modalContract',['customScript'=>true])
@endsection

@section('script')
  @parent
  <script>
    var no = 0;

    var tableEdit = $("#table-edit").dataTable({
      processing: true,
      serverSide: true,
      ajax:{
        url: "{{ url('transaksi/eksekusi/getDetail') }}",
        dataType: "json",
        type: "GET",
        error: function(){  // error handling
          $(".lookup-error").html("");
          $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="10">No data found in the server</th></tr></tbody>');
          $("#lookup_processing").css("display","none");

        }
      },
      'bSort': false,
      'bPaginate': false,
      "scrollX": true,
      'scrollY': '100vh',
      "scrollCollapse": true,
      'searching' : false,
      'autoWidth': false,
      createdRow: function ( row, data, index ) {
          $(row).attr('id','row_'+index);
      },
      columns: [
        {data: 'id',width: "80px"},
        {data: 'stockCode',width: "80px"},
        {data: 'requestQuantity',width: "200px"},
        {data: 'requestPrice',width: "150px"},
        {data: 'matchedQuantity',width: "200px"},
        {data: 'matchedPrice',width: "150px"},
        {data: 'grossAmount',width: "250px"},
        {data: 'charges',width: "150px"},
        {data: 'netAmount',width: "200px"},
        {data: 'allocationQuantity',width: "200px"},
        {data: 'allocationAmount',width: "250px"},
        {data: 'id',width: "80px"}
      ],
      columnDefs: [
        {
          "targets": [0],
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();

            $(td)
            .append($('<button>')
                .attr('type','button')
                .addClass('details-control')
                .text('+')
            )
            .append($('<input>')
                .attr('type','hidden')
                .val(rowData.id)
              )
          },
          orderable: false
        },
        {
          "targets": [1],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm')
                        .val(cellData)
                        .attr('readonly','readonly')
                        .attr('id','detail_'+row+'_stockcode')
                    )
          },
        },
        {
          "targets": [2],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val(cellData)
                        .attr('id','detail_'+row+'_requestQuantity')
                    )
          },
        },
        {
          "targets": [3],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val(cellData)
                        .attr('id','detail_'+row+'_requestPrice')
                    )
          },
        },
        {
          "targets": [4],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val(cellData)
                        .attr('id','detail_'+row+'_matchedQuantity')
                    )
          },
        },
        {
          "targets": [5],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val(cellData)
                        .attr('id','detail_'+row+'_matchedPrice')
                    )
          },
        },
        {
          "targets": [6],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val(rowData.matchedPrice * rowData.matchedQuantity)
                        .attr('id','detail_'+row+'_grossAmount')
                    )
          },
        },
        {
          "targets": [7],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val(cellData)
                        .attr('id','detail_'+row+'_charges')
                    )
          },
        },
        {
          "targets": [8],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val((rowData.matchedPrice * rowData.matchedQuantity) - rowData.charges)
                        .attr('id','detail_'+row+'_netAmount')
                    )
          },
        },
        {
          "targets": [9],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val(cellData)
                        .attr('id','detail_'+row+'_alocationQuantity')
                    )
          },
        },
        {
          "targets": [10],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val(cellData)
                        .attr('id','detail_'+row+'_allocationAmount')
                    )
          },
        },
        {
          "targets": [11],
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td)
              .append($('<a>')
                .addClass('btn btn-primary btn-sm btn-modify')
                .attr('title','Add Detail')
                .append($('<span>')
                  .addClass('far fa-edit fa-xs')
                )
              );
          },
          orderable: false
        },
      ],
      drawCallback: function(settings) {
        initAutoNumeric();
      },
    });

    function createDetail(index){
      return $('<table>')
          .addClass('table detail-table table-striped')
          .attr('id','detail_'+index)
              .append($('<thead>')
                .addClass('thead-light')
                  .append($('<tr>')
                      .append($('<th>')
                          .text('Contract Code')
                      )
                      .append($('<th>')
                          .text('Quantity')
                      )
                      .append($('<th>')
                          .text('Amount')
                      )
                  )
              )
    }

    $('#table-edit tbody').on('click', '.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tableEdit.api().row( tr );

        var id = tr.attr('id').split('_');

        var index = id[1];

        var data = tableEdit.fnGetData();

        var dataAlokasi = data[index].alokasi;

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();

            $(this).find('.details-control').text('+');
        }else {
            // Open this row

            row.child( createDetail(index)).show();

            $('#detail_'+index).dataTable({
              data: dataAlokasi,
              bPaginate: false,
              searching : false,
              bSort: false,
              bInfo: false,
              columns: [
                  { data: 'contractCode' },
                  { data: 'quantity' },
                  { data: 'amount' },
              ],
              columnDefs: [
                {
                  "targets": [0],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                        .addClass('form-control form-control-sm')
                        .val(cellData)
                        .attr('readonly','readonly')
                    );
                  }
                },
                {
                  "targets": [1,2],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<input>')
                        .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                        .val(cellData)
                    );
                  },
                }
              ],
              drawCallback: function(settings) {
                 initAutoNumeric();
              },
            });

            $(this).find('.details-control').text('-');
        }
    } );

    $('#table-edit').on('click','.btn-modify',function(){
      $('#modal-alokasi-eksekusi').modal();
      var tr = $(this).closest('tr');
      var row = tableEdit.api().row( tr );

      var id = tr.attr('id').split('_');

      var index = id[1];

      var data = tableEdit.fnGetData();

      loadDataEdit(data[index]);
    })
  </script>
@endsection
