@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Pengembalian Agunan</h4>
              <p class="card-description">
                Input Form
              </p>
              <form id="fpro" class="forms-sample" action="{{url('transaksi/stock-withdraw/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">No Kontrak</label>
                      <div class="col-sm-2 input-group input-group-sm">
                        <input type="text" id="noKontrak" name="noKontrak" class="form-control form-control-sm no-border" required="required">
                        <div class="input-group-append">
                            <button class="btn btn-outline-info btn-search-contract" type="button"><i class="fas fa-search"></i></button>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Kode Nasabah</label>
                      <div class="col-sm-2">
                        <input type="text" id="kodeNasabah" class="form-control form-control-sm" placeholder="" disabled="disabled" value="">
                      </div>
                      <label class=" col-sm-2 col-form-label">Nama Nasabah</label>
                      <div class="col-sm-6">
                        <input type="text" id="namaNasabah" class="form-control form-control-sm" placeholder="" disabled="disabled" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Total Kewajiban</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoComma" id="totalKewajiban" name="totalKewajiban" placeholder="" disabled="disabled">
                      </div>
                      <label class="col-sm-2 col-form-label">Total Agunan</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoComma" id="totalAgunan" name="totalAgunan" placeholder="" disabled="disabled">
                      </div>
                      <label class="col-sm-2 col-form-label">Rasio pinjaman</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm percent" id="rasioPinjaman" name="rasioPinjaman" placeholder="" disabled="disabled">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Status</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm " id="status" name="status" placeholder="" disabled="disabled">
                      </div>
                      <label class="col-sm-2 col-form-label">Topup Cash</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoComma" id="topupCash" name="topupCash" placeholder="" disabled="disabled">
                      </div>
                      <label class="col-sm-2 col-form-label">Topup Saham</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoComma" id="topupSaham" name="topupSaham" placeholder="" disabled="disabled">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Tgl Serah Efek</label>
                      <div class="col-sm-2">
                        <input type="text" id="tglPengajuan" name="tglPengajuan" class="form-control form-control-sm datepicker" placeholder="" value="">
                      </div>
                      <label class=" col-sm-2 col-form-label">Kustodi</label>
                      <div class="col-sm-6">
                        <select class="form-control form-control-sm" name="kustodi" id="kustodi" required="required">
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12" style="margin-top:20px">
                    <!-- <button type="button" class="btn btn-sm btn-success float-right" id="btn-add-record"><i class="fas fa-plus"></i> Saham</button> -->
                  </div>
                  <div class="col-sm-12" style="margin-top:2%">
                    <table id="table-add" class="table table-sm table-striped table-bordered" style="width: 100%">
                      <thead>
                        <th width="20%">Kode Saham</th>
                        <th width="30%" class="text-center">Qty On Hand (Lembar)</th>
                        <th width="30%" class="text-center">Stok Tersedia (Lembar)</th>
                        <th width="40%" class="text-center">Jumlah Pengambilan (Lembar)</th>
                      </thead>
                    </table>
                  </div>
                </div><br>
                  @include('inc.button.cancel')
                  @include('inc.button.btnsimpan')
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
  @parent
  <script>
    var dataEfek = [];
    var no = 0;

    $( document ).ready(function(){

    });

    $("#noKontrak").keypress(function (e) {
      if (e.which == 13) {
        return false;
      }
    });

    var tableEditAlokasi = $('#table-add').DataTable({
        bPaginate: false,
        searching : false,
        bSort: false,
        bInfo: false,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        columns: [
            { data: 'kodeEfek' },
            { data: 'efekQty' },
            { data: 'qtyOnhand' },
            { data: 'kodeEfek' },
        ]
    });

    function loadData(data){

      console.log(data);

      $('#table-add').dataTable().fnDestroy();

      tableEditAlokasi = $('#table-add').dataTable({
        data: data,
        bPaginate: false,
        searching : false,
        bSort: false,
        bInfo: false,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        autoWidth: false,
        columns: [
          { data: 'kodeEfek' },
          { data: 'efekQty' },
          { data: 'qtyOnhand' },
          { data: 'kodeEfek' }
        ],
        createdRow: function( row, data, dataIndex ) {
          $(row).attr('id', 'tr_'+dataIndex);
        },
        columnDefs: [
          {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','detail_'+row+'_kodeEfek-input')
                      );
                      $(td).append($('<input>')
                                  .addClass('form-control form-control-sm')
                                  .val(cellData)
                                  .css('display','none')
                                  .attr('name','kodeEfek['+row+']')
                                  .attr('id','detail_'+row+'_kodeEfek')
                              )
            },
          },
          {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                          .val(cellData)
                          .attr('id','detail_'+row+'_onHand')
                      )
            },
          },
          {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              var qtyOnhand = rowData.qtyOnhand - rowData.qtyWithdraw - rowData.qtyEksekusi - rowData.qtyDlv;
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                          .val(qtyOnhand)
                          .attr('id','detail_'+row+'_available')
                          .attr('name','detail_'+row+'_available')
                      )
            },
          },
          {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoComma qtyReq')
                          .val(0)
                          .attr('name','qtyRequest['+row+']')
                          .attr('id','detail_'+row+'_qtyRequest')
                      )
            },
          }
        ],
        drawCallback: function(settings) {
           initAutoNumeric();
        },
      });
    }

    function loadStock(noKontrak){
      console.log(noKontrak);
      // alert('{{ url('transaksi/stock-withdraw/getDetail2') }}/' + noKontrak)
      $.ajax({
          url : '{{ url('transaksi/stock-withdraw/getDetail2') }}/' + noKontrak,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataEfek = data.transaksiGadaiEfekDetailList;
             console.log(dataEfek);
             loadData(dataEfek);
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
              console.log(errorMsg);
          }
      });
    }

    $('#btnsimpan').on('click', function(){
        submitForm();
    });

  function submitForm(){

    var str = $('form').serialize();
    console.log(str)
    $.ajax({
      url: "{!! url('transaksi/stock-withdraw/store') !!}?",
      data: str,
      dataType: "json",
      type: "post",
      success:function(data)
        {
          console.log(data[1]);
          if(data[0]!=200){

            $.confirm({
            title: 'Information',
            content: data[1]['message'],
            buttons: {
                        ok: function () {},
                      }
            });

          }else{
            location.href="{{url('transaksi/stock-withdraw')}}";
          }
          console.log(data);
        },
        error: function (jqXHR, textStatus, errorThrown){
            var errorMsg = 'Ajax request failed table with: ' + errorThrown;
            console.log(errorMsg);
        }
      });
}

    $('.table').on('change','.qtyReq', function(){

        var tr = $(this).closest('tr');
        var id = tr.attr('id').split('_');
        var index = id[1];

        var qtyAvail = $('#detail_'+index+'_available').val();
        var qtyReq = $('#detail_'+index+'_qtyRequest').val();

        var hslQtyAvail = qtyAvail.replace('.','');
        var hslQtyReq = qtyReq.replace('.','');

        var hslQtyAvail2 = parseInt(hslQtyAvail);
        var hslQtyReq2 = parseInt(hslQtyReq);

        console.log(hslQtyReq2);
        console.log(" - ");
        console.log(hslQtyAvail2);

        if(index==0){
          if(hslQtyReq2>hslQtyAvail2){

            $.confirm({
            title: 'Information',
            content: 'Pengajuan Efek lebih besar dari Stok tersedia',
            buttons: {
                ok: function () {
                },
            }
        });

        }
      } else{
        if(hslQtyReq2>hslQtyAvail2){

        $.confirm({
        title: 'Information',
        content: 'Pengajuan Efek lebih besar dari Stok tersedia',
        buttons: {
            ok: function () {
            },
        }
      });

    } else{
      console.log('lolos')
      }
    }


        // loadData(data[index]);
    });

    $('.btn-search-contract').on('click',function(){

            if($('#noKontrak').val()==""){
                $.alert({
                    title: 'Warning',
                    content: 'Nomor Pengajuan tidak boleh kosong',
                });
            }else{
              var noPengajuan = $('#noKontrak').val();
              $.ajax({
                  url: "{!! url('transaksi/check-pengajuan-withdraw') !!}/" + noPengajuan ,
                  data: {},
                  dataType: "json",
                  type: "get",
              success:function(data)
                {
                  if(data[0]==400||data[0]=="400"){

                    $.alert({
                        title: 'Error',
                        content: 'Nomor Kontrak tidak ditemukan',
                    });
                    $('#noKontrak').val("");

                  }else{
                    $('#kodeNasabah').val(data[1]["kodeNasabah"]);
                    $('#namaNasabah').val(data[1]["namaNasabah"]);
                    $('#totalKewajiban').val(data[1]["nilaiKewajiban"]);
                    $('#totalAgunan').val(data[1]["totalCollateral"]);
                    $('#rasioPinjaman').val(data[1]["ratioPinjaman"]*100);
                    $('#topupCash').val(data[1]["topUpCash"]);
                    $('#topupSaham').val(data[1]["topUpCollateral"]);

                    // $('#kustodi')
                    // .append($('<option>')
                    //     .attr('value','ABNA1')
                    //     .html("ABNA1 - THE ROYAL BANK OF SCOTLAND N.V.")
                    // ).append($('<option>')
                    //   .attr('value','AD001')
                    //   .html("AD001 - PT OSO SECURITIES"));

                    $('#kustodi')
                    @foreach($brokers as $broker)
                      .append($('<option>')
                          .attr('value','{{ $broker['kodeCustodi'] }}')
                          .html("{{ $broker['kodeCustodi']." - ".$broker['namaCustodi'] }}")
                          @if($broker['kodeCustodi'])
                            .attr('selected','selected')
                          @endif
                      )
                    @endforeach

                    if(data[1]["status"]=="N"){
                      $('#status').val("Normal");
                      $('#status').removeClass("bg-warning");
                      $('#status').removeClass("bg-danger");
                      $('#status').addClass("bg-success");
                      $('#status').addClass("text-white");
                    }
                    else if(data[1]["status"]=="MC"){
                      $('#status').val("Topup");
                      $('#status').removeClass("bg-success");
                      $('#status').removeClass("bg-danger");
                      $('#status').removeClass("text-white");
                      $('#status').addClass("bg-warning");
                      $('#status').addClass("text-black");
                    }
                    else{
                      $('#status').removeClass("bg-success");
                      $('#status').removeClass("bg-warning");
                      $('#status').val("Eksekusi");
                      $('#status').addClass("bg-danger");
                      $('#status').addClass("text-white");
                    }

                    loadStock(noPengajuan);
                  }
                }
              });
            }
        })
  </script>
@endsection
