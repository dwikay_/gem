@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Pelunasan Pinjaman</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('transaksi/stock-withdraw/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">No Kontrak</label>
                      <div class="col-sm-2 input-group input-group-sm">
                        <input type="text" id="noKontrak" name="noKontrak" class="form-control form-control-sm no-border" required="required">
                        <div class="input-group-append">
                            <button class="btn btn-outline-info btn-search-contract" type="button"><i class="fas fa-search"></i></button>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Kode Nasabah</label>
                      <div class="col-sm-2">
                        <input type="text" id="kodeNasabah" class="form-control form-control-sm" placeholder="" disabled="disabled" value="">
                      </div>
                      <label class=" col-sm-2 col-form-label">Nama Nasabah</label>
                      <div class="col-sm-6">
                        <input type="text" id="namaNasabah" class="form-control form-control-sm" placeholder="" disabled="disabled" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Total Kewajiban</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoComma" id="totalKewajiban" name="totalKewajiban" placeholder="" disabled="disabled">
                      </div>
                      <label class="col-sm-2 col-form-label">Total Agunan</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoComma" id="totalAgunan" name="totalAgunan" placeholder="" disabled="disabled">
                      </div>
                      <label class="col-sm-2 col-form-label">Rasio pinjaman</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm percent" id="rasioPinjaman" name="rasioPinjaman" placeholder="" disabled="disabled">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Status</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm " id="status" name="status" placeholder="" disabled="disabled">
                      </div>
                      <label class="col-sm-2 col-form-label">Topup Cash</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoComma" id="topupCash" name="topupCash" placeholder="" disabled="disabled">
                      </div>
                      <label class="col-sm-2 col-form-label">Topup Saham</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoComma" id="topupSaham" name="topupSaham" placeholder="" disabled="disabled">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Tgl Serah Efek</label>
                      <div class="col-sm-2">
                        <input type="text" id="tglPengajuan" name="tglPengajuan" class="form-control form-control-sm datepicker" placeholder="" value="">
                      </div>
                      <label class=" col-sm-2 col-form-label">Kustodi</label>
                      <div class="col-sm-6">
                        <select class="form-control form-control-sm" name="kustodi" id="kustodi">
                          <option value="">-</option>
                          @foreach($brokers as $broker)
                            <option value="{{ $broker['kodeCustodi'] }}">{{ $broker['kodeCustodi']." - ".$broker['namaCustodi'] }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12" style="margin-top:20px">
                    <button type="button" class="btn btn-sm btn-success float-right" id="btn-add-record"><i class="fas fa-plus"></i> Saham</button>
                  </div>
                  <div class="col-sm-12" style="margin-top:2%">
                    <table id="table-add" class="table table-sm table-striped" style="width: 100%">
                      <thead>
                        <th width="40%">Kode Saham</th>
                        <th width="50%">Stok Tersedia (Lembar)</th>
                        <th width="50%">Jumlah Pengambilan (Lembar)</th>
                        <th width="50px"></th>
                      </thead>
                    </table>
                  </div>
                </div>
                  @include('inc.button.cancel')
                  @include('inc.button.submit')
              </form>
            </div>
          </div>
        </div>
@endsection

@section('modal')
    <!-- @include('modals.modalCustomer') -->
    @include('modals.modalContract')
@endsection

@section('script')
  @parent
  <script>
    var dataEfek = [];
    var no = 0;

    $( document ).ready(function(){
        loadStock();
    });

    $("#noKontrak").keypress(function (e) {
      if (e.which == 13) {
        return false;
      }
    });

    var tableadd = $('#table-add').DataTable({
        autoWidth: false,
        'bSort': false,
        'bPaginate': false,
        "scrollX": true,
        'scrollY': '100vh',
        "scrollCollapse": true,
        'searching' : false,
        'fnInitComplete': function(){
            $('#table-add').parent().css('max-height','calc(100vh - 500px)').css('height','calc(100vh - 500px)');
        },
        'bInfo': false,
        'drawCallback': function(){

        }
    });

    $('#btn-add-record').on('click',function(){
      if($('#noKontrak').val().length != 0){
        addRecord();
      }else{
        if($('#noKontrak').val().length == 0){
          $.alert({
              title: 'Warning',
              content: 'Harap mencari data pengajuan terlebih dahulu',
          });
        }
      }
    })

    $('#table-add').on('change', '.stock-code', function() {
      $(this).closest(".quantity").val(0);

      refreshStockCodeList();
    });

    $('#table-add').on('click', '.btn-remove-record', function() {
            var elem = $(this).parents('tr');

            $.confirm({
                title: 'Konfirmasi',
                content: 'Hapus data ini?',
                buttons: {
                    cancel: function () {
                    },
                    confirm: function () {
                        tableadd.row(elem).remove().draw( true );
                        refreshStockCodeList();
                    },
                }
            });
        });

    $('#modal-customer').find('.btn-ok').on('click',function(){
        var status = 1;
        if(tableadd.data().any()){
            status = 0;

            $.confirm({
                title: 'Confirmation',
                content: "You'll lose your previous data! are you sure to continue this process?",
                buttons: {
                    confirm: function () {
                        status = 1;
                    },
                    cancel: function () {
                        status = 0;
                    },
                }
            });
        }
        if(status == 1){
          if(!$('#modal-customer').find('.table-success').length){
              $.alert({
                  title: 'Warning',
                  content: 'Please choose a customer!',
              });
          }else{
            setCustomer();
            $('#modal-customer').modal('hide');
          }
        }
    })

    $('#modal-contract').find('.btn-ok').on('click',function(){
        var status = 1;
        if(tableadd.data().any()){
            status = 0;

            $.confirm({
                title: 'Confirmation',
                content: "You'll lose your previous data! are you sure to continue this process?",
                buttons: {
                    confirm: function () {
                        status = 1;
                    },
                    cancel: function () {
                        status = 0;
                    },
                }
            });
        }
        if(status == 1){
          if(!$('#modal-contract').find('.table-success').length){
              $.alert({
                  title: 'Warning',
                  content: 'Please choose a contract!',
              });
          }else{
            setContract();
            $('#modal-contract').modal('hide');
          }
        }
    })

    function addRecord(){
      var row = $('<tr>')
        .attr('id','row_'+no)
        .append($('<td>')
            .append($('<select>')
                .attr('id','stock_'+no+'_stockcode')
                .attr('name',"kodeEfek[]")
                .addClass('form-control form-control-sm form-control-chosen stock-code')
            )
        )
        .append($('<td>')
            .append($('<input>')
                .attr('id','stock_'+no+'_quantity')
                .attr('name',"qtyEfek[]")
                .addClass('form-control form-control-sm currencyNoComma quantity')
                .attr('type','text')
                .val(0)
            )
        )
        .append($('<td>')
            .append($('<input>')
                .attr('id','stock_'+no+'_quantity')
                .attr('name',"qtyEfek[]")
                .addClass('form-control form-control-sm currencyNoComma quantity')
                .attr('type','text')
                .val(0)
            )
        )
        .append($('<td>')
            .append($('<btn>')
              .addClass('btn btn-sm btn-danger btn-remove-record')
              .append($('<span>')
                .addClass('fas fa-times')
              )
            )
        );

      tableadd.row.add(row).draw( true );
      initAutoNumeric();

      var stockIdList = [];

      $.each($(".stock-code option:selected"), function(){
          stockIdList.push(parseInt($(this).val()));
      });

      dataEfek.forEach(function(currentValue,index){
          if($.inArray(currentValue.id, stockIdList)  === -1 ){
            $('#stock_'+no+'_stockcode').append($('<option>', {
                value: currentValue.id,
                text : currentValue.kodeEfek
            }));
          }
      })

      refreshStockCodeList();

      no++;
    }

    function refreshStockCodeList(){
        var stockIdList = [];

        $.each($(".stock-code option:selected"), function(){
            stockIdList.push($(this).val());
        });

        $.each($(".stock-code"), function(){
            var stockid = $(this).val()

            $(this)
                .find('option')
                .remove()

            var id = $(this).attr('id');

            dataEfek.forEach(function(currentValue,index){
                $('#'+id).append($('<option>', {
                    value: currentValue.kodeEfek,
                    text : currentValue.kodeEfek + " - " + currentValue.namaEfek
                }));
            });

            $(this).val(stockid);

            stockIdList.forEach(function(currentValue,index){
                if(stockid != currentValue){
                    $("#"+id+" option[value='"+currentValue+"']").remove();
                }
            });
        });
    }

    function setCustomer(){
      tableCustomerLookup.rows().every(function() {
          if(this.nodes().to$().hasClass('table-success')){
            $('#idNasabah').val(this.data().id);
            $('#kodeNasabah').val(this.data().kodeNasabah);
            $('#namaNasabah').val(this.data().namaNasabah);

            $('#noKontrak').val('');
            $('#nilaiKewajiban').val('');
            $('#nilaiAgunan').val('');
            $('#rasio').val('');
            $('#rasioStatus').val('');
            $('#topupAgunan').val('');

          };
      });
    }

    function setContract(){
      tableContractLookup.rows().every(function() {
          if(this.nodes().to$().hasClass('table-success')){
            $('#noKontrak').val(this.data().noKontrak);
            $('#nilaiKewajiban').val(this.data().nilaiKewajiban);
            $('#nilaiAgunan').val(this.data().nilaiAgunan);
            $('#rasio').val(this.data().rasio);
            $('#rasioStatus').val(this.data().rasioStatus);
            $('#topupAgunan').val(this.data().topupAgunan);
          };
      });
    }

    function loadStock(){
      $.ajax({
          url : '{{ url('/lookup/stock') }}/',
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataEfek = data.data;
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
              console.log(errorMsg);
          }
      });
    }
  </script>
@endsection
