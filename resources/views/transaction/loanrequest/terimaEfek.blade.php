@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Pengajuan Pinjaman</h4>
              <p class="card-description">
                Terima Efek
              </p>
              <form class="forms-sample" action="{{url('transaksi/loan-request/updateEfek/'.$pengajuanGadai['noPengajuan'])}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">No Pengajuan Gadai</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm"  id="noPengajuan" placeholder="" readonly value="{{$pengajuanGadai['noPengajuan']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Tgl Pengajuan Gadai</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm datepicker" placeholder="" disabled="disabled" value="{{$pengajuanGadai['tanggalTransaksi']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Kode Nasabah</label>
                      <div class="col-sm-2">
                        <input type="text" id="kodeNasabah" class="form-control form-control-sm" placeholder="" disabled="disabled" value="{{$pengajuanGadai['kodeNasabah']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Nama Nasabah</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm" placeholder="" disabled="disabled" value="{{$pengajuanGadai['namaNasabah']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Pengajuan Pinjaman</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" placeholder="" disabled="disabled" value="{{$pengajuanGadai['nilaiPengajuan']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Nilai Pinjaman Harga Saat ini</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" placeholder="" disabled="disabled" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Valuasi Agunan</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" placeholder="" disabled="disabled" value="{{$pengajuanGadai['valuasiAgunan']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Valuasi Agunan Harga Saat ini</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" placeholder="" disabled="disabled" value="{{$pengajuanGadai['currValuasiAgunan']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Rasio Pinjaman</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm percent" placeholder="" disabled="disabled" value="{{$pengajuanGadai['rasioPinjaman']*100}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Rasio Pinjaman Harga Saat ini</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm percent" placeholder="" disabled="disabled" value="{{$pengajuanGadai['currRasioPinjaman']*100}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Tgl Terima Efek</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datepicker" disabled="disabled" name="tglEfekTerima" id="tglEfekTerima" placeholder="" value="{{$pengajuanGadai['tanggalSettlment'] or ''}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Kustodi</label>
                      <div class="col-sm-3">
                        <select class="form-control form-control-sm" disabled name="kustodi" id="kustodi" readonly>
                          <option value="">-</option>
                          @foreach($brokers as $broker)
                            <option value="{{ $broker['kodeCustodi'] }}" {{$pengajuanGadai['kodeKustodi'] == $broker['kodeCustodi'] ? 'selected' : '' }}>{{ $broker['kodeCustodi']." - ".$broker['namaCustodi'] }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <br><table id="monitoring-lookup" class="table table-sm table-striped" style="width: 100%">
                      <thead>
                        <th>
                          Kode Efek
                        </th>
                        <th>
                          Jumlah Pengajuan
                        </th>
                        <th>
                          Jumlah Diterima
                        </th>
                        <th>
                          Harga Penutupan
                        </th>
                        <th>
                          Haircut
                        </th>
                      </thead>
                    </table>
                  </div>
                </div><br>
                  @include('inc.button.cancel')
                  @include('inc.button.submit')
                  @include('inc.button.instruksi')
              </form>
            </div>
          </div>
        </div>
@endsection
@section('modal')
  @include('modals.modalGenerate')  
@endsection

@section('script')
  @parent
  <script>
$(document).ready(function() {

  $.ajax({
    url: "{!! url('transaksi/loan-request/getDetail') !!}/" + $('#noPengajuan').val(),
    data: {},
    dataType: "json",
    type: "get",
    success:function(data)
      {
        loadData(data);
      }
    });


    var tableEditAlokasi = $('#monitoring-lookup').DataTable({
        bPaginate: false,
        searching : false,
        bSort: false,
        bInfo: false,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        columns: [
            { data: 'stockCode' },
            { data: 'qtyRequest' },
            { data: 'qtySettle' },
            { data: 'currPrice' },
            { data: 'currHaircut' },
        ]
    });

    function loadData(data){

      console.log(data.pengajuanEfekDetails);

      $('#monitoring-lookup').dataTable().fnDestroy();

      tableEditAlokasi = $('#monitoring-lookup').dataTable({
        data: data.pengajuanEfekDetails,
        bPaginate: false,
        searching : false,
        bSort: false,
        bInfo: false,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        autoWidth: false,
        columns: [
          { data: 'stockCode' },
          { data: 'qtyRequest' },
          { data: 'qtySettle' },
          { data: 'currPrice' },
          { data: 'currHaircut' },
        ],
        columnDefs: [
          {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','detail_'+row+'_kodeEfek')
                      );
                      $(td).append($('<input>')
                                  .addClass('form-control form-control-sm')
                                  .val(cellData)
                                  .css('display','none')
                                  .attr('name','kodeEfek['+row+']')
                                  .attr('id','detail_'+row+'_kodeEfek')
                              )
            },
          },
          {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoCommaReadOnly')
                          .val(cellData)
                          .attr('id','detail_'+row+'_qtyRequest')
                      )
            },
          },
          {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoComma')
                          .val(cellData)
                          .attr('name','settle['+row+']')
                          .attr('id','detail_'+row+'_qtySettle')
                      )
            },
          },
          {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm currencyNoComma')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','detail_'+row+'_closeprice')
                      )
            },
          },
          {
            "targets": [4],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('<input>')
                          .addClass('form-control form-control-sm percent')
                          .val(cellData)
                          .attr('readonly','readonly')
                          .attr('id','detail_'+row+'_haircut')
                      )
            },
          },
        ],
        drawCallback: function(settings) {
           initAutoNumeric();
        },
      });
    }

    $('.btn-instruksi').on('click', function(){
      $('#modalDownload').modal();
    });

$('#btn-instruksi-xml').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              var type = 'xml';
              generate(type);
            },
            cancel: function () {

            }
        }
    });
  });

  $('#btn-instruksi-csv').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              var type = 'csv';
              generate(type);
            },
            cancel: function () {

            }
        }
    });
  });

function generate(type){    
        var id = $('#noPengajuan').val();
        console.log(type);
        $.ajax({
           type:'POST',
           url:"{{ url('transaksi/loan-request/doc-send-intruksi-all') }}",
           data: {checks:id,type : type},
           success:function(data){
            if (type = "csv"){
              data = data.replace(/"/g,'');
            }

            var blob = new Blob([data], { type: 'application/'+type+'charset=utf-8;' });
            
            var link = document.createElement("a");
              if (link.download !== undefined) { // feature detection
                  // Browsers that support HTML5 download attribute
                  var url = URL.createObjectURL(blob);
                  link.setAttribute("href", url);
                  link.setAttribute("download", id+'.'+type);
                  link.style.visibility = 'hidden';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
              }
            console.log(data);
           }
        });      
  }

});
</script>
@endsection
