@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Pengajuan Pinjaman</h4>
              <p class="card-description">
                Konfirmasi Efek
              </p>
              <form class="forms-sample" action="{{url('transaksi/loan-request/store/'.$pengajuanGadai['noPengajuan'])}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">No Pengajuan Gadai</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" placeholder="" id="noPengajuan" disabled="disabled" value="{{$pengajuanGadai['noPengajuan']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Tgl Pengajuan Gadai</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm" id="tanggalPengajuan" placeholder="" disabled="disabled" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Kode Nasabah</label>
                      <div class="col-sm-2">
                        <input type="text" id="kodeNasabah" class="form-control form-control-sm" placeholder="" disabled="disabled" value="{{$pengajuanGadai['kodeNasabah']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Nama Nasabah</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm" placeholder="" disabled="disabled" value="{{$pengajuanGadai['namaNasabah']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Pengajuan Pinjaman</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" placeholder="" disabled="disabled" value="{{$pengajuanGadai['nilaiPengajuan']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Nilai Pinjaman Setelah Penerimaan</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" placeholder="" disabled="disabled" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Valuasi Agunan</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" placeholder="" disabled="disabled" value="{{$pengajuanGadai['valuasiAgunan']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Valuasi Agunan Setelah Penerimaan</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm currencyNoCommaReadOnly" placeholder="" disabled="disabled" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Rasio Pinjaman</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm percent" placeholder="" disabled="disabled" value="{{$pengajuanGadai['rasioPinjaman']}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Rasio Pinjaman Setelah Penerimaan</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm percentNoComma" placeholder="" disabled="disabled" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Tgl Terima Efek</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datepicker" name="tglEfekTerima" id="tglEfekTerima" placeholder="" value="{{$pengajuanGadai['tanggalSettlment'] or ''}}">
                      </div>
                      <label class=" col-sm-3 col-form-label">Kustodi</label>
                      <div class="col-sm-3">
                        <select class="form-control form-control-sm" name="kustodi" id="kustodi">
                          <option value="">-</option>
                          @foreach($brokers as $broker)
                            <option value="{{ $broker['kodeCustodi'] }}" {{$pengajuanGadai['kodeKustodi'] == $broker['kodeCustodi'] ? 'selected' : '' }}>{{ $broker['kodeCustodi']." - ".$broker['namaCustodi'] }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div><br>
                  @include('inc.button.submit')
                  @include('inc.button.cancel')
                  @include('inc.button.instruksi')
              </form>
            </div>
          </div>
        </div>
@endsection

@section('script')
  @parent
  <script>

  $(document).ready(function() {
    var dates = "{{$pengajuanGadai['tanggalTransaksi']}}";
    var date = moment(dates).locale('id').format('ll');
    console.log(date);
    $('#tanggalPengajuan').val(date);
  });

  $('#btnInstruksi').on('click', function(){
    // alert("test");
      location.href="{{url('transaksi/loan-request/instruksiKsei')}}" + "/" + $('#noPengajuan').val();
  });
  </script>
@endsection
