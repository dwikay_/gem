@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title float-left">Pengajuan Pinjaman</h4>
                  <div class="form-group mt-10" role="group" aria-label="Basic example">
                    @include('inc.button.instruksi')
                  </div>
              </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table class="table table-sm table-striped" id="lookup" width="100%">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Tanggal Transaksi</th>
                        <th>Kode Transaksi</th>
                        <th>Kode Nasabah</th>
                        <th>Nama Nasabah</th>
                        <th>Nilai Pengajuan</th>
                        <th>Valuasi Agunan</th>
                        <th>Rasio Pinjaman</th>
                        <th>Nomor Kustodi</th>
                        <th>Tanggal Settlement</th>
                        <th class="text-center">Status</th>
                        <th></th>
                        <th class="text-center"><input type="checkbox" clacc="form-control" id="select_all" style="cursor:pointer"></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('modal')
  @include('modals.modalGenerate')
@endsection
@section('script')
<script>
$(document).ready(function() {

var table = $("#lookup").dataTable({
    columns: [
      {data: 'id'},
      {data: 'tanggalTransaksi'},
      {data: 'noPengajuan'},
      {data: 'kodeNasabah'},
      {data: 'namaNasabah'},
      {data: 'nilaiPengajuan'},
      {data: 'valuasiAgunan'},
      {data: 'rasioPinjaman'},
      {data: 'kodeKustodi'},
      {data: 'tanggalSettlment'},
      {data: 'status'},
      {data: 'id'},
    ],
  });

$(document).ready(function() {
  // $('#form').submit();
  var msg = '{{Session::get('info')}}';
  var msgclass = '{{Session::get('alert')}}';
  var exist = '{{Session::has('info')}}';
  if(exist){
    swal(msgclass,"",msg)
  }

  $('#select_all').on('click',function(){
      if(this.checked){
          $('.checkbox').each(function(){
              this.checked = true;
          });
      }else{
           $('.checkbox').each(function(){
              this.checked = false;
          });
      }
  });

  $('#lookup').dataTable().fnDestroy();

  table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    ajax:{
      url: "{{ url('transaksi/loan-request/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="11">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
      {data: 'noPengajuan'},
      {data: 'tanggalTransaksi'},
      {data: 'noPengajuan'},
      {data: 'kodeNasabah'},
      {data: 'namaNasabah'},
      {data: 'nilaiPengajuan'},
      {data: 'valuasiAgunan'},
      {data: 'rasioPinjaman'},
      {data: 'kodeKustodi'},
      {data: 'tanggalSettlment'},
      {data: 'status'},
      {data: 'status'},
      {data: 'noPengajuan'},
    ],
    "scrollX": '100vw',
    'scrollY': '100vh',
    "scrollCollapse": true,
    'autoWidth': true,
    'bSort': true,
    'bPaginate': true,
    'searching' : true,
    columnDefs: [
      {
        "targets": [0],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();

          $(td)
          .append($('<button>')
              .attr('type','button')
              .addClass('details-control')
              .text('+')
          )
          .append($('<input>')
              .attr('type','hidden')
              .val(rowData.id)
            )
        },
        orderable: false
      },
      {
          "targets": [1],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            var dates = moment(cellData).locale('id').format('ll');
            $(td).text(dates);
          },
      },
      {
        "targets": [5,6],
        "data": null,
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).append($('<span>')
                      .addClass('currencyNoCommaReadOnly')
                      .text(cellData)
                  )
        },
      },
      {
        "targets": [7],
        "data": null,
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).append($('<span>')
                      .addClass('percent')
                      .text(cellData*100)
                  )
        },
      },
      {
          "targets": [9],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            var dates = moment(cellData).locale('id').format('ll');
            $(td).text(dates);
          },
      },
      {
        "targets": [10],
        "data": null,
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          var status;
          var classnya;
          switch(cellData){
            case '0' : status = 'Lengkapi Data Nasabah'; classnya = "danger"; break;
            case '1' : status = 'Menunggu Subrek'; classnya = "info"; break;
            case '2' : status = 'Request SubRek'; classnya = "secondary"; break;
            case '3' : status = 'Konfirmasi Efek'; classnya = "warning"; break;
            case '4' : status = 'Menunggu Konfirmasi'; classnya = "warning"; break;
            case '5' : status = 'Terima Efek'; classnya = "info text-white"; break;
            case '6' : status = 'Cair'; classnya = "success"; break;
            case '7' : status = 'Batal'; classnya = "danger"; break;
          }
          $(td).append($('<span>')
                      .addClass('badge badge-'+classnya)
                      .text(status)
                  )
        },
      },
      {
        "targets": [11],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          var warna;
          var icon;
          var title;
          var cursor;
          switch(cellData){
            case '0' : icon = 'fas fa-user-edit fa-xs'; warna = "btn-success text-white"; title="Lengkapi Data Nasabah"; cursor=""; break;
            case '1' : icon = 'far fa-address-book fa-xs'; warna = "btn-info text-white"; title="Lihat Data Nasabah"; cursor=""; break;
            case '2' : icon = 'fa fa-id-card-alt fa-xs'; warna = "btn-secondary text-white"; title="Edit Sub Rekening"; cursor=""; break;
            case '3' : icon = 'far fa-edit fa-xs'; warna = "btn-primary"; title="Ubah CounterPart"; cursor=""; break;
            case '4' : icon = 'fas fa-file-import fa-xs'; warna = "btn-info"; title="Kirim Email"; cursor=""; break;
            case '5' : icon = 'fas fa-box'; warna = "btn-primary"; title="Terima Efek"; cursor=""; break;
            case '6' : icon = 'fas fa-times'; warna = "btn-danger text-white"; title="Pembatalan Pengajuan"; cursor="no-drop"; break;
            case '7' : icon = 'fas fa-ban'; warna = "btn-secondary text-white"; title=""; cursor="no-drop"; break;
          }
          $(td)
            .append($('<a>')
              .addClass('btn btn-sm btn-modify '+warna)
              .attr('title', title)
              .css('cursor', cursor)
              .append($('<span>')
                .addClass(icon)
              )
            );
        },
        orderable: false
      },
      {
        "targets": [12],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          // console.log(rowData.status);
          if(rowData.status == 0 || rowData.status == '0'){
            $(td).append(
              $('<input type="checkbox" style="cursor:pointer" value="'+cellData+'")" disabled>')
            );
          }else{
            $(td).append(
              $('<input type="checkbox" class="checkbox checkbox-instruksi" style="cursor:pointer" id="checks'+cellData+'" name="checks['+cellData+']" value="'+cellData+'")">')
            );
          }
          // $(td).append(
          //   $('<input type="hidden" class="efek" style="cursor:pointer" id="efek'+rowData.tipeEfek+'" name="efek['+rowData.tipeEfek+']" value="'+rowData.tipeEfek+'" onclick="efek('+rowData.tipeEfek+')">')
          // );
        },
        orderable: false
      },
    ],
    drawCallback: function(settings) {
      initAutoNumeric();
    },
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    }
  });
});

function createDetail(index){
  return $('<table>')
    .addClass('table detail-table table-striped table-bordered')
    .attr('id','detail_'+index)
        .append($('<thead>')
          .addClass('thead-light')
            .append($('<tr>')
                .append($('<th>')
                    .addClass("text-center")

                    .text('Kode Efek')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Jumlah Pengajuan')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Jumlah Diterima')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Harga Penutupan')
                )
                .append($('<th>')
                .addClass("text-center")
                .text('Haircut (%)')
                )
            )
        )
}

$('#lookup tbody').on('click', '.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.api().row( tr );

    var id = tr.attr('id').split('_');

    var index = id[1];

    var data = table.fnGetData();

    var data_detail = data[index].pengajuanEfekDetails;

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        $(this).find('.details-control').text('+');
    }else {
        // Open this row

        row.child( createDetail(index)).show();

        $('#detail_'+index).dataTable({
          data: data_detail,
          bPaginate: false,
          searching : false,
          bSort: true,
          bInfo: false,
          columns: [
              { data: 'stockCode' },
              { data: 'qtyRequest' },
              { data: 'qtySettle' },
              { data: 'price' },
              { data: 'haircut' },
          ],
          columnDefs: [
            {
              "targets": [1,2,3],
              "data": 0,
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();
                $(td).append($('<span>')
                            .addClass('currencyNoComma')
                            .text(cellData)
                        )
              },
            },
            {
              "targets": [4],
              "data": 0,
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();
                $(td).append($('<span>')
                            .addClass('percentNoComma')
                            .text(cellData)
                        )
              },
            },
          ],
          drawCallback: function(settings) {
             initAutoNumeric();
          },
        });

        $(this).find('.details-control').text('-');
    }
});

$('.table').on('click','.btn-modify', function(){

    var tr = $(this).closest('tr');
    var row = table.api().row( tr );
    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData();

    var url;
    var kode = data[index].kodeNasabah
    var res = kode.substring(0, 2);
    var tipeNasabah;
    if(res=="ID"){
      tipeNasabah = "R";
    }else{
      tipeNasabah = "C";
    }
    switch(data[index].status){
      case '0' : url = '{{url('dataStatis/nasabah/reqSubrek/')}}'; routing = data[index].kodeNasabah + "/" + tipeNasabah;break;
      case '1' : url = '{{url('dataStatis/nasabah/reqSubrek-View/')}}'; routing = data[index].kodeNasabah + "/" + tipeNasabah + "/"+ data[index].kodeNasabah;break;
      case '2' : url = '{{url('dataStatis/nasabah/reqSubrek/')}}'; routing = data[index].kodeNasabah + "/" + tipeNasabah;break;
      case '3' : url = '{{url('transaksi/loan-request/edit')}}'; routing = data[index].noPengajuan; break;
      case '4' : url = '{{url('transaksi/loan-request/send-surel')}}'; routing = data[index].noPengajuan; break;
      case '5' : url = '{{url('transaksi/loan-request/terimaEfek')}}'; routing = data[index].noPengajuan; break;
      case '6' : url = '#'; break;
      case '7' : url = '#'; break;
    }
    location.href= url + "/" + routing;
});

$('.btn-instruksi').on('click', function(){
    if($('.checkbox-instruksi').is(':checked')){
      $('#modalDownload').modal();
    }else{
      $.alert({
          title: 'Data Kosong !',
          content: 'Harap cek data terlebih dahulu',
      });
    }
  });

$('#btn-instruksi-xml').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              var type = 'xml';
              generateXML(type);
            },
            cancel: function () {

            }
        }
    });

  });

  $('#btn-instruksi-csv').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              generateCSV();
            },
            cancel: function () {

            }
        }
    });
  });

function generateXML(type){
    $('.checkbox-instruksi').each(function( index ) {
      if($(this).is(':checked')){
        var checks = $(this).val();

        // console.log(checks);

        $.ajax({
           type:'POST',
           url:"{{ url('transaksi/loan-request/doc-send-intruksi-all') }}",
           data: {checks:checks},
           success:function(data){
            var blob = new Blob([data], { type: 'application/'+type+'charset=utf-8;' });

            var link = document.createElement("a");
              if (link.download !== undefined) { // feature detection
                  // Browsers that support HTML5 download attribute
                  var url = URL.createObjectURL(blob);
                  link.setAttribute("href", url);
                  link.setAttribute("download", checks+'.'+type);
                  link.style.visibility = 'hidden';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
              }
            // console.log(data);
           }
        });
      }
    });
  }

  function generateCSV(){
    $('.checkbox-instruksi').each(function( index ) {
      if($(this).is(':checked')){
        var checks = $(this).val();

        $.ajax({
            type:'POST',
            url:"{{ url('transaksi/loan-request/doc-send-intruksi-all') }}",
            data: {checks:checks,type:'csv'},
            success:function(data){
              // console.log(data);
              // data = data.replace(/"/g,'');
              // var csvContent = "data:text/csv;charset=utf-8,"+data;

              var blob = new Blob([data], { type: 'data:text/csv;charset=utf-8;' });

              var link = document.createElement("a");
              if (link.download !== undefined) { // feature detection
                  // Browsers that support HTML5 download attribute
                  var url = URL.createObjectURL(blob);
                  link.setAttribute("href", url);
                  link.setAttribute("download", checks+'.csv');
                  link.style.visibility = 'hidden';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
              }
            }
        });
      }
    });
  }

});
</script>
@endsection
