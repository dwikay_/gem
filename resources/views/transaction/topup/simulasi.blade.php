@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Topup</h4>
              <p class="card-description">
                Simulasi Topup
              </p>
              <form class="forms-sample" action="" method="post">
                {{csrf_field()}}
                <div class="row">

                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label">Kode Saham</label>
                      <div class="col-sm-6">
                        <select class="form-control" name="kodeEfek" id="kodeEfek">
                          <option value="" disabled selected></option>
                          @foreach($efek as $efeks)
                          <option value="{{$efeks['kodeEfek']}}">{{$efeks['kodeEfek']}} - {{$efeks['namaEfek']}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>

                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Lembar Agunan</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control form-control-sm currencyNoComma" name="closePrice" id="lembarAgunan" placeholder="" value="">
                        </div>

                        <label class=" col-sm-3 col-form-label">Harga Saham</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control form-control-sm currencyNoComma" readonly placeholder="" name="hargaSaham" id="hargaSaham" value="">
                          <input type="hidden" class="form-control form-control-sm" readonly placeholder="" name="hargaSaham" id="hargaSahams" value="">
                          <input type="hidden" class="form-control form-control-sm" readonly placeholder="" name="ltvMaks" id="ltvMaks" value="">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Nilai Agunan</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control form-control-sm currencyNoComma" name="nilaiAgunan" id="nilaiAgunan" readonly placeholder="" value="">
                        </div>
                        <label class=" col-sm-3 col-form-label">UP Maksimal</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control form-control-sm currencyNoComma" name="upMaksimal" id="upMaksimal" readonly placeholder="" value="">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Rasio Uang Pinjam</label>
                        <div class="col-sm-2">
                          <input type="text" id="rasioPinjam" name="rasioPinjam" class="form-control form-control-sm percent" readonly placeholder="" value="">
                        </div>
                        <label class=" col-sm-3 col-form-label">Rasio Topup</label>
                        <div class="col-sm-3">
                          <input type="text" id="rasioTopup" name="rasioTopup" class="form-control form-control-sm percent" readonly placeholder=""  value="{{$paramGlobal['doubleVal']}}">
                          <input type="hidden" id="rasioTopupss" name="rasioTopup" class="form-control form-control-sm" readonly placeholder=""  value="{{$paramGlobal['doubleVal']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Harga Pada saat Topup</label>
                        <div class="col-sm-2">
                          <input type="text" id="hargaTopup" name="hargaTopup" class="form-control form-control-sm currencyNoComma" readonly placeholder="" value="">
                        </div>
                      </div>
                    </div>
                  <div class="col-md-12 mt-10">
                      @include('inc.button.hitung')
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>
  $(document).ready(function() {

    $('#kodeEfek').on('change', function(){
      $.ajax({
         url:"{{ url('transaksi/stock-deposit/simulation/getEfek') }}/" + $('#kodeEfek').val(),
         dataType: "json",
         type: "GET",
         success:function(data){
            $('#hargaSaham').val(data.closingPrice)
            $('#hargaSahams').val(data.closingPrice)
            $('#ltvMaks').val(data.ltvMax)
         }
      });

    })

    $('.btn-hitung').on('click', function(){

      if($('#kodeEfek').val() == null){
        $.confirm({
            title: 'Konfirmasi',
            content: 'Kode Efek tidak boleh kosong',
            icon: 'fas fa-error',
            buttons: {
                confirm: function () {
                }
            }
        });
      }

      else if($('#lembarAgunan').val() == ""){
          $.confirm({
              title: 'Konfirmasi',
              content: 'Lembar Agunan tidak boleh kosong',
              icon: 'fas fa-error',
              buttons: {
                  confirm: function () {
                  }
              }
          });
        }else{

          var nilaiAgunan = $('#hargaSahams').val() * $('#lembarAgunan').val();
          var upMaks = ($('#ltvMaks').val()/100)*nilaiAgunan;
          var rasioUP = upMaks/nilaiAgunan;
          var hargaTopup = ($('#rasioTopupss').val()*nilaiAgunan) / $('#lembarAgunan').val();
          $('#nilaiAgunan').val(nilaiAgunan);
          $('#upMaksimal').val(upMaks);
          $('#hargaTopup').val(hargaTopup);
          $('#rasioPinjam').val(rasioUP);
        }
    });

  });
</script>
@endsection
