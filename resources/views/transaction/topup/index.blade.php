@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
              <div class="form-group mt-10" role="group" aria-label="Basic example">
                @include('inc.button.instruksi')
                @include('inc.button.add', ['urls' => url('/transaksi/stock-deposit/create')])
              </div>
                <h4 class="card-title">Topup Agunan</h4><br>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Tipe Efek</label>
                    <div class="col-md-10">
                      <select class="form-control form-control-sm col-md-2" name="type_efek" id="type_efek" width="100%">
                        <option value="A">All</option>
                        <option value="S">Saham</option>
                        <option value="O">Obligasi</option>
                      </select>
                  </div>
                </div>
            </div>
            <div class="card-body">
              <!-- <div class="form-group row">
                <label class="col-form-label col-md-2">Tipe</label>
                <select class="form-control form-control-sm col-md-2" name="type_efek" id="type_efek">
                  <option value="A">All</option>
                  <option value="S">Saham</option>
                  <option value="O">Obligasi</option>
                </select> -->
                <!-- <input class="form-control form-control-sm col-md-2" name="status_gadai" id="status_gadai"> -->
              <!-- </div> -->
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <form id="fpro" action="{{url('transaksi/stock-deposit/doc-send-intruksi-all')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                  <table id="lookup" class="table table-sm table-striped" style="width:100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>No. Deposit</th>
                        <th>No. Kontrak</th>
                        <th>Kode Nasabah</th>
                        <th>Nama Nasabah</th>
                        <th>SID</th>
                        <th>Rekening Efek</th>
                        <th>Tipe Efek</th>
                        <th>Kode Efek</th>
                        <th>Nama Efek</th>
                        <th>Jumlah</th>
                        <th>Tanggal Input</th>
                        <th>Tanggal Penyelesaian</th>
                        <th>Kustodi</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
                        <th class="text-center"><input type="checkbox" clacc="form-control" id="select_all" style="cursor:pointer"></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('modal')
  @include('modals.modalGenerate')
@endsection
@section('script')
<script>
$(document).ready(function() {

var table = $("#lookup").dataTable({
  dom: 'lrtip',
  "scrollX": '100vw',
  'scrollY': '100vh',
  columns:
    [
      {data: 'noDeposit'},
      {data: 'noDeposit'},
      {data: 'noKontrak'},
      {data: 'kodeNasabah'},
      {data: 'namaNasabah'},
      {data: 'sid'},
      {data: 'rekeningEfek'},
      {data: 'tipeEfek'},
      {data: 'kodeEfek'},
      {data: 'nameEfek'},
      {data: 'jumlahEfek'},
      {data: 'entryDate'},
      {data: 'settleDate'},
      {data: 'custodyCode'},
      {data: 'status'},
      {data: 'status'},
      {data: 'status'}
    ],
  });

$(document).ready(function() {
  loadData();
})

$('#select_all').on('click',function(){
      if(this.checked){
          $('.checkbox').each(function(){
              this.checked = true;
          });
      }else{
           $('.checkbox').each(function(){
              this.checked = false;
          });
      }
  });

$('#type_efek').on('change',function(){
  loadData();
})

function loadData(){

  $('#lookup').dataTable().fnDestroy();

  var type_efek = $('#type_efek').val();

  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    dom: 'lrtip',
    ajax:{
      url: "{{ url('transaksi/stock-deposit/getIndex') }}",
      dataType: "json",
      type: "POST",
      data: {
        type_efek: type_efek
      },
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="8">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns:
      [
        {data: 'noDeposit'},
        {data: 'noDeposit'},
        {data: 'noKontrak'},
        {data: 'kodeNasabah'},
        {data: 'namaNasabah'},
        {data: 'sid'},
        {data: 'rekeningEfek'},
        {data: 'tipeEfek'},
        {data: 'kodeEfek'},
        {data: 'nameEfek'},
        {data: 'jumlahEfek'},
        {data: 'entryDate'},
        {data: 'settleDate'},
        {data: 'custodyCode'},
        {data: 'status'},
        {data: 'status'},
        {data: 'status'}
      ],
      "scrollX": '100vw',
      'scrollY': '100vh',
      "scrollCollapse": true,
      'autoWidth': true,
      'bSort': true,
      'bPaginate': true,
      'searching' : true,
    columnDefs:
    [
      {
        "targets": [0],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).text(row+1);
        },
        orderable: false
      },
      {
          "targets": [5],
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();

            var sid;
            if(cellData==null){
              sid = "000";
            }else{
              sid = cellData;
            }
            $(td).text(sid);
          },
      },
      {
          "targets": [7],
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();

            var tipeEfek;
            switch(cellData){
              case 'S' : tipeEfek = 'Saham'; break;
              case 'O' : tipeEfek = 'Obligasi'; break;
              case 'R' : tipeEfek = 'Rights'; break;
              case 'W' : tipeEfek = 'Warrant'; break;
            }
            $(td).text(tipeEfek);
          },
      },
      {
        "targets": [10],
        "data": null,
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).append($('<span>')
                      .addClass('currencyNoCommaReadOnly')
                      .text(cellData)
                  )
        },
      },
      {
        "targets": [11,12],
        "createdCell": function (td, cellData, rowData, row, col) {
          var dates = moment(cellData).locale('en').format('LL');
          $(td).text(dates);
        },
        orderable: false
      },
      {
        "targets": [14],
        "data": null,
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          var status;
          var classnya;
          switch(cellData){
            case '1' : status = 'Pengajuan Topup'; classnya = "warning"; break;
            case '2' : status = 'Terima Efek'; classnya = "info text-white"; break;
            case '3' : status = 'Settle'; classnya = "success text-white"; break;
            case '4' : status = 'Cancel'; classnya = "danger text-white"; break;
          }
          $(td).append($('<span>')
                      .addClass('badge badge-'+classnya)
                      .text(status)
                  )
        },
      },
      {
        "targets": [15],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          if(rowData.status=="1"){
            $(td).append($('@include('inc.button.instruksiGroup')'));
          }else if(rowData.status=="2"){
            $(td).append($('@include('inc.button.group_btn_settle')'));
          }else{
            $(td).append($('@include('inc.button.btnBlockInstruksi')'));
          }

        },
        orderable: false
      },
      {
        "targets": [16],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).append(
            $('<input type="checkbox" class="checkbox checkbox-instruksi" style="cursor:pointer" id="checks'+rowData.noDeposit+'" name="checks['+rowData.noDeposit+']" value="'+rowData.noDeposit+'")">')
          );
          // $(td).append(
          //   $('<input type="hidden" class="efek" style="cursor:pointer" id="efek'+rowData.tipeEfek+'" name="efek['+rowData.tipeEfek+']" value="'+rowData.tipeEfek+'" onclick="efek('+rowData.tipeEfek+')">')
          // );
        },
        orderable: false
      },
    ],
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    },
    drawCallback: function(settings) {
           initAutoNumeric();
        },
  });
};

$('.table').on('click','.btn-settle-record', function(){
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData()

    $.confirm({
        title: 'Confirmation',
        content: 'Apa anda yakin ingin menyetujui transaksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
                location.href="{{url('transaksi/stock-deposit/settle')}}"
                + "/" + 0
                + "/" + data[index].noDeposit
                + "/" + data[index].jumlahEfek;
            },
            cancel: function () {

            }
        }
    });
});

$('.table').on('click','.btn-unsettle-record', function(){
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData()

    $.confirm({
        title: 'Confirmation',
        content: 'Apa anda yakin ingin membatalkan transaksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
                location.href="{{url('transaksi/stock-deposit/settle')}}"
                + "/" + 1
                + "/" + data[index].noDeposit
                + "/" + data[index].jumlahEfek;
            },
            cancel: function () {

            }
        }
    });
});

$('.table').on('click','.btn-send-instruksi', function(){
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData();

    // chooses(data[index].noDeposit);

    $.confirm({
        title: 'Confirmation',
        content: 'Apa anda yakin ingin mengajukan transaksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
                location.href="{{url('transaksi/stock-deposit/sendInstruksi')}}"
                + "/" + data[index].noDeposit
                + "/" + data[index].tipeEfek;
            },
            cancel: function () {

            }
        }
    });
});

$('.btn-instruksi').on('click', function(){
    if($('.checkbox-instruksi').is(':checked')){
      $('#modalDownload').modal();
    }else{
      $.alert({
          title: 'Data Kosong !',
          content: 'Harap cek data terlebih dahulu',
      });
    }
  });

$('#btn-instruksi-xml').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              var type = 'xml';
              generateXML(type);
            },
            cancel: function () {

            }
        }
    });
  });

  $('#btn-instruksi-csv').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              generateCSV();
            },
            cancel: function () {

            }
        }
    });
  });

function generateXML(type){
    $('.checkbox-instruksi').each(function( index ) {
      if($(this).is(':checked')){
        var checks = $(this).val();

        // console.log(checks);

        $.ajax({
           type:'POST',
           url:"{{ url('transaksi/stock-deposit/doc-send-intruksi-all') }}",
           data: {checks:checks},
           success:function(data){
            var blob = new Blob([data], { type: 'application/'+type+'charset=utf-8;' });

            var link = document.createElement("a");
              if (link.download !== undefined) { // feature detection
                  // Browsers that support HTML5 download attribute
                  var url = URL.createObjectURL(blob);
                  link.setAttribute("href", url);
                  link.setAttribute("download", checks+'.'+type);
                  link.style.visibility = 'hidden';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
              }
            // console.log(data);
           }
        });
      }
    });
  }

  function generateCSV(){
    $('.checkbox-instruksi').each(function( index ) {
      if($(this).is(':checked')){
        var checks = $(this).val();

        $.ajax({
            type:'POST',
            url:"{{ url('transaksi/stock-deposit/doc-send-intruksi-all') }}",
            data: {checks:checks,type:'csv'},
            success:function(data){
              // data = data.replace(/"/g,'');
              // var csvContent = "data:text/csv;charset=utf-8,"+data;

              var blob = new Blob([data], { type: 'data:text/csv;charset=utf-8;' });

              var link = document.createElement("a");
              if (link.download !== undefined) { // feature detection
                  // Browsers that support HTML5 download attribute
                  var url = URL.createObjectURL(blob);
                  link.setAttribute("href", url);
                  link.setAttribute("download", checks+'.csv');
                  link.style.visibility = 'hidden';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
              }
            }
        });
      }
    });
  }


});
</script>
@endsection
