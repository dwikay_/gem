@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title float-left">Topup Agunan</h4>
                  @include('inc.button.add', ['urls' => '/transaksi/stock-deposit/create'])
              </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table class="table table-sm table-striped" id="lookup">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th>
                          Kode Topup
                        </th>
                        <th>
                          Tanggal Topup
                        </th>
                        <th>
                          Kode Nasabah
                        </th>
                        <th>
                          No Kontrak
                        </th>
                        <th>
                          Action
                        </th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

var table = $("#lookup").dataTable({
    columns: [
      {data: 'id'},
      {data: 'id'},
      {data: 'noTopUp'},
      {data: 'tanggalTopUp'},
      {data: 'kodeNasabah'},
      {data: 'noKontrak'},
      {data: 'id'}
    ],
  });

$(document).ready(function() {
  var msg = '{{Session::get('info')}}';
  var msgclass = '{{Session::get('alert')}}';
  var exist = '{{Session::has('info')}}';
  if(exist){
    swal(msgclass,"",msg)
  }

  $('#lookup').dataTable().fnDestroy()

  table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax:{
      url: "{{ url('transaksi/stock-deposit/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="10">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    createdRow: function ( row, data, index ) {
        $(row).attr('id','row_'+index);
    },
    columns: [
      {data: 'id'},
      {data: 'id'},
      {data: 'noTopUp'},
      {data: 'tanggalTopUp'},
      {data: 'kodeNasabah'},
      {data: 'noKontrak'},
      {data: 'id'}
    ],
    columnDefs: [
      {
        "targets": [0],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).text(row+1);
        },
      },
      {
        "targets": [1],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();

          $(td)
            .append($('<button>')
                .attr('type','button')
                .addClass('details-control')
                .text('+')
            )
            .append($('<input>')
                .attr('type','hidden')
                .val(rowData.CASHLOAN_REQ_ID)
              );
        },
        orderable: false
      },
      {
        "targets": [6],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td)
            .append($('<a>')
              .addClass('btn btn-primary btn-sm')
              .attr('href','topup/edit/'+cellData)
              .attr('title','Edit')
              .append($('<span>')
                .addClass('fas fa-pencil-alt fa-xs')
              )
            );
        },
        orderable: false
      },
    ]
  });
});

function createDetail(index){
  return $('<table>')
    .addClass('table detail-table table-striped')
    .attr('id','detail_'+index)
        .append($('<thead>')
          .addClass('thead-light')
            .append($('<tr>')
                .append($('<th>')
                    .text('Stock Code')
                )
                .append($('<th>')
                    .text('Quantity')
                )
            )
        )
}

$('#lookup tbody').on('click', '.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.api().row( tr );

    var id = tr.attr('id').split('_');

    var index = id[1];

    var data = table.fnGetData();

    var data_detail = data[index].topUpEfekDtls;

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();

        $(this).find('i').removeClass('fa-minus');
        $(this).find('i').addClass('fa-plus');
    }else {
        // Open this row

        row.child( createDetail(index)).show();

        $('#detail_'+index).dataTable({
          data: data_detail,
          bPaginate: false,
          searching : false,
          bSort: true,
          bInfo: false,
          columns: [
              { data: 'STOCK_CODE' },
              { data: 'QTY' },
          ],
          columnDefs: [
            {
              "targets": [1],
              "data": null,
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();
                $(td).append($('<span>')
                            .addClass('currencyNoComma')
                            .text(cellData)
                        )
              },
            },
          ],
          drawCallback: function(settings) {
             initAutoNumeric();
          },
        });

        $(this).find('i').removeClass('fa-plus');
        $(this).find('i').addClass('fa-minus');
    }
});

</script>
@endsection
