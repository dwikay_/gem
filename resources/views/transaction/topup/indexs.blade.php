@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
                <h4 class="card-title float-left">Topup Agunan</h4>
                @include('inc.button.add', ['urls' => '/transaksi/stock-deposit/create'])
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-form-label col-md-2">Tipe</label>
                <select class="form-control form-control-sm col-md-2" name="type_efek" id="type_efek">
                  <option value="A">All</option>
                  <option value="S">Saham</option>
                  <option value="O">Obligasi</option>
                </select>
              </div>
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>No. Deposit</th>
                        <th>No. Kontrak</th>
                        <th>Kode Nasabah</th>
                        <th>Nama Nasabah</th>
                        <th>SID</th>
                        <th>Rekening Efek</th>
                        <th>Tipe Efek</th>
                        <th>Kode Efek</th>
                        <th>Nama Efek</th>
                        <th>Jumlah</th>
                        <th>Tanggal Input</th>
                        <th>Tanggal Penyelesaian</th>
                        <th>Kustodi</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>
var table = $("#lookup").dataTable({
  dom: 'lrtip',
  columns:
    [
      {data: 'noDeposit'},
      {data: 'noDeposit'},
      {data: 'noKontrak'},
      {data: 'kodeNasabah'},
      {data: 'namaNasabah'},
      {data: 'sid'},
      {data: 'rekeningEfek'},
      {data: 'tipeEfek'},
      {data: 'kodeEfek'},
      {data: 'nameEfek'},
      {data: 'jumlahEfek'},
      {data: 'entryDate'},
      {data: 'settleDate'},
      {data: 'custodyCode'},
      {data: 'status'},
      {data: 'status'}
    ],
  });
$(document).ready(function() {
  loadData();
})


$('#type_efek').on('change',function(){
  loadData();
})

function loadData(){

  $('#lookup').dataTable().fnDestroy();
  alert($('#type_efek').val());
  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    dom: 'lrtip',
    ajax:{
      url: "{{ url('transaksi/stock-deposit/getIndex') }}",
      dataType: "json",
      type: "POST",
      data: {
        type_efek: type_efek
      },
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="8">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
              {data: 'noDeposit'},
              {data: 'noDeposit'},
              {data: 'noKontrak'},
              {data: 'kodeNasabah'},
              {data: 'namaNasabah'},
              {data: 'sid'},
              {data: 'rekeningEfek'},
              {data: 'tipeEfek'},
              {data: 'kodeEfek'},
              {data: 'nameEfek'},
              {data: 'jumlahEfek'},
              {data: 'entryDate'},
              {data: 'settleDate'},
              {data: 'custodyCode'},
              {data: 'status'},
              {data: 'status'}
          ],
          'bSort': true,
          'bPaginate': true,
          "scrollX": true,
          'scrollY': '100vh',
          "scrollCollapse": true,
          'searching' : true,
          'autoWidth': true,
          columnDefs: [
            {
              "targets": [0],
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).text(row+1);
              },
              orderable: false
            },
            {
                "targets": [5],
                "createdCell": function (td, cellData, rowData, row, col) {
                  $(td).empty();

                  var sid;
                  if(cellData==null){
                    sid = "000";
                  }else{
                    sid = cellData;
                  }
                  $(td).text(sid);
                },
            },
            {
                "targets": [7],
                "createdCell": function (td, cellData, rowData, row, col) {
                  $(td).empty();

                  var tipeEfek;
                  switch(cellData){
                    case 'S' : tipeEfek = 'Saham'; break;
                    case 'O' : tipeEfek = 'Obligasi'; break;
                    case 'R' : tipeEfek = 'Rights'; break;
                    case 'W' : tipeEfek = 'Warrant'; break;
                  }
                  $(td).text(tipeEfek);
                },
            },
            {
              "targets": [11,12],
              "createdCell": function (td, cellData, rowData, row, col) {
                var dates = moment(cellData).locale('en').format('LL');
                $(td).text(dates);
              },
              orderable: false
            },
            {
              "targets": [14],
              "data": null,
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();
                var status;
                var classnya;
                switch(cellData){
                  case '1' : status = 'Request'; classnya = "warning"; break;
                  case '2' : status = 'Settle'; classnya = "success text-white"; break;
                  case '3' : status = 'Cancel'; classnya = "danger text-white"; break;
                }
                $(td).append($('<span>')
                            .addClass('badge badge-'+classnya)
                            .text(status)
                        )
              },
            },
            {
              "targets": [15],
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();
                  $(td)
                      .append($('@include('inc.button.group_btn_settle')'));
              },
              orderable: false
            },
          ],
          drawCallback: function(settings) {
                 initAutoNumeric();
              },
      createdRow: function ( row, data, index ) {
          $(row).attr('id','table_'+index);
      },
    });
}

$('.table').on('click','.btn-edit-record', function(){ //settle

    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData()

    location.href="{{url('dataStatis/provinsi/edit/')}}" + "/" + data[index].id;
});

$('.table').on('click','.btn-remove-record', function(){ //Cancel
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData()

    deletes(data[index].id);
});

</script>
@endsection
