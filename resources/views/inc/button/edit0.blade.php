@extends('layouts.app')
@section('content')
    <div class="content -dark -with-left-sidebar -collapsible">
      <div class="container-fluid">
        <div class="row">
          <div class="col -lg-12">
            <!-- Page Heading-->
            <div class="page-heading -dark">
              <h2> Direktori Gambar
              </h2>
              <!-- Breadcrumb-->
              <div class="breadcrumb">
                <a class="breadcrumb-item" href="">Home</a>
                <a class="breadcrumb-item" href="">Direktori Gambar</a>
                <a class="breadcrumb-item -active"> Tambah</a>
              </div>
              <!-- /Breadcrumb-->
            </div>
            <!-- /Page Heading-->
          </div>
        </div>
        <form class="form -dark" id="form-eksplorasi" action="{{url('image-library/update/'.$libs->id)}}" data-toggle="validator" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}
         <div class="row">
          <div class="col -lg-12">
            <div class="panel -dark">
              <div class="panel-heading">
                <h4>Gambar</h4>
              </div>
              <div class="panel-body">
              	 <!-- Form Group-->
                  <div class="form-group">
                    <label for="form-elements-file">Judul</label>
                    <input class="form-control" name="judul" id="judul" type="text" value="{{$libs->judul}}" required/>
                  </div>
                  <div class="form-group">
                    <label for="form-elements-file">Image</label>
                    <input class="form-control" id="image" name="image" type="file"/>
                    <span style="color:red"><i>*) change the file if you want to change</i></span>
                  </div>
                  <div class="form-group _margin-top-2x">
                    <div class="form-check -checkbox">
                      <button class="btn -primary">Simpan</button>
                    </div>
                  </div>
                  <!-- /Form Group-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('script')
<script>

      var simplemde = new SimpleMDE({ element: document.getElementById("konten") });
      $('#btnsimpan').on('click', function(){
        $('#form-eksplorasi').submit();
      });

</script>
@endsection
