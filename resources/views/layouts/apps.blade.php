<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Gadai Efek Mesin</title>

  <meta id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="{{asset('admins/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('admins/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css')}}">
  <link rel="stylesheet" href="{{asset('admins/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css')}}">
  <link rel="stylesheet" href="{{asset('admins/vendors/css/vendor.bundle.base.css')}}">
  <link rel="stylesheet" href="{{asset('admins/vendors/css/vendor.bundle.addons.css')}}">

  <link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
  <link rel="stylesheet" href="{{asset('admins/css/style.css')}}">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/jquery-confirm.css') }}">
  <link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
  <link rel="stylesheet" href="{{ asset('css/datepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('css/preloader.css') }}">
  <link rel="stylesheet" href="{{ asset('css/build.css') }}">

  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <nav class="navbar default-layout col-lg-12 col-12 p-0">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="{{url('')}}">
          <!-- <img src="{{asset('admins/images/logo.svg')}}" alt="logo" /> -->
        </a>
        <a class="navbar-brand brand-logo-mini" href="index-2.html">
          <!-- <img src="{{asset('admins/images/logo-mini.svg')}}" alt="logo" /> -->
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center px-0">

        <ul class="navbar-nav navbar-nav-right">

          <li class="nav-item dropdown d-none d-xl-inline-block user-dropdown">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <div class="dropdown-toggle-wrapper">
                <div class="inner">
                  <img class="img-xs rounded-circle" src="{{asset('admins/images/faces/face1.jpg')}}" alt="Profile image">
                </div>
                <div class="inner">
                  <div class="inner">
                    <span class="profile-text font-weight-bold">{{Auth::user()->name}}</span>
                    <?php $role = App\Model\Role::find(Auth::user()->role_id); ?>
                    <small class="profile-text small">{{$role->role_name}}</small>
                  </div>
                  <div class="inner">
                    <div class="icon-wrapper">
                      <i class="mdi mdi-chevron-down"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item p-0">
                <div class="d-flex border-bottom">
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                    <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                  </div>
                </div>
              </a>
              <a href="{{url('users/change-password/'.Auth::user()->email.'/'.Auth::user()->id)}}" class="dropdown-item">
                Change Password
              </a>
              <a href="{{url('logout')}}" class="dropdown-item">
                Sign Out
              </a>
            </div>
          </li>



        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>

    <div class="container-fluid page-body-wrapper px-0">
      <nav class="sidebar sidebar-offcanvas sidebar-dark" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <img src="{{asset('admins/images/sidebar_big.png')}}" alt="profile image" width="300">
            <!-- <p class="text-center font-weight-medium">Gadai Efek Mesin</p> -->
          </li>

          <!-- START MENU -->
          <?php
            $module_parent = DB::table('role_acl')->join('modules','role_acl.module_parent','=','modules.id')
            ->where('role_id', Auth::user()->role_id)
            ->where(function ($query){
              $query->where('create_acl','<>',0)
                    ->orWhere('read_acl','<>',0)
                    ->orWhere('update_acl','<>',0)
                    ->orWhere('delete_acl','<>',0);
            })
            ->groupBy('module_parent')
            ->orderBy('menu_order','asc')
            ->get();
            // dd($module_parent);
          ?>
          @foreach($module_parent as $parent)
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#{{$parent->pathParent}}" aria-expanded="false" aria-controls="apps-dropdown">
              <i class="menu-icon {{$parent->menu_icon}}"></i>
              <span class="menu-title">{{$parent->module_name}}</span>
            </a>
            <div class="collapse {{ Request::is($parent->pathParent.'/*') ? 'show' : '' }}" id="{{$parent->pathParent}}">
              <ul class="nav flex-column sub-menu">

                <?php
                  $module_childs = DB::table('role_acl')
                            ->join('modules','role_acl.module_id','=','modules.kdModule')
                            ->where('module_parent',$parent->module_parent)
                            ->where('role_id', Auth::user()->role_id)
                            ->where(function ($query) {
                                $query->where('create_acl','<>',0)
                                        ->orWhere('read_acl','<>',0)
                                        ->orWhere('update_acl','<>',0)
                                        ->orWhere('delete_acl','<>',0);
                            })
                            ->orderBy('menu_order','asc')
                            ->get();

                ?>
            @foreach($module_childs as $child)
              <li class="nav-item">
                <a class="nav-link" href="{{url($child->menu_path)}}">{{$child->module_name}}</a>
              </li>
            @endforeach
              </ul>
            </div>
          </li>
          @endforeach


          <!-- END MENU -->
        </ul>
      </nav>
      <div class="main-panel">
        @yield('content')
        <!-- <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="#" target="_blank">PT. Pegadaian Persero</a>. All rights reserved.</span>
          </div>
        </footer> -->
      </div>

      @yield('modal')
    </div>
  </div>

  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/autoNumeric.js') }}"></script>
  <script src="{{ asset('js/datepicker.js') }}"></script>
  <script src="{{ asset('js/jszip.js') }}"></script>
  <script src="{{ asset('js/datatables.js') }}"></script>

 <!-- <script src="{{asset('admins/js/off-canvas.js')}}"></script>
  <script src="{{asset('admins/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('admins/js/misc.js')}}"></script>
  <script src="{{asset('admins/js/settings.js')}}"></script>
  <script src="{{asset('admins/js/todolist.js')}}"></script>
  <script src="{{asset('admins/js/dashboard.js')}}"></script>
  <script src="{{asset('admins/js/alerts.js')}}"></script> -->

  <!-- JQuery Confirm -->
  <script src="{{ asset('js/jquery-confirm.js') }}"></script>

  <!-- Chosen Select -->
  <script src="{{ asset('js/chosen.js') }}"></script>

  <script src="{{asset('js/jstnumber.js')}}"></script>
  <script src="{{asset('js/parent.js')}}"></script>
  <script src="{{asset('js/maskMoney.js')}}"></script>
  <script src="{{asset('js/moment.js')}}"></script>
  <script src="{{asset('js/moment-locale.js')}}"></script>
  <script src="{{asset('PDFObject/pdfobject.js')}}"></script>
  @yield('script')
  <script>
  @if(count($errors) > 0 || Session::has('success') || Session::has('info') || Session::has('warning'))
    $.confirm({
      title: '{{Session::get('info')}}',
      content: '{{Session::get('alert')}}',
      type: '{{Session::get('colors')}}',
      icon: '{{Session::get('icons')}}',
      typeAnimated: true,
      buttons: {
          close: function () {
          }
        }
      });
  @elseif(count($errors) == 0)
  return false;
  @else
  $.confirm({
    title: '{{Session::has('info')}}',
    content: '{{Session::get('alert')}}',
    type: 'red',
    typeAnimated: true,
    icon: 'fas fa-exclamation-triangle',
    buttons: {
        close: function () {
        }
    }
});
  @endif
  </script>
</body>
</html>
