@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
                @include('inc.button.get_data', ['urls' => '/monitoring/closingPrice'])
                @include('inc.button.download', ['urlss' => 'javascript:void(0)'])
                @include('inc.button.upload', ['urls' => url('/monitoring/closingPrice/upload')])
                <h4 class="card-title">Harga Penutupan</h4><br>
                <div class="form-group row">
                  <label class="col-form-label col-md-1">Tanggal</label>
                  <input class="form-control form-control-sm col-md-2" name="datepik" id="datepik">
                  <input class="form-control form-control-sm col-md-2" name="datepiks" type="hidden" id="datepiks">
                  <label class="col-form-label col-md-1">Efek</label>
                  <select class="form-control form-control-sm col-md-2" name="tipeEfek" id="tipeEfek">
                    <option value="S">Saham</option>
                    <option value="O">Obligasi</option>
                  </select>
                </div>
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <form>
                  @csrf
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Stock Code</th>
                        <th class="text-center">Harga</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('modal')
    @include('modals.modalCloseprice')

      <div class="modal" id="modalDownload" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Download Sampel</h5>
              <button type="button" id="close-sampel" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 text-center">
                    <a href="javascript:void(0)" id="btn-obligasi" class="btn btn-lg btn-primary">Obligasi</a>
                    <a href="javascript:void(0)" id="btn-saham" class="btn btn-lg btn-primary">Saham</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@endsection


@section('script')
<script>

$('#btnSampel').on('click', function(){
   $('#modalDownload').modal();
});

$('#btn-obligasi').on('click', function(){
   $('#close-sampel').click();
   location.href = '{{url('monitoring/closingPrice/downloadO')}}';
});

$('#btn-saham').on('click', function(){
   $('#close-sampel').click();
   location.href = '{{url('monitoring/closingPrice/downloadS')}}';
});

function getDate(){
    var yest = new Date(new Date() - 24 * 60 * 60 * 1000);
    var month = yest.getMonth();
    var year = yest.getFullYear();
    var date = yest.getDate();
    var startDate = new Date(year, month, date);
    $('#datepik').datepicker({
        locale: 'id',
        format: 'dd M yyyy',
        autoclose: true
    });
    $('#datepiks').datepicker({
        locale: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('#datepik').datepicker('setDate', startDate);
    $('#datepiks').datepicker('setDate', startDate);
}

// $("#downloadSample").confirm({
//       title: 'Confirmation',
//       content: 'Accept this record ?',
//       icon: 'fas fa-exclamation-triangle',
//       buttons: {
//           confirm: function () {
//               location.href = "{{url('monitoring/closingPrice/downloadS')}}";
//           },
//           cancel: function () {
//               location.href = "{{url('monitoring/closingPrice/downloadO')}}";
//           }
//       },
//       confirmButton: 'Yes i agree',
//       cancelButton: 'NO never !'
// });


  function convertMoment(date){
    var dates = moment(date).locale('id').format('YYYY-MM-DD');
    $('#datepiks').val(dates);
    // return dates;
  }

var table = $("#lookup").dataTable({
  columns:
    [
      {data: 'id'},
      {data: 'tanggal'},
      {data: 'kodeEfek'},
      {data: 'harga'}
    ],
  });

  $('.btn-get').on('click', function(){
      $('#modal-closeprice').modal();
      $('.btn-ok').attr('id','btngetsaham')
  });

  $(document).ready(function() {
    getDate();
    var tanggal = $('#datepiks').val();
    var tipeEfek = $('#tipeEfek').val();
    loadData(tanggal,tipeEfek);
  });

  $('#datepik').on('change', function(){
      convertMoment($('#datepik').val());
      loadData();
  });
  $('#tipeEfek').on('change', function(){
      loadData();
  });

  function loadData(tanggal,tipeEfek){
    $('#lookup').dataTable().fnDestroy();

    var tanggal = $('#datepiks').val();
    var tipeEfek = $('#tipeEfek').val();
    var table = $("#lookup").dataTable({
      processing: true,
      serverSide: true,
      ajax:{
        url: "{{ url('monitoring/closingPrice/getIndex/') }}",
        dataType: "json",
        type: "POST",
        data: {
          tanggal: tanggal,
          tipeEfek: tipeEfek,
        },

      },
      autoWidth: false,
      columns: [
                {data: 'id',width: "30px"},
                {data: 'tanggal',width: "30%"},
                {data: 'kodeEfek',width: "30%"},
                {data: 'harga',width: "30%"}
            ],
      columnDefs: [
              {
                  "targets": [0],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).text(row+1);
                },
                orderable: true
              },
              {
                  "targets": [1],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).addClass("text-center");
                    var dates = moment(cellData).locale('id').format('ll');
                    $(td).text(dates);
                  },
              },
              {
                  "targets": [3],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    if($('#tipeEfek').val()=="O"){
                      $(td).append($('<span>')
                                  .addClass('percentComma')
                                  .text(cellData*100)
                              )
                    }else{
                      $(td).append($('<span>')
                                  .addClass('currencyNoComma')
                                  .text(cellData)
                              )
                    }

                  },
              }
          ],
      drawCallback: function(settings) {
             initAutoNumeric();
          },
      });
  }
</script>
@endsection
