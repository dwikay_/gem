@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Harga Penutupan</h4>
              <p class="card-description">
                Upload Form
              </p>
              <form class="forms-sample" action="{{url('monitoring/closingPrice/upload/store')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">File Saham</label>
                      <input type="file" class="form-control form-control-sm clasesform" id="saham" name="saham" placeholder="" accept=".csv, .txt">
                    </div>                    
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">File Obligasi</label>
                      <input type="file" class="form-control form-control-sm clasesform" id="obligasi" name="obligasi" placeholder="" accept=".csv, .txt">
                    </div>
                    <div class="form-group mt-10 text-right">
                      @include('inc.button.submit')
                      @include('inc.button.cancel')
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
