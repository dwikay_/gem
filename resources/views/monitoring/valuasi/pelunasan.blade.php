@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title float-left">Monitoring Pelunasan</h4>
              </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No. Kontrak</th>
                        <th>Kode Nasabah</th>
                        <th>Nama Nasabah</th>
                        <th>Nilai Kewajiban</th>
                        <th>Total Kewajiban</th>
                        <th>Total Agunan</th>
                        <th class="text-center">Rasio</th>
                        <th>Status</th>
                        <th>Topup Cash</th>
                        <th>Topup Collateral</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('modal')
    @include('modals.modalDetailMonitorings')
@endsection
@section('script')
<script>

var table = $("#lookup").dataTable({
  columns: [
    {data: 'noKontrak'},
    {data: 'kodeNasabah'},
    {data: 'namaNasabah'},
    {data: 'nilaiKewajiban'},
    {data: 'totalKewajiban'},
    {data: 'totalCollateral'},
    {data: 'ratioPinjaman'},
    {data: 'status'},
    {data: 'topUpCash'},
    {data: 'topUpCollateral'},
    {data: 'noKontrak'}
  ],
  });

$(document).ready(function() {

  $('#lookup').dataTable().fnDestroy();

  table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    ajax:{
      url: "{{ url('monitoring/pelunasan/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="11">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
      {data: 'noKontrak'},
      {data: 'kodeNasabah'},
      {data: 'namaNasabah'},
      {data: 'nilaiKewajiban'},
      {data: 'totalKewajiban'},
      {data: 'totalCollateral'},
      {data: 'ratioPinjaman'},
      {data: 'status'},
      {data: 'topUpCash'},
      {data: 'topUpCollateral'},
      {data: 'noKontrak'}
    ],
    "scrollX": true,
    'scrollY': '100vh',
    "scrollCollapse": true,
    'autoWidth': true,
    'bSort': true,
    'bPaginate': true,
    'searching' : true,
    columnDefs: [

      {
          "targets": [3],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [4],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [5],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [6],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('percent')
                        .text(cellData*100)
                    )
          },
      },
      {
          "targets": [7],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            var status;
            var classnya;
            switch(cellData){
              case 'N' : status = 'Normal'; classnya = "success"; break;
              case 'MC' : status = 'Topup'; classnya = "warning"; break;
              case 'E' : status = 'Eksekusi'; classnya = "danger"; break;
            }
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('badge badge-'+classnya)
                        .text(status)
                    )
          },
      },
      {
          "targets": [8],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [9],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
        "targets": [10],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).addClass('text-center');
          $(td)
            .append($('@include('inc.button.btn_group_pelunasan')'));
        },
        orderable: false
      },
    ],
    drawCallback: function(settings) {
           initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }
  });

  $('.table').on('click','.btn-detail', function(){

      var tr = $(this).closest('tr');
      var row = table.api().row( tr );
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData();

      $.ajax({
        url: "{!! url('monitoring/transaksi/getDetail') !!}/" + data[index].noKontrak,
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
        {
          // $('#idrActual').html(data[0]["actualPrice"]);
          loadData(data);
        }
        });
        $('#modal-monitoring').modal();

      // loadData(data[index]);
  });

  $('.table').on('click','.btn-settle-record', function(){

      var tr = $(this).closest('tr');
      var row = table.api().row( tr );
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData();

      $.confirm({
          title: 'Confirmation',
          content: 'Apa anda yakin menyelesaikan transaksi ini ?',
          icon: 'fas fa-exclamation-triangle',
          buttons: {
              confirm: function () {
                  location.href="{{url('monitoring/pelunasan/settle')}}"
                  + "/" + data[index].noKontrak
                  + "/" + 0
              },
              cancel: function () {

              }
          }
      });
  });

  $('.table').on('click','.btn-withdraw', function(){

      var tr = $(this).closest('tr');
      var row = table.api().row( tr );
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData();

      location.href="{{url('monitoring/pelunasan/create-from-monitoring')}}"
      + "/" + data[index].noKontrak;
  });


});
</script>
@endsection
