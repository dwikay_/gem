@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Stock Mutation</h4>
              <p class="card-description">
                Stock Mutation Report
              </p>
              <form class="forms-sample" action="{{url('transaksi/stock-deposit/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">No Kontrak</label>
                      <div class="col-sm-2 input-group input-group-sm">
                        <input type="text" id="noKontrak" name="noKontrak" disabled class="form-control form-control-sm no-border" value="{{$param['noKontrak']}}" required="required">
                      </div>
                      <label class=" col-sm-2 col-form-label">Nama Nasabah</label>
                      <div class="col-sm-6">
                        <input type="text" id="namaNasabah" class="form-control form-control-sm" placeholder="" disabled="disabled" value="{{$param['namaNasabah']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Kode Nasabah</label>
                      <div class="col-sm-2">
                        <input type="text" id="kodeNasabah" class="form-control form-control-sm" placeholder="" disabled="disabled" value="{{$param['kodeNasabah']}}">
                      </div>
                      <label class=" col-sm-2 col-form-label">Efek</label>
                      <div class="col-sm-6">
                        <input type="text" id="namaNasabah" class="form-control form-control-sm" placeholder="" disabled="disabled" value="{{$param['trHeaderStockMutationList'][0]['kodeEfek']}} - {{$param['trHeaderStockMutationList'][0]['namaEfek']}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-2 col-form-label">CIF</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="totalKewajiban" value="{{$param['cif']}}" name="totalKewajiban" placeholder="" disabled="disabled">
                      </div>
                      <label class="col-sm-2 col-form-label">SubRek</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="totalAgunan" value="{{$param['subRek']}}" name="totalAgunan" placeholder="" disabled="disabled">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 pt-5">
                    <table class="table table-sm table-striped" id="lookup" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Kode Transaksi</th>
                          <th>Tanggal Settle</th>
                          <th>Tanggal Transaksi</th>
                          <th>Tipe Transaksi</th>
                          <th class="text-center">Harga Gadai</th>
                          <th class="text-center">Kredit</th>
                          <th class="text-center">Debit</th>
                          <th class="text-center">Balance</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 1;?>
                        @foreach($list as $lists)
                        <tr>
                          <td>{{$i++}}</td>
                          <td>{{$lists['transCode']}}</td>
                          <td>{{$lists['settleDate']}}</td>
                          <td>{{$lists['transDate']}}</td>
                          @if($lists['transType'] == "PG")
                          <td>Pencairan Gadai</td>
                          @elseif($lists['transType'] == "TU")
                          <td>Top Up Saham</td>
                          @else
                          <td>Cicil Gadai</td>
                          @endif
                          <td class="text-right">{!! number_format($lists['getGadaiPrice'] ,0,",",".") !!}</td>
                          <td class="text-right">{!! number_format($lists['credit'] ,0,",",".") !!}</td>
                          <td class="text-right">{!! number_format($lists['debit'] ,0,",",".") !!}</td>
                          <td class="text-right">{!! number_format($lists['balance'] ,0,",",".") !!}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                  @include('inc.button.cancel')
              </form>
            </div>
          </div>
        </div>
@endsection

@section('script')
  <script>
  </script>
@endsection
