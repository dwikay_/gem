@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
              <div class="form-group mt-10" role="group" aria-label="Basic example">
                <!-- @include('inc.button.eksekusi') -->
              </div>
                <h4 class="card-title">Monitoring Transaksi</h4><br>
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No. Kontrak</th>
                        <th>Kode Nasabah</th>
                        <th>Nama Nasabah</th>
                        <th>Nilai Kewajiban</th>
                        <th>Total Kewajiban</th>
                        <th>Total Agunan</th>
                        <th class="text-center">Rasio</th>
                        <th>Status</th>
                        <th>Topup Cash</th>
                        <th>Topup Collateral</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('modal')
    @include('modals.modalDetailMonitorings')
    @include('modals.modalViewEksekusi')
@endsection
@section('script')
<script>

var table = $("#lookup").dataTable({
  dom: 'lrtip',
  columns: [
    {data: 'noKontrak'},
    {data: 'kodeNasabah'},
    {data: 'namaNasabah'},
    {data: 'nilaiKewajiban'},
    {data: 'totalKewajiban'},
    {data: 'totalCollateral'},
    {data: 'ratioPinjaman'},
    {data: 'status'},
    {data: 'topUpCash'},
    {data: 'topUpCollateral'},
    {data: 'noKontrak'}
    ],
  });

  function detailTransaksi(kodeEfek, noKontrak){
    var url = "{{url('monitoring/transaksi/detailTransaksi')}}/" + kodeEfek + "/" + noKontrak;
    window.open(url,'_blank');
  }

$(document).ready(function() {

  $('#lookup').dataTable().fnDestroy();

  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    "scrollX": true,
    'scrollY': '100vh',
    "scrollCollapse": true,
    'autoWidth': true,
    'bSort': true,
    'bPaginate': true,
    'searching' : true,
    ajax:{
      url: "{{ url('monitoring/transaksi/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="11">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
      {data: 'noKontrak'},
      {data: 'kodeNasabah'},
      {data: 'namaNasabah'},
      {data: 'nilaiKewajiban'},
      {data: 'totalKewajiban'},
      {data: 'totalCollateral'},
      {data: 'ratioPinjaman'},
      {data: 'status'},
      {data: 'topUpCash'},
      {data: 'topUpCollateral'},
      {data: 'noKontrak'}
    ],
    columnDefs: [

      {
          "targets": [3],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [4],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [5],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [6],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('percent')
                        .text(cellData*100)
                    )
          },
      },
      {
          "targets": [7],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            var status;
            var classnya;
            switch(cellData){
              case 'N' : status = 'Normal'; classnya = "success"; break;
              case 'MC' : status = 'Topup'; classnya = "warning"; break;
              case 'E' : status = 'Eksekusi'; classnya = "danger"; break;
            }
            $(td).empty();
            if(cellData=="E"){
              $(td).append($('<a>')
                  .attr('href','{{url('monitoring/transaksi/eksekusi/')}}' + "/"
                  + rowData.defaultBrokerPartner
                  + "/"
                  + rowData.tipeEfek
                  + "/"
                  + rowData.noKontrak)
                  .append($('<span>')
                              .addClass('badge badge-'+classnya)
                              .text(status)
                          )

            )
          }else{
            $(td).append($('<span>')
                        .addClass('badge badge-'+classnya)
                        .text(status)
                    )
          }

          },
      },
      {
          "targets": [8],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [9],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
        "targets": [10],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td)
            .append($('@include('inc.button.detail')'));
        },
        orderable: false
      },
    ],
    drawCallback: function(settings) {
           initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }
  });

  $('.table').on('click','.btn-detail', function(){

      var tr = $(this).closest('tr');
      var row = table.api().row( tr );
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData();
      $.ajax({
        url: "{!! url('monitoring/transaksi/getDetail') !!}/" + data[index].noKontrak,
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
        {
          loadData(data);
        }
        });
        $('#modal-monitoring').modal();
  });

  $('.btn-eksekusi').on('click', function(){
    $('#modal-eksekusi').modal();
  });

});
</script>
@endsection
