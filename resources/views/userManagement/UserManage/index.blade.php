@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title float-left">Manajemen User</h4>
                  @include('inc.button.add', ['urls' => url('/usersGadai/userManage/create')])
              </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

function checkDeleteAkses(id){

  $.ajax({
      url : "{{ url('/usersGadai/userManage/checking-delete') }}/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        console.log(data['info']);
        if(data['info']=="Error"){

          $.confirm({
            title: data['info'],
            content: data['alert'],
            type: data['colors'],
            icon: data['icons'],
            typeAnimated: true,
            buttons: {
                close: function () {
                }
              }
            });

        }else{
          deletes(id);
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
          var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
          console.log(errorMsg);
      }
  });

}
function deletes(id){
  $.confirm({
      title: 'Confirmation',
      content: 'Delete this record ?',
      icon: 'fas fa-exclamation-triangle',
      buttons: {
          confirm: function () {
              location.href="{{url('usersGadai/userManage/delete')}}" + "/" + id;
          },
          cancel: function () {
          }
      }
  });
}

$(document).ready(function() {

var table = $("#lookup").dataTable({
  processing: true,
  serverSide: true,
  ajax:{
    url: "{{ url('usersGadai/userManage/getGlobal') }}",
    dataType: "json",
    type: "GET",
    error: function(){  // error handling
      $(".lookup-error").html("");
      $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="6">No data found in the server</th></tr></tbody>');
      $("#lookup_processing").css("display","none");

    }
  },
  columns: [
            {data: 'id'},
            {data: 'name'},
            {data: 'email'},
            {data: 'email'},
            {data: 'status_user'},
            {data: 'id'}
        ],
        columnDefs: [
          {
            "targets": [0],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).text(row+1);
            },
            orderable: false
          },
          {
            "targets": [3],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).text(rowData.role.role_name);
            },
            orderable: false
          },
          {
            "targets": [4],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              var status;
              if(cellData=="1"){
                $(td).text("Aktif")
                $(td).css('color','green');
                $(td).css('font-weight','700');
              }else{
                  $(td).text("Tidak Aktif")
                  $(td).css('color','red');
                  $(td).css('font-weight','700');
              }
            },
            orderable: false
          },
          {
            "targets": [5],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).addClass("text-center");
              if(rowData.role.id==1 || rowData.role.id=="1"){
              }else{
                if(rowData.limit_password == 3){
                  $(td).append($('@include('inc.button.btnUsersEn')'));
                }else{
                  $(td).append($('@include('inc.button.btnUsersDis')'));
                }

              }
            },
            orderable: false
          },
        ],
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    }
      });

    $('.table').on('click','.btn-edit-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        location.href="{{url('usersGadai/userManage/edit/')}}" + "/" + data[index].id;
    });

    $('.table').on('click','.btn-remove-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        // deletes(data[index].id);
        checkDeleteAkses(data[index].id);
    });

    $('.table').on('click','.btn-enable-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        $.confirm({
            title: 'Confirmation',
            content: 'Enable this user ?',
            icon: 'fas fa-exclamation-triangle',
            buttons: {
                confirm: function () {
                    location.href="{{url('usersGadai/userManage/enabled')}}" + "/" + data[index].id;
                },
                cancel: function () {
                }
            }
        });
    });

    $('.table').on('click','.btn-disable-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        $.confirm({
            title: 'Confirmation',
            content: 'Disable this user ?',
            icon: 'fas fa-exclamation-triangle',
            buttons: {
                confirm: function () {
                    location.href="{{url('usersGadai/userManage/disabled')}}" + "/" + data[index].id;
                },
                cancel: function () {
                }
            }
        });

    });

});
</script>
@endsection
