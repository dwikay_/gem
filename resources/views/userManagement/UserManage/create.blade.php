@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Manajemen User</h4>
              <p class="card-description">
                Input Form
              </p>
              <form id="fpro" class="forms-sample">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Nama</label>
                      <input type="text" class="form-control form-control-sm" id="nama" name="nama" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Username</label>
                      <input type="text" class="form-control" id="email" name="email" placeholder="" required>
                    </div>
                  </div><br>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Akses</label>
                      <select name="role" id="role" class="form-control form-control-sm">
                        @foreach($role as $roles)
                        <option value="{{$roles->id}}">{{$roles->role_name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Status</label>
                      <select name="status_user" id="status_user" class="form-control form-control-sm">
                        <option value="1">Aktif</option>
                        <option value="2">Tidak Aktif</option>
                      </select>
                    </div>
                  </div><br>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Password</label>
                      <input type="password" class="form-control" id="password" name="password" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Konfirmasi Password</label>
                      <input type="password" class="form-control" id="confirm" name="confirm" placeholder="" required>
                    </div>
                  </div>
                    <div class="col-md-12 mt-10">
                       <a class="btn btn-sm btn-success" style="color:white;"><span class="fa fa-check fa-xs"></span>&nbsp;&nbsp;Simpan</a>
                        @include('inc.button.cancel')
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

  $('.btn-success').on('click', function(){
      var password = $('#password').val();
      var confirm = $('#confirm').val();
      var nama = $('#nama').val();
      var email = $('#email').val();
      if(nama==""){
        $.alert({
          title: 'Error',
          content: 'Nama tidak boleh kosong!',
          icon: 'fa fa-times'
        });
      }
      else if(email==""){
        $.alert({
          title: 'Error',
          content: 'Email tidak boleh kosong!',
          icon: 'fa fa-times'
        });
      }
      else if(password==""){
        $.alert({
          title: 'Error',
          content: 'Password tidak boleh kosong!',
          icon: 'fa fa-times'
        });
      }
      else if(confirm==""){
        $.alert({
          title: 'Error',
          content: 'Password Konfirmasi tidak boleh kosong!',
          icon: 'fa fa-times'
        });
      }
      else{
        if(password!=confirm){
          $.alert({
            title: 'Error',
            content: 'Password tidak sama!',
            icon: 'fa fa-times'
          });
        }
        else{
          $('#fpro').attr('action','{{url('usersGadai/userManage/store')}}')
          $('#fpro').attr('method','post')
          $('#fpro').submit();
        }
      }
  });
</script>
@endsection
