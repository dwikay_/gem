@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Grup User</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('usersGadai/userGroup/update/'.$role->id)}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Nama Role</label>
                      <input type="text" class="form-control form-control-sm" id="role_name" name="role_name" value="{{$role->role_name}}" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Deskripsi</label>
                      <input type="text" class="form-control form-control-sm" id="description" name="description" value="{{$role->description}}" placeholder="" required>
                    </div>
                  </div>

                  <div class="col-md-12 grid-margin stretch-card mt-4">
                    <div class="card">
                      <div class="card-body">
                        <p class="card-description">
                          Hak Akses
                        </p>
                                <div class="box-tab" id="mtab2">
                                              <ul class="nav nav-tabs tab-basic">
                                                  <li class="nav-item active" id="91"><a class="nav-link active" href="#pospengaturan" data-toggle="tab">Monitoring Transaksi</a></li>
                                                  <li class="nav-item" id="92"><a class="nav-link" href="#pospenjualan" data-toggle="tab">Data Statis</a></li>
                                                  <li class="nav-item" id="93"><a class="nav-link" href="#posinventory" data-toggle="tab">Transaksi</a></li>
                                                  <li class="nav-item" id="95"><a class="nav-link" href="#posreport" data-toggle="tab">Laporan</a></li>
                                                  <li class="nav-item" id="94"><a class="nav-link" href="#poslaporan" data-toggle="tab">Pengaturan User</a></li>
                                              </ul><br>
                                              <div class="tab-content text-center">
                                                  <div class="tab-pane fade in active show" id="pospengaturan">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <?php $pengaturan91 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '9999999991')->first() ;?>
                                                              <tr id='minimal-checkbox-9999999991'>
                                                                  <td>Transaksi</td>
                                                                  <td></td>
                                                                  <td><input name="9999999991_read" type="checkbox" id="minimal-checkbox-9999999991" value="9999999991" {{$pengaturan91 != null && $pengaturan91->read_acl == '9999999991' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999991" value="all" name="all" onChange="check(9999999991)" /></td>
                                                              </tr>
                                                              <?php $pengaturan920 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999920')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999920'>
                                                                  <td>Pelunasan</td>
                                                                  <td></td>
                                                                  <td><input name="99999999920_read" type="checkbox" id="minimal-checkbox-99999999920" value="99999999920" {{$pengaturan920 != null && $pengaturan920->read_acl == '99999999920' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999920" value="all" name="all" onChange="check(99999999920)" /></td>
                                                              </tr>
                                                              <?php $pengaturan92 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '9999999992')->first() ;?>
                                                              <tr id='minimal-checkbox-9999999992'>
                                                                  <td>Harga Penutupan</td>
                                                                  <td><input name="9999999992_create" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992" {{$pengaturan92 != null && $pengaturan92->create_acl == '9999999992' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999992_read" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992" {{$pengaturan92 != null && $pengaturan92->read_acl == '9999999992' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999992_update" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992" {{$pengaturan92 != null && $pengaturan92->update_acl == '9999999992' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999992" value="all" name="all" onChange="check(9999999992)" /></td>
                                                              </tr>
                                                              <?php $pengaturan93 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '9999999993')->first() ;?>
                                                              <tr id='minimal-checkbox-9999999993'>
                                                                  <td>Haircut</td>
                                                                  <td><input name="9999999993_create" type="checkbox" id="minimal-checkbox-9999999993" value="9999999993" {{$pengaturan93 != null && $pengaturan93->create_acl == '9999999993' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999993_read" type="checkbox" id="minimal-checkbox-9999999993" value="9999999993" {{$pengaturan93 != null && $pengaturan93->read_acl == '9999999993' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999993" value="all" name="all" onChange="check(9999999993)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="pospenjualan">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <?php $pengaturan94 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '9999999994')->first() ;?>
                                                              <tr id='minimal-checkbox-9999999994'>
                                                                  <td>Global Parameter</td>
                                                                  <td></td>
                                                                  <td><input name="9999999994_read" type="checkbox" id="minimal-checkbox-9999999994" value="9999999994" {{$pengaturan94 != null && $pengaturan94->read_acl == '9999999994' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999994_update" type="checkbox" id="minimal-checkbox-9999999994" value="9999999994" {{$pengaturan94 != null && $pengaturan94->update_acl == '9999999994' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999994" value="all" name="all" onChange="check(9999999994)" /></td>
                                                              </tr>
                                                              <?php $pengaturan924 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999924')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999924'>
                                                                  <td>Batas Pinjaman</td>
                                                                  <td><input name="99999999924_create" type="checkbox" id="minimal-checkbox-99999999924" value="99999999924" {{$pengaturan924 != null && $pengaturan924->create_acl == '99999999924' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999924_read" type="checkbox" id="minimal-checkbox-99999999924" value="99999999924" {{$pengaturan924 != null && $pengaturan924->read_acl == '99999999924' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999924" value="all" name="all" onChange="check(99999999924)" /></td>
                                                              </tr>
                                                              <?php $pengaturan925 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999925')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999925'>
                                                                  <td>BMPK Emiten</td>
                                                                  <td><input name="99999999925_create" type="checkbox" id="minimal-checkbox-99999999925" value="99999999925" {{$pengaturan925 != null && $pengaturan925->create_acl == '99999999925' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999925_read" type="checkbox" id="minimal-checkbox-99999999925" value="99999999925" {{$pengaturan925 != null && $pengaturan925->read_acl == '99999999925' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999925" value="all" name="all" onChange="check(99999999925)" /></td>
                                                              </tr>
                                                              <?php $pengaturan95 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '9999999995')->first() ;?>
                                                              <tr id='minimal-checkbox-9999999995'>
                                                                  <td>Nasabah</td>
                                                                  <td></td>
                                                                  <td><input name="9999999995_read" type="checkbox" id="minimal-checkbox-9999999995" value="9999999995" {{$pengaturan95 != null && $pengaturan95->read_acl == '9999999995' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999995_update" type="checkbox" id="minimal-checkbox-9999999995" value="9999999995" {{$pengaturan95 != null && $pengaturan95->update_acl == '9999999995' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999995" value="all" name="all" onChange="check(9999999995)" /></td>
                                                              </tr>
                                                              <?php $pengaturan96 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '9999999996')->first() ;?>
                                                              <tr id='minimal-checkbox-9999999996'>
                                                                  <td>Sekuritas</td>
                                                                  <td><input name="9999999996_create" type="checkbox" id="minimal-checkbox-9999999996" value="9999999996" {{$pengaturan96 != null && $pengaturan96->create_acl == '9999999996' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999996_read" type="checkbox" id="minimal-checkbox-9999999996" value="9999999996" {{$pengaturan96 != null && $pengaturan96->read_acl == '9999999996' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999996_update" type="checkbox" id="minimal-checkbox-999999999" value="9999999996" {{$pengaturan96 != null && $pengaturan96->update_acl == '9999999996' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999996_delete" type="checkbox" id="minimal-checkbox-999999999" value="9999999996" {{$pengaturan96 != null && $pengaturan96->delete_acl == '9999999996' ? 'checked' : ''}}></td>
                                                                  <td><input type="checkbox" id="all-9999999996" value="all" name="all" onChange="check(9999999996)" /></td>
                                                              </tr>
                                                              <?php $pengaturan97 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '9999999997')->first() ;?>
                                                              <tr id='minimal-checkbox-9999999997'>
                                                                  <td>Kustodi</td>
                                                                  <td><input name="9999999997_create" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997" {{$pengaturan97 != null && $pengaturan97->create_acl == '9999999997' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999997_read" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997" {{$pengaturan97 != null && $pengaturan97->read_acl == '9999999997' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999997_update" type="checkbox" id="minimal-checkbox-999999997" value="9999999997" {{$pengaturan97 != null && $pengaturan97->update_acl == '9999999997' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999997_delete" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997" {{$pengaturan97 != null && $pengaturan97->delete_acl == '9999999997' ? 'checked' : ''}}></td>
                                                                  <td><input type="checkbox" id="all-9999999997" value="all" name="all" onChange="check(9999999997)" /></td>
                                                              </tr>
                                                              <?php $pengaturan98 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '9999999998')->first() ;?>
                                                              <tr id='minimal-checkbox-9999999998'>
                                                                  <td>Efek</td>
                                                                  <td></td>
                                                                  <td><input name="9999999998_read" type="checkbox" id="minimal-checkbox-9999999998" value="9999999998" {{$pengaturan98 != null && $pengaturan98->read_acl == '9999999998' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999998_update" type="checkbox" id="minimal-checkbox-999999998" value="9999999998" {{$pengaturan98 != null && $pengaturan98->update_acl == '9999999998' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999998" value="all" name="all" onChange="check(9999999998)" /></td>
                                                              </tr>
                                                              <?php $pengaturan99 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '9999999999')->first() ;?>
                                                              <tr id='minimal-checkbox-9999999999'>
                                                                  <td>Hari Libur</td>
                                                                  <td><input name="9999999999_create" type="checkbox" id="minimal-checkbox-9999999999" value="9999999999" {{$pengaturan99 != null && $pengaturan99->create_acl == '9999999999' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999999_read" type="checkbox" id="minimal-checkbox-9999999999" value="9999999999" {{$pengaturan99 != null && $pengaturan99->read_acl == '9999999999' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999999_update" type="checkbox" id="minimal-checkbox-9999999999" value="9999999999" {{$pengaturan99 != null && $pengaturan99->update_acl == '9999999999' ? 'checked' : ''}}></td>
                                                                  <td><input name="9999999999_delete" type="checkbox" id="minimal-checkbox-9999999999" value="9999999999" {{$pengaturan99 != null && $pengaturan99->delete_acl == '9999999999' ? 'checked' : ''}}></td>
                                                                  <td><input type="checkbox" id="all-9999999999" value="all" name="all" onChange="check(9999999999)" /></td>
                                                              </tr>
                                                              <?php $pengaturan910 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999910')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999910'>
                                                                  <td>Negara</td>
                                                                  <td><input name="99999999910_create" type="checkbox" id="minimal-checkbox-99999999910" value="99999999910" {{$pengaturan910 != null && $pengaturan910->create_acl == '99999999910' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999910_read" type="checkbox" id="minimal-checkbox-99999999910" value="99999999910" {{$pengaturan910 != null && $pengaturan910->read_acl == '99999999910' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999910_update" type="checkbox" id="minimal-checkbox-99999999910" value="99999999910" {{$pengaturan910 != null && $pengaturan910->update_acl == '99999999910' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999910_delete" type="checkbox" id="minimal-checkbox-99999999910" value="99999999910" {{$pengaturan910 != null && $pengaturan910->delete_acl == '99999999910' ? 'checked' : ''}}></td>
                                                                  <td><input type="checkbox" id="all-99999999910" value="all" name="all" onChange="check(99999999910)" /></td>
                                                              </tr>
                                                              <?php $pengaturan911 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999911')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999911'>
                                                                  <td>Provinsi</td>
                                                                  <td><input name="99999999911_create" type="checkbox" id="minimal-checkbox-99999999911" value="99999999911" {{$pengaturan911 != null && $pengaturan911->create_acl == '99999999911' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999911_read" type="checkbox" id="minimal-checkbox-99999999911" value="99999999911" {{$pengaturan911 != null && $pengaturan911->read_acl == '99999999911' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999911_update" type="checkbox" id="minimal-checkbox-99999999911" value="99999999911" {{$pengaturan911 != null && $pengaturan911->update_acl == '99999999911' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999911_delete" type="checkbox" id="minimal-checkbox-99999999911" value="99999999911" {{$pengaturan911 != null && $pengaturan911->delete_acl == '99999999911' ? 'checked' : ''}}></td>
                                                                  <td><input type="checkbox" id="all-99999999911" value="all" name="all" onChange="check(99999999911)" /></td>
                                                              </tr>
                                                              <?php $pengaturan912 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999912')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999912'>
                                                                  <td>Kota</td>
                                                                  <td><input name="99999999912_create" type="checkbox" id="minimal-checkbox-99999999912" value="99999999912" {{$pengaturan912 != null && $pengaturan912->create_acl == '99999999912' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999912_read" type="checkbox" id="minimal-checkbox-99999999912" value="99999999912" {{$pengaturan912 != null && $pengaturan912->read_acl == '99999999912' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999912_update" type="checkbox" id="minimal-checkbox-99999999912" value="99999999912" {{$pengaturan912 != null && $pengaturan912->update_acl == '99999999912' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999912_delete" type="checkbox" id="minimal-checkbox-99999999912" value="99999999912" {{$pengaturan912 != null && $pengaturan912->delete_acl == '99999999912' ? 'checked' : ''}}></td>
                                                                  <td><input type="checkbox" id="all-99999999912" value="all" name="all" onChange="check(99999999912)" /></td>
                                                              </tr>
                                                              <?php $pengaturan913 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999913')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999913'>
                                                                  <td>Kode Pajak</td>
                                                                  <td><input name="99999999913_create" type="checkbox" id="minimal-checkbox-99999999913" value="99999999913" {{$pengaturan913 != null && $pengaturan913->create_acl == '99999999913' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999913_read" type="checkbox" id="minimal-checkbox-99999999913" value="99999999913" {{$pengaturan913 != null && $pengaturan913->read_acl == '99999999913' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999913_update" type="checkbox" id="minimal-checkbox-99999999913" value="99999999913" {{$pengaturan913 != null && $pengaturan913->update_acl == '99999999913' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999913_delete" type="checkbox" id="minimal-checkbox-99999999913" value="99999999913" {{$pengaturan913 != null && $pengaturan913->delete_acl == '99999999913' ? 'checked' : ''}}></td>
                                                                  <td><input type="checkbox" id="all-99999999913" value="all" name="all" onChange="check(99999999913)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="posinventory">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                                <?php $pengaturan914 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999914')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999914'>
                                                                  <td>Pengajuan Pinjaman</td>
                                                                  <td></td>
                                                                  <td><input name="99999999914_read" type="checkbox" id="minimal-checkbox-99999999914" value="99999999914" {{$pengaturan914 != null && $pengaturan914->read_acl == '99999999914' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999914_update" type="checkbox" id="minimal-checkbox-99999999914" value="99999999914" {{$pengaturan914 != null && $pengaturan914->update_acl == '99999999914' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999914" value="all" name="all" onChange="check(99999999914)" /></td>
                                                              </tr>
                                                              <?php $pengaturan915 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999915')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999915'>
                                                                  <td>Topup Agunan</td>
                                                                  <td><input name="99999999915_create" type="checkbox" id="minimal-checkbox-99999999915" value="99999999915" {{$pengaturan915 != null && $pengaturan915->create_acl == '99999999915' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999915_read" type="checkbox" id="minimal-checkbox-99999999915" value="99999999915" {{$pengaturan915 != null && $pengaturan915->read_acl == '99999999915' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999915_update" type="checkbox" id="minimal-checkbox-99999999915" value="99999999915" {{$pengaturan915 != null && $pengaturan915->read_acl == '99999999915' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999915" value="all" name="all" onChange="check(99999999915)" /></td>
                                                              </tr>
                                                              <?php $pengaturan916 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999916')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999916'>
                                                                  <td>Eksekusi</td>
                                                                  <td><input name="99999999916_create" type="checkbox" id="minimal-checkbox-99999999916" value="99999999916" {{$pengaturan916 != null && $pengaturan916->create_acl == '99999999916' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999916_read" type="checkbox" id="minimal-checkbox-99999999916" value="99999999916" {{$pengaturan916 != null && $pengaturan916->read_acl == '99999999916' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999916_update" type="checkbox" id="minimal-checkbox-99999999916" value="99999999916" {{$pengaturan916 != null && $pengaturan916->read_acl == '99999999916' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999916" value="all" name="all" onChange="check(99999999916)" /></td>
                                                              </tr>
                                                              <?php $pengaturan917 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999917')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999917'>
                                                                  <td>Pelunasan Pinjaman</td>
                                                                  <td><input name="99999999917_create" type="checkbox" id="minimal-checkbox-99999999917" value="99999999917" {{$pengaturan917 != null && $pengaturan917->create_acl == '99999999917' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999917_read" type="checkbox" id="minimal-checkbox-99999999917" value="99999999917" {{$pengaturan917 != null && $pengaturan917->read_acl == '99999999917' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999917_update" type="checkbox" id="minimal-checkbox-99999999917" value="99999999917" {{$pengaturan917 != null && $pengaturan917->read_acl == '99999999917' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999917" value="all" name="all" onChange="check(99999999917)" /></td>
                                                              </tr>
                                                              <?php $pengaturan926 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999926')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999926'>
                                                                  <td>Simulasi Topup</td>
                                                                  <td></td>
                                                                  <td><input name="99999999926_read" type="checkbox" id="minimal-checkbox-99999999926" value="99999999926" {{$pengaturan926 != null && $pengaturan926->read_acl == '99999999926' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999926" value="all" name="all" onChange="check(99999999926)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="posreport">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <?php $pengaturan921 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999921')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999921'>
                                                                  <td>Risk Parameter</td>
                                                                  <td></td>
                                                                  <td><input name="99999999921_read" type="checkbox" id="minimal-checkbox-99999999921" value="99999999921" {{$pengaturan921 != null && $pengaturan921->read_acl == '99999999921' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999921" value="all" name="all" onChange="check(99999999921)" /></td>
                                                              </tr>
                                                              <?php $pengaturan922 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999922')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999922'>
                                                                  <td>Stock Concentration Limit</td>
                                                                  <td></td>
                                                                  <td><input name="99999999922_read" type="checkbox" id="minimal-checkbox-99999999922" value="99999999922" {{$pengaturan922 != null && $pengaturan922->read_acl == '99999999922' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999922" value="all" name="all" onChange="check(99999999922)" /></td>
                                                              </tr>
                                                              <?php $pengaturan923 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999923')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999923'>
                                                                  <td>Stock Position</td>
                                                                  <td></td>
                                                                  <td><input name="99999999923_read" type="checkbox" id="minimal-checkbox-99999999923" value="99999999923" {{$pengaturan923 != null && $pengaturan923->read_acl == '99999999923' ? 'checked' : ''}}></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999923" value="all" name="all" onChange="check(99999999923)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="poslaporan">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                                <?php $pengaturan918 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999918')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999918'>
                                                                  <td>Manajemen User</td>
                                                                  <td><input name="99999999918_create" type="checkbox" id="minimal-checkbox-99999999918" value="99999999918" {{$pengaturan918 != null && $pengaturan918->create_acl == '99999999918' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999918_read" type="checkbox" id="minimal-checkbox-99999999918" value="99999999918" {{$pengaturan918 != null && $pengaturan918->read_acl == '99999999918' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999918_update" type="checkbox" id="minimal-checkbox-99999999918" value="99999999918" {{$pengaturan918 != null && $pengaturan918->update_acl == '99999999918' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999918_delete" type="checkbox" id="minimal-checkbox-99999999918" value="99999999918" {{$pengaturan918 != null && $pengaturan918->delete_acl == '99999999918' ? 'checked' : ''}}></td>
                                                                  <td><input type="checkbox" id="all-99999999918" value="all" name="all" onChange="check(99999999918)" /></td>
                                                              </tr>
                                                              <?php $pengaturan919 = \App\Model\Roleacl::where('role_id', $role->id)->where('module_id', '99999999919')->first() ;?>
                                                              <tr id='minimal-checkbox-99999999919'>
                                                                  <td>Grup User</td>
                                                                  <td><input name="99999999919_create" type="checkbox" id="minimal-checkbox-99999999919" value="99999999919" {{$pengaturan919 != null && $pengaturan919->create_acl == '99999999919' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999919_read" type="checkbox" id="minimal-checkbox-99999999919" value="99999999919" {{$pengaturan919 != null && $pengaturan919->read_acl == '99999999919' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999919_update" type="checkbox" id="minimal-checkbox-99999999919" value="99999999919" {{$pengaturan919 != null && $pengaturan919->update_acl == '99999999919' ? 'checked' : ''}}></td>
                                                                  <td><input name="99999999919_delete" type="checkbox" id="minimal-checkbox-99999999919" value="99999999919" {{$pengaturan919 != null && $pengaturan919->delete_acl == '99999999919' ? 'checked' : ''}}></td>
                                                                  <td><input type="checkbox" id="all-99999999919" value="all" name="all" onChange="check(99999999919)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                    </div>
                  </div>
                </div>
                    <div class="col-md-12 mt-10">
                        @include('inc.button.submit')
                        @include('inc.button.cancel')
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

$('input[type="checkbox"]').css('cursor','pointer');

function check(no) {
            document.getElementById('minimal-checkbox-'+no).addEventListener('change', function(e) {
                var el = e.target;
                var inputs = document.getElementById('minimal-checkbox-'+no).getElementsByTagName('input');

                if (el.id === 'all-'+no) {
                    for (var i = 0, input; input = inputs[i++]; ) {
                        input.checked = el.checked;
                    }
                } else {
                    var numChecked = 0;

                    for (var i = 1, input; input = inputs[i++]; ) {
                        if (input.checked) {
                            numChecked++;
                        }
                    }
                    inputs[0].checked = numChecked === inputs.length - 1;
                }
            }, false);
        }
</script>
@endsection
