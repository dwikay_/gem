@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Grup User</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('usersGadai/userGroup/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Nama Role</label>
                      <input type="text" class="form-control form-control-sm" id="role_name" name="role_name" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Deskripsi</label>
                      <input type="text" class="form-control form-control-sm" id="description" name="description" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-12 grid-margin stretch-card mt-4">
                    <div class="card">
                      <div class="card-body">
                        <p class="card-description">
                          Hak Akses
                        </p>
                            <div class="box-tab" id="mtab2">
                                              <ul class="nav nav-tabs tab-basic">
                                                  <li class="nav-item active" id="91"><a class="nav-link active" href="#pospengaturan" data-toggle="tab">Monitoring Transaksi</a></li>
                                                  <li class="nav-item" id="92"><a class="nav-link" href="#pospenjualan" data-toggle="tab">Data Statis</a></li>
                                                  <li class="nav-item" id="93"><a class="nav-link" href="#posinventory" data-toggle="tab">Transaksi</a></li>
                                                  <li class="nav-item" id="95"><a class="nav-link" href="#posreport" data-toggle="tab">Laporan</a></li>
                                                  <li class="nav-item" id="94"><a class="nav-link" href="#poslaporan" data-toggle="tab">Pengaturan User</a></li>
                                              </ul><br>
                                              <div class="tab-content text-center">
                                                  <div class="tab-pane fade in active show" id="pospengaturan">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <tr id='minimal-checkbox-9999999991'>
                                                                  <td>Transaksi</td>
                                                                  <td></td>
                                                                  <td><input name="9999999991_read" type="checkbox" id="minimal-checkbox-9999999991" value="9999999991"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999991" value="all" name="all" onChange="check(9999999991)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999920'>
                                                                  <td>Pelunasan</td>
                                                                  <td></td>
                                                                  <td><input name="99999999920_read" type="checkbox" id="minimal-checkbox-99999999920" value="99999999920"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999920" value="all" name="all" onChange="check(99999999920)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999992'>
                                                                  <td>Harga Penutupan</td>
                                                                  <td><input name="9999999992_create" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992"></td>
                                                                  <td><input name="9999999992_read" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992"></td>
                                                                  <td><input name="9999999992_update" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992"></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999992" value="all" name="all" onChange="check(9999999992)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999993'>
                                                                  <td>Haircut</td>
                                                                  <td><input name="9999999993_create" type="checkbox" id="minimal-checkbox-9999999993" value="9999999993"></td>
                                                                  <td><input name="9999999993_read" type="checkbox" id="minimal-checkbox-9999999993" value="9999999993"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999993" value="all" name="all" onChange="check(9999999993)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="pospenjualan">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <tr id='minimal-checkbox-9999999994'>
                                                                  <td>Global Parameter</td>
                                                                  <td></td>
                                                                  <td><input name="9999999994_read" type="checkbox" id="minimal-checkbox-9999999994" value="9999999994"></td>
                                                                  <td><input name="9999999994_update" type="checkbox" id="minimal-checkbox-9999999994" value="9999999994"></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999994" value="all" name="all" onChange="check(9999999994)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999924'>
                                                                  <td>Batas Pinjaman</td>
                                                                  <td><input name="99999999924_create" type="checkbox" id="minimal-checkbox-99999999924" value="99999999924"></td>
                                                                  <td><input name="99999999924_read" type="checkbox" id="minimal-checkbox-99999999924" value="99999999924"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999924" value="all" name="all" onChange="check(99999999924)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999925'>
                                                                  <td>BMPK Emiten</td>
                                                                  <td><input name="99999999925_create" type="checkbox" id="minimal-checkbox-99999999925" value="99999999925"></td>
                                                                  <td><input name="99999999925_read" type="checkbox" id="minimal-checkbox-99999999925" value="99999999925"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999925" value="all" name="all" onChange="check(99999999925)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999995'>
                                                                  <td>Nasabah</td>
                                                                  <td></td>
                                                                  <td><input name="9999999995_read" type="checkbox" id="minimal-checkbox-9999999995" value="9999999995"></td>
                                                                  <td><input name="9999999995_update" type="checkbox" id="minimal-checkbox-9999999995" value="9999999995"></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999995" value="all" name="all" onChange="check(9999999995)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999996'>
                                                                  <td>Sekuritas</td>
                                                                  <td><input name="9999999996_create" type="checkbox" id="minimal-checkbox-9999999996" value="9999999996"></td>
                                                                  <td><input name="9999999996_read" type="checkbox" id="minimal-checkbox-9999999996" value="9999999996"></td>
                                                                  <td><input name="9999999996_update" type="checkbox" id="minimal-checkbox-999999999" value="9999999996"></td>
                                                                  <td><input name="9999999996_delete" type="checkbox" id="minimal-checkbox-999999999" value="9999999996"></td>
                                                                  <td><input type="checkbox" id="all-9999999996" value="all" name="all" onChange="check(9999999996)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999997'>
                                                                  <td>Kustodi</td>
                                                                  <td><input name="9999999997_create" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997"></td>
                                                                  <td><input name="9999999997_read" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997"></td>
                                                                  <td><input name="9999999997_update" type="checkbox" id="minimal-checkbox-999999997" value="9999999997"></td>
                                                                  <td><input name="9999999997_delete" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997"></td>
                                                                  <td><input type="checkbox" id="all-9999999997" value="all" name="all" onChange="check(9999999997)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999998'>
                                                                  <td>Efek</td>
                                                                  <td></td>
                                                                  <td><input name="9999999998_read" type="checkbox" id="minimal-checkbox-9999999998" value="9999999998"></td>
                                                                  <td><input name="9999999998_update" type="checkbox" id="minimal-checkbox-999999998" value="9999999998"></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999998" value="all" name="all" onChange="check(9999999998)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999999'>
                                                                  <td>Hari Libur</td>
                                                                  <td><input name="9999999999_create" type="checkbox" id="minimal-checkbox-9999999999" value="9999999999"></td>
                                                                  <td><input name="9999999999_read" type="checkbox" id="minimal-checkbox-9999999999" value="9999999999"></td>
                                                                  <td><input name="9999999999_update" type="checkbox" id="minimal-checkbox-9999999999" value="9999999999"></td>
                                                                  <td><input name="9999999999_delete" type="checkbox" id="minimal-checkbox-9999999999" value="9999999999"></td>
                                                                  <td><input type="checkbox" id="all-9999999999" value="all" name="all" onChange="check(9999999999)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999910'>
                                                                  <td>Negara</td>
                                                                  <td><input name="99999999910_create" type="checkbox" id="minimal-checkbox-99999999910" value="99999999910"></td>
                                                                  <td><input name="99999999910_read" type="checkbox" id="minimal-checkbox-99999999910" value="99999999910"></td>
                                                                  <td><input name="99999999910_update" type="checkbox" id="minimal-checkbox-99999999910" value="99999999910"></td>
                                                                  <td><input name="99999999910_delete" type="checkbox" id="minimal-checkbox-99999999910" value="99999999910"></td>
                                                                  <td><input type="checkbox" id="all-99999999910" value="all" name="all" onChange="check(99999999910)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999911'>
                                                                  <td>Provinsi</td>
                                                                  <td><input name="99999999911_create" type="checkbox" id="minimal-checkbox-99999999911" value="99999999911"></td>
                                                                  <td><input name="99999999911_read" type="checkbox" id="minimal-checkbox-99999999911" value="99999999911"></td>
                                                                  <td><input name="99999999911_update" type="checkbox" id="minimal-checkbox-99999999911" value="99999999911"></td>
                                                                  <td><input name="99999999911_delete" type="checkbox" id="minimal-checkbox-99999999911" value="99999999911"></td>
                                                                  <td><input type="checkbox" id="all-99999999911" value="all" name="all" onChange="check(99999999911)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999912'>
                                                                  <td>Kota</td>
                                                                  <td><input name="99999999912_create" type="checkbox" id="minimal-checkbox-99999999912" value="99999999912"></td>
                                                                  <td><input name="99999999912_read" type="checkbox" id="minimal-checkbox-99999999912" value="99999999912"></td>
                                                                  <td><input name="99999999912_update" type="checkbox" id="minimal-checkbox-99999999912" value="99999999912"></td>
                                                                  <td><input name="99999999912_delete" type="checkbox" id="minimal-checkbox-99999999912" value="99999999912"></td>
                                                                  <td><input type="checkbox" id="all-99999999912" value="all" name="all" onChange="check(99999999912)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999913'>
                                                                  <td>Kode Pajak</td>
                                                                  <td><input name="99999999913_create" type="checkbox" id="minimal-checkbox-99999999913" value="99999999913"></td>
                                                                  <td><input name="99999999913_read" type="checkbox" id="minimal-checkbox-99999999913" value="99999999913"></td>
                                                                  <td><input name="99999999913_update" type="checkbox" id="minimal-checkbox-99999999913" value="99999999913"></td>
                                                                  <td><input name="99999999913_delete" type="checkbox" id="minimal-checkbox-99999999913" value="99999999913"></td>
                                                                  <td><input type="checkbox" id="all-99999999913" value="all" name="all" onChange="check(99999999913)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="posinventory">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <tr id='minimal-checkbox-99999999914'>
                                                                  <td>Pengajuan Pinjaman</td>
                                                                  <td></td>
                                                                  <td><input name="99999999914_read" type="checkbox" id="minimal-checkbox-99999999914" value="99999999914"></td>
                                                                  <td><input name="99999999914_update" type="checkbox" id="minimal-checkbox-99999999914" value="99999999914"></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999914" value="all" name="all" onChange="check(99999999914)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999915'>
                                                                  <td>Topup Agunan</td>
                                                                  <td><input name="99999999915_create" type="checkbox" id="minimal-checkbox-99999999915" value="99999999915"></td>
                                                                  <td><input name="99999999915_read" type="checkbox" id="minimal-checkbox-99999999915" value="99999999915"></td>
                                                                  <td><input name="99999999915_update" type="checkbox" id="minimal-checkbox-99999999915" value="99999999915"></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999915" value="all" name="all" onChange="check(99999999915)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999916'>
                                                                  <td>Eksekusi</td>
                                                                  <td><input name="99999999916_create" type="checkbox" id="minimal-checkbox-99999999916" value="99999999916"></td>
                                                                  <td><input name="99999999916_read" type="checkbox" id="minimal-checkbox-99999999916" value="99999999916"></td>
                                                                  <td><input name="99999999916_update" type="checkbox" id="minimal-checkbox-99999999916" value="99999999916"></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999916" value="all" name="all" onChange="check(99999999916)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999917'>
                                                                  <td>Pelunasan Pinjaman</td>
                                                                  <td><input name="99999999917_create" type="checkbox" id="minimal-checkbox-99999999917" value="99999999917"></td>
                                                                  <td><input name="99999999917_read" type="checkbox" id="minimal-checkbox-99999999917" value="99999999917"></td>
                                                                  <td><input name="99999999917_update" type="checkbox" id="minimal-checkbox-99999999917" value="99999999917"></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999917" value="all" name="all" onChange="check(99999999917)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999926'>
                                                                  <td>Simulasi Topup</td>
                                                                  <td></td>
                                                                  <td><input name="99999999926_read" type="checkbox" id="minimal-checkbox-99999999926" value="99999999926"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999926" value="all" name="all" onChange="check(99999999926)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="posreport">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <tr id='minimal-checkbox-99999999921'>
                                                                  <td>Risk Parameter</td>
                                                                  <td></td>
                                                                  <td><input name="99999999921_read" type="checkbox" id="minimal-checkbox-99999999921" value="99999999921"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999921" value="all" name="all" onChange="check(99999999921)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999922'>
                                                                  <td>Stock Concentration Limit</td>
                                                                  <td></td>
                                                                  <td><input name="99999999922_read" type="checkbox" id="minimal-checkbox-99999999922" value="99999999922"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999922" value="all" name="all" onChange="check(99999999922)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999923'>
                                                                  <td>Stock Position</td>
                                                                  <td></td>
                                                                  <td><input name="99999999923_read" type="checkbox" id="minimal-checkbox-99999999923" value="99999999923"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999923" value="all" name="all" onChange="check(99999999923)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="poslaporan">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="bg-success text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <tr id='minimal-checkbox-99999999918'>
                                                                  <td>Manajemen User</td>
                                                                  <td><input name="99999999918_create" type="checkbox" id="minimal-checkbox-99999999918" value="99999999918"></td>
                                                                  <td><input name="99999999918_read" type="checkbox" id="minimal-checkbox-99999999918" value="99999999918"></td>
                                                                  <td><input name="99999999918_update" type="checkbox" id="minimal-checkbox-99999999918" value="99999999918"></td>
                                                                  <td><input name="99999999918_delete" type="checkbox" id="minimal-checkbox-99999999918" value="99999999918"></td>
                                                                  <td><input type="checkbox" id="all-99999999918" value="all" name="all" onChange="check(99999999918)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999919'>
                                                                  <td>Grup User</td>
                                                                  <td><input name="99999999919_create" type="checkbox" id="minimal-checkbox-99999999919" value="99999999919"></td>
                                                                  <td><input name="99999999919_read" type="checkbox" id="minimal-checkbox-99999999919" value="99999999919"></td>
                                                                  <td><input name="99999999919_update" type="checkbox" id="minimal-checkbox-99999999919" value="99999999919"></td>
                                                                  <td><input name="99999999919_delete" type="checkbox" id="minimal-checkbox-99999999919" value="99999999919"></td>
                                                                  <td><input type="checkbox" id="all-99999999919" value="all" name="all" onChange="check(99999999919)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                    </div>
                  </div>
                </div>
                    <div class="col-md-12 mt-10">
                        @include('inc.button.submit')
                        @include('inc.button.cancel')
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

$('input[type="checkbox"]').css('cursor','pointer');

function check(no) {
            document.getElementById('minimal-checkbox-'+no).addEventListener('change', function(e) {
                var el = e.target;
                var inputs = document.getElementById('minimal-checkbox-'+no).getElementsByTagName('input');

                if (el.id === 'all-'+no) {
                    for (var i = 0, input; input = inputs[i++]; ) {
                        input.checked = el.checked;
                    }
                } else {
                    var numChecked = 0;

                    for (var i = 1, input; input = inputs[i++]; ) {
                        if (input.checked) {
                            numChecked++;
                        }
                    }
                    inputs[0].checked = numChecked === inputs.length - 1;
                }
            }, false);
        }
</script>
@endsection
