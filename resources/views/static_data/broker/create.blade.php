@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Sekuritas</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/broker/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Broker Code</label>
                      <input type="text" class="form-control" id="kodeBroker" name="kodeBroker" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Broker Name</label>
                      <input type="text" class="form-control" id="namaBroker" name="namaBroker" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Kustodi</label>
                      <select class="form-control" id="kustodi" name="kustodi">
                          @foreach($kustodi as $kustodis)
                            <option value="{{ $kustodis['id'] }}">{{ $kustodis['kodeCustodi'] }} - {{ $kustodis['namaCustodi'] }}</option>
                          @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12 mt-10">
                    @include('inc.button.cancel')
                    @include('inc.button.submit')
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
@endsection
