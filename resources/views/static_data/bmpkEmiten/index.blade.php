@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
              <div class="form-group mt-10" role="group" aria-label="Basic example">
                @include('inc.button.download', ['urlss' => url('/dataStatis/bmpkEmiten/download')])
                @include('inc.button.upload', ['urls' => url('/dataStatis/bmpkEmiten/create')])
              </div>
                <h4 class="card-title">BMPK Emiten</h4><br>
                <div class="form-group row">
                    <label class="col-md-1 col-form-label">Tanggal</label>
                    <div class="col-md-4">
                    <input class="form-control" id="datepik" name="datepik" style="width:170px">
                    <input type="hidden" class="form-control" id="datepiks" name="datepiks" style="width:170px">
                  </div>
                </div>
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Kode Efek</th>
                        <th>BMPK</th>
                        <th>Keterangan</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>
$('#datepik').datepicker({
    locale: 'id',
    format: 'dd M yyyy',
    autoclose: true
});
var table = $("#lookup").dataTable({
  columns:
    [
      {data: 'id'},
      {data: 'tanggal'},
      {data: 'kodeEfek'},
      {data: 'bmpk'},
      {data: 'keterangan'}
    ],
  });

  $(document).ready(function() {
    var dates = moment().locale('id').format('ll');
    var date = moment().locale('en').format('YYYY-MM-DD');
    $('#datepik').val(dates);
    $('#datepiks').val(date);
  });

  $('#datepik').on('change', function(){
      convertMoment($('#datepik').val());
      loadData();
  });

  function convertMoment(date){
    var dates = moment(date).locale('id').format('YYYY-MM-DD');
    $('#datepiks').val(dates);
    // return dates;
  }

  function loadData(){

    $('#lookup').dataTable().fnDestroy();

    var tanggal = $('#datepiks').val();

    var table = $("#lookup").dataTable({
      processing: true,
      serverSide: true,
      ajax:{
        url: "{{ url('dataStatis/bmpkEmiten/getIndex/') }}",
        dataType: "json",
        type: "POST",
        data: {
          tanggal: tanggal
        },

      },
      autoWidth: false,
      columns: [
                {data: 'id'},
                {data: 'tanggal'},
                {data: 'kodeEfek'},
                {data: 'bmpk'},
                {data: 'keterangan'}
            ],
      columnDefs: [
              {
                  "targets": [0],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).text(row+1);
                },
                orderable: true
              },
              {
                  "targets": [1],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    var dates = moment(cellData).locale('id').format('ll');
                    $(td).text(dates);
                  },
              },
              {
                  "targets": [3],
                  "data": null,
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append($('<span>')
                                .addClass('percent')
                                .text(cellData)
                            )
                  },
              }
          ],
      drawCallback: function(settings) {
             initAutoNumeric();
          },
      });
  }
</script>
@endsection
