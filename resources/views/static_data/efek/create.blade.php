@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Efek</h4>
              <p class="card-description">
                Edit Efek
              </p>
              <form class="forms-sample" action="{{url('dataStatis/efek-resources/store/')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1">Kode Efek</label>
                      <input type="text" class="form-control form-control-sm" id="kodeEfek" name="kodeEfek" value="" placeholder="" >
                    </div>
                    <div class="form-group mt-10">
                      <label for="exampleInputPassword1">Nama Efek</label>
                      <input type="text" class="form-control" id="namaEfek" name="namaEfek" value="" placeholder="" >
                    </div>
                    <div class="form-group mt-10">
                      <label for="exampleInputPassword1">Jml Efek Beredar</label>
                      <input type="text" class="form-control" id="efekBeredar"  onkeypress="return justnumber(event, false)" value="" name="efekBeredar" placeholder="">
                    </div>
                    <div class="form-group mt-10">
                      <label for="exampleInputPassword1">Satuan</label>
                      <input type="text" class="form-control" id="satuan"  onkeypress="return justnumber(event, false)" value="" name="satuan" placeholder="">
                    </div>
                    <div class="form-group mt-10">
                      <label for="exampleInputPassword1">BMPK</label>
                      <input type="text" class="form-control" id="bmpk"  onkeypress="return justnumber(event, false)" value="" name="bmpk" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">

                    <div class="form-group mt-10">
                      <label for="exampleInputPassword1">Maksimum Pengajuan</label>
                      <input type="text" class="form-control" id="maxLoanQty"  onkeypress="return justnumber(event, false)" value="" name="maxLoanQty" placeholder="">
                    </div>
                    <div class="form-group mt-10">
                      <label for="exampleInputPassword1">Pengajuan Terpakai</label>
                      <input type="text" class="form-control" id="usedLoanQty"  onkeypress="return justnumber(event, false)" value="" name="usedLoanQty" placeholder="">
                    </div>
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1">Tipe Efek</label>
                      <input type="text" class="form-control form-control-sm" id="keterangan_efek" name="keterangan_efek" value="" placeholder="" >
                    </div>
                    <div class="form-group mt-10">
                      <label for="exampleInputPassword1">Status</label>
                      <select class="form-control" id="status_gadai" name="status_gadai">
                            <option value="A">Aktif</option>
                            <option value="S">Suspen</option>
                            <option value="D">D</option>
                      </select>
                    </div>
                    <div class="form-group mt-10">
                      <label for="exampleInputPassword1">Status Gadai</label>
                      <select class="form-control" id="status_gadai" name="status_gadai">
                            <option value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12 mt-10">
                      @include('inc.button.cancel')
                      @include('inc.button.submit')
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
@endsection
