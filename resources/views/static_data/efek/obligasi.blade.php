@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Efek</h4>
              <p class="card-description">
                Edit Efek
              </p>
              <form class="forms-sample" action="{{url('dataStatis/efek-resources/updateObligasi/'.$efek['id'])}}" method="post">
                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Kode Efek</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control form-control-sm" placeholder="" value="{{$efek['kodeEfek']}}" name="kodeEfek">
                        </div>
                        <label class=" col-sm-1 col-form-label">Tipe</label>
                        <div class="col-sm-2">
                          <select class="form-control form-control-sm" name="tipeEfek" id="tipeEfek" disabled>
                            <option value="S" {{$efek['tipeEfek']['tipeEfek'] == "S" ? 'selected' : ''}}>Saham</option>
                            <option value="O" {{$efek['tipeEfek']['tipeEfek'] == "O" ? 'selected' : ''}}>Obligasi</option>
                          </select>
                        </div>
                        <label class=" col-sm-2 col-form-label">Status Gadai</label>
                        <div class="col-sm-2">
                          <input type="checkbox" name="statusGadai" id="statusGadai" class="" style="cursor:pointer" {{ $efek['statusGadai'] == true ? "checked" : "" }}>
                          <input name="status" id="status" type="hidden" value="{{$efek['status']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Nama Efek</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control form-control-sm" placeholder="" name="namaEfek"  value="{{$efek['namaEfek']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Harga Penutupan</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control form-control-sm currencyNoComma" name="closePrice" id="closePrice" readonly placeholder="" value="{{$efek['closingPrice']}}">
                        </div>

                        <label class=" col-sm-3 col-form-label">Tanggal Harga Penutupan</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control form-control-sm" readonly placeholder="" name="closingDate" value="{{$efek['closingDate']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Haircut</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control form-control-sm percent" name="hairCut" readonly placeholder="" value="{{$efek['hairCut']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">Tanggal Haircut</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control form-control-sm" name="hairCutDate" readonly placeholder="" value="{{$efek['hairCutDate']}}">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">BMPK Individu</label>
                        <div class="col-sm-2">
                          <input type="text" id="bmpk" name="bmpk" class="form-control form-control-sm text-right" placeholder="" value="{{$efek['bmpk']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">BMPK Institusi</label>
                        <div class="col-sm-3">
                          <input type="text" id="bmpkInsti" name="bmpkInsti" class="form-control form-control-sm text-right" placeholder="" value="{{$efek['bmpkInstitusi']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">BMPK Global</label>
                        <div class="col-sm-2">
                          <input type="text" id="bmpkGlobal" name="bmpkGlobal" class="form-control form-control-sm text-right" placeholder="" value="">
                        </div>
                        <label class=" col-sm-3 col-form-label">Kode Penerbit</label>
                        <div class="col-sm-3">
                          <input type="text" id="bondIssuerCode" name="bondIssuerCode" class="form-control form-control-sm" placeholder="" value="{{$efek['bondIssuerCode']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Tipe Obligasi</label>
                        <div class="col-sm-2">
                          <input type="text" id="tipeBond" name="tipeBond" class="form-control form-control-sm" placeholder="" value="{{$efek['tipeBond']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">Rating Obligasi</label>
                        <div class="col-sm-3">
                          <input type="text" id="bondRating" name="bondRating" class="form-control form-control-sm" placeholder="" value="{{$efek['bondRating']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Tanggal Daftar Obligasi</label>
                        <div class="col-sm-2">
                          <input type="text" id="bondListingDate" name="bondListingDate" class="form-control form-control-sm datepicker" placeholder="" value="{{$efek['bondListingDate']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">Jatuh Tempo Obligasi</label>
                        <div class="col-sm-3">
                          <input type="text" id="bondMaturityDate" name="bondMaturityDate" class="form-control form-control-sm datepicker" placeholder="" value="{{$efek['bondMaturityDate']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Rating Kupon</label>
                        <div class="col-sm-2">
                          <input type="text" id="bondCouponRate" name="bondCouponRate" class="form-control form-control-sm text-right" placeholder="" value="{{$efek['bondCouponRate']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">Nama Perusahaan</label>
                        <div class="col-sm-3">
                          <input type="text" id="bondTrustee" name="bondTrustee" class="form-control form-control-sm" placeholder="" value="{{$efek['bondTrustee']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Obligasi TTM</label>
                        <div class="col-sm-2">
                          <input type="text" id="bondTtm" name="bondTtm" class="form-control form-control-sm text-right" placeholder="" value="{{$efek['bmpk']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">Isin Obligasi</label>
                        <div class="col-sm-3">
                          <input type="text" id="bondIsin" name="bondIsin" class="form-control form-control-sm text-right" placeholder="" value="{{$efek['bmpkInstitusi']}}">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Maksimum Pengajuan</label>
                        <div class="col-sm-2">
                          <input type="text" id="maxLoanQty" name="maxLoanQty" class="form-control form-control-sm currencyNoComma" readonly placeholder="" value="{{$efek['maxLoanQty']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">Maksimum Pengajuan Penerimaan</label>
                        <div class="col-sm-3">
                          <input type="text" id="maxSumLoanQty" name="maxSumLoanQty" class="form-control form-control-sm currencyNoComma" readonly placeholder=""  value="{{$efek['maxLoanQty']*$efek['closingPrice']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Penggunaan Pengajuan</label>
                        <div class="col-sm-2">
                          <input type="text" id="usedLoanQty" name="usedLoanQty" class="form-control form-control-sm currencyNoComma" readonly placeholder="" value="{{$efek['usedLoanQty']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">Maksimum Penggunaan Pengajuan</label>
                        <div class="col-sm-3">
                          <input type="text" id="sumUsedLoanQty" name="sumUsedLoanQty" class="form-control form-control-sm currencyNoComma" readonly placeholder=""  value="{{$efek['usedLoanQty']*$efek['closingPrice']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Ketersediaan Pinjaman</label>
                        <div class="col-sm-2">
                          <input type="text" id="ketersediaanPinjaman" name="ketersediaanPinjaman" class="form-control form-control-sm currencyNoComma" readonly placeholder="" value="{{$efek['maxLoanQty']-$efek['usedLoanQty']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">Ketersediaan Nilai Pinjaman</label>
                        <div class="col-sm-3">
                          <input type="text" id="ketNilaiPinjaman" name="ketNilaiPinjaman" class="form-control form-control-sm currencyNoComma" readonly placeholder=""  value="{{($efek['maxLoanQty']*$efek['closingPrice'])-($efek['usedLoanQty']*$efek['closingPrice'])}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Jumlah Efek Beredar</label>
                        <div class="col-sm-2">
                          <input type="text" id="beredar" name="beredar" class="form-control form-control-sm currencyNoComma" placeholder="" value="{{$efek['jlhEfekBeredar']}}">
                        </div>
                        <label class=" col-sm-3 col-form-label">Harga Satuan</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control form-control-sm currencyNoComma" name="satuan" id="satuan" placeholder="" value="{{$efek['satuan']}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label">Status Efek</label>
                        <div class="col-sm-2">
                          <select class="form-control form-control-sm" name="status" id="status">
                            <option value="A" {{$efek['status'] == "A" ? 'selected' : ''}}>Aktif</option>
                            <option value="S" {{$efek['status'] == "S" ? 'selected' : ''}}>Suspend</option>
                            <option value="D" {{$efek['status'] == "D" ? 'selected' : ''}}>Delisting</option>
                            <input type="hidden" id="statusEfek" name="statusEfek" value="{{$efek['tipeEfek']['id']}}">
                          </select>
                        </div>
                      </div>
                    </div>
                  <div class="col-md-12 mt-10"><br>
                      @include('inc.button.cancel')
                      @include('inc.button.submit')
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>
  $(document).ready(function() {

      // $('#ketNilaiPinjaman').maskMoney();
      // $('#maxSumLoanQty').maskMoney();
      // $('#ketersediaanPinjaman').maskMoney();

      $('#beredar').on('change', function(){

        var bmpks = $('#bmpk').val();
        var bmpk1 = bmpks.replace(/[%]/g,'');
        var bmpk = bmpk1.replace(/\,/g, ".");

        var jmlEfekBeredar = $('#beredar').val();
        var rep_jmlEfekBeredar = jmlEfekBeredar.replace(/\./g, "");

        var kalkulasi2 = (bmpk/100)*rep_jmlEfekBeredar;
        $('#maxLoanQty').val(kalkulasi2);

        var bcloseprice = $('#closePrice').val();
        var closePrice = bcloseprice.replace(/\./g, "");
        var maxSumLoanQty = kalkulasi2*closePrice;

        var usedLoanQtys = $('#usedLoanQty').val();
        var usedLoanQty = usedLoanQtys.replace(/\./g, "");
        var sumUsedLoanQty = usedLoanQty*closePrice;

        var ketersediaanPinjaman = kalkulasi2-usedLoanQty;
        var ketNilaiPinjaman = maxSumLoanQty-sumUsedLoanQty;

        $('#maxSumLoanQty').val(maxSumLoanQty);
        $('#sumUsedLoanQty').val(sumUsedLoanQty);
        $('#ketersediaanPinjaman').val(ketersediaanPinjaman);
        $('#ketNilaiPinjaman').val(ketNilaiPinjaman);

      });

      $('#bmpk').on('change', function(){

        var bmpks = $('#bmpk').val();
        var bmpk1 = bmpks.replace(/[%]/g,'');
        var bmpk = bmpk1.replace(/\,/g, ".");

        var jmlEfekBeredar = $('#beredar').val();
        var rep_jmlEfekBeredar = jmlEfekBeredar.replace(/\./g, "");

        var kalkulasi2 = (bmpk/100)*rep_jmlEfekBeredar;
        $('#maxLoanQty').val(kalkulasi2);

        var bcloseprice = $('#closePrice').val();
        var closePrice = bcloseprice.replace(/\./g, "");
        var maxSumLoanQty = kalkulasi2*closePrice;

        var usedLoanQtys = $('#usedLoanQty').val();
        var usedLoanQty = usedLoanQtys.replace(/\./g, "");
        var sumUsedLoanQty = usedLoanQty*closePrice;

        var ketersediaanPinjaman = kalkulasi2-usedLoanQty;
        var ketNilaiPinjaman = maxSumLoanQty-sumUsedLoanQty;

        $('#maxSumLoanQty').val(maxSumLoanQty);
        $('#sumUsedLoanQty').val(sumUsedLoanQty);
        $('#ketersediaanPinjaman').val(ketersediaanPinjaman);
        $('#ketNilaiPinjaman').val(ketNilaiPinjaman);

      });

      $('#status').on('change', function(){
        if($('#status').val()=="A"){
          $('#statusEfek').val("1");
        }else if($('#status').val()=="S"){

        }
      });
  });
</script>
@endsection
