@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title float-left">Efek</h4>
                  <!-- @include('inc.button.add', ['urls' => '/efek-resources/create']) -->
              </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-form-label col-md-2">Pilihan</label>
                <select class="form-control form-control-sm col-md-2" name="type_efek" id="type_efek">
                  <option value="A">All</option>
                  <option value="S">Saham</option>
                  <option value="O">Obligasi</option>
                </select>
                <!-- <label class="col-form-label col-md-2">Status Gadai</label>
                <select class="form-control form-control-sm col-md-2" name="status_gadai" id="status_gadai">
                  <option value="">All</option>
                  <option value="Aktif">Aktif</option>
                  <option value="Tidak Aktif">Tidak Aktif</option>
                </select> -->
                <!-- <input class="form-control form-control-sm col-md-2" name="status_gadai" id="status_gadai"> -->
              </div>
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Kode Efek</th>
                        <th>Nama Efek</th>
                        <th>Tipe Efek</th>
                        <th>Harga Penutupan</th>
                        <th class="text-center">Gadai</th>
                        <th>Jml Efek Beredar</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>
var table = $("#lookup").dataTable({
  dom: 'lrtip',
  columns:
    [
      {data: 'id'},
      {data: 'kodeEfek'},
      {data: 'namaEfek'},
      {data: 'tipeEfek'},
      {data: 'closingPrice'},
      {data: 'statusGadai'},
      {data: 'jlhEfekBeredar'},
      {data: 'id'}
    ],
  });

$(document).ready(function() {
  loadData();
})


$('#type_efek').on('change',function(){
  loadData();
})


function deletes(id){
  $.confirm({
      title: 'Konfirmasi',
      content: 'Hapus data ini ?',
      icon: 'fas fa-exclamation-triangle',
      buttons: {
          confirm: function () {
              location.href="{{url('dataStatis/efek-resources/delete')}}" + "/" + id;
          },
          cancel: function () {

          }
      }
  });
}
function loadData(){

  $('#lookup').dataTable().fnDestroy();

  var type_efek = $('#type_efek').val();

  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    dom: 'lrtip',
    ajax:{
      url: "{{ url('dataStatis/efek-resources/getEfek') }}",
      dataType: "json",
      type: "POST",
      data: {
        type_efek: type_efek
      },
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="8">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns:
      [
          {data: 'id'},
          {data: 'kodeEfek'},
          {data: 'namaEfek'},
          {data: 'tipeEfek'},
          {data: 'closingPrice'},
          {data: 'statusGadai'},
          {data: 'jlhEfekBeredar'},
          {data: 'id'}
      ],
    columnDefs:
    [
      {
        "targets": [0],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).text(row+1);
        },
        orderable: true
      },
      {
          "targets": [3],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();

            var tipeEfek;

            switch(cellData.tipeEfek){
              case 'S' : tipeEfek = 'Saham'; break;
              case 'O' : tipeEfek = 'Obligasi'; break;
              case 'R' : tipeEfek = 'Rights'; break;
              case 'W' : tipeEfek = 'Warrant'; break;
            }
            $(td).text(tipeEfek);
          },
      },
      {
          "targets": [4],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {

            $(td).empty();
            if(rowData.tipeEfek.tipeEfek=="O"){
              var dats = cellData*100;
              $(td).append($('<span>')
                          .addClass('percentComma')
                          .text(dats)
                      )
            }else{
              $(td).append($('<span>')
                          .addClass('currencyNoComma')
                          .text(cellData)
                      )
            }

          },
      },
      {
          "targets": [5],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty()
            $(td).addClass("text-center")
            if(cellData==null||cellData==false){
              $(td).append($('<input>')
                .attr('id','checkboxGadai_'+rowData.id+'')
                .attr('name',"checkSource[]")
                .attr('type','checkbox')
                .css('margin-left','-5px')
                .css('cursor','pointer')
                .css('margin-top','-6px')
                .addClass('form-check-input checkboxGadai')
            )
            }else{
              $(td).append($('<input>')
                .attr('id','checkboxGadai_'+rowData.id+'')
                .attr('name',"checkSource[]")
                .attr('type','checkbox')
                .attr('checked','checked')
                .css('margin-left','-5px')
                .css('cursor','pointer')
                .css('margin-top','-6px')
                .addClass('form-check-input checkboxGadai')
            )
          }

          },
      },
      {
          "targets": [6],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
        "targets": [7],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
            $(td)
                .append($('@include('inc.button.group_edit')'));
        },
        orderable: false
      },
    ],
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    },
    drawCallback: function(settings) {
           initAutoNumeric();
        },
  });
};

$('.table').on('click','.btn-edit-record', function(){
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData()

    location.href="{{url('dataStatis/efek-resources/edit/')}}" + "/" + data[index].kodeEfek;
});

$('.table').on('click','.checkboxGadai', function(){
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData()
    var checked = $('#checkboxGadai_'+data[index].id).is(':checked');
    console.log(checked);






});

$('.table').on('click','.btn-remove-record', function(){
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData()

    deletes(data[index].id);
});

</script>
@endsection
