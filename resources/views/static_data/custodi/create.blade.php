@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Bank Kustodi</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/custodi/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Custody Code</label>
                      <input type="text" class="form-control" id="kodeCustodi" name="kodeCustodi" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Custody Name</label>
                      <input type="text" class="form-control" id="namaCustodi" name="namaCustodi" placeholder="">
                    </div>
                  </div>
                    <div class="col-md-12 mt-10">
                        @include('inc.button.cancel')
                        @include('inc.button.submit')
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
@endsection
