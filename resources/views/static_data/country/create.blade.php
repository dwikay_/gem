@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Negara</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/country/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Kode Negara</label>
                      <input type="text" class="form-control" id="countryCode" value="" name="countryCode" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Nama Negara</label>
                      <input type="text" class="form-control" id="countryName" value="" name="countryName" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Nama Kewarganegaraan</label>
                      <input type="text" class="form-control" id="nationality" value="" name="nationality" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Kode Pegadaian</label>
                      <input type="text" class="form-control" id="pegadaianCode" value="" name="pegadaianCode" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-12 mt-10">
                      @include('inc.button.cancel')
                      @include('inc.button.submit')
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
@endsection
