@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Global Parameter</h4>
              <div class"row">
              </div>
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Parameter ID</th>
                        <th>Parameter</th>
                        <th>Parameter Value</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>
$(document).ready(function() {

var table = $("#lookup").dataTable({
  processing: true,
  serverSide: true,
  ajax:{
    url: "{{ url('dataStatis/globalParam/getGlobal') }}",
    dataType: "json",
    type: "GET",
    error: function(){  // error handling
      $(".lookup-error").html("");
      $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="10">No data found in the server</th></tr></tbody>');
      $("#lookup_processing").css("display","none");

    }
  },
  columns: [
            {data: 'id'},
            {data: 'prmId'},
            {data: 'prmnName'},
            {data: 'prmTy'},
            {data: 'id'}
        ],
        columnDefs: [
          {
            "targets": [0],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).text(row+1);
            },
            orderable: true
          },
          {
            "targets": [3],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              if(cellData=="S"){
                $(td)
                .text(rowData.strVal);
              }else if(cellData=="L"){
                $(td).text(rowData.longVal)
              }else if(cellData=="D"){
              $(td).text(rowData.doubleVal)
              // .addClass("currencyNoComma");
              }else if(cellData=="T"){
                var dates = moment(rowData.dateVal).locale('id').format('ll');
                $(td).text(dates);
              }
            },
            orderable: false
          },
          {
            "targets": [4],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
                $(td)
                    .append($('@include('inc.button.group_edit')'));
            },
            orderable: false
          },
        ],
        drawCallback: function(settings) {
               initAutoNumeric();
            },
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    }
      });

    $('.table').on('click','.btn-edit-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        location.href="{{url('dataStatis/globalParam/edit/')}}" + "/" + data[index].id;
    });
});
</script>
@endsection
