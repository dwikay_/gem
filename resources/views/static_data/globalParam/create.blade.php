@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Global Parameter</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/globalParam/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Parameter ID</label>
                      <input type="text" class="form-control form-control-sm" id="prmId" name="prmId" placeholder="">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Parameter Name</label>
                      <input type="text" class="form-control form-control-sm" id="prmName" name="prmName" placeholder="">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Parameter Type</label>
                      <select id="prmType" name="prmType" class="form-control form-control-sm">
                        <option value="S">String</option>
                        <option value="I">Integer</option>
                        <option value="F">Float</option>
                        <option value="D">Date</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Parameter Value</label>
                      <input type="text" class="form-control form-control-sm" id="hasVal" onkeypress="return justnumber(event, false)" name="hasVal" placeholder="">
                    </div>
                    <div class="form-group">
                      <div class="form-check form-check-flat" style="margin-top:46px">
                      <label class="form-check-label" style="cursor:pointer">
                        <input name="editable" type="checkbox" class="form-check-input"> Edit
                      </label>
                      </div>
                      <div class="form-check form-check-flat" style="margin-top:10px">
                      <label class="form-check-label" style="cursor:pointer">
                        <input name="showable" type="checkbox" class="form-check-input"> Show
                      </label>
                      </div>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-success mr-2">Submit</button>
                <a class="btn btn-light" href="{{url('globalParam')}}">Cancel</a>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

$(document).ready(function() {
  var msg = '{{Session::get('info')}}';
  var msgclass = '{{Session::get('alert')}}';
  var exist = '{{Session::has('info')}}';
    if(exist){
      swal(msgclass,"",msg)
    }

  $('#prmType').on('change', function(){

    if($('#prmType').val()=="S"){
      $('#hasVal').val("");
      $('#hasVal').removeAttr("onkeypress");
      $("#hasVal").datepicker("destroy");
    }else if($('#prmType').val()=="D"){
      $('#hasVal').val("");
      $('#hasVal').removeAttr("onkeypress");
      $("input[name='hasVal']").datepicker({
          format: 'yyyy-mm-dd',
          autoclose :true
      });
    }else{
      $('#hasVal').val("");
      $("#hasVal").datepicker("destroy");
      $('#hasVal').attr("onkeypress","return justnumber(event, false)");
    }

  });

});
</script>
@endsection
