@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Global Parameter</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/globalParam/update/'.$globalParam['id'])}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <labelclass="col-form-label">Parameter ID</label>
                      <input type="text" class="form-control" id="prmId" value="{{$globalParam['prmId']}}" name="prmId" placeholder="" readonly>
                    </div>
                    <div class="form-group">
                      <label class="col-form-label">Parameter Name</label>
                      <input type="text" class="form-control" readonly id="prmName" name="prmName" value="{{$globalParam['prmnName']}}" placeholder="">
                    </div>
                    <div class="form-group" style="display:none">
                      <label class="control-label">Parameter Type</label>
                      <select id="prmType" name="prmType" class="form-control">
                        <option value="S" {{$globalParam['prmTy'] == "S" ? 'selected' : ''}}>String</option>
                        <option value="L" {{$globalParam['prmTy'] == "L" ? 'selected' : ''}}>Long</option>
                        <option value="D" {{$globalParam['prmTy'] == "D" ? 'selected' : ''}}>Double</option>
                        <option value="T" {{$globalParam['prmTy'] == "T" ? 'selected' : ''}}>Date</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-form-label" for="exampleInputEmail1">Parameter Value</label>
                      <input type="text" class="form-control" id="hasVal" name="hasVal" maxlength="50" placeholder="" value="{{$vals}}">
                    </div>
                    <div class="form-group" style="display:none">
                      <div class="form-check form-check-flat" style="margin-top:46px">
                      <label class="form-check-label" style="cursor:pointer">
                        <input name="editable" type="checkbox" class="form-check-input" {{$editable}}> Edit
                      </label>
                      </div>
                      <div class="form-check form-check-flat" style="margin-top:10px">
                      <label class="form-check-label" style="cursor:pointer">
                        <input name="showable" type="checkbox" class="form-check-input" {{$showable}}> Show
                      </label>
                      </div>
                    </div>
                  </div>
                    <div class="col-md-12 mt-10">
                        @include('inc.button.cancel')
                        @include('inc.button.submit')
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>


var msg = '{{Session::get('info')}}';
  var msgclass = '{{Session::get('alert')}}';
  var exist = '{{Session::has('info')}}';
    if(exist){
      swal(msgclass,"",msg)
    }
</script>
@endsection
