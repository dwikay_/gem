@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
                <h4 class="card-title float-left">Hari Libur</h4>
                @include('inc.button.add', ['urls' => url('/dataStatis/holiday/create')])
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-form-label col-md-2">Pilih Tahun</label>
                <select class="form-control form-control-sm col-md-2" name="tahun" id="tahun">
                  @for ($i = 2009; $i <= 2025; $i++)
                  <option value="{{$i}}" {{$i == date('Y') ? 'selected' : ''}}>{{$i}}</option>
                  @endfor
                </select>
              </div>
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

var table = $("#lookup").dataTable({
  responsive: true,
  columns:
    [
      {data: 'id'},
      {data: 'tanggal'},
      {data: 'keterangan'},
      {data: 'id'}
    ],
  });

$(document).ready(function() {

    var tahun = "2019";
    loadData(tahun);

});
function deletes(id){
  $.confirm({
      confirmButton: 'Hapus',
      cancelButton: 'Kembali',
      title: 'Konfirmasi',
      content: 'Hapus Data ini ?',
      icon: 'fas fa-exclamation-triangle',
      buttons: {
          confirm: function () {
              location.href="{{url('dataStatis/holiday/delete')}}" + "/" + id;
          },
          cancel: function () {
          }
      }
  });
}

$('#tahun').on('change', function(){
    var tahun = $('#tahun').val();
    loadData(tahun);
});

function loadData(tahun){

  $('#lookup').dataTable().fnDestroy();

var table = $("#lookup").dataTable({
  processing: true,
  serverSide: true,
  ajax:{
    url: "{{ url('dataStatis/holiday/getHoliday') }}" + "/" + tahun,
    dataType: "json",
    type: "GET",
    error: function(){  // error handling
      $(".lookup-error").html("");
      $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="4">No data found in the server</th></tr></tbody>');
      $("#lookup_processing").css("display","none");

    }
  },
  columns: [
            {data: 'id'},
            {data: 'tanggal'},
            {data: 'keterangan'},
            {data: 'id'}
        ],
        columnDefs: [
          {
            "targets": [0],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).text(row+1);
            },
            orderable: true
          },
          {
              "targets": [1],
              "data": null,
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();
                var dates = moment(cellData).locale('id').format('ll');
                $(td).text(dates);
              },
          },
          {
            "targets": [3],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append($('@include('inc.button.group_btn_table')'));
            },
            orderable: false
          },
        ],
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    },
      });

      $('.table').on('click','.btn-edit-record', function(){
          var tr = $(this).closest('tr');

          var id = tr.attr('id').split('_');
          var index = id[1];
          var data = table.fnGetData()

          location.href="{{url('dataStatis/holiday/edit/')}}" + "/" + data[index].id;
      });

      $('.table').on('click','.btn-remove-record', function(){
          var tr = $(this).closest('tr');

          var id = tr.attr('id').split('_');
          var index = id[1];
          var data = table.fnGetData()

          deletes(data[index].id);
      });
}
</script>
@endsection
