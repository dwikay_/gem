@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Hari Libur</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/holiday/update/'.$holiday['id'])}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Tanggal</label>
                      <input type="text" class="form-control" id="tanggal" name="tanggal" value="{{$holiday['tanggal']}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Keterangan</label>
                      <input type="text" class="form-control" id="keterangan" name="keterangan" value="{{$holiday['keterangan']}}">
                    </div>
                  </div>
                  <div class="col-md-12 mt-10">
                      @include('inc.button.cancel')
                      @include('inc.button.submit')
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

$("input[name='tanggal']").datepicker({
    format: 'yyyy-mm-dd',
    autoclose :true
});
</script>
@endsection
