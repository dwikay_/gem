@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Hari Libur</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/holiday/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Tanggal</label>
                      <input type="text" class="form-control datepicker" id="tanggal" name="tanggal" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Keterangan</label>
                      <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-12 mt-10">
                      @include('inc.button.cancel')
                      @include('inc.button.submit')
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
