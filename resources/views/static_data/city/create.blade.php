@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Kota</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/city/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Kode Kota</label>
                      <input type="text" class="form-control" id="cityCode" value="" name="cityCode" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Nama Kota</label>
                      <input type="text" class="form-control" id="cityName" value="" name="cityName" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Provinsi</label>
                      <select class="form-control" id="province" name="province">
                          @foreach($provinces as $province)
                            <option value="{{ $province['id'] }}">{{ $province['provinceName'] }}</option>
                          @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Kode Pegadaian</label>
                      <input type="text" class="form-control" id="pegadaianCode" value="" name="pegadaianCode" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-12 mt-10">
                      @include('inc.button.cancel')
                      @include('inc.button.submit')
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>
</script>
@endsection
