@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
                <h4 class="card-title float-left">Kota</h4>
                @include('inc.button.add', ['urls' => url('/dataStatis/city/create')])
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Kode Kota</th>
                        <th>Nama Kota</th>
                        <th>Nama Provinsi</th>
                        <th>Kode Pegadaian</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

function deletes(id){
  $.confirm({
      confirmButton: 'Hapus',
      cancelButton: 'Kembali',
      title: 'Konfirmasi',
      content: 'Hapus Data ini ?',
      icon: 'fas fa-exclamation-triangle',
      buttons: {
          confirm: function () {
              location.href="{{url('dataStatis/city/delete')}}" + "/" + id;
          },
          cancel: function () {
          }
      }
  });
}


$(document).ready(function() {

  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    ajax:{
      url: "{{ url('dataStatis/city/getCity') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="10">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
      {data: 'id'},
      {data: 'cityCode'},
      {data: 'cityName'},
      {data: 'cityName'},
      {data: 'pegadaianCode'},
      {data: 'id'},
    ],
      createdRow: function ( row, data, index ) {
          $(row).attr('id','table_'+index);
      },
    columnDefs: [
      {
        "targets": [0],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).text(row+1);
        },
        orderable: true
      },
      {
        "targets": [3],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).text(rowData.province.provinceName);
        },
        orderable: true
      },
      {
        "targets": [5],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).append($('@include('inc.button.group_btn_table')'));
        },
        orderable: false
      },
    ]
  });

  $('.table').on('click','.btn-edit-record', function(){
      var tr = $(this).closest('tr');

      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData()

      location.href="{{url('dataStatis/city/edit/')}}" + "/" + data[index].id;
  });

    $('.table').on('click','.btn-remove-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        deletes(data[index].id);
    });
});
</script>
@endsection
