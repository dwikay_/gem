@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <div class="form-group mt-10" role="group" aria-label="Basic example">
                @include('inc.button.instruksi')
              </div>
              <h4 class="card-title">Nasabah</h4>
              <div class"row">
                <div class="col-md-12 text-right">
                  <!-- <a href="{{url('nasabah/create')}}" class="btn btn-md btn-success" style="color:white;"><span class="fa fa-plus"></span>&nbsp;&nbsp;Add</a> -->
                </div>
              </div>
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Kode Nasabah</th>
                        <th>Nama Nasabah</th>
                        <th>Tipe</th>
                        <th>SID</th>
                        <th>Sub Rekening</th>
                        <th class="text-center">Actions</th>
                        <th class="text-center"><input type="checkbox" clacc="form-control" id="select_all" style="cursor:pointer"></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('modal')
  @include('modals.modalGenerate')
@endsection
@section('script')
<script>
$(document).ready(function() {

  $('#select_all').on('click',function(){
      if(this.checked){
          $('.checkbox').each(function(){
              this.checked = true;
          });
      }else{
           $('.checkbox').each(function(){
              this.checked = false;
          });
      }
  });

var table = $("#lookup").dataTable({
  processing: true,
  serverSide: true,
  ajax:{
    url: "{{ url('dataStatis/nasabah/getNasabah') }}",
    dataType: "json",
    type: "GET",
    error: function(){
      $(".lookup-error").html("");
      $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="7">No data found in the server</th></tr></tbody>');
      $("#lookup_processing").css("display","none");

    }
  },
  columns: [
            {data: 'id'},
            {data: 'kodeNasabah'},
            {data: 'namaNasabah'},
            {data: 'tipeNasabah'},
            {data: 'sid'},
            {data: 'bankSubRek'},
            {data: 'id'},
            {data: 'kodeNasabah'}
        ],
        columnDefs: [
          {
            "targets": [0],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).text(row+1);
            },
            orderable: true
          },
          {
              "targets": [3],
              "data": null,
              "createdCell": function (td, cellData, rowData, row, col) {
                $(td).empty();

                var tipeEfek;

                switch(cellData){
                  case 'C' : tipeEfek = 'Coorporate'; break;
                  case 'R' : tipeEfek = 'Retail'; break;
                }
                $(td).text(tipeEfek);
              },
          },
          {
            "targets": [6],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td)
                .append($('@include('inc.button.nasabahButton')'))
                .addClass("text-center");

            },
            orderable: false
          },
          {
            "targets": [7],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).append(            
                $('<input type="checkbox" class="checkbox checkbox-instruksi" style="cursor:pointer" id="checks'+cellData+'" name="checks['+cellData+']" value="'+cellData+'")">')
              );
              // $(td).append(
              //   $('<input type="hidden" class="efek" style="cursor:pointer" id="efek'+rowData.tipeEfek+'" name="efek['+rowData.tipeEfek+']" value="'+rowData.tipeEfek+'" onclick="efek('+rowData.tipeEfek+')">')
              // );
            },
            orderable: false
        }

        ],
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
            var data = table.fnGetData();
            if(data[index].statusSubRek!="R"){
              // alert(data[index].statusSubRek)
            }
        },
      });

      $('.table').on('click','.btn-edit-record', function(){
          var tr = $(this).closest('tr');

          var id = tr.attr('id').split('_');
          var index = id[1];
          var data = table.fnGetData()
          location.href="{{url('dataStatis/nasabah/edit/')}}" + "/" +
          data[index].tipeNasabah + "/" +
          data[index].kodeNasabah + "/" +
          data[index].id;
      });

      $('.table').on('click','.btn-subrek-record', function(){
          var tr = $(this).closest('tr');

          var id = tr.attr('id').split('_');
          var index = id[1];
          var data = table.fnGetData()
          if(data[index].kodeNasabah==null ||data[index].kodeNasabah==""){
            location.href="{{url('dataStatis/nasabah/reqSubrek')}}" + "/" +
            data[index].kodeNasabah + "/" + data[index].tipeNasabah;
          }else{
            location.href="{{url('dataStatis/nasabah/editSubRek')}}" + "/" +
            data[index].kodeNasabah + "/" + data[index].tipeNasabah;
          }

          // if(data[index].statusSubRek=="R"){
          //   location.href="{{url('dataStatis/nasabah/editSubrRek')}}" + "/" +
          //   data[index].kodeNasabah;
          // }else{
          //   $.alert({
          //       title: 'Error',
          //       content: 'Sub Rekening sudah ada',
          //       icon: 'fas fa-exclamation-triangle'
          //   });
          // }
      });

$('.btn-instruksi').on('click', function(){
    if($('.checkbox-instruksi').is(':checked')){
      $('#modalDownload').modal();
    }else{
      $.alert({
          title: 'Data Kosong !',
          content: 'Harap cek data terlebih dahulu',
      });
    }
  });

$('#btn-instruksi-xml').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              var type = 'xml';
              generate(type);
            },
            cancel: function () {

            }
        }
    });
  });

  $('#btn-instruksi-csv').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Generate Instruksi ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              var type = "csv";
              generate(type);
            },
            cancel: function () {

            }
        }
    });
  });

function generate(type){
    $('.checkbox-instruksi').each(function( index ) {
      if($(this).is(':checked')){
        var checks = $(this).val();        

        $.ajax({
           type:'POST',
           url:"{{ url('dataStatis/nasabah/download') }}",
           data: {checks:checks,type:type},
           success:function(data){
            if(type == "csv"){
              data = data.replace(/"/g,'');
            }
            var blob = new Blob([data], { type: 'application/'+type+'charset=utf-8;' });
            
            var link = document.createElement("a");
              if (link.download !== undefined) { // feature detection
                  // Browsers that support HTML5 download attribute
                  var url = URL.createObjectURL(blob);
                  link.setAttribute("href", url);
                  link.setAttribute("download", checks+'.'+type);
                  link.style.visibility = 'hidden';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
              }
            // console.log(data);
           }
        });
      } 
    });
  }

});
</script>
@endsection
