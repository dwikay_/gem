@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Ubah Sub Rekening</h4>
              <p class="card-description">
                Edit Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/nasabah/updateSubRek/'.$kodeNasabah)}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Sub Rek 001 ({{$kodeNasabah}})</label>
                      <input type="text" class="form-control" id="subRek" maxlength="14" name="subRek" value="" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-12 mt-10">
                    @include('inc.button.cancel')
                    @include('inc.button.submit')
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
@endsection
