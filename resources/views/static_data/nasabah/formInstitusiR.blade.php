@extends('layouts.apps')

@section('content')
<div class="container-fluid">
	<div class="row m-0">
		<div class="col-md-12 p-0">
			<div class="card card-default">
				<div class="card-header">
					Institutional Client Participant
					<a href="{{ URL::previous() }}" class="float-right clickable close-icon" data-effect="fadeOut"><i class="fa fa-times"></i></a>
				</div>
				<div class="card-body">
					<form action="{{url('dataStatis/nasabah/update/'.$nasabah['id'].'/'.$tipeNasabah.'/'.$idNasabah)}}" id="insert-form-client" method="post" enctype="multipart/form-data">
						{{csrf_field()}}
					<div id="page1">
						<div class="form-group row">
							<label for="acccode" class="col-sm-2 col-form-label">Account Code</label>
							<div class="col-sm-2">
								<input type="text" name="ACCCODE" class="form-control no-border" id="acccode" placeholder="Participant Code"  value="{{$nasabah['accountClientCode']}}" required="required">
							</div>
							<label for="sid" class="col-sm-1 col-form-label">SID <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input type="text" name="SID" class="form-control no-border" id="sid" placeholder="SID" required="required" value="{{$nasabah['nasabah']['sid']}}" >
							</div>
							<label for="ksei_acc" class="col-sm-2 col-form-label">Sub Rek 001 <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<input type="text" name="KSEI_ACC" class="form-control" id="ksei_acc" value="{{$nasabah['nasabah']['bankSubRek']}}" placeholder="Sub Account" required="required" value="" disabled >
							</div>
						</div>
						<div class="form-group row">
							<label for="inv_type" class="col-sm-2 col-form-label">Investor Type</label>
							<div class="col-sm-2">
								<input type="text" name="INV_TYPE" class="form-control no-border" id="inv_type" placeholder="Investor Type" value="INSTITUTIONAL"  required="required">
							</div>
							<label for="inv_client_type" class="col-sm-2 col-form-label">Investor Client Type <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<select name="INV_CLIENT_TYPE" id="inv_client_type" class="form-control" required="required">
									<option value=""></option>
									<option value="D" {{  $nasabah['investorCategory'] == "D" ? "selected" : "" }}>Direct</option>
									<option value="I" {{  $nasabah['investorCategory'] == "I" ? "selected" : "" }}>Indirect</option>
								</select>
							</div>
							<label for="direct_sid" class="col-sm-2 col-form-label">Direct SID</label>
							<div class="col-sm-2">
								<input type="text" name="DIRECT_SID" class="form-control" id="direct_sid" placeholder="Direct SID" value="{{$nasabah['directSid'] or ''}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="taxid" class="col-sm-2 col-form-label">Account Tax Code <span style="color:red">*</span></label>
							<div class="col-sm-4">
								<select name="TAXID" id="taxid" class="form-control" required="required">
									<option value=""></option>
									@foreach($taxid as $tax)
									<option value="{{$tax['id']}}" {{  $nasabah['accountTaxCode'] == $tax['taxCode'] ? "selected" : "" }}>{{$tax['taxCode']}} - {{$tax['longDesc']}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="companyname" class="col-sm-2 col-form-label">Company Name <span style="color:red">*</span></label>
							<div class="col-sm-4">
								<input type="text" name="investorCompanyName" class="form-control" id="investorCompanyName" placeholder="Company Name" required="required" value="{{ $nasabah['investorCompanyName']}}" >
							</div>
							<label for="biccode" class="col-sm-2 col-form-label">BIC Code</label>
							<div class="col-sm-2">
								<input type="text" name="BICCODE" class="form-control" id="biccode" placeholder="BIC Code" value="{{ $nasabah['investorCompanyBICCode']}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="legaldomicile" class="col-sm-2 col-form-label">Legal Domicile <span style="color:red">*</span></label>
							<div class="col-sm-4">
								<select name="LEGALDOMICILE" id="legaldomicile" class="form-control" required="required">
									<option value=""></option>
									@foreach($Countries as $Nationality)
									<option value="{{$Nationality['id']}}" {{  $nasabah['investorLegalDomicile'] == $Nationality['countryCode'] ? "selected" : "" }}>{{$Nationality['countryName']}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="npwp" class="col-sm-2 col-form-label">NPWP</label>
							<div class="col-sm-2">
								<input type="text" name="NPWP" class="form-control" id="npwp" placeholder="NPWP Number" value="{{ $nasabah['investorNPWPNumber']}}" >
							</div>
							<label for="npwp_regdate" class="col-sm-2 col-form-label">NPWP Registration Date</label>
							<div class="col-sm-2">
								<input type="text" class="datepicker form-control" placeholder="NPWP Expired Date" name="NPWP_REGDATE" id="npwp_regdate" value="{{  $nasabah['investorNPWPRegistrationDate'] == '1970-01-01' ? "" :  $nasabah['investorNPWPRegistrationDate']}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="skd" class="col-sm-2 col-form-label">SKD</label>
							<div class="col-sm-2">
								<input type="text" name="investorSKDNumber" class="form-control" id="investorSKDNumber" placeholder="SKD Number" value="{{ $nasabah['investorSKDNumber']}}" >
							</div>
							<label for="skd_expdate" class="col-sm-2 col-form-label">SKD Expired Date</label>
							<div class="col-sm-2">
								<input type="text" class="datepicker form-control" placeholder="SKD Expired Date" name="investorSKDExpiredDate" id="investorSKDExpiredDate" value="{{  $nasabah['investorSKDExpiredDate'] == '1970-01-01' ? "" :  $nasabah['investorSKDExpiredDate']}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="company_establish_place" class="col-sm-2 col-form-label">Company Established Place <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<input type="text" name="investorCompanyEstablishmentPlace" class="form-control" id="investorCompanyEstablishmentPlace" placeholder="Company Established Place" required="required" value="{{ $nasabah['investorCompanyEstablishmentPlace']}}" >
							</div>
							<label for="company_establish_date" class="col-sm-2 col-form-label">Company Established  Date <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<input type="text" class="datepicker form-control" placeholder="Company Established  Date" name="investorCompanyEstablishmentDate" id="investorCompanyEstablishmentDate" required="required" value="{{  $nasabah['investorCompanyEstablishmentDate'] == '1970-01-01' ? "" :  $nasabah['investorCompanyEstablishmentDate']}}" >
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-6 col-form-label">Page 1 of 6</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-next">Next <i class="fas fa fa-arrow-right"></i></button>
							</div>
						</div>
					</div>
					<div id="page2">
						<div class="form-group row">
							<label for="business_type" class="col-sm-2 col-form-label">Type of Business <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<select name="BUSINESS_TYPE" id="business_type" class="form-control" required="required">
									<option value=""></option>
									<option value="1" {{  $nasabah['investorBusinessType'] == '1' ? "selected" : "" }}>OTH : Others (Lainnya)</option>
									<option value="2" {{  $nasabah['investorBusinessType'] == '2' ? "selected" : "" }}>CP: Coporate (Perusahaan Terbatas)</option>
									<option value="3" {{  $nasabah['investorBusinessType'] == '3' ? "selected" : "" }}>FD : Foundation (Yayasan)</option>
									<option value="4" {{  $nasabah['investorBusinessType'] == '4' ? "selected" : "" }}>IB : Financial Institution (Institusi Keuangan)</option>
									<option value="5" {{  $nasabah['investorBusinessType'] == '5' ? "selected" : "" }}>IS : Insurance (Asuransi)</option>
									<option value="6" {{  $nasabah['investorBusinessType'] == '6' ? "selected" : "" }}>MF : Mutual Fund (Reksa Dana)</option>
									<option value="7" {{  $nasabah['investorBusinessType'] == '7' ? "selected" : "" }}>PF : Pension Fund (Pengelola Dana Pensiun)</option>
									<option value="8" {{  $nasabah['investorBusinessType'] == '8' ? "selected" : "" }}>SC : Securities Company (Perusahaan Sekuritas)</option>
								</select>
							</div>
							<label for="company_characteristic" class="col-sm-2 col-form-label">Company Characteristic <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<select name="COMPANY_CHARACTERISTIC" id="company_characteristic" class="form-control" required="required">
									<option value=""></option>
									<option value="1" {{  $nasabah['investorCompanyCharacteristic'] == '1' ? "selected" : "" }}>Other</option>
									<option value="2" {{  $nasabah['investorCompanyCharacteristic'] == '2' ? "selected" : "" }}>BUMN (State Own Company)</option>
									<option value="3" {{  $nasabah['investorCompanyCharacteristic'] == '3' ? "selected" : "" }}>Local Investment Company</option>
									<option value="4" {{  $nasabah['investorCompanyCharacteristic'] == '4' ? "selected" : "" }}>Social</option>
									<option value="5" {{  $nasabah['investorCompanyCharacteristic'] == '5' ? "selected" : "" }}>Joint Venture</option>
									<option value="6" {{  $nasabah['investorCompanyCharacteristic'] == '6' ? "selected" : "" }}>Foreign Investment Company</option>
									<option value="7" {{  $nasabah['investorCompanyCharacteristic'] == '7' ? "selected" : "" }}>Family Corporation</option>
									<option value="8" {{  $nasabah['investorCompanyCharacteristic'] == '8' ? "selected" : "" }}>Affiliated</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="sourcefund" class="col-sm-2 col-form-label">Source Fund <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="investorFundSource[]" id="sourcefund_1" value="1" {{ in_array("1",$fund) ? "checked" : "" }} >
									<label class="form-check-label" for="sourcefund_1">
										Others
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="investorFundSource[]"  id="sourcefund_3" value="3" {{ in_array("3",$fund) ? "checked" : "" }} >
									<label class="form-check-label" for="sourcefund_3">
										Business Profit
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="investorFundSource[]" id="sourcefund_4" value="4" {{ in_array("4",$fund) ? "checked" : "" }} >
									<label class="form-check-label" for="sourcefund_4">
										Interest
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="investorFundSource[]" id="sourcefund_8" value="8" {{ in_array("8",$fund) ? "checked" : "" }} >
									<label class="form-check-label" for="sourcefund_8">
										Pension Funds
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="investorFundSource[]" id="sourcefund_9" value="9" {{ in_array("9",$fund) ? "checked" : "" }} >
									<label class="form-check-label" for="sourcefund_9">
										Lottery
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="investorFundSource[]" id="sourcefund_10" value="10" {{ in_array("10",$fund) ? "checked" : "" }} >
									<label class="form-check-label" for="sourcefund_10">
										Proceed from Investment
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="investorFundSource[]" id="sourcefund_11" value="11" {{ in_array("11",$fund) ? "checked" : "" }} >
									<label class="form-check-label" for="sourcefund_11">
										Deposit
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="investorFundSource[]" id="sourcefund_12" value="12" {{ in_array("12",$fund) ? "checked" : "" }} >
									<label class="form-check-label" for="sourcefund_12">
										Capital
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="investorFundSource[]" id="sourcefund_13" value="13" {{ in_array("13",$fund) ? "checked" : "" }} >
									<label class="form-check-label" for="sourcefund_13">
										Loan
									</label>
								</div>
							</div>
							<label for="inv_objective" class="col-sm-2 col-form-label">Investment Objective <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="INV_OBJECTIVE[]"   id="inv_objective_1" value="1" {{ in_array("1",$obj) ? "checked" : "" }} >
									<label class="form-check-label" for="inv_objective_1">
										Others
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="INV_OBJECTIVE[]"  id="inv_objective_2" value="2" {{ in_array("2",$obj) ? "checked" : "" }} >
									<label class="form-check-label" for="inv_objective_2">
										Price Appreciation
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="INV_OBJECTIVE[]" id="inv_objective_3" value="3" {{ in_array("3",$obj) ? "checked" : "" }} >
									<label class="form-check-label" for="inv_objective_3">
										Investment
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="INV_OBJECTIVE[]"  id="inv_objective_4" value="4" {{ in_array("4",$obj) ? "checked" : "" }} >
									<label class="form-check-label" for="inv_objective_4">
										Speculation
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter" type="checkbox" name="INV_OBJECTIVE[]"  id="inv_objective_5" value="5" {{ in_array("5",$obj) ? "checked" : "" }} >
									<label class="form-check-label" for="inv_objective_5">
										Income
									</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="sourcefundothers" class="col-sm-2 col-form-label">Other Source Fund</label>
							<div class="col-sm-3">
								<input type="text" name="SOURCEFUNDOTHERS" class="form-control" id="sourcefundothers" placeholder="Other Source Fund" value="" >
							</div>
							<label for="asset_owner" class="col-sm-2 col-form-label">Asset Owner <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<select name="assetOwner" id="assetOwner" class="form-control" required="required">
									<option value=""></option>
									<option value="1" {{  $nasabah['assetOwner'] == '1' ? "selected" : "" }}>Myself</ption>
									<option value="2" {{  $nasabah['assetOwner'] == '2' ? "selected" : "" }}>Representing Other Party</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="article_of_association" class="col-sm-2 col-form-label">Article of Association <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input type="text" name="investorArticleOfAssociation" class="form-control" id="investorArticleOfAssociation" placeholder="Article of Association" value="{{ $nasabah['investorArticleOfAssociation']}}" >
							</div>
							<label for="buss_reg_cert_no" class="col-sm-2 col-form-label">Business Registration Certificate No.</label>
							<div class="col-sm-3">
								<input type="text" name="investorBusinessRegistrationCertificateNumber" class="form-control" id="investorBusinessRegistrationCertificateNumber" placeholder="Business Registation Certificate No" value="{{ $nasabah['investorBusinessRegistrationCertificateNumber']}}" >
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-6 col-form-label">Page 2 of 6</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-next">Next <i class="fas fa fa-arrow-right"></i></button>
								<button type="button" class="btn btn-sm btn-secondary float-right btn-prev mr-2"><i class="fas fa fa-arrow-left"></i> Prev</button>
							</div>
						</div>
					</div>
					<div id="page3">
						<div class="form-group row">
							<label for="investor_asset_1" class="col-sm-2 col-form-label">Latest Year Financial Information Assets <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<select name="investorAsset1" id="investorAsset1" class="form-control" required="required" >
									<option value=""></option>
									<option value="1" {{  $nasabah['investorAsset1'] == '1' ? "selected" : "" }}>< Rp 100 Billion</option>
									<option value="2" {{  $nasabah['investorAsset1'] == '2' ? "selected" : "" }}>Rp 100 Billion - Rp 500 Billion</option>
									<option value="3" {{  $nasabah['investorAsset1'] == '3' ? "selected" : "" }}>Rp 500 Billion - Rp 1 Trillion</option>
									<option value="4" {{  $nasabah['investorAsset1'] == '4' ? "selected" : "" }}>Rp 1 Trillion - Rp 5 Trillion</option>
									<option value="5" {{  $nasabah['investorAsset1'] == '5' ? "selected" : "" }}>>Rp 5 Trillion</option>
								</select>
							</div>
							<label for="investor_operating_profit_1" class="col-sm-2 col-form-label">Latest year financial information Operating profit <span style="color:red">*</span></label>
							<div class="col-sm-4">
								<select name="investorOperatingProfit1" id="investorOperatingProfit1" class="form-control" required="required">
									<option value=""></option>
									<option value="1" {{  $nasabah['investorOperatingProfit1'] == '1' ? "selected" : "" }}>< Rp 1 Billion</option>
									<option value="2" {{  $nasabah['investorOperatingProfit1'] == '2' ? "selected" : "" }}>Rp 1 Billion - Rp 5 Billion</option>
									<option value="3" {{  $nasabah['investorOperatingProfit1'] == '3' ? "selected" : "" }}>Rp 5 Billion - Rp 10 Billion</option>
									<option value="4" {{  $nasabah['investorOperatingProfit1'] == '4' ? "selected" : "" }}>Rp 10 Billion - Rp 50 Billion</option>
									<option value="5" {{  $nasabah['investorOperatingProfit1'] == '5' ? "selected" : "" }}>>Rp 50 Billion</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="investor_asset_2" class="col-sm-2 col-form-label">Last 2 Year Financial Information Assets</label>
							<div class="col-sm-3">
								<select name="investorAsset2" id="investor_asset_2" class="form-control" >
									<option value=""></option>
									<option value="1" {{  $nasabah['investorAsset2'] == '1' ? "selected" : "" }}>< Rp 100 Billion</option>
									<option value="2" {{  $nasabah['investorAsset2'] == '2' ? "selected" : "" }}>Rp 100 Billion - Rp 500 Billion</option>
									<option value="3" {{  $nasabah['investorAsset2'] == '3' ? "selected" : "" }}>Rp 500 Billion - Rp 1 Trillion</option>
									<option value="4" {{  $nasabah['investorAsset2'] == '4' ? "selected" : "" }}>Rp 1 Trillion - Rp 5 Trillion</option>
									<option value="5" {{  $nasabah['investorAsset2'] == '5' ? "selected" : "" }}>>Rp 5 Trillion</option>
								</select>
							</div>
							<label for="investor_operating_profit_2" class="col-sm-2 col-form-label">Last 2 year financial information Operating profit</label>
							<div class="col-sm-4">
								<select name="INVESTOR_OPERATING_PROFIT_2" id="investor_operating_profit_2" class="form-control">
									<option value=""></option>
									<option value="1" {{  $nasabah['investorOperatingProfit2'] == '1' ? "selected" : "" }}>< Rp 1 Billion</option>
									<option value="2" {{  $nasabah['investorOperatingProfit2'] == '2' ? "selected" : "" }}>Rp 1 Billion - Rp 5 Billion</option>
									<option value="3" {{  $nasabah['investorOperatingProfit2'] == '3' ? "selected" : "" }}>Rp 5 Billion - Rp 10 Billion</option>
									<option value="4" {{  $nasabah['investorOperatingProfit2'] == '4' ? "selected" : "" }}>Rp 10 Billion - Rp 50 Billion</option>
									<option value="5" {{  $nasabah['investorOperatingProfit2'] == '5' ? "selected" : "" }}>>Rp 50 Billion</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="investor_asset_3" class="col-sm-2 col-form-label">Last 2 Year Financial Information Assets</label>
							<div class="col-sm-3">
								<select name="INVESTOR_ASSET_3" id="investor_asset_3" class="form-control" >
									<option value=""></option>
									<option value="1" {{  $nasabah['investorAsset3'] == '1' ? "selected" : "" }}>< Rp 100 Billion</option>
									<option value="2" {{  $nasabah['investorAsset3'] == '2' ? "selected" : "" }}>Rp 100 Billion - Rp 500 Billion</option>
									<option value="3" {{  $nasabah['investorAsset3'] == '3' ? "selected" : "" }}>Rp 500 Billion - Rp 1 Trillion</option>
									<option value="4" {{  $nasabah['investorAsset3'] == '4' ? "selected" : "" }}>Rp 1 Trillion - Rp 5 Trillion</option>
									<option value="5" {{  $nasabah['investorAsset3'] == '5' ? "selected" : "" }}>>Rp 5 Trillion</option>
								</select>
							</div>
							<label for="investor_operating_profit_3" class="col-sm-2 col-form-label">Last 2 year financial information Operating profit</label>
							<div class="col-sm-4">
								<select name="INVESTOR_OPERATING_PROFIT_3" id="investor_operating_profit_3" class="form-control" >
									<option value=""></option>
									<option value="1" {{  $nasabah['investorOperatingProfit3'] == '1' ? "selected" : "" }}>< Rp 1 Billion</option>
									<option value="2" {{  $nasabah['investorOperatingProfit3'] == '2' ? "selected" : "" }}>Rp 1 Billion - Rp 5 Billion</option>
									<option value="3" {{  $nasabah['investorOperatingProfit3'] == '3' ? "selected" : "" }}>Rp 5 Billion - Rp 10 Billion</option>
									<option value="4" {{  $nasabah['investorOperatingProfit3'] == '4' ? "selected" : "" }}>Rp 10 Billion - Rp 50 Billion</option>
									<option value="5" {{  $nasabah['investorOperatingProfit3'] == '5' ? "selected" : "" }}>>Rp 50 Billion</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="description" class="col-sm-2 col-form-label">Description</label>
							<div class="col-sm-10">
								<input type="text" name="accountDescription" class="form-control" id="accountDescription" placeholder="Description" value="{{ $nasabah['accountDescription']}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="supplementary_doctype" class="col-sm-2 col-form-label">Type of Supplementary Documents</label>
							<div class="col-sm-4">
								<select name="investorTypeOfSupplementaryDocs" id="investorTypeOfSupplementaryDocs" class="form-control" >
									<option value=""></option>
									<option value="0" {{  $nasabah['investorTypeOfSupplementaryDocs'] == '1' ? "selected" : "" }}>Kosong</option>
									<option value="1" {{  $nasabah['investorTypeOfSupplementaryDocs'] == '1' ? "selected" : "" }}>Copy AD/ART</option>
									<option value="2" {{  $nasabah['investorTypeOfSupplementaryDocs'] == '2' ? "selected" : "" }}>SKMK (Surat Keterangan Mentri Keuangan)</option>
									<option value="3" {{  $nasabah['investorTypeOfSupplementaryDocs'] == '3' ? "selected" : "" }}>Surat Ketetapan Pemerintah RI</option>
									<option value="4" {{  $nasabah['investorTypeOfSupplementaryDocs'] == '4' ? "selected" : "" }}>Surat Pernyataan Efektif dari OJK</option>
									<option value="5" {{  $nasabah['investorTypeOfSupplementaryDocs'] == '5' ? "selected" : "" }}>Keputusan Dirjen Pajak</option>
									<option value="6" {{  $nasabah['investorTypeOfSupplementaryDocs'] == '6' ? "selected" : "" }}>Surat Keputusan dari OJK</option>
								</select>
							</div>
							<label for="supplementary_docexp" class="col-sm-2 col-form-label">Expired Date of Supplementary Documents</label>
							<div class="col-sm-4">
								<input type="text" class="datepicker form-control" placeholder="Company Established  Date" name="investorExpiredDateOfSupplementaryDocs" id="investorExpiredDateOfSupplementaryDocs" value="{{  $nasabah['investorExpiredDateOfSupplementaryDocs'] == '1970-01-01' ? "" :  $nasabah['investorExpiredDateOfSupplementaryDocs']}}" >
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-6 col-form-label">Page 3 of 6</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-next">Next <i class="fas fa fa-arrow-right"></i></button>
								<button type="button" class="btn btn-sm btn-secondary float-right btn-prev mr-2"><i class="fas fa fa-arrow-left"></i> Prev</button>
							</div>
						</div>
					</div>
					<div id="page4">
						<div class="form-group">
							<p>Client Participant Address</p>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Address <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input type="text" name="ADDRESS1" class="form-control" id="address1" placeholder="Address 1" required="required" value="{{$nasabah['investorAddress1'] or ''}}" >
							</div>
							<div class="col-sm-3">
								<input type="text" name="ADDRESS2" class="form-control" id="address2" placeholder="Address 2" value="{{$nasabah['investorAddress2'] or ''}}" >
							</div>
							<div class="col-sm-3">
								<input type="text" name="ADDRESS3" class="form-control" id="address3" placeholder="Address 3" value="{{$nasabah['investorAddress3'] or ''}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">City <span style="color:red">*</span></label>
							<div class="col-sm-4">
								<select name="CITY" class="form-control" id="city" required="required" >
									<option value=""></option>
									@foreach($Cities as $City)
										<option value="{{$City['id']}}" {{ $nasabah['investorCity'] or '' == $City['cityCode'] ? "selected" : "" }}>{{$City['cityName']}}</option>
									@endforeach
								</select>
							</div>
							<label for="province" class="col-sm-2 col-form-label">Province <span style="color:red">*</span></label>
							<div class="col-sm-4">
								<select name="PROVCODE" class="form-control" id="province" required="required" >
									<option value=""></option>
									@foreach($Provinces as $Province)
										<option value="{{$Province['id']}}" {{ $nasabah['investorProvince'] or '' == $Province['provinceCode'] ? "selected" : "" }}>{{$Province['provinceName']}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="description" class="col-sm-2 col-form-label">Country <span style="color:red">*</span></label>
							<div class="col-sm-4">
								<select name="COUNTCODE" class="form-control" id="country" required="required">
									<option value=""></option>
									@foreach($Countries as $country)
										<option value="{{$Nationality['id']}}" {{  $nasabah['investorCountry'] == $country['countryCode'] ? "selected" : "" }}>{{$Nationality['countryName']}}</option>
									@endforeach
								</select>
							</div>
							<label for="zipcode" class="col-sm-2 col-form-label">Zip Code</label>
							<div class="col-sm-2">
								<input type="text" name="ZIPCODE" class="form-control" id="zipcode" placeholder="Zip Code" value="{{$nasabah['investorPostalCode'] or ''}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="phone" class="col-sm-2 col-form-label">Phone <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input type="tel" name="PHONE" class="form-control" id="phone" placeholder="Phone Number" required="required" value="{{$nasabah['investorHomePhone'] or ''}}" >
							</div>
							<label for="mobilephone" class="col-sm-2 col-form-label">Mobile Phone</label>
							<div class="col-sm-3">
								<input type="tel" name="MOBILEPHONE" class="form-control" id="mobilephone" placeholder="Mobile Phone Number" value="{{$nasabah['investorMobilePhone'] or ''}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="fax" class="col-sm-2 col-form-label">Fax</label>
							<div class="col-sm-3">
								<input type="tel" name="FAX" class="form-control" id="fax" placeholder="Fax Number" value="{{$nasabah['investorFax'] or ''}}" >
							</div>
							<label for="email" class="col-sm-2 col-form-label">Email</label>
							<div class="col-sm-3">
								<input type="email" name="EMAIL" class="form-control" id="email" placeholder="Email Address" value="{{$nasabah['investorEmail'] or ''}}" >
							</div>
						</div>

						<div class="form-group">
							<p>Client Participant Other Address</p>
						</div>
						<div class="form-group row">
							<label for="otheraddress" class="col-sm-2 col-form-label">Address</label>
							<div class="col-sm-3">
								<input type="text" name="investorOtherAddress1" class="form-control" id="investorOtherAddress1" placeholder="Address 1" value="{{$nasabah['investorOtherAddress1'] or ''}}" >
							</div>
							<div class="col-sm-3">
								<input type="text" name="investorOtherAddress2" class="form-control" id="investorOtherAddress2" placeholder="Address 2" value="{{$nasabah['investorOtherAddress2'] or ''}}" >
							</div>
							<div class="col-sm-3">
								<input type="text" name="investorOtherAddress3" class="form-control" id="investorOtherAddress3" placeholder="Address 3" value="{{$nasabah['investorOtherAddress3'] or ''}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">City</label>
							<div class="col-sm-4">
								<select name="OTHERCITY" class="form-control" id="othercity" >
									<option value=""></option>
									@foreach($Cities as $City)
										<option value="{{$City['cityCode']}}" {{ (isset($nasabah['investorOtherCity']) ? $nasabah['investorOtherCity'] : "" == $City['cityCode']) ? "selected" : "" }}>{{$City['cityName']}}</option>
									@endforeach
								</select>
							</div>
							<label for="province" class="col-sm-2 col-form-label">Province</label>
							<div class="col-sm-4">
								<select name="investorOtherProvince" class="form-control" id="investorOtherProvince" >
									<option value=""></option>
									@foreach($Provinces as $Province)
										<option value="{{$Province['provinceCode']}}" {{ (isset($nasabah['investorOtherProvince']) ? $nasabah['investorOtherProvince'] : "" == $Province['provinceCode']) ? "selected" : "" }}>{{$Province['provinceName']}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="description" class="col-sm-2 col-form-label">Country</label>
							<div class="col-sm-4">
								<select name="investorOtherCountry" class="form-control" id="investorOtherCountry" >
									<option value=""></option>
									@foreach($Countries as $country)
										<option value="{{$country['countryCode']}}" {{(isset($nasabah['investorOtherCountry']) ? $nasabah['investorOtherCountry'] : "" == $country['countryCode']) ? "selected" : "" }}>{{$country['countryName']}}</option>
									@endforeach
								</select>
							</div>
							<label for="zipcode" class="col-sm-2 col-form-label">Zip Code</label>
							<div class="col-sm-2">
								<input type="text" name="investorOtherPostalCode" class="form-control" id="investorOtherPostalCode" placeholder="Zip Code" value="{{$nasabah['investorOtherPostalCode'] or ''}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="phone" class="col-sm-2 col-form-label">Phone</label>
							<div class="col-sm-3">
								<input type="tel" name="investorOtherHomePhone" class="form-control" id="investorOtherHomePhone" placeholder="Phone Number" value="{{$nasabah['investorOtherHomePhone'] or ''}}" >
							</div>
							<label for="mobilephone" class="col-sm-2 col-form-label">Mobile Phone</label>
							<div class="col-sm-3">
								<input type="tel" name="investorOtherMobilePhone" class="form-control" id="investorOtherMobilePhone" placeholder="Mobile Phone Number" value="{{$nasabah['investorOtherMobilePhone'] or ''}}" >
							</div>
						</div>
						<div class="form-group row">
							<label for="fax" class="col-sm-2 col-form-label">Fax</label>
							<div class="col-sm-3">
								<input type="tel" name="investorOtherFax" class="form-control" id="otherfax" placeholder="Fax Number" value="{{$nasabah['investorOtherFax'] or ''}}" >
							</div>
							<label for="email" class="col-sm-2 col-form-label">Email</label>
							<div class="col-sm-3">
								<input type="email" name="investorOtherEmail" class="form-control" id="otheremail" placeholder="Email Address" value="{{$nasabah['investorOtherEmail'] or ''}}" >
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-6 col-form-label">Page 4 of 6</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-next">Next <i class="fas fa fa-arrow-right"></i></button>
								<button type="button" class="btn btn-sm btn-secondary float-right btn-prev mr-2"><i class="fas fa fa-arrow-left"></i> Prev</button>
							</div>
						</div>
					</div>
					<div id="page5">
						<div class="form-group">
							<p>Investor Account Bank (1)</p>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Investor Bank Name </label>
							<div class="col-sm-5">
								<input type="text" name="accBankName1" class="form-control" id="accBankName1" placeholder="Bank Name" required="required" value="{{$nasabah['investorBankAccountName1'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">Bank Account Number</label>
							<div class="col-sm-4">
								<input type="text" name="CITY" class="form-control" id="accBankNumber1" placeholder="Account Number" value="{{$nasabah['investorBankAccountNumber1'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Investor BI Code</label>
							<div class="col-sm-4">
								<input type="text" name="PROVCODE" class="form-control" id="investorBiCode1" placeholder="BI Code" value="{{$nasabah['investorBankAccountBICCode1'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">Bank Acc Holder Name</label>
							<div class="col-sm-4">
								<input type="text" name="PROVCODE" class="form-control" id="bankAccHolderName1" placeholder="Holder Name" value="{{$nasabah['investorBankAccountHolderName1'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Account Currency</label>
							<div class="col-sm-4">
								<input type="text" name="PROVCODE" class="form-control" id="investorAccCurrency1" placeholder="account currency" value="{{$nasabah['investorBankAccountCurrency1'] or ''}}"  >
							</div>
						</div><br>
						<div class="form-group">
							<p>Investor Account Bank (2)</p>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Investor Bank Name </label>
							<div class="col-sm-5">
								<input type="text" name="investorBankAccountName2" class="form-control" id="investorBankAccountName2" placeholder="Bank Name" required="required" value="{{$nasabah['investorBankAccountName2'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">Bank Account Number</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountNumber2" class="form-control" id="investorBankAccountNumber2" placeholder="Account Number" value="{{$nasabah['investorBankAccountNumber2'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Investor BI Code</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountBICCode2" class="form-control" id="investorBankAccountBICCode2" placeholder="BI Code" value="{{$nasabah['investorBankAccountBICCode2'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">Bank Acc Holder Name</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountHolderName2" class="form-control" id="investorBankAccountHolderName2" placeholder="Holder Name" value="{{$nasabah['investorBankAccountHolderName2'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Account Currency</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountCurrency2" class="form-control" id="investorBankAccountCurrency2" placeholder="account currency" value="{{$nasabah['investorBankAccountCurrency2'] or ''}}"  >
							</div>
						</div><br>
						<div class="form-group">
							<p>Investor Account Bank (3)</p>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Investor Bank Name </label>
							<div class="col-sm-5">
								<input type="text" name="accBankName3" class="form-control" id="accBankName3" placeholder="Bank Name" required="required" value="{{$nasabah['investorBankAccountName3'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">Bank Account Number</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountName3" class="form-control" id="investorBankAccountName3" placeholder="Account Number" value="{{$nasabah['investorBankAccountNumber3'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Investor BI Code</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountBICCode3" class="form-control" id="investorBankAccountBICCode3" placeholder="BI Code" value="{{$nasabah['investorBankAccountBICCode3'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">Bank Acc Holder Name</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountHolderName3" class="form-control" id="investorBankAccountHolderName3" placeholder="Holder Name" value="{{$nasabah['investorBankAccountHolderName3'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Account Currency</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountCurrency3" class="form-control" id="investorBankAccountCurrency3" placeholder="account currency" value="{{$nasabah['investorBankAccountCurrency3'] or ''}}"  >
							</div>
						</div><br>
						<div class="form-group row">
							<label class="col-sm-6 col-form-label">Page 5 of 6</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-next">Next <i class="fas fa fa-arrow-right"></i></button>
								<button type="button" class="btn btn-sm btn-secondary float-right btn-prev mr-2"><i class="fas fa fa-arrow-left"></i> Prev</button>
							</div>
						</div>
					</div>
					<div id="page6">
						@for($i=1;$i<=4;$i++)
						<div class="form-group">
							<p>Client Participant Authorized Person ({{$i}})</p>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Authorized First Name Person </label>
							<div class="col-sm-2">
								<input type="text" name="investorAuthorizedPersonFirstName{{$i}}" class="form-control" id="investorAuthorizedPersonFirstName{{$i}}" required="required" value="{{$nasabah['investorAuthorizedPersonFirstName'.$i] or ''}}"  >
							</div>
							<label for="address" class="col-sm-2 col-form-label">Authorized Middle Name Person </label>
							<div class="col-sm-2">
								<input type="text" name="investorAuthorizedPersonMiddleName{{$i}}" class="form-control" id="investorAuthorizedPersonMiddleName{{$i}}" required="required" value="{{$nasabah['investorAuthorizedPersonMiddleName'.$i] or ''}}"  >
							</div>
							<label for="address" class="col-sm-2 col-form-label">Authorized Last Name Person </label>
							<div class="col-sm-2">
								<input type="text" name="investorAuthorizedPersonLastName{{$i}}" class="form-control" id="investorAuthorizedPersonLastName{{$i}}" required="required" value="{{$nasabah['investorAuthorizedPersonLastName'.$i] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">Authorized Person Position</label>
							<div class="col-sm-4">
								<input type="text" name="investorAuthorizedPersonPosition{{$i}}" class="form-control" id="investorAuthorizedPersonPosition{{$i}}" value="{{$nasabah['investorAuthorizedPersonPosition'.$i] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">KTP person </label>
							<div class="col-sm-4">
								<input type="text" name="investorAuthorizedPersonKTPNumber{{$i}}" class="form-control" id="investorAuthorizedPersonKTPNumber{{$i}}" value="{{$nasabah['investorAuthorizedPersonKTPNumber'.$i] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">KTP Expired Date</label>
							<div class="col-sm-4">
								<input type="text" name="investorAuthorizedPersonKTPExpiredDate{{$i}}" class="form-control" id="investorAuthorizedPersonKTPExpiredDate{{$i}}" value="{{$nasabah['investorAuthorizedPersonKTPExpiredDate'.$i] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">NPWP person </label>
							<div class="col-sm-4">
								<input type="text" name="investorAuthorizedPersonNPWPNumber{{$i}}" class="form-control" id="investorAuthorizedPersonNPWPNumber{{$i}}" value="{{$nasabah['investorAuthorizedPersonNPWPNumber'.$i] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">NPWP Expired Date</label>
							<div class="col-sm-4">
								<input type="text" name="investorAuthorizedPersonNPWPRegistrationDate{{$i}}" class="form-control" id="investorAuthorizedPersonNPWPRegistrationDate{{$i}}" value="{{$nasabah['investorAuthorizedPersonNPWPRegistrationDate'.$i] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">Passport person </label>
							<div class="col-sm-4">
								<input type="text" name="investorAuthorizedPersonPassportNumber{{$i}}" class="form-control" id="investorAuthorizedPersonPassportNumber{{$i}}" value="{{$nasabah['investorAuthorizedPersonPassportNumber'.$i] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Passport Expired Date</label>
							<div class="col-sm-4">
								<input type="text" name="investorAuthorizedPersonPassportExpiredDate{{$i}}" class="form-control" id="investorAuthorizedPersonPassportExpiredDate{{$i}}" value="{{$nasabah['investorAuthorizedPersonPassportExpiredDate'.$i] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">KITAS person </label>
							<div class="col-sm-4">
								<input type="text" name="investorAuthorizedPersonKitasSKDNumber{{$i}}" class="form-control" id="investorAuthorizedPersonKitasSKDNumber{{$i}}" value="{{$nasabah['investorAuthorizedPersonKitasSKDNumber'.$i] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">KITAS Expired Date</label>
							<div class="col-sm-4">
								<input type="text" name="investorAuthorizedPersonKitasSKDNumber{{$i}}" class="form-control" id="investorAuthorizedPersonKitasSKDNumber{{$i}}" value="{{$nasabah['investorAuthorizedPersonKitasSKDNumber'.$i] or ''}}"  >
							</div>
						</div>
						@endfor
						<br>
						<div class="form-group row mt-4">
							<label class="col-sm-6 col-form-label">Page 6 of 6</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-submit"><i class="fas fa fa-check"></i> Submit</button>
								<button type="button" class="btn btn-sm btn-secondary float-right btn-prev mr-2"><i class="fas fa fa-arrow-left"></i> Prev</button>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('script')
	<script>
		$( document ).ready(function(){
			$('#nationality').val('ID');

			$('#page2').css('display','none');
			$('#page3').css('display','none');
			$('#page4').css('display','none');
			$('#page5').css('display','none');
			$('#page6').css('display','none');


			// $('input,select,radio').each(function(){
			// 	$(this).removeAttr('required');
			// });

		})

		$('#page1').find('.btn-next').on('click',function(){
			status = 1;
			$('#page1').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})
			if(status == 1){
				$('#page1').fadeOut('fast', function(){
					$('#page2').fadeIn('slow');
				});
			}
		})

		$('#page2').find('.btn-prev').on('click',function(){
			$('#page2').fadeOut('fast', function(){
				$('#page1').fadeIn('slow');
			});
		})

		$('#page2').find('.btn-next').on('click',function(){
			status = 1;
			$('#page2').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})
			if(status == 1){
				$('#page2').fadeOut('fast', function(){
					$('#page3').fadeIn('slow');
				});
			}
		})

		$('#page3').find('.btn-prev').on('click',function(){
			$('#page3').fadeOut('fast', function(){
				$('#page2').fadeIn('slow');
			});
		})

		$('#page3').find('.btn-next').on('click',function(){
			status = 1;
			$('#page3').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})
			if(status == 1){
				$('#page3').fadeOut('fast', function(){
					$('#page4').fadeIn('slow');
				});
			}
		})

		$('#page4').find('.btn-prev').on('click',function(){
			$('#page4').fadeOut('fast', function(){
				$('#page3').fadeIn('slow');
			});
		})

		$('#page4').find('.btn-next').on('click',function(){
			status = 1;
			$('#page4').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})
			if(status == 1){
				$('#page4').fadeOut('fast', function(){
					$('#page5').fadeIn('slow');
				});
			}
		})

		$('#page5').find('.btn-prev').on('click',function(){
			$('#page5').fadeOut('fast', function(){
				$('#page4').fadeIn('slow');
			});
		})

		$('#page5').find('.btn-next').on('click',function(){
			status = 1;
			$('#page5').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})
			if(status == 1){
				$('#page5').fadeOut('fast', function(){
					$('#page6').fadeIn('slow');
				});
			}
		})

		$('#page6').find('.btn-prev').on('click',function(){
			$('#page6').fadeOut('fast', function(){
				$('#page5').fadeIn('slow');
			});
		})

		$('#page6').find('.btn-submit').on('click',function(){
			status = 1;
			$('#page6').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})
			if(status == 1){
				$('#insert-form-client').submit();
			}
		})

		$('#inv_client_type').on('blur',function(){
			if($(this).val() == 'D'){
				$('#company_establish_place').attr('required','required');
				$('[for="company_establish_place"]').text('Company Established Place');
				$('[for="company_establish_place"]').append('<span style="color:red">&nbsp;*</span>');

				$('#company_characteristic').attr('required','required');
				$('[for="company_characteristic"]').text('Company Characteristic');
				$('[for="company_characteristic"]').append('<span style="color:red">&nbsp;*</span>');

				$('#investor_asset_1').attr('required','required');
				$('[for="investor_asset_1"]').text('Latest year financial information Assets');
				$('[for="investor_asset_1"]').append('<span style="color:red">&nbsp;*</span>');

				$('#investor_operating_profit_1').attr('required','required');
				$('[for="investor_operating_profit_1"]').text('Latest year financial information Operating profit');
				$('[for="investor_operating_profit_1"]').append('<span style="color:red">&nbsp;*</span>');

				$('[for="inv_objective"]').text('Investment Objective');
				$('[for="inv_objective"]').append('<span style="color:red">&nbsp;*</span>');

				$('#mother_name').attr('required','required');
				$('[for="mother_name"]').text('Mother Name');
				$('[for="mother_name"]').append('<span style="color:red">&nbsp;*</span>');

				$('#direct_sid').removeAttr('required');
				$('[for="direct_sid"]').text('Direct SID');
			}else{
				$('#company_establish_place').removeAttr('required');
				$('[for="company_establish_place"]').text('Company Established Place');

				$('#company_characteristic').removeAttr('required');
				$('[for="company_characteristic"]').text('Company Characteristic');

				$('#investor_asset_1').removeAttr('required');
				$('[for="investor_asset_1"]').text('Latest year financial information Assets');

				$('#investor_operating_profit_1').removeAttr('required');
				$('[for="investor_operating_profit_1"]').text('Latest year financial information Operating profit');

				$('[for="inv_objective"]').text('Investment Objective');

				$('#mother_name').removeAttr('required');
				$('[for="mother_name"]').text('Mother Name');

				$('#direct_sid').attr('required','required');
				$('[for="direct_sid"]').text('Direct SID *');
			}
		})

		$('#sourcefund').on('click',function(){
			if($(this).is(':checked')){
				$('#sourcefundothers').attr('required','required');
				$('[for="sourcefundothers"]').text('Other Source Fund *');
			}else{
				$('#sourcefundothers').removeAttr('required');
				$('[for="sourcefundothers"]').text('Other Source Fund');
			}
		})

		$('#legaldomicile').on('blur',function(){
			if($(this).val() == 'ID'){
				$('[for="npwp"]').text('NPWP *');
				$('#npwp').attr('required','required');
				$('[for="npwp_regdate"]').text('NPWP Registation Date *');
				$('#npwp_regdate').attr('required','required');

				$('[for="article_of_association"]').text('Article of Association *');
				$('#article_of_association').attr('required','required');
				$('[for="buss_reg_cert_no"]').text('Business Registration Certificate No.');
				$('#buss_reg_cert_no').removeAttr('required');
			}else{
				$('[for="npwp"]').text('NPWP');
				$('#npwp').removeAttr('required');
				$('[for="npwp_regdate"]').text('NPWP Registation Date');
				$('#npwp_regdate').removeAttr('required');

				$('[for="article_of_association"]').text('Article of Association');
				$('#article_of_association').removeAttr('required');
				$('[for="buss_reg_cert_no"]').text('Business Registration Certificate No. *');
				$('#buss_reg_cert_no').attr('required','required');
			}
		});
	</script>
	<script>
	$(window).on('beforeunload', function(){
	    var c=confirm();
	  if(c){
	    return true;
	  }
	  else{
	    return false;
	  }
	});
	</script>
@endsection
