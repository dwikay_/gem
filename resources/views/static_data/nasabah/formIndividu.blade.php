@extends('layouts.apps')

@section('content')
<div class="container-fluid mt-3">
	<div class="row m-0">
		<div class="col-md-12 p-0">
			<div class="card card-default">
				<div class="card-header">
					Individual Client Participant
					<a href="{{ URL::previous() }}" class="float-right clickable close-icon" data-effect="fadeOut"><i class="fa fa-times"></i></a>
				</div>
				<div class="card-body">
					<form id="insert-form-client" method="post" action="{{url('dataStatis/nasabah/update/'.$nasabah['id'].'/'.$tipeNasabah.'/'.$idNasabah)}}" files="true">
						{{csrf_field()}}
					<div id="page1">
						<div class="form-group row">
							<label for="acccode" class="col-sm-2 col-form-label">Account Code</label>
							<div class="col-sm-2">
								<input type="text" name="ACCCODE" class="form-control no-border" id="acccode" value="{{$nasabah['nasabah']['kodeNasabah']}}">
							</div>
							<label for="sid" class="col-sm-1 col-form-label">SID <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input type="text" name="SID" class="form-control no-border" id="sid" required="required" value="{{$nasabah['nasabah']['sid']}}">
							</div>
							<label for="ksei_acc" class="col-sm-2 col-form-label">Sub Rek 001<span style="color:red">*</span></span></label>
							<div class="col-sm-2">
								<input type="text" name="KSEI_ACC" class="form-control" disabled id="ksei_acc" value="{{$nasabah['nasabah']['bankSubRek']}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="inv_type" class="col-sm-2 col-form-label">Investor Type</label>
							<div class="col-sm-2">
								<input type="text" name="INV_TYPE" class="form-control no-border" id="inv_type"value="INDIVIDUAL"   required="required">
							</div>
							<label for="inv_client_type" class="col-sm-2 col-form-label">Investor Category <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<select name="INV_CLIENT_TYPE" id="inv_client_type" class="form-control" required="required">
									<option value=""></option>
									<option value="DIRECT" {{ $nasabah['investorCategory'] == "DIRECT" ? "selected" : "" }}>Direct</option>
									<option value="INDIRECT" {{ $nasabah['investorCategory'] == "INDIRECT" ? "selected" : "" }}>Indirect</option>
								</select>
							</div>
							<label for="direct_sid" class="col-sm-2 col-form-label">Direct SID</label>
							<div class="col-sm-2">
								<input type="text" name="DIRECT_SID" class="form-control" id="direct_sid" value="{{$nasabah['directSid'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="taxid" class="col-sm-2 col-form-label">Account Tax Code <span style="color:red">*</span></label>
							<div class="col-sm-2">
							<select name="TAXID" id="TAXID" class="form-control" required="required">
								<option value=""></option>
								@foreach($taxid as $tax)
								<option value="{{$tax['taxCode']}}" {{ $nasabah['accountTaxCode'] == $tax['taxCode'] ? "selected" : "" }}> {{$tax['taxCode']}} - {{$tax['longDesc']}}</option>
								@endforeach
							</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="firstname" class="col-sm-2 col-form-label">First Name <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<input type="text" name="FIRSTNAME" class="form-control" id="firstname" required="required" value="{{$nasabah['investorFirstName']}}"  >
							</div>
							<label for="middlename" class="col-sm-2 col-form-label">Middle Name</label>
							<div class="col-sm-2">
								<input type="text" name="MIDDLENAME" class="form-control" id="middlename" value="{{$nasabah['investorMiddleName']}}"  >
							</div>
							<label for="lastname" class="col-sm-2 col-form-label">Last Name</label>
							<div class="col-sm-2">
								<input type="text" name="LASTNAME" class="form-control" id="lastname" value="{{$nasabah['investorLastName']}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="nationality" class="col-sm-2 col-form-label">Nationality <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<select name="NATIONALITY" class="form-control" id="nationality" required="required">
									<option value=""></option>
									@foreach($Countries as $country)
										<option value="{{$country['countryCode']}}" {{ $nasabah['investorNationality'] == $country['countryCode'] ? "selected" : "" }}>{{$country['countryName']}}</option>
									@endforeach
								</select>
							</div>
							<label for="sex" class="col-sm-2 col-form-label">Gender / Sex <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<select name="SEX" id="sex" class="form-control" required="required"  >
									<option value="M" {{ $nasabah['investorSex'] == 'M' ? "selected" : "" }}>1: Pria</option>
									<option value="F" {{ $nasabah['investorSex'] == 'F' ? "selected" : "" }}>2: Wanita</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="ktp" class="col-sm-2 col-form-label">KTP <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<input type="text" name="KTP" class="form-control" id="ktp" required="required" value="{{$nasabah['investorKTPNumber']}}"  >
							</div>
							<label for="ktp_expdate" class="col-sm-2 col-form-label">KTP Expired Date <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<input type="text" class="datepicker form-control" name="KTP_EXPDATE" id="ktp_expdate" data-toggle="tooltip" data-placement="top" title="Fill with 31-Dec-9999 if lifetime" required="required" value="{{ $nasabah['investorKTPExpiredDate'] == '1970-01-01' ? "" : $nasabah['investorKTPExpiredDate']}}"  >
							</div>
							<label for="ktp_expdate" class="col-sm-2 col-form-label">KTP Reg Date <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<input type="text" class="datepicker form-control" name="KTP_REG" id="KTP_REG" data-toggle="tooltip" data-placement="top" title="Fill with 31-Dec-9999 if lifetime" required="required" value="{{ $nasabah['investorAuthorizedPersonKTPRegistrationDate2'] == '1970-01-01' ? "" : $nasabah['investorKTPExpiredDate']}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="npwp" class="col-sm-2 col-form-label">NPWP</label>
							<div class="col-sm-2">
								<input type="text" name="NPWP" class="form-control" id="npwp" value="{{ $nasabah['investorNPWPNumber']}}"  >
							</div>
							<label for="npwp_regdate" class="col-sm-2 col-form-label">NPWP Registration Date</label>
							<div class="col-sm-2">
								<input type="text" class="datepicker form-control" name="NPWP_REGDATE" id="npwp_regdate" value="{{ $nasabah['investorNPWPRegistrationDate'] == '1970-01-01' ? "" : $nasabah['investorNPWPRegistrationDate']}}"  >
							</div>
							<!-- <label for="acccode" class="col-sm-2 col-form-label">Account Status</label>
							<div class="col-sm-2">
								<input type="text" name="STATUS" class="form-control no-border" id="status" value="{{ $nasabah['nasabah']['status'] == "A" ? "Active" : ($nasabah['nasabah']['status'] == 'S' ? "Suspend" : "Closed") }}" required="required">
							</div> -->
						</div>
						<div class="form-group row">
							<label for="passport" class="col-sm-2 col-form-label">Passport</label>
							<div class="col-sm-2">
								<input type="text" name="PASSPORT" class="form-control" id="passport" value="{{$nasabah['investorPassportNumber']}}"  >
							</div>
							<label for="passport_expdate" class="col-sm-2 col-form-label">Passport Expired Date</label>
							<div class="col-sm-2">
								<input type="text" class="datepicker form-control" name="PASSPORT_EXPDATE" id="passport_expdate" value="{{ $nasabah['investorPassportExpiredDate'] == '1970-01-01' ? "" : $nasabah['investorPassportExpiredDate']}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="birth_place" class="col-sm-2 col-form-label">Birth Place <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<input type="text" name="BIRTH_PLACE" class="form-control" id="birth_place" required="required" value="{{$nasabah['investorBirthPlace']}}"  >
							</div>
							<label for="birth_date" class="col-sm-2 col-form-label">Birth Date <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<input type="text" class="datepicker form-control" name="BIRTH_DATE" id="birth_date" required="required" value="{{ $nasabah['investorBirthDate'] == '1970-01-01' ? "" : $nasabah['investorBirthDate']}}"  >
							</div>
						</div>
						<div class="form-group row mt-4">
							<label class="col-sm-6 col-form-label">Page 1 of 5</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-next">Next <i class="fas fa fa-arrow-right"></i></button>
							</div>
						</div>
					</div>
					<div id="page2">
						<div class="form-group row">
							<label for="marital_status" class="col-sm-2 col-form-label">Marital Status <span style="color:red">*</span></label>
							<div class="col-sm-2">
								<select name="MARITAL_STATUS" id="marital_status" class="form-control" required="required">
									<option value=""></option>
									<option value="1" {{ $nasabah['investorMaritalStatus'] == '1' ? "selected" : "" }}>1: Single</option>
									<option value="2" {{ $nasabah['investorMaritalStatus'] == '2' ? "selected" : "" }}>2: Menikah</option>
									<option value="2" {{ $nasabah['investorMaritalStatus'] == '3' ? "selected" : "" }}>3: Duda</option>
									<option value="2" {{ $nasabah['investorMaritalStatus'] == '4' ? "selected" : "" }}>4: Janda</option>
								</select>
							</div>
							<label for="spouse_name" class="col-sm-1 col-form-label">Spouse</label>
							<div class="col-sm-3">
								<input type="text" name="SPOUSE_NAME" class="form-control" id="spouse_name" value="{{$nasabah['investorSpouseName']}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="mother_name" class="col-sm-2 col-form-label">Mother Name</label>
							<div class="col-sm-2">
								<input type="text" name="MOTHER_NAME" class="form-control" id="mother_name" value="{{$nasabah['investorMothersMaidenName']}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="heir_name" class="col-sm-2 col-form-label">Heir Name</label>
							<div class="col-sm-3">
								<input type="text" name="HEIR_NAME" class="form-control" id="heir_name" value="{{$nasabah['investorHeirName']}}"  >
							</div>
							<label for="heir_relation" class="col-sm-2 col-form-label">Heir Relation</label>
							<div class="col-sm-3">
								<input type="text" name="HEIR_RELATION" class="form-control" id="heir_relation" value="{{$nasabah['investorHeirRelation']}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="description" class="col-sm-2 col-form-label">Description</label>
							<div class="col-sm-10">
								<input type="text" name="DESCRIPTION" class="form-control" id="description" value="{{$nasabah['accountDescription']}}"  >
							</div>
						</div>
						<div class="form-group row mt-4">
							<label class="col-sm-6 col-form-label">Page 2 of 5</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-next">Next <i class="fas fa fa-arrow-right"></i></button>
								<button type="button" class="btn btn-sm btn-secondary float-right btn-prev mr-2"><i class="fas fa fa-arrow-left"></i> Prev</button>
							</div>
						</div>
					</div>
					<div id="page3">
						<div class="form-group">
							<!-- <p class="h4">Client Participant Address</p> -->
						</div>
						<div class="form-group row">
							<label for="education_background" class="col-sm-2 col-form-label">Education <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<select name="EDUCATION_BACKGROUND" id="education_background" class="form-control" required="required">
									<option value=""></option>
									<option value="1" {{ $nasabah['investorEducationalBackground'] == '1' ? "selected" : "" }}>OTH : Lainnya</option>
									<option value="2" {{ $nasabah['investorEducationalBackground'] == '2' ? "selected" : "" }}>SD: Sekolah Dasar</option>
									<option value="3" {{ $nasabah['investorEducationalBackground'] == '3' ? "selected" : "" }}>SMP: Sekolah Menengah Pertama</option>
									<option value="4" {{ $nasabah['investorEducationalBackground'] == '4' ? "selected" : "" }}>SMA: Sekolah Menengah Atas (Termasuk D-1 dan D2)</option>
									<option value="5" {{ $nasabah['investorEducationalBackground'] == '5' ? "selected" : "" }}>D3: Akademi - D3</option>
									<option value="6" {{ $nasabah['investorEducationalBackground'] == '6' ? "selected" : "" }}>S1: Sarjana, Statra 1</option>
									<option value="7" {{ $nasabah['investorEducationalBackground'] == '7' ? "selected" : "" }}>S2: Sarjana, Statra 2</option>
									<option value="8" {{ $nasabah['investorEducationalBackground'] == '8' ? "selected" : "" }}>S3: Sarjana, Statra 3</option>
								</select>
							</div>
							<label for="occupation" class="col-sm-2 col-form-label">Ocupation <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<select name="OCCUPATION" id="occupation" class="form-control" required="required">
									<option value=""></option>
									<option value="1" {{ $nasabah['investorOccupation'] == '1' ? "selected" : "" }}>Lainnya</option>
									<option value="2" {{ $nasabah['investorOccupation'] == '2' ? "selected" : "" }}>Pegawai Swasta</option>
									<option value="3" {{ $nasabah['investorOccupation'] == '3' ? "selected" : "" }}>Pegawai Negeri</option>
									<option value="4" {{ $nasabah['investorOccupation'] == '4' ? "selected" : "" }}>Ibu Rumah Tangga</option>
									<option value="5" {{ $nasabah['investorOccupation'] == '5' ? "selected" : "" }}>Pengusaha</option>
									<option value="6" {{ $nasabah['investorOccupation'] == '6' ? "selected" : "" }}>Pelajar</option>
									<option value="7" {{ $nasabah['investorOccupation'] == '7' ? "selected" : "" }}>TNI/Polisi</option>
									<option value="8" {{ $nasabah['investorOccupation'] == '8' ? "selected" : "" }}>Pensiunan</option>
									<option value="9" {{ $nasabah['investorOccupation'] == '9' ? "selected" : "" }}>Guru</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="occupation_other" class="col-sm-2 col-form-label">Ocupation Other</label>
							<div class="col-sm-3">
								<input type="text" name="OCCUPATION_OTHER" readonly class="form-control" id="occupation_other" value="" >
							</div>
							<label for="nature_of_business" class="col-sm-2 col-form-label">Nature of Business</label>
							<div class="col-sm-3">
								<input type="text" name="NATURE_OF_BUSINESS" class="form-control" id="nature_of_business" value="{{$nasabah['investorNatureofBusiness']}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="annual_income" class="col-sm-2 col-form-label">Annual Income <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<select name="ANNUAL_INCOME" id="annual_income" class="form-control" required="required">
									<option value=""></option>
									<option value="1" {{ $nasabah['investorIncomePerAnnum'] == '1' ? "selected" : "" }}>1: < Rp 10 Juta</option>
									<option value="2" {{ $nasabah['investorIncomePerAnnum'] == '2' ? "selected" : "" }}>2: Rp 10 juta - 50 juta</option>
									<option value="3" {{ $nasabah['investorIncomePerAnnum'] == '3' ? "selected" : "" }}>3: Rp 50 juta - 100 juta</option>
									<option value="4" {{ $nasabah['investorIncomePerAnnum'] == '4' ? "selected" : "" }}>4: Rp 100 juta - 500 juta</option>
									<option value="5" {{ $nasabah['investorIncomePerAnnum'] == '5' ? "selected" : "" }}>5: Rp 500 juta - 1 miliyar</option>
									<option value="6" {{ $nasabah['investorIncomePerAnnum'] == '6' ? "selected" : "" }}>6: > 1 miliyar</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="sourcefund" class="col-sm-2 col-form-label">Source Fund <span style="color:red">*</span></label>
							<div class="col-sm-3">
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_1" value="1" {{ in_array("1",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_1">
							      Others
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_2" value="2" {{ in_array("2",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_2">
							      Salary
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_3" value="3"  {{ in_array("3",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_3">
							      Business Profit
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_4" value="4"  {{ in_array("4",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_4">
							      Interest
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_5" value="5" {{ in_array("5",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_5">
							      Heritage
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_6" value="6"  {{ in_array("6",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_6">
							      Grant from Parent or Kids
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_7" value="7" {{ in_array("7",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_7">
							      Grant from Spouse
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_8" value="8" {{ in_array("8",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_8">
							      Pension Funds
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_9" value="9" {{ in_array("9",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_9">
							      Lottery
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_10" value="10" {{ in_array("10",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_10">
							      Proceed from Investment
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_11" value="11" {{ in_array("11",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_11">
							      Deposit
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_12" value="12" {{ in_array("12",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_12">
							      Capital
							    </label>
							  </div>
							  <div class="form-check">
							    <input class="form-check-input check-filter sourceFund" type="checkbox" name="SOURCEFUND[]" id="sourcefund_13" value="13" {{ in_array("13",$fund) ? "checked" : "" }}>
							    <label class="form-check-label" for="sourcefund_13">
							      Loan
							    </label>
							  </div>
							</div>
							<label for="inv_objective" class="col-sm-2 col-form-label">Investment Objective <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<div class="form-check">
									<input class="form-check-input check-filter invensmentObj" type="checkbox" name="INV_OBJECTIVE[]" id="inv_objective_a" value="1"  {{ in_array("1",$obj) ? "checked" : "" }}>
									<label class="form-check-label" for="inv_objective_1">
										Others
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter invensmentObj" type="checkbox" name="INV_OBJECTIVE[]" id="inv_objective_b" value="2" {{ in_array("2",$obj) ? "checked" : "" }}>
									<label class="form-check-label" for="inv_objective_2">
										Price Appreciation
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter invensmentObj" type="checkbox" name="INV_OBJECTIVE[]" id="inv_objective_c" value="3" {{ in_array("3",$obj) ? "checked" : "" }}>
									<label class="form-check-label" for="inv_objective_3">
										Long Term Investment
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter invensmentObj" type="checkbox" name="INV_OBJECTIVE[]" id="inv_objective_d" value="4" {{ in_array("4",$obj) ? "checked" : "" }}>
									<label class="form-check-label" for="inv_objective_4">
										Speculation
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input check-filter invensmentObj" type="checkbox" name="INV_OBJECTIVE[]" id="inv_objective_e" value="5"  {{ in_array("5",$obj) ? "checked" : "" }}>
									<label class="form-check-label" for="inv_objective_5">
										Income
									</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<!-- <label for="sourcefundothers" class="col-sm-2 col-form-label">Other Source Fund</label>
							<div class="col-sm-3">
								<input type="text" name="SOURCEFUNDOTHERS" class="form-control" id="sourcefundothers" value="{{$nasabah['investorFundSource']}}"  >
							</div> -->
							<label for="asset_owner" class="col-sm-2 col-form-label">Asset Owner <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<select name="ASSET_OWNER" id="asset_owner" class="form-control" required="required">
									<option value=""></option>
									<option value="1" {{ $nasabah['assetOwner'] == '1' ? "selected" : "" }}>Myself</option>
									<option value="2" {{ $nasabah['assetOwner'] == '2' ? "selected" : "" }}>Representing Other Party</option>
								</select>
							</div>
						</div>
						<div class="form-group row mt-4">
							<label class="col-sm-6 col-form-label">Page 3 of 5</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-next">Next <i class="fas fa fa-arrow-right"></i></button>
								<button type="button" class="btn btn-sm btn-secondary float-right btn-prev mr-2"><i class="fas fa fa-arrow-left"></i> Prev</button>
							</div>
						</div>
					</div>
					<div id="page4">
						<div class="form-group">
							<p>Client Participant Address</p>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Address <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input type="text" name="ADDRESS1" class="form-control" id="address1" required="required" value="{{$nasabah['investorAddress1'] or ''}}"  >
							</div>
							<div class="col-sm-3">
								<input type="text" name="ADDRESS2" class="form-control" id="address2" value="{{$nasabah['investorAddress2'] or ''}}"  >
							</div>
							<div class="col-sm-3">
								<input type="text" name="ADDRESS3" class="form-control" id="address3" value="{{$nasabah['investorAddress3'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="description" class="col-sm-2 col-form-label">Country <span style="color:red">*</span></label>
							<div class="col-sm-4">
								<select name="COUNTCODE" class="form-control" id="country" required="required">
									<option value=""></option>
									@foreach($Countries as $country)
										<option value="{{$country['countryCode']}}" {{ $nasabah['investorCountry'] == $country['countryCode'] ? "selected" : "" }}>{{$country['countryName']}}</option>
									@endforeach
								</select>
							</div>
							<label for="province" class="col-sm-2 col-form-label">Province <span style="color:red">*</span></label>
							<div class="col-sm-4">
							<select name="investorProvince" class="form-control" id="PROVCODE" required="required">
								<option value=""></option>
								@foreach($Provinces as $Province)
									<option value="{{$Province['provinceCode']}}" {{ $nasabah['investorProvince'] == $Province['provinceCode'] ? "selected" : "" }}>{{$Province['provinceName']}}</option>
								@endforeach
							</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">City <span style="color:red">*</span></label>
							<div class="col-sm-4">
								<select name="investorCity" class="form-control" id="city" required="required">
									<option value=""></option>
									@foreach($Cities as $City)
										<option value="{{$City['cityCode']}}" {{ $nasabah['investorCity'] == $City['cityCode'] ? "selected" : "" }}>{{$City['cityName']}}</option>
									@endforeach
								</select>
							</div>
							<label for="zipcode" class="col-sm-2 col-form-label">Zip Code</label>
							<div class="col-sm-2">
								<input type="text" name="ZIPCODE" class="form-control" id="zipcode" value="{{$nasabah['investorPostalCode'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="phone" class="col-sm-2 col-form-label">Phone <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input type="tel" name="PHONE" class="form-control" id="phone" required="required" value="{{$nasabah['investorHomePhone'] or ''}}"  >
							</div>
							<label for="mobilephone" class="col-sm-2 col-form-label">Mobile Phone</label>
							<div class="col-sm-3">
								<input type="tel" name="MOBILEPHONE" class="form-control" id="mobilephone" value="{{$nasabah['investorMobilePhone'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="fax" class="col-sm-2 col-form-label">Fax</label>
							<div class="col-sm-3">
								<input type="tel" name="FAX" class="form-control" id="fax" value="{{$nasabah['investorFax'] or ''}}"  >
							</div>
							<label for="email" class="col-sm-2 col-form-label">Email</label>
							<div class="col-sm-3">
								<input type="email" name="EMAIL" class="form-control" id="email" value="{{$nasabah['investorEmail'] or ''}}"  >
							</div>
						</div><br>
						<div class="form-group">
							<p>Client Participant Secondary Address</p>
						</div>
						<div class="form-group row">
							<label for="otheraddress" class="col-sm-2 col-form-label">Address</label>
							<div class="col-sm-3">
								<input type="text" name="secAddress1Line1" class="form-control" id="secAddress2Line1" value="{{$nasabah['secAddress1Line1'] or ''}}"  >
							</div>
							<div class="col-sm-3">
								<input type="text" name="secAddress2Line2" class="form-control" id="OTHERADDRESS2" value="{{$nasabah['secAddress2Line2'] or ''}}"  >
							</div>
							<div class="col-sm-3">
								<input type="text" name="secAddress3Line3" class="form-control" id="OTHERADDRESS3" value="{{$nasabah['secAddress3Line3'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
								<label for="description" class="col-sm-2 col-form-label">Country</label>
								<div class="col-sm-4">
									<select name="investorCountry" class="form-control" id="investorCountry">
										<option value=""></option>
										@foreach($Countries as $country)
											<option value="{{$country['countryCode']}}" {{ $nasabah['investorCountry'] == $country['countryCode'] ? "selected" : "" }}>{{$country['countryName']}}</option>
										@endforeach
									</select>
								</div>
							<label for="province" class="col-sm-2 col-form-label">Province</label>
							<div class="col-sm-4">
							<select name="investorProvince" class="form-control" id="investorProvince">
								<option value=""></option>
								@foreach($Provinces as $Province)
									<option value="{{$Province['provinceCode']}}" {{ $nasabah['investorProvince'] == $Province['provinceCode'] ? "selected" : "" }}>{{$Province['provinceName']}}</option>
								@endforeach
							</select>
							</div>
						</div>
						<div class="form-group row">
						<label for="city" class="col-sm-2 col-form-label">City</label>
						<div class="col-sm-4">
							<select name="investorCity" class="form-control" id="investorCity">
								<option value=""></option>
								@foreach($Cities as $City)
									<option value="{{$City['cityCode']}}" {{ $nasabah['investorCity'] == $City['cityCode'] ? "selected" : "" }}>{{$City['cityName']}}</option>
								@endforeach
							</select>
						</div>
							<label for="zipcode" class="col-sm-2 col-form-label">Zip Code</label>
							<div class="col-sm-2">
								<input type="text" name="secPostalCode" class="form-control" id="secPostalCode" value="{{$nasabah['secPostalCode'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="secPhone" class="col-sm-2 col-form-label">Phone</label>
							<div class="col-sm-3">
								<input type="tel" name="secPhone" class="form-control" id="secPhone" value="{{$nasabah['secPhone'] or ''}}"  >
							</div>
							<label for="mobilephone" class="col-sm-2 col-form-label">Mobile Phone</label>
							<div class="col-sm-3">
								<input type="tel" name="secMobilePhone" class="form-control" id="secMobilePhone" value="{{$nasabah['secMobilePhone'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="fax" class="col-sm-2 col-form-label">Fax</label>
							<div class="col-sm-3">
								<input type="tel" name="secEmailAddress" class="form-control" id="secEmailAddress" value="{{$nasabah['secFaxNumber'] or ''}}"  >
							</div>
							<label for="email" class="col-sm-2 col-form-label">Email</label>
							<div class="col-sm-3">
								<input type="email" name="secFaxNumber" class="form-control" id="secFaxNumber" value="{{$nasabah['secEmailAddress'] or ''}}"  >
							</div>
						</div><br>
						<div class="form-group">
							<p>Client Participant Other Address</p>
						</div>
						<div class="form-group row">
							<label for="otheraddress" class="col-sm-2 col-form-label">Address</label>
							<div class="col-sm-3">
								<input type="text" name="investorOtherAddress1" class="form-control" id="investorOtherAddress1" value="{{$nasabah['investorOtherAddress1'] or ''}}"  >
							</div>
							<div class="col-sm-3">
								<input type="text" name="investorOtherAddress2" class="form-control" id="investorOtherAddress2" value="{{$nasabah['investorOtherAddress2'] or ''}}"  >
							</div>
							<div class="col-sm-3">
								<input type="text" name="investorOtherAddress3" class="form-control" id="investorOtherAddress3" value="{{$nasabah['investorOtherAddress3'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
								<label for="description" class="col-sm-2 col-form-label">Country</label>
								<div class="col-sm-4">
									<select name="investorOtherCountry" class="form-control" id="investorOtherCountry">
										<option value=""></option>
										@foreach($Countries as $country)
											<option value="{{$country['countryCode']}}" {{ $nasabah['investorOtherCountry'] == $country['countryCode'] ? "selected" : "" }}>{{$country['countryName']}}</option>
										@endforeach
									</select>
								</div>
							<label for="province" class="col-sm-2 col-form-label">Province</label>
							<div class="col-sm-4">
							<select name="investorOtherProvince" class="form-control" id="investorOtherProvince">
								<option value=""></option>
								@foreach($Provinces as $Province)
									<option value="{{$Province['provinceCode']}}" {{ $nasabah['investorOtherProvince'] == $Province['provinceCode'] ? "selected" : "" }}>{{$Province['provinceName']}}</option>
								@endforeach
							</select>
							</div>
						</div>
						<div class="form-group row">
						<label for="city" class="col-sm-2 col-form-label">City</label>
						<div class="col-sm-4">
							<select name="investorOtherCity" class="form-control" id="investorOtherCity">
								<option value=""></option>
								@foreach($Cities as $City)
									<option value="{{$City['cityCode']}}" {{ $nasabah['investorOtherCity'] == $City['cityCode'] ? "selected" : "" }}>{{$City['cityName']}}</option>
								@endforeach
							</select>
						</div>
							<label for="zipcode" class="col-sm-2 col-form-label">Zip Code</label>
							<div class="col-sm-2">
								<input type="text" name="investorOtherPostalCode" class="form-control" id="investorOtherPostalCode" value="{{$nasabah['investorOtherPostalCode'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="investorOtherHomePhone" class="col-sm-2 col-form-label">Phone</label>
							<div class="col-sm-3">
								<input type="tel" name="investorOtherHomePhone" class="form-control" id="investorOtherHomePhone" value="{{$nasabah['investorOtherHomePhone'] or ''}}"  >
							</div>
							<label for="mobilephone" class="col-sm-2 col-form-label">Mobile Phone</label>
							<div class="col-sm-3">
								<input type="tel" name="investorOtherMobilePhone" class="form-control" id="investorOtherMobilePhone" value="{{$nasabah['investorOtherMobilePhone'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="fax" class="col-sm-2 col-form-label">Fax</label>
							<div class="col-sm-3">
								<input type="tel" name="investorOtherFax" class="form-control" id="investorOtherFax" value="{{$nasabah['investorOtherFax'] or ''}}"  >
							</div>
							<label for="email" class="col-sm-2 col-form-label">Email</label>
							<div class="col-sm-3">
								<input type="email" name="investorOtherEmail" class="form-control" id="investorOtherEmail" value="{{$nasabah['investorOtherEmail'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row mt-4">
							<label class="col-sm-6 col-form-label">Page 4 of 5</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-next">Next <i class="fas fa fa-arrow-right"></i></button>
								<button type="button" class="btn btn-sm btn-secondary float-right btn-prev mr-2"><i class="fas fa fa-arrow-left"></i> Prev</button>
							</div>
						</div>
					</div>
					<div id="page5">
						<div class="form-group">
							<p>Investor Account Bank (1)</p>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Investor Bank Name </label>
							<div class="col-sm-4">
								<input type="text" name="accBankName1" class="form-control" id="accBankName1" placeholder="Bank Name" value="{{$nasabah['investorBankAccountName1'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">Bank Account Number</label>
							<div class="col-sm-4">
								<input type="text" name="CITY" class="form-control" id="accBankNumber1" placeholder="Account Number" value="{{$nasabah['investorBankAccountNumber1'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Investor BI Code</label>
							<div class="col-sm-4">
								<input type="text" name="PROVCODE" class="form-control" id="investorBiCode1" placeholder="BI Code" value="{{$nasabah['investorBankAccountBICCode1'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">Bank Acc Holder Name</label>
							<div class="col-sm-4">
								<input type="text" name="PROVCODE" class="form-control" id="bankAccHolderName1" placeholder="Holder Name" value="{{$nasabah['investorBankAccountHolderName1'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Account Currency</label>
							<div class="col-sm-4">
								<input type="text" name="PROVCODE" class="form-control" id="investorAccCurrency1" placeholder="account currency" value="{{$nasabah['investorBankAccountCurrency1'] or ''}}"  >
							</div>
						</div><br>
						<div class="form-group">
							<p>Investor Account Bank (2)</p>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Investor Bank Name </label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountName2" class="form-control" id="investorBankAccountName2" placeholder="Bank Name" value="{{$nasabah['investorBankAccountName2'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">Bank Account Number</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountNumber2" class="form-control" id="investorBankAccountNumber2" placeholder="Account Number" value="{{$nasabah['investorBankAccountNumber2'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Investor BI Code</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountBICCode2" class="form-control" id="investorBankAccountBICCode2" placeholder="BI Code" value="{{$nasabah['investorBankAccountBICCode2'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">Bank Acc Holder Name</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountHolderName2" class="form-control" id="investorBankAccountHolderName2" placeholder="Holder Name" value="{{$nasabah['investorBankAccountHolderName2'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Account Currency</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountCurrency2" class="form-control" id="investorBankAccountCurrency2" placeholder="account currency" value="{{$nasabah['investorBankAccountCurrency2'] or ''}}"  >
							</div>
						</div><br>
						<div class="form-group">
							<p>Investor Account Bank (3)</p>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Investor Bank Name </label>
							<div class="col-sm-4">
								<input type="text" name="accBankName3" class="form-control" id="accBankName3" placeholder="Bank Name" value="{{$nasabah['investorBankAccountName3'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="city" class="col-sm-2 col-form-label">Bank Account Number</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountName3" class="form-control" id="investorBankAccountName3" placeholder="Account Number" value="{{$nasabah['investorBankAccountNumber3'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Investor BI Code</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountBICCode3" class="form-control" id="investorBankAccountBICCode3" placeholder="BI Code" value="{{$nasabah['investorBankAccountBICCode3'] or ''}}"  >
							</div>
						</div>
						<div class="form-group row">
							<label for="province" class="col-sm-2 col-form-label">Bank Acc Holder Name</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountHolderName3" class="form-control" id="investorBankAccountHolderName3" placeholder="Holder Name" value="{{$nasabah['investorBankAccountHolderName3'] or ''}}"  >
							</div>
							<label for="province" class="col-sm-2 col-form-label">Account Currency</label>
							<div class="col-sm-4">
								<input type="text" name="investorBankAccountCurrency3" class="form-control" id="investorBankAccountCurrency3" placeholder="account currency" value="{{$nasabah['investorBankAccountCurrency3'] or ''}}"  >
							</div>
						</div><br>
						<div class="form-group row mt-4">
							<label class="col-sm-6 col-form-label">Page 5 of 5</label>
							<div class="col-sm-6">
								<button type="button" class="btn btn-sm btn-success float-right btn-submit"><i class="fas fa fa-check"></i> Submit</button>
								<button type="button" class="btn btn-sm btn-secondary float-right btn-prev mr-2"><i class="fas fa fa-arrow-left"></i> Prev</button>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('script')
	<script>
		$( document ).ready(function(){
			$('#nationality').val('ID');
			// $('.datepicker').val('');
			$('#page2').css('display','none');
			$('#page3').css('display','none');
			$('#page4').css('display','none');
			$('#page5').css('display','none');

			// $('input,select,radio').each(function(){
			// 	$(this).removeAttr('required');
			// });

			$('.datepicker').each(function(){
				console.log($(this));
			});
		})

		$('#page1').find('.btn-next').on('click',function(){
			var status = 1;
			$('#page1').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length==0){
					status = 0;
					$(this).css('border','2px solid red');
					// $(this).append('<span style="color:red">Field is required</span>')
				}
			})
			if(status == 1){
				$('#page1').fadeOut('fast', function(){
					$('#page2').fadeIn('slow');
				});
			}
		})

		$('#page2').find('.btn-prev').on('click',function(){
			$('#page2').fadeOut('fast', function(){
				$('#page1').fadeIn('slow');
			});
		})

		$('#page2').find('.btn-next').on('click',function(){
			status = 1;
			$('#page2').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})
			if(status == 1){
				$('#page2').fadeOut('fast', function(){
					$('#page3').fadeIn('slow');
				});
			}
		})

		$('#page3').find('.btn-prev').on('click',function(){
			$('#page3').fadeOut('fast', function(){
				$('#page2').fadeIn('slow');
			});
		})

		$('#page3').find('.btn-next').on('click',function(){
			status = 1;
			$('#page3').find('input,select,radio').filter('[required]:visible').each(function(){

				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})

			if($('#inv_client_type').val()=="DIRECT"){
				checkedSf = $(".sourceFund:checkbox:checked").length;

				if(!checkedSf) {
					$.confirm({
							title: 'Error',
							content: 'Source Funds cant be empty',
							icon: 'fas fa-times'
					});
					return false;
				}

				checkedInv = $(".invensmentObj:checkbox:checked").length;

				if(!checkedInv) {
					$.confirm({
							title: 'Error',
							content: 'Investment Objective cant be empty',
							icon: 'fas  fa-times'
					});
					return false;
				}
			}

			if(status == 1){
				$('#page3').fadeOut('fast', function(){
					$('#page4').fadeIn('slow');
				});
			}
		})

		$('#page4').find('.btn-prev').on('click',function(){
			$('#page4').fadeOut('fast', function(){
				$('#page3').fadeIn('slow');
			});
		})

		$('#page4').find('.btn-next').on('click',function(){
			status = 1;
			$('#page4').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})
			if(status == 1){
				$('#page4').fadeOut('fast', function(){
					$('#page5').fadeIn('slow');
				});
			}
		})

		$('#page5').find('.btn-prev').on('click',function(){
			$('#page5').fadeOut('fast', function(){
				$('#page4').fadeIn('slow');
			});
		})

		$('#page5').find('.btn-submit').on('click',function(){
			status = 1;
			$('#page5').find('input,select,radio').filter('[required]:visible').each(function(){
				if($(this).val().length === 0){
					status = 0;
					$(this).css('border','2px solid red');
				}
			})
			if(status == 1){

				$.confirm({
			      title: 'Confirmation',
			      content: 'Save this data ?',
			      icon: 'fas fa-exclamation-triangle',
			      buttons: {
			          confirm: function () {
			              $('#insert-form-client').submit();
			          },
			          cancel: function () {
			          }
			      }
			  });
			}
		})

		$('#inv_client_type').on('blur',function(){
			if($(this).val() == 'D'){
				$('#birth_place').attr('required','required');
				$('[for="birth_place"]').text('Birth Place');
				$('[for="birth_place"]').append('<span style="color:red">&nbsp;*</span>');

				$('#phone').attr('required','required');
				$('[for="phone"]').text('Phone');
				$('[for="phone"]').append('<span style="color:red">&nbsp;*</span>');

				$('#sex').attr('required','required');
				$('[for="sex"]').text('Gender / Sex');
				$('[for="sex"]').append('<span style="color:red">&nbsp;*</span>');

				$('#marital_status').attr('required','required');
				$('[for="marital_status"]').text('Marital Status');
				$('[for="marital_status"]').append('<span style="color:red">&nbsp;*</span>');

				$('#education_background').attr('required','required');
				$('[for="education_background"]').text('Education Background');
				$('[for="education_background"]').append('<span style="color:red">&nbsp;*</span>');

				$('#occupation').attr('required','required');
				$('[for="occupation"]').text('Occupation');
				$('[for="occupation"]').append('<span style="color:red">&nbsp;*</span>');

				$('#annual_income').attr('required','required');
				$('[for="annual_income"]').text('Annual Income');
				$('[for="annual_income"]').append('<span style="color:red">&nbsp;*</span>');

				$('#sourcefund').attr('required','required');
				$('[for="sourcefund"]').text('Source Fund');
				$('[for="sourcefund"]').append('<span style="color:red">&nbsp;*</span>');

				$('[for="inv_objective"]').text('Investment Objective');
				$('[for="inv_objective"]').append('<span style="color:red">&nbsp;*</span>');

				$('#mother_name').attr('required','required');
				$('[for="mother_name"]').text('Mother Name');
				$('[for="mother_name"]').append('<span style="color:red">&nbsp;*</span>');

				$('#direct_sid').removeAttr('required');
				$('[for="direct_sid"]').text('Direct SID');
				// $('[for="direct_sid"]').append('<span style="color:red">&nbsp;*</span>');
			}else{
				$('#birth_place').removeAttr('required');
				$('[for="birth_place"]').text('Birth Place');
				$('[for="birth_place"]').append('<span style="color:red">&nbsp;*</span>');

				$('#phone').removeAttr('required');
				$('[for="phone"]').text('Phone');

				$('#sex').removeAttr('required');
				$('[for="sex"]').text('Gender / Sex');

				$('#marital_status').removeAttr('required');
				$('[for="marital_status"]').text('Marital Status');

				$('#education_background').removeAttr('required');
				$('[for="education_background"]').text('Education Background');

				$('#occupation').removeAttr('required');
				$('[for="occupation"]').text('Occupation');

				$('#annual_income').removeAttr('required');
				$('[for="annual_income"]').text('Annual Income');

				$('#sourcefund').removeAttr('required');
				$('[for="sourcefund"]').text('Source Fund');

				$('[for="inv_objective"]').text('Investment Objective');

				$('#mother_name').removeAttr('required');
				$('[for="mother_name"]').text('Mother Name');

				$('#direct_sid').attr('required','required');
				$('[for="direct_sid"]').text('Direct SID');
				$('[for="direct_sid"]').append('<span style="color:red">&nbsp;*</span>');
			}
		})

		$('#marital_status').on('blur',function(){
			if($(this).val() == 2){
				$('#spouse_name').attr('required','required');
				$('[for="spouse_name"]').text('Spouse Name');
				$('[for="spouse_name"]').append('<span style="color:red">&nbsp;*</span>');
			}else{
				$('#spouse_name').removeAttr('required');
				$('[for="spouse_name"]').text('Spouse Name');
			}
		})

		$('#sourcefund_1').on('click',function(){
			if($(this).is(':checked')){
				$('#sourcefundothers').attr('required','required');
				$('[for="sourcefundothers"]').text('Other Source Fund');
				$('[for="sourcefundothers"]').append('<span style="color:red">&nbsp;*</span>');
			}else{
				$('#sourcefundothers').removeAttr('required');
				$('[for="sourcefundothers"]').text('Other Source Fund');
			}
		})

		$('#occupation').on('blur',function(){
			if($(this).val() == 1){

				$('#occupation_other').removeAttr('required');
				$('#occupation_other').removeAttr('readonly');
				$('[for="occupation_other"]').text('Ocupation Other');
				$('[for="occupation_other"]').append('<span style="color:red">&nbsp;*</span>');
			}else{
				$('#occupation_other').attr('readonly','readonly');
				$('[for="occupation_other"]').text('Ocupation Other');
			}

			if($(this).val() == 5){
				$('#nature_of_business').attr('required','required');
				$('[for="nature_of_business"]').text('Nature of Business');
				$('[for="nature_of_business"]').append('<span style="color:red">&nbsp;*</span>');
			}else{
				$('#nature_of_business').removeAttr('required');
				$('[for="nature_of_business"]').text('Nature of Business');
			}
		})

		$('#nationality').on('blur',function(){
			if($(this).val() == 'ID'){
				$('[for="ktp"]').text('KTP *');
				$('#ktp').attr('required','required');
				$('[for="ktp_expdate"]').text('KTP Expired Date');
				$('[for="ktp_expdate"]').append('<span style="color:red">&nbsp;*</span>');
				$('#ktp_expdate').attr('required','required');

				$('[for="passport"]').text('Passport');
				$('#passport').removeAttr('required');
				$('[for="passport_expdate"]').text('Passport Expired Date');
				$('#passport_expdate').removeAttr('required');
			}else{
				$('#passport').attr('required','required');
				$('[for="passport"]').text('Passport');
				$('[for="passport"]').append('<span style="color:red">&nbsp;*</span>');

				$('[for="passport_expdate"]').text('Passport Expired Date');
				$('[for="passport_expdate"]').append('<span style="color:red">&nbsp;*</span>');
				$('#passport_expdate').attr('required','required');

				$('[for="ktp"]').text('KTP');
				$('#ktp').removeAttr('required');
				$('[for="ktp_expdate"]').text('KTP Expired Date');
				$('#ktp_expdate').removeAttr('required');
			}
		});
	</script>
@endsection
