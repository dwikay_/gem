@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Provinsi</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/provinsi/update/'.$provinsi['id'])}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Kode Provinsi</label>
                      <input type="text" class="form-control" id="kodeProvinsi" name="kodeProvinsi" value="{{$provinsi['provinceCode']}}" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Nama Provinsi</label>
                      <input type="text" class="form-control" id="namaProvinsi" name="namaProvinsi" value="{{$provinsi['provinceName']}}" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-form-label">Negara</label>
                      <select class="form-control form-control-sm" name="namaNegara" id="namaNegara">
                        @foreach($negara as $value)
                        <option {{$provinsi['country']['id'] == $value['id'] ? 'selected' : ''}} value="{{$value['id']}}">{{$value['countryName']}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Kode Pegadaian</label>
                      <input type="text" class="form-control" id="kodePegadaian" value="{{$provinsi['pegadaianCode']}}" name="kodePegadaian" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-12 mt-10">
                      @include('inc.button.cancel')
                      @include('inc.button.submit')
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
@endsection
