@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Provinsi</h4>
              <p class="card-description">
                Input Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/provinsi/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1" class="col-form-label">Kode Provinsi</label>
                        <input type="text" class="form-control" id="kodeProvinsi" name="kodeProvinsi" placeholder="">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1" class="col-form-label">Nama Provinsi</label>
                        <input type="text" class="form-control" id="namaProvinsi" name="namaProvinsi" placeholder="">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="col-form-label">Negara</label>
                        <select class="form-control form-control-sm" name="country" id="country">
                          <option value="A">All</option>
                          @foreach($negara as $value)
                          <option value="{{$value['id']}}">{{$value['countryName']}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1" class="col-form-label">Kode Pegadaian</label>
                        <input type="text" class="form-control" id="kodePegadaian" name="kodePegadaian" placeholder="">
                      </div>
                    </div>
                    <div class="col-md-12 mt-10">
                        @include('inc.button.cancel')
                        @include('inc.button.submit')
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
@endsection
