@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
                <h4 class="card-title float-left">Provinsi</h4>
                @include('inc.button.add', ['urls' => url('/dataStatis/provinsi/create')])
            </div>
            <div class="card-body">
              <!-- <div class="form-group row">
                <label class="col-form-label col-md-2">Filter By</label>
                <select class="form-control form-control-sm col-md-2" name="type_efek" id="type_efek">
                  <option value="A">All</option>
                  @foreach($param as $value)
                  <option value="{{$value['id']}}">{{$value['countryName']}}</option>
                  @endforeach
                </select>
              </div> -->
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" width="100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Kode Provinsi</th>
                        <th>Provinsi</th>
                        <th>Negara</th>
                        <th>Kode Pegadaian</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

function deletes(id){
  $.confirm({
      confirmButton: 'Hapus',
      cancelButton: 'Kembali',
      title: 'Konfirmasi',
      content: 'Hapus Data ini ?',
      icon: 'fas fa-exclamation-triangle',
      buttons: {
          confirm: function () {
              location.href="{{url('dataStatis/provinsi/delete')}}" + "/" + id;
          },
          cancel: function () {
          }
      }
  });
}

$(document).ready(function() {

var table = $("#lookup").dataTable({
  processing: true,
  serverSide: true,
  ajax:{
    url: "{{ url('dataStatis/provinsi/getIndex') }}",
    dataType: "json",
    type: "GET",
    error: function(){  // error handling
      $(".lookup-error").html("");
      $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="4">No data found in the server</th></tr></tbody>');
      $("#lookup_processing").css("display","none");

    }
  },
  columns: [
            {data: 'id'},
            {data: 'provinceCode'},
            {data: 'provinceName'},
            {data: 'id'},
            {data: 'pegadaianCode'},
            {data: 'id'}
        ],
        columnDefs: [
          {
            "targets": [0],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).text(row+1);
            },
            orderable: true
          },
          {
            "targets": [3],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
                $(td).text(rowData.country.countryName);
            },
            orderable: false
          },
          {
            "targets": [5],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
                $(td)
                    .append($('@include('inc.button.group_btn_table')'));
            },
            orderable: false
          },
        ],
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    },
      });

    $('.table').on('click','.btn-edit-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        location.href="{{url('dataStatis/provinsi/edit/')}}" + "/" + data[index].id;
    });

    $('.table').on('click','.btn-remove-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        deletes(data[index].id);
    });
});
</script>
@endsection
