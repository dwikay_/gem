@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Pajak</h4>
              <p class="card-description">
                Edit Form
              </p>
              <form class="forms-sample" action="{{url('dataStatis/tax/update/'.$tax['id'])}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Kode Pajak</label>
                      <input type="text" class="form-control" id="taxCode" value="{{$tax['taxCode']}}" name="taxCode" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Deskripsi Singkat</label>
                      <input type="text" class="form-control" id="shortDesc" value="{{$tax['shortDesc']}}" name="shortDesc" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group mt-10">
                      <label for="exampleInputEmail1" class="col-form-label">Keterangan</label>
                      <input type="text" class="form-control" id="longDesc" value="{{$tax['longDesc']}}" name="longDesc" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-12 mt-10">
                      @include('inc.button.cancel')
                      @include('inc.button.submit')
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
@endsection
