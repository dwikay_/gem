@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Batas Pinjaman</h4>
              <p class="card-description">
                Upload Form
              </p>
              <form id="fpro" class="forms-sample" action="{{url('dataStatis/batasPinjaman/store')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">File</label>
                      <input type="file" class="form-control form-control-sm clasesform" id="doc" name="doc" placeholder="" accept=".txt">
                    </div>
                    <div id="fbtn" class="form-group mt-10">
                      <a href="{{url('dataStatis/batasPinjaman')}}" class="btn btn-sm btn-secondary" style="color:white;"><span class="fa fa-arrow-left fa-xs"></span>&nbsp;&nbsp;Kembali</a>
                      @include('inc.button.upload_submit')
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

$('.btn-simpan').on('click', function(){

  if($('#doc').val()==""){
    $.alert({
      title: 'Error!',
      content: 'File tidak boleh kosong!',
    });
  }else{
    $.confirm({
        title: 'Konfirmasi',
        content: 'Upload file ini ?',
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            confirm: function () {
              $('#fpro').submit();
            },
            cancel: function () {
            }
        }
    });
  }
});
</script>
@endsection
