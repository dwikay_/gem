<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(url('login'));
});

Route::get('login', 'Auth\LoginController@getLogin')->name('login');
Route::post('login', 'Auth\LoginController@postLogin')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('verification', 'Get\VerificationController@getVerification')->name('verification');
Route::post('verification', 'Get\VerificationController@postVerification')->name('verification');

Route::group(['middleware' => ['web']], function () {

   Route::group(['middleware' => ['auth'] ], function () {

     Route::get('/dashboard', 'HomeController@index')->name('home');

     Route::get('/lookup/customer', 'LookupController@customer')->name('lookupCustomer');
     Route::get('/lookup/contract', 'LookupController@contract')->name('lookupContact');
     Route::get('/lookup/stock', 'LookupController@stock')->name('lookupStock');

     Route::get('users/change-password/{user}/{id}', 'Users\UserManageController@viewPassword')->name('lookupStock');
     Route::post('users/change-password/update/{id}', 'Users\UserManageController@updatePassword')->name('lookupStock');

     Route::group(['prefix' => 'dataStatis'], function () {

      Route::group(['prefix' => 'globalParam'], function () {
          Route::group(['middleware' => 'roler:dataStatis/globalParam'], function () {
            Route::get('', 'Parameter\GlobalController@index')->name('globalParam');
            Route::get('getGlobal', 'Parameter\GlobalController@getIndex')->name('globalParam');
          });
          Route::group(['middleware' => 'rolec:dataStatis/globalParam'], function () {
            Route::get('create', 'Parameter\GlobalController@create')->name('globalParam');
            Route::post('store', 'Parameter\GlobalController@store')->name('globalParam');
          });
          Route::group(['middleware' => 'roleu:dataStatis/globalParam'], function () {
            Route::get('edit/{id}', 'Parameter\GlobalController@edit')->name('globalParam');
            Route::post('update/{id}', 'Parameter\GlobalController@update')->name('globalParam');
          });
          Route::group(['middleware' => 'roled:dataStatis/globalParam'], function () {
            Route::get('delete/{id}', 'Parameter\GlobalController@delete')->name('globalParam');
          });
        });

        Route::group(['prefix' => 'batasPinjaman'], function () {
            Route::group(['middleware' => 'roler:dataStatis/batasPinjaman'], function () {
              Route::get('', 'Parameter\BatasPinjamanController@index')->name('batasPinjaman');
              Route::post('getIndex', 'Parameter\BatasPinjamanController@getIndex')->name('batasPinjaman');
              Route::get('download', 'Parameter\BatasPinjamanController@download')->name('batasPinjaman');
            });
            Route::group(['middleware' => 'rolec:dataStatis/batasPinjaman'], function () {
              Route::get('create', 'Parameter\BatasPinjamanController@create')->name('batasPinjaman');
              Route::post('store', 'Parameter\BatasPinjamanController@import')->name('batasPinjaman');
            });
          });

          Route::group(['prefix' => 'bmpkEmiten'], function () {
              Route::group(['middleware' => 'roler:dataStatis/bmpkEmiten'], function () {
                Route::get('', 'Parameter\BMPKEmitenController@index')->name('bmpkEmiten');
                Route::post('getIndex', 'Parameter\BMPKEmitenController@getIndex')->name('bmpkEmiten');
                Route::get('download', 'Parameter\BMPKEmitenController@download')->name('bmpkEmiten');
              });
              Route::group(['middleware' => 'rolec:dataStatis/BMPKEmiten'], function () {
                Route::get('create', 'Parameter\BMPKEmitenController@create')->name('bmpkEmiten');
                Route::post('store', 'Parameter\BMPKEmitenController@import')->name('bmpkEmiten');
              });
            });

        Route::group(['prefix' => 'holiday'], function () {
            Route::group(['middleware' => 'roler:dataStatis/holiday'], function () {
            Route::get('', 'Holiday\HolidayController@index')->name('holiday');
            Route::get('getHoliday/{tahun}', 'Holiday\HolidayController@getIndex')->name('holiday');
            });
            Route::group(['middleware' => 'rolec:dataStatis/holiday'], function () {
            Route::get('create', 'Holiday\HolidayController@create')->name('holiday');
            Route::post('store', 'Holiday\HolidayController@store')->name('holiday');
            });
            Route::group(['middleware' => 'roleu:dataStatis/holiday'], function () {
            Route::get('edit/{id}', 'Holiday\HolidayController@edit')->name('holiday');
            Route::post('update/{id}', 'Holiday\HolidayController@update')->name('holiday');
            });
            Route::group(['middleware' => 'roled:dataStatis/holiday'], function () {
            Route::get('delete/{id}', 'Holiday\HolidayController@delete')->name('holiday');
            });
        });

        Route::group(['prefix' => 'tax'], function () {
            Route::group(['middleware' => 'roler:dataStatis/tax'], function () {
            Route::get('', 'Tax\TaxController@index')->name('tax');
            Route::get('getIndex', 'Tax\TaxController@getIndex')->name('tax');
            });
            Route::group(['middleware' => 'rolec:dataStatis/tax'], function () {
            Route::get('create', 'Tax\TaxController@create')->name('tax');
            Route::post('store', 'Tax\TaxController@store')->name('tax');
            });
            Route::group(['middleware' => 'roleu:dataStatis/tax'], function () {
            Route::get('edit/{id}', 'Tax\TaxController@edit')->name('tax');
            Route::post('update/{id}', 'Tax\TaxController@update')->name('tax');
            });
            Route::group(['middleware' => 'roled:dataStatis/tax'], function () {
            Route::get('delete/{id}', 'Tax\TaxController@delete')->name('tax');
            });
        });


        Route::group(['prefix' => 'provinsi'], function () {
            Route::group(['middleware' => 'roler:dataStatis/provinsi'], function () {
            Route::get('', 'Provinsi\ProvinsiController@index')->name('provinsi');
            Route::get('getIndex', 'Provinsi\ProvinsiController@getIndex')->name('provinsi');
            });
            Route::group(['middleware' => 'rolec:dataStatis/provinsi'], function () {
            Route::get('create', 'Provinsi\ProvinsiController@create')->name('provinsi');
            Route::post('store', 'Provinsi\ProvinsiController@store')->name('provinsi');
            });
            Route::group(['middleware' => 'roleu:dataStatis/provinsi'], function () {
            Route::get('edit/{id}', 'Provinsi\ProvinsiController@edit')->name('provinsi');
            Route::post('update/{id}', 'Provinsi\ProvinsiController@update')->name('provinsi');
            });
            Route::group(['middleware' => 'roled:dataStatis/provinsi'], function () {
            Route::get('delete/{id}', 'Provinsi\ProvinsiController@delete')->name('provinsi');
            });
        });

        Route::group(['prefix' => 'broker'], function () {
            Route::group(['middleware' => 'roler:dataStatis/broker'], function () {
            Route::get('', 'Broker\BrokerController@index')->name('broker');
            Route::get('getIndex', 'Broker\BrokerController@getIndex')->name('broker');
            });
            Route::group(['middleware' => 'rolec:dataStatis/broker'], function () {
            Route::get('create', 'Broker\BrokerController@create')->name('broker');
            Route::post('store', 'Broker\BrokerController@store')->name('broker');
            });
            Route::group(['middleware' => 'roleu:dataStatis/broker'], function () {
            Route::get('edit/{id}', 'Broker\BrokerController@edit')->name('broker');
            Route::post('update/{id}', 'Broker\BrokerController@update')->name('broker');
            });
            Route::group(['middleware' => 'roled:dataStatis/broker'], function () {
            Route::get('delete/{id}', 'Broker\BrokerController@delete')->name('broker');
            });
        });

        Route::group(['prefix' => 'custodi'], function () {
            Route::group(['middleware' => 'roler:dataStatis/custodi'], function () {
            Route::get('', 'Bank\CustodyController@index')->name('custodi');
            Route::get('getIndex', 'Bank\CustodyController@getIndex')->name('custodi');
            });
            Route::group(['middleware' => 'rolec:dataStatis/custodi'], function () {
            Route::get('create', 'Bank\CustodyController@create')->name('custodi');
            Route::post('store', 'Bank\CustodyController@store')->name('custodi');
            });
            Route::group(['middleware' => 'roleu:dataStatis/custodi'], function () {
            Route::get('edit/{id}', 'Bank\CustodyController@edit')->name('custodi');
            Route::post('update/{id}', 'Bank\CustodyController@update')->name('custodi');
            });
            Route::group(['middleware' => 'roled:dataStatis/custodi'], function () {
            Route::get('delete/{id}', 'Bank\CustodyController@delete')->name('custodi');
            });
        });


        Route::group(['prefix' => 'nasabah'], function () {
            Route::group(['middleware' => 'roler:dataStatis/nasabah'], function () {
              Route::get('', 'Nasabah\NasabahController@index')->name('nasabah');
              Route::get('getNasabah', 'Nasabah\NasabahController@getIndex')->name('nasabah');
            });
            Route::group(['middleware' => 'roleu:dataStatis/nasabah'], function () {
              Route::get('edit/{tipeNasabah}/{kodeNasabah}/{id}', 'Nasabah\NasabahController@edit')->name('nasabah');
              Route::post('update/{id}/{tipe}/{idNasabah}', 'Nasabah\NasabahController@update')->name('nasabah');
              Route::post('updateSubRek/{kodeNasabah}', 'Nasabah\NasabahController@updateSubRek')->name('nasabah');
              Route::get('reqSubrek/{kodeNasabah}/{tipeNasabah}', 'Nasabah\NasabahController@reqSubrek')->name('nasabah');
              Route::post('download', 'Nasabah\NasabahController@kseidoc')->name('nasabah');
              Route::get('reqSubrek-view/{kodeNasabah}/{tipeNasabah}/{id}', 'Nasabah\NasabahController@reqSubrekView')->name('nasabah');
              Route::get('create/kseidoc', 'Nasabah\NasabahController@kseiDoc')->name('nasabah');
              //
              Route::get('editSubRek/{kodeNasabah}/{tipeNasabah}', 'Nasabah\NasabahController@reqSubrek2')->name('nasabah');
            });
        });

        Route::group(['prefix' => 'efek-resources'], function () {
            Route::group(['middleware' => 'roler:dataStatis/efek-resources'], function () {
            Route::get('', 'Efek\EfekController@index')->name('efek-resources');
            Route::post('getEfek', 'Efek\EfekController@getIndex')->name('efek-resources');
            });
            Route::group(['middleware' => 'roleu:dataStatis/efek-resources'], function () {
            Route::get('edit/{id}', 'Efek\EfekController@edit')->name('efek-resources');
            Route::post('update/{id}', 'Efek\EfekController@update')->name('efek-resources');
            Route::post('updateObligasi/{id}', 'Efek\EfekController@updateObligasi')->name('efek-resources');
            });
        });

        Route::group(['prefix' => 'country'], function () {
            Route::group(['middleware' => 'roler:dataStatis/country'], function () {
            Route::get('', 'Country\CountryController@index')->name('country');
            Route::get('getCountry', 'Country\CountryController@getIndex')->name('country');
            });
            Route::group(['middleware' => 'rolec:dataStatis/country'], function () {
            Route::get('create', 'Country\CountryController@create')->name('country');
            Route::post('store', 'Country\CountryController@store')->name('country');
            });
            Route::group(['middleware' => 'roleu:dataStatis/country'], function () {
            Route::get('edit/{id}', 'Country\CountryController@edit')->name('country');
            Route::post('update/{id}', 'Country\CountryController@update')->name('country');
            });
            Route::group(['middleware' => 'roled:dataStatis/country'], function () {
            Route::get('delete/{id}', 'Country\CountryController@delete')->name('country');
            });
        });

        Route::group(['prefix' => 'city'], function () {
            Route::group(['middleware' => 'roler:dataStatis/city'], function () {
            Route::get('', 'City\CityController@index')->name('city');
            Route::get('getCity', 'City\CityController@getIndex')->name('city');
            });
            Route::group(['middleware' => 'rolec:dataStatis/city'], function () {
            Route::get('create', 'City\CityController@create')->name('city');
            Route::post('store', 'City\CityController@store')->name('city');
            });
            Route::group(['middleware' => 'roleu:dataStatis/city'], function () {
            Route::get('edit/{id}', 'City\CityController@edit')->name('city');
            Route::post('update/{id}', 'City\CityController@update')->name('city');
            });
            Route::group(['middleware' => 'roled:dataStatis/city'], function () {
            Route::get('delete/{id}', 'City\CityController@delete')->name('city');
            });
        });

      });

     //Transaksi

     Route::group(['prefix' => 'transaksi'], function () {

       Route::group(['prefix' => 'loan-request'], function () {
         Route::group(['middleware' => 'roler:transaksi/loan-request'], function () {
           Route::get('', 'LoanRequest\LoanRequestController@index')->name('loan-request');
           Route::get('getDetail/{pengajuan}', 'LoanRequest\LoanRequestController@getDetail')->name('loan-request');
           Route::get('getIndex', 'LoanRequest\LoanRequestController@getIndex')->name('loan-request');
         });
         Route::group(['middleware' => 'roleu:transaksi/loan-request'], function () {
           Route::get('create', 'LoanRequest\LoanRequestController@create')->name('loan-request');
           Route::post('store/{kodePengajuan}', 'LoanRequest\LoanRequestController@store')->name('loan-request');
           Route::get('edit/{id}', 'LoanRequest\LoanRequestController@edit')->name('loan-request');
           Route::get('terimaEfek/{pengajuan}', 'LoanRequest\LoanRequestController@terimaEfek')->name('loan-request');
           Route::post('edit/subrek/{nasabah}', 'LoanRequest\LoanRequestController@editsubrek')->name('loan-request');
           Route::post('updateEfek/{noPengajuan}', 'LoanRequest\LoanRequestController@updatePengajuan')->name('loan-request');
           Route::get('instruksiKsei/{kodeNasabah}', 'LoanRequest\LoanRequestController@kseiDoc')->name('loan-request');
           Route::post('doc-send-intruksi-all', 'LoanRequest\LoanRequestController@kseiDoc')->name('loan-request');
         });
       });

       Route::group(['prefix' => 'stock-deposit'], function () {

         Route::group(['middleware' => 'rolec:transaksi/stock-deposit'], function () {
           Route::get('create', 'Topup\TopupController@create')->name('stock-deposit');
           Route::post('store', 'Topup\TopupController@topupCollateral')->name('stock-deposit');
         });

         Route::group(['middleware' => 'roler:transaksi/stock-deposit'], function () {
           Route::get('', 'Topup\TopupController@index')->name('stock-deposit');
           Route::post('getIndex', 'Topup\TopupController@getIndex')->name('stock-deposit');

           Route::get('simulation', 'Topup\TopupController@indexSimulation')->name('stock-deposit');
           Route::get('simulation/getEfek/{kodeEfek}', 'Topup\TopupController@getEfek')->name('stock-deposit');
           
         });

         Route::group(['middleware' => 'roleu:transaksi/stock-deposit'], function () {
           Route::get('settle/{action}/{noDeposit}/{qty}', 'Topup\TopupController@settle')->name('stock-deposit');
           Route::get('sendInstruksi/{noDeposit}/{tipeEfek}', 'Topup\TopupController@pengajuan')->name('stock-deposit');
           Route::post('send-intruksi-all', 'Topup\TopupController@sendInstruksi')->name('stock-deposit');
           Route::post('doc-send-intruksi-all', 'Topup\TopupController@kseiDoc')->name('stock-deposit');
           Route::post('doc-send-intruksi-all-csv', 'Topup\TopupController@kseiDocCsv')->name('stock-deposit');

         });
       });

       Route::group(['prefix' => 'stock-withdraw'], function () {

         Route::group(['middleware' => 'rolec:transaksi/stock-withdraw'], function () {
           Route::get('create', 'Withdraw\WithdrawController@create')->name('stock-withdraw');
           Route::post('store', 'Withdraw\WithdrawController@withdrawCollateral')->name('stock-withdraw');
         });

         Route::group(['middleware' => 'roler:transaksi/stock-withdraw'], function () {
           Route::get('', 'Withdraw\WithdrawController@index')->name('stock-withdraw');
           Route::post('getIndex', 'Withdraw\WithdrawController@getIndex')->name('stock-withdraw');
         });

         Route::group(['middleware' => 'roleu:transaksi/stock-withdraw'], function () {
           Route::get('settle/{action}/{noDeposit}/{qty}', 'Withdraw\WithdrawController@settle')->name('stock-withdraw');
           Route::get('sendInstruksi/{noDeposit}/{tipeEfek}', 'Withdraw\WithdrawController@pengajuan')->name('stock-withdraw');
           Route::post('send-intruksi-all', 'Withdraw\WithdrawController@sendInstruksi')->name('stock-withdraw');
           Route::post('doc-send-intruksi-all', 'Withdraw\WithdrawController@kseiDoc')->name('stock-deposit');
           Route::get('getDetail2/{pengajuan}', 'Withdraw\WithdrawController@getDetail2')->name('monitoring');
         });
       });


     });

     Route::get('monitoring/transaksi/eksekusi/{sekuritas}/{tipeEfek}/{noKontrak}', 'Eksekusi\EksekusiController@monitoringEksekusi')->name('eksekusi');
     Route::get('monitoring/transaksi/eksekusiGet/{noKontrak}', 'Eksekusi\EksekusiController@monitoringEksekusiGet')->name('eksekusi');
     Route::post('transaksi/eksekusi/updateEksekusi/{broker}/{noKontrak}/{tipeEfek}', 'Eksekusi\EksekusiController@updateEksekusi')->name('eksekusi');

     Route::get('transaksi/eksekusi', 'Eksekusi\EksekusiController@index')->name('eksekusi');
     Route::get('transaksi/eksekusi/getIndex', 'Eksekusi\EksekusiController@getIndex')->name('eksekusi');
     Route::get('transaksi/eksekusi/getDetail/{noEksekusi}', 'Eksekusi\EksekusiController@subDetail')->name('eksekusi');
     Route::get('transaksi/eksekusi/alokasi/create/{noEksekusi}', 'Eksekusi\EksekusiController@alokasi')->name('eksekusi');
     Route::get('transaksi/eksekusi/alokasi/view/{noEksekusi}', 'Eksekusi\EksekusiController@alokasiView')->name('eksekusi');
     Route::get('transaksi/eksekusi/alokasi/getCreate/{noEksekusi}', 'Eksekusi\EksekusiController@getAlokasi')->name('eksekusi');
     Route::post('transaksi/eksekusi/alokasi/postAlokasi', 'Eksekusi\EksekusiController@postAlokasi')->name('eksekusi');
     Route::post('transaksi/eksekusi/doc-send-intruksi-all', 'Eksekusi\EksekusiController@kseiDoc')->name('eksekusi');
     Route::post('transaksi/eksekusi/doc-send-intruksi-all-csv', 'Eksekusi\EksekusiController@kseiDocCsv')->name('eksekusi');
     Route::get('transaksi/eksekusi/settle/{status}/{noEksekusi}', 'Eksekusi\EksekusiController@settleEksekusi')->name('eksekusi');

     // Route::get('transaksi/eksekusi/create', 'Eksekusi\EksekusiController@create')->name('eksekusi');
     // Route::get('transaksi/eksekusi/getDetail', 'Execution\ExecutionController@getDetail')->name('eksekusi');
     // Route::get('transaksi/eksekusi/edit/', 'Execution\ExecutionController@edit')->name('eksekusi');

     Route::get('transaksi/check-pengajuan/{noPengajuan}', 'Topup\TopupController@getPengajuan')->name('stock-deposit'); //pisah akses

     Route::get('transaksi/check-pengajuan-withdraw/{noPengajuan}', 'Withdraw\WithdrawController@getPengajuan')->name('stock-withdraw'); //pisah akses
     // Route::get('send-instruksi-all', 'Topup\TopupController@kseiDoc')->name('loan-request');

     //REPORT
     Route::get('laporan/riskParameter', 'Report\ReportController@indexRisk')->name('report');
     Route::get('laporan/riskParameter-generate-download', 'Report\ReportController@riskParameterDownload')->name('report');
     Route::get('laporan/riskParameter-generate-preview', 'Report\ReportController@riskParameterPreview')->name('report');

     Route::get('laporan/stockLimit', 'Report\ReportController@indexStockLimit')->name('report');
     Route::get('laporan/stockLimit-generate-download', 'Report\ReportController@stockLimitDownload')->name('report');
     Route::get('laporan/stockLimit-generate-preview', 'Report\ReportController@stockLimitPreview')->name('report');

     Route::get('laporan/stockPosition', 'Report\ReportController@indexStockPosition')->name('report');
     Route::get('laporan/stockPosition-generate-download', 'Report\ReportController@stockPositionDownload')->name('report');
     Route::get('laporan/stockPosition-generate-preview', 'Report\ReportController@stockPositionPreview')->name('report');




     //monitoring

     Route::group(['prefix' => 'monitoring'], function () {

       Route::group(['prefix' => 'transaksi'], function () {

         Route::group(['middleware' => 'roler:monitoring/transaksi'], function () {
           Route::get('', 'Monitoring\MonitoringController@index')->name('monitoring');
           Route::get('getIndex', 'Monitoring\MonitoringController@getIndex')->name('monitoring');
           Route::get('getDetail/{pengajuan}', 'Monitoring\MonitoringController@getDetail')->name('monitoring');
           Route::get('detailTransaksi/{kodeEfek}/{noKontrak}', 'Monitoring\MonitoringController@stockMutationReport')->name('monitoring');
         });

       });

       Route::group(['prefix' => 'pelunasan'], function () {

         Route::group(['middleware' => 'roler:monitoring/pelunasan'], function () {
         Route::get('', 'Monitoring\MonitoringController@indexPelunasan')->name('monitoring');
         Route::get('create-from-monitoring/{noKontrak}', 'Monitoring\MonitoringController@createFromMonitoring')->name('monitoring');
         Route::get('get-create-from-monitoring/{noKontrak}', 'Monitoring\MonitoringController@getCreateFromMonitoring')->name('monitoring');
         Route::get('getIndex', 'Monitoring\MonitoringController@getIndexPelunasan')->name('monitoring');
         Route::get('getDetail/{pengajuan}', 'Monitoring\MonitoringController@getDetail')->name('monitoring');
         Route::get('settle/{noPengajuan}/{action}', 'Monitoring\MonitoringController@actionPelunasan')->name('monitoring');
         });

       });

       Route::group(['prefix' => 'haircut'], function () {
           Route::group(['middleware' => 'roler:monitoring/haircut'], function () {
             Route::get('', 'Haircut\HaircutController@index')->name('haircut');
             Route::post('getIndex', 'Haircut\HaircutController@getIndex')->name('haircut');
             Route::get('download', 'Haircut\HaircutController@download')->name('haircut');
           });

           Route::group(['middleware' => 'rolec:monitoring/haircut'], function () {
             Route::get('create', 'Haircut\HaircutController@create')->name('haircut');
             Route::get('test', 'Haircut\HaircutController@test')->name('haircut');
             Route::post('store', 'Haircut\HaircutController@import')->name('haircut');
           });
       });

       Route::group(['prefix' => 'closingPrice'], function () {
           Route::group(['middleware' => 'roler:monitoring/closingPrice'], function () {
             Route::get('', 'ClosePrice\ClosePriceController@index')->name('closingPrice');
             Route::get('getSaham', 'ClosePrice\ClosePriceController@getSaham')->name('closingPrice');
             Route::get('downloadS', 'ClosePrice\ClosePriceController@downloadS')->name('closingPrice');
             Route::get('download', 'ClosePrice\ClosePriceController@download')->name('closingPrice');
             Route::post('getIndex', 'ClosePrice\ClosePriceController@getIndex')->name('closingPrice');
             Route::get('downloadO', 'ClosePrice\ClosePriceController@downloadO')->name('closingPrice');
             Route::get('upload', 'ClosePrice\ClosePriceController@indexUpload')->name('closingPrice');
             Route::post('upload/store', 'ClosePrice\ClosePriceController@import')->name('closingPrice');
           });
           Route::group(['middleware' => 'rolec:monitoring/closingPrice'], function () {
           });
       });

     });

     //USER

     Route::group(['prefix' => 'usersGadai'], function () {

       Route::group(['prefix' => 'userManage'], function () {

         Route::group(['middleware' => 'roler:usersGadai/userManage'], function () {
           Route::get('', 'Users\UserManageController@index')->name('userManage');
           Route::get('getGlobal', 'Users\UserManageController@getUsers')->name('userManage');
         });

         Route::group(['middleware' => 'rolec:usersGadai/userManage'], function () {
           Route::get('create', 'Users\UserManageController@create')->name('userManage');
           Route::post('store', 'Users\UserManageController@store')->name('userManage');
         });

         Route::group(['middleware' => 'roleu:usersGadai/userManage'], function () {
           Route::get('edit/{id}', 'Users\UserManageController@edit')->name('userManage');
           Route::post('update/{id}', 'Users\UserManageController@update')->name('userManage');
           Route::get('disabled/{id}', 'Users\UserManageController@disabled')->name('userManage');
           Route::get('enabled/{id}', 'Users\UserManageController@enabled')->name('userManage');
         });

         Route::group(['middleware' => 'roled:usersGadai/userManage'], function () {
           Route::get('delete/{id}', 'Users\UserManageController@delete')->name('userManage');
         });

         Route::group(['middleware' => 'rolecd:usersGadai/userManage'], function () {
           Route::get('checking-delete/{id}', 'Users\UserManageController@checkingDelete')->name('userManage');
         });

       });

       Route::group(['prefix' => 'userGroup'], function () {
         Route::group(['middleware' => 'roler:usersGadai/userGroup'], function () {
           Route::get('', 'Users\UserGroupController@index')->name('userGroup');
           Route::get('getGlobal', 'Users\UserGroupController@getUsers')->name('userGroup');
         });
         Route::group(['middleware' => 'rolec:usersGadai/userGroup'], function () {
           Route::get('create', 'Users\UserGroupController@create')->name('userGroup');
           Route::post('store', 'Users\UserGroupController@store')->name('userGroup');
         });
         Route::group(['middleware' => 'roleu:usersGadai/userGroup'], function () {
           Route::get('edit/{id}', 'Users\UserGroupController@edit')->name('userGroup');
           Route::post('update/{id}', 'Users\UserGroupController@update')->name('userGroup');
         });
         Route::group(['middleware' => 'roled:usersGadai/userGroup'], function () {
           Route::get('delete/{id}', 'Users\UserGroupController@delete')->name('userGroup');
         });
       });

       Route::get('/users/change-password/{username}/{id}', 'Users\UserManageController@index')->name('home');

     });

   });
});
