<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(url('login'));
});

Route::get('login', 'Auth\LoginController@getLogin')->name('login');
Route::post('login', 'Auth\LoginController@postLogin')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('verification', 'Get\VerificationController@getVerification')->name('verification');
Route::post('verification', 'Get\VerificationController@postVerification')->name('verification');

Route::group(['middleware' => ['web']], function () {

   Route::group(['middleware' => ['auth'] ], function () {

     Route::get('/dashboard', 'HomeController@index')->name('home');

     Route::get('/lookup/customer', 'LookupController@customer')->name('lookupCustomer');
     Route::get('/lookup/contract', 'LookupController@contract')->name('lookupContact');
     Route::get('/lookup/stock', 'LookupController@stock')->name('lookupStock');


     // Route::get('dataStatis/globalParam', 'Parameter\GlobalController@index')->name('globalParam');
     // Route::get('dataStatis/globalParam/getGlobal', 'Parameter\GlobalController@getIndex')->name('globalParam');
     // Route::get('dataStatis/globalParam/create', 'Parameter\GlobalController@create')->name('globalParam');
     // Route::post('dataStatis/globalParam/store', 'Parameter\GlobalController@store')->name('globalParam');
     // Route::get('dataStatis/globalParam/edit/{id}', 'Parameter\GlobalController@edit')->name('globalParam');
     // Route::post('dataStatis/globalParam/update/{id}', 'Parameter\GlobalController@update')->name('globalParam');
     // Route::get('dataStatis/globalParam/delete/{id}', 'Parameter\GlobalController@delete')->name('globalParam');


     Route::group(['prefix' => 'dataStatis'], function () {

      Route::group(['prefix' => 'globalParam'], function () {
          Route::group(['middleware' => 'roler:dataStatis/globalParam'], function () {
          Route::get('', 'Parameter\GlobalController@index')->name('globalParam');
          Route::get('getGlobal', 'Parameter\GlobalController@getIndex')->name('globalParam');
          });
          Route::group(['middleware' => 'rolec:dataStatis/globalParam'], function () {
          Route::get('create', 'Parameter\GlobalController@create')->name('globalParam');
          Route::post('store', 'Parameter\GlobalController@store')->name('globalParam');
          });
          Route::group(['middleware' => 'roleu:dataStatis/globalParam'], function () {
          Route::get('edit/{id}', 'Parameter\GlobalController@edit')->name('globalParam');
          Route::post('update/{id}', 'Parameter\GlobalController@update')->name('globalParam');
          });
          Route::group(['middleware' => 'roled:dataStatis/globalParam'], function () {
          Route::get('delete/{id}', 'Parameter\GlobalController@delete')->name('globalParam');
          });
        });

      });

     Route::get('dataStatis/holiday', 'Holiday\HolidayController@index')->name('holiday');
     Route::get('dataStatis/holiday/getHoliday/{tahun}', 'Holiday\HolidayController@getIndex')->name('holiday');
     Route::get('dataStatis/holiday/create', 'Holiday\HolidayController@create')->name('holiday');
     Route::post('dataStatis/holiday/store', 'Holiday\HolidayController@store')->name('holiday');
     Route::get('dataStatis/holiday/edit/{id}', 'Holiday\HolidayController@edit')->name('holiday');
     Route::post('dataStatis/holiday/update/{id}', 'Holiday\HolidayController@update')->name('holiday');
     Route::get('dataStatis/holiday/delete/{id}', 'Holiday\HolidayController@delete')->name('holiday');

     Route::get('dataStatis/tax', 'Tax\TaxController@index')->name('tax');
     Route::get('dataStatis/tax/getIndex', 'Tax\TaxController@getIndex')->name('tax');
     Route::get('dataStatis/tax/create', 'Tax\TaxController@create')->name('tax');
     Route::post('dataStatis/tax/store', 'Tax\TaxController@store')->name('tax');
     Route::get('dataStatis/tax/edit/{id}', 'Tax\TaxController@edit')->name('tax');
     Route::post('dataStatis/tax/update/{id}', 'Tax\TaxController@update')->name('tax');
     Route::get('dataStatis/tax/delete/{id}', 'Tax\TaxController@delete')->name('tax');

     Route::get('dataStatis/provinsi', 'Provinsi\ProvinsiController@index')->name('provinsi');
     Route::get('dataStatis/provinsi/getIndex', 'Provinsi\ProvinsiController@getIndex')->name('provinsi');
     Route::get('dataStatis/provinsi/create', 'Provinsi\ProvinsiController@create')->name('provinsi');
     Route::post('dataStatis/provinsi/store', 'Provinsi\ProvinsiController@store')->name('provinsi');
     Route::get('dataStatis/provinsi/edit/{id}', 'Provinsi\ProvinsiController@edit')->name('provinsi');
     Route::post('dataStatis/provinsi/update/{id}', 'Provinsi\ProvinsiController@update')->name('provinsi');
     Route::get('dataStatis/provinsi/delete/{id}', 'Provinsi\ProvinsiController@delete')->name('provinsi');

     Route::get('dataStatis/broker', 'Broker\BrokerController@index')->name('broker');
     Route::get('dataStatis/broker/getBroker', 'Broker\BrokerController@getIndex')->name('broker');
     Route::get('dataStatis/broker/create', 'Broker\BrokerController@create')->name('broker');
     Route::post('dataStatis/broker/store', 'Broker\BrokerController@store')->name('broker');
     Route::get('dataStatis/broker/edit/{id}', 'Broker\BrokerController@edit')->name('broker');
     Route::post('dataStatis/broker/update/{id}', 'Broker\BrokerController@update')->name('broker');
     Route::get('dataStatis/broker/delete/{id}', 'Broker\BrokerController@delete')->name('broker');

     Route::get('dataStatis/custodi', 'Bank\CustodyController@index')->name('custodi');
     Route::get('dataStatis/custodi/getCustodi', 'Bank\CustodyController@getIndex')->name('custodi');
     Route::get('dataStatis/custodi/create', 'Bank\CustodyController@create')->name('custodi');
     Route::post('dataStatis/custodi/store', 'Bank\CustodyController@store')->name('custodi');
     Route::get('dataStatis/custodi/edit/{id}', 'Bank\CustodyController@edit')->name('custodi');
     Route::post('dataStatis/custodi/update/{id}', 'Bank\CustodyController@update')->name('custodi');
     Route::get('dataStatis/custodi/delete/{id}', 'Bank\CustodyController@delete')->name('custodi');

     Route::get('dataStatis/nasabah', 'Nasabah\NasabahController@index')->name('nasabah');
     Route::get('dataStatis/nasabah/1', 'Nasabah\NasabahController@individu')->name('nasabah');
     Route::get('dataStatis/nasabah/2', 'Nasabah\NasabahController@institusi')->name('nasabah');
     Route::get('dataStatis/nasabah/edit/{tipeNasabah}/{kodeNasabah}/{id}', 'Nasabah\NasabahController@edit')->name('nasabah');
     Route::get('dataStatis/nasabah/getNasabah', 'Nasabah\NasabahController@getIndex')->name('nasabah');
     Route::post('dataStatis/nasabah/update/{id}/{tipe}/{idNasabah}', 'Nasabah\NasabahController@nasbahUpdate')->name('nasabah');
     Route::post('dataStatis/nasabah/updateSubRek/{kodeNasabah}', 'Nasabah\NasabahController@updateSubRek')->name('nasabah');
     Route::get('dataStatis/nasabah/editSubrRek/{idNasabah}', 'Nasabah\NasabahController@reqSubrek')->name('nasabah');
     Route::get('dataStatis/nasabah/reqSubrek/{idNasabah}', 'Nasabah\NasabahController@reqSubrek')->name('nasabah');

     Route::get('dataStatis/efek-resources', 'Efek\EfekController@index')->name('efek');
     Route::get('dataStatis/efek-resources/create', 'Efek\EfekController@create')->name('efek');
     Route::post('dataStatis/efek-resources/getEfek', 'Efek\EfekController@getIndex')->name('efek');
     Route::post('dataStatis/efek-resources/store', 'Efek\EfekController@store')->name('efek');
     Route::get('dataStatis/efek-resources/edit/{id}', 'Efek\EfekController@edit')->name('efek');
     Route::post('dataStatis/efek-resources/update/{id}', 'Efek\EfekController@update')->name('efek');
     Route::get('dataStatis/efek-resources/delete/{id}', 'Efek\EfekController@delete')->name('efek');

     Route::get('dataStatis/country', 'Country\CountryController@index')->name('country');
     Route::get('dataStatis/country/getCountry', 'Country\CountryController@getIndex')->name('country');
     Route::get('dataStatis/country/create', 'Country\CountryController@create')->name('country');
     Route::post('dataStatis/country/store', 'Country\CountryController@store')->name('country');
     Route::get('dataStatis/country/edit/{id}', 'Country\CountryController@edit')->name('country');
     Route::post('dataStatis/country/update/{id}', 'Country\CountryController@update')->name('country');
     Route::get('dataStatis/country/delete/{id}', 'Country\CountryController@delete')->name('country');

     Route::get('dataStatis/city', 'City\CityController@index')->name('city');
     Route::get('dataStatis/city/getCity', 'City\CityController@getIndex')->name('city');
     Route::get('dataStatis/city/create', 'City\CityController@create')->name('city');
     Route::post('dataStatis/city/store', 'City\CityController@store')->name('city');
     Route::get('dataStatis/city/edit/{id}', 'City\CityController@edit')->name('city');
     Route::post('dataStatis/city/update/{id}', 'City\CityController@update')->name('city');
     Route::get('dataStatis/city/delete/{id}', 'City\CityController@delete')->name('city');

     //Transaksi

     Route::get('transaksi/eksekusi', 'Execution\ExecutionController@index')->name('eksekusi');
     Route::get('transaksi/eksekusi/create', 'Execution\ExecutionController@create')->name('eksekusi');
     Route::get('transaksi/eksekusi/getIndex', 'Execution\ExecutionController@getIndex')->name('eksekusi');
     Route::get('transaksi/eksekusi/getDetail', 'Execution\ExecutionController@getDetail')->name('eksekusi');
     Route::get('transaksi/eksekusi/edit/', 'Execution\ExecutionController@edit')->name('eksekusi');

     Route::get('transaksi/stock-deposit', 'Topup\TopupController@index')->name('stock-deposit');
     Route::get('transaksi/stock-deposit/create', 'Topup\TopupController@create')->name('stock-deposit');
     Route::get('transaksi/stock-deposit/getIndex', 'Topup\TopupController@getIndex')->name('stock-deposit');

     Route::get('transaksi/stock-withdraw', 'Withdraw\WithdrawController@index')->name('stock-withdraw');
     Route::get('transaksi/stock-withdraw/create', 'Withdraw\WithdrawController@create')->name('stock-withdraw');
     Route::get('transaksi/stock-withdraw/getIndex', 'Withdraw\WithdrawController@getIndex')->name('stock-withdraw');

     Route::get('transaksi/loan-request', 'LoanRequest\LoanRequestController@index')->name('loan-request');
     Route::get('transaksi/loan-request/create', 'LoanRequest\LoanRequestController@create')->name('loan-request');
     Route::post('transaksi/loan-request/store/{kodePengajuan}', 'LoanRequest\LoanRequestController@store')->name('loan-request');
     Route::get('transaksi/loan-request/edit/{id}', 'LoanRequest\LoanRequestController@edit')->name('loan-request');
     Route::get('transaksi/loan-request/terimaEfek/{pengajuan}', 'LoanRequest\LoanRequestController@terimaEfek')->name('loan-request');
     Route::get('transaksi/loan-request/getDetail/{pengajuan}', 'LoanRequest\LoanRequestController@getDetail')->name('loan-request');
     Route::get('transaksi/loan-request/getIndex', 'LoanRequest\LoanRequestController@getIndex')->name('loan-request');
     Route::get('transaksi/loan-request/edit/subrek/{nasabah}', 'LoanRequest\LoanRequestController@editsubrek')->name('loan-request');
     Route::post('transaksi/loan-request/updateEfek/{noPengajuan}', 'LoanRequest\LoanRequestController@updatePengajuan')->name('loan-request');

     //monitoring

     Route::get('monitoring/transaksi', 'Monitoring\MonitoringController@index')->name('monitoring');
     Route::get('monitoring/transaksi/getIndex', 'Monitoring\MonitoringController@getIndex')->name('monitoring');
     Route::get('monitoring/transaksi/getDetail/{noKontrak}', 'Monitoring\MonitoringController@getDetail')->name('monitoring');

     Route::get('monitoring/haircut', 'Haircut\HaircutController@index')->name('haircut');
     Route::get('monitoring/haircut/create', 'Haircut\HaircutController@create')->name('haircut');
     Route::post('monitoring/haircut/getIndex', 'Haircut\HaircutController@getIndex')->name('haircut');
     Route::get('monitoring/haircut/upload', 'Haircut\HaircutController@upload')->name('haircut');
     Route::post('monitoring/haircut/store', 'Haircut\HaircutController@import')->name('haircut');
     Route::get('monitoring/haircut/download', 'Haircut\HaircutController@download')->name('haircut');

     Route::get('monitoring/closingPrice', 'ClosePrice\ClosePriceController@index')->name('closePrice');
     Route::get('monitoring/closingPrice/create', 'ClosePrice\ClosePriceController@create')->name('closePrice');
     Route::post('monitoring/closingPrice/getIndex', 'ClosePrice\ClosePriceController@getIndex')->name('haircut');
     Route::get('monitoring/closingPrice/upload', 'ClosePrice\ClosePriceController@upload')->name('closePrice');
     Route::post('monitoring/closingPrice/store', 'ClosePrice\ClosePriceController@import')->name('closePrice');
     Route::get('monitoring/closePrice/getSaham', 'ClosePrice\ClosePriceController@getSaham')->name('closePrice');

     //USER
     Route::get('usersGadai/userManage', 'Users\UserManageController@index')->name('userManage');
     Route::get('usersGadai/userManage/getGlobal', 'Users\UserManageController@getUsers')->name('userManage');
     Route::get('usersGadai/userManage/create', 'Users\UserManageController@create')->name('userManage');
     Route::post('usersGadai/userManage/store', 'Users\UserManageController@store')->name('userManage');
     Route::get('usersGadai/userManage/edit/{id}', 'Users\UserManageController@edit')->name('userManage');
     Route::post('usersGadai/userManage/update/{id}', 'Users\UserManageController@update')->name('userManage');
     Route::get('usersGadai/userManage/delete/{id}', 'Users\UserManageController@delete')->name('userManage');

     Route::get('usersGadai/userGroup', 'Users\UserGroupController@index')->name('userGroup');
     Route::get('usersGadai/userGroup/getGlobal', 'Users\UserGroupController@getUsers')->name('userGroup');
     Route::get('usersGadai/userGroup/create', 'Users\UserGroupController@create')->name('userGroup');
     Route::post('usersGadai/userGroup/store', 'Users\UserGroupController@store')->name('userGroup');
     Route::get('usersGadai/userGroup/edit/{id}', 'Users\UserGroupController@edit')->name('userGroup');
     Route::post('usersGadai/userGroup/update/{id}', 'Users\UserGroupController@update')->name('userGroup');
     Route::get('usersGadai/userGroup/delete/{id}', 'Users\UserGroupController@delete')->name('userGroup');

   });
});
